Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_2021_01.Environment.

Definition transaction : Set := Sapling.UTXO.transaction.

Definition transaction_encoding : Data_encoding.t Sapling.UTXO.transaction :=
  Sapling.UTXO.transaction_encoding.

Module diff.
  Record record : Set := Build {
    commitments_and_ciphertexts :
      list (Sapling.Commitment.t * Sapling.Ciphertext.t);
    nullifiers : list Sapling.Nullifier.t }.
  Definition with_commitments_and_ciphertexts commitments_and_ciphertexts
    (r : record) :=
    Build commitments_and_ciphertexts r.(nullifiers).
  Definition with_nullifiers nullifiers (r : record) :=
    Build r.(commitments_and_ciphertexts) nullifiers.
End diff.
Definition diff := diff.record.

Definition diff_encoding : Data_encoding.encoding diff :=
  Data_encoding.conv
    (fun (d : diff) =>
      (d.(diff.commitments_and_ciphertexts), d.(diff.nullifiers)))
    (fun (function_parameter :
      list (Sapling.Commitment.t * Sapling.Ciphertext.t) *
        list Sapling.Nullifier.t) =>
      let '(commitments_and_ciphertexts, nullifiers) := function_parameter in
      let '_ :=
        match commitments_and_ciphertexts with
        | [] => tt
        | cons (_cm_hd, ct_hd) rest =>
          let memo_size := Sapling.Ciphertext.get_memo_size ct_hd in
          List.iter
            (fun (function_parameter :
              Sapling.Commitment.t * Sapling.Ciphertext.t) =>
              let '(_cm, ct) := function_parameter in
              (* ❌ Assert instruction is not handled. *)
              assert unit ((Sapling.Ciphertext.get_memo_size ct) =i memo_size))
            rest
        end in
      {| diff.commitments_and_ciphertexts := commitments_and_ciphertexts;
        diff.nullifiers := nullifiers |}) None
    (Data_encoding.obj2
      (Data_encoding.req None None "commitments_and_ciphertexts"
        (Data_encoding.list_value None
          (Data_encoding.tup2 Sapling.Commitment.encoding
            Sapling.Ciphertext.encoding)))
      (Data_encoding.req None None "nullifiers"
        (Data_encoding.list_value None Sapling.Nullifier.encoding))).

Module Memo_size.
  Definition t : Set := int.
  
  Definition encoding : Data_encoding.encoding int := Data_encoding.uint16.
  
  Definition equal : int -> int -> bool := Compare.Int.(Compare.S.op_eq).
  
  Definition max_uint16 : int := 65535.
  
  Definition max_uint16_z : Z.t := Z.of_int max_uint16.
  
  Definition err {A : Set} : Pervasives.result A string :=
    Pervasives.Error
      (Pervasives.op_caret "a positive 16-bit integer (between 0 and "
        (Pervasives.op_caret (Pervasives.string_of_int max_uint16) ")")).
  
  Definition parse_z (z : Z.t) : Pervasives.result int string :=
    if (Z.zero <=Z z) && (z <=Z max_uint16_z) then
      Pervasives.Ok (Z.to_int z)
    else
      err.
  
  Definition unparse_to_z : int -> Z.t := Z.of_int.
End Memo_size.

Definition transaction_get_memo_size (transaction : Sapling.UTXO.transaction)
  : option int :=
  match transaction.(Sapling.UTXO.transaction.outputs) with
  | [] => None
  | cons {| Sapling.UTXO.output.ciphertext := ciphertext |} _ =>
    Some (Sapling.Ciphertext.get_memo_size ciphertext)
  end.
