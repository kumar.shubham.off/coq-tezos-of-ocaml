Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Alpha_context.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.

Require TezosOfOCaml.Proto_alpha.Lazy_storage_diff.

(* @TODO *)
Axiom encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True) Lazy_storage_diff.encoding.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.
