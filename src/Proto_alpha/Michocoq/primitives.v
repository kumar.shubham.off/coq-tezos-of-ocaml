(* Open Source License *)
(* Copyright (c) 2021 Nomadic Labs. <contact@nomadic-labs.com> *)

(* Permission is hereby granted, free of charge, to any person obtaining a *)
(* copy of this software and associated documentation files (the "Software"), *)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense, *)
(* and/or sell copies of the Software, and to permit persons to whom the *)
(* Software is furnished to do so, subject to the following conditions: *)

(* The above copyright notice and this permission notice shall be included *)
(* in all copies or substantial portions of the Software. *)

(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR *)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER *)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER *)
(* DEALINGS IN THE SOFTWARE. *)

Require Import Coq.Strings.String Ascii.
Require Import error.

Inductive primitive :=
  | K_parameter
  | K_storage
  | K_code
  | D_False
  | D_Elt
  | D_Left
  | D_None
  | D_Pair
  | D_Right
  | D_Some
  | D_True
  | D_Unit
  | I_PACK
  | I_UNPACK
  | I_BLAKE2B
  | I_SHA256
  | I_SHA512
  | I_KECCAK
  | I_SHA3
  | I_ABS
  | I_ADD
  | I_AMOUNT
  | I_AND
  | I_BALANCE
  | I_CAR
  | I_CDR
  | I_CHAIN_ID
  | I_CHECK_SIGNATURE
  | I_COMPARE
  | I_CONCAT
  | I_CONS
  | I_CREATE_ACCOUNT
  | I_CREATE_CONTRACT
  | I_IMPLICIT_ACCOUNT
  | I_DIP
  | I_DROP
  | I_DUP
  | I_EDIV
  | I_EMPTY_BIG_MAP
  | I_EMPTY_MAP
  | I_EMPTY_SET
  | I_EQ
  | I_EXEC
  | I_APPLY
  | I_FAILWITH
  | I_GE
  | I_GET
  | I_GT
  | I_HASH_KEY
  | I_IF
  | I_IF_CONS
  | I_IF_LEFT
  | I_IF_NONE
  | I_INT
  | I_LAMBDA
  | I_LE
  | I_LEFT
  | I_LOOP
  | I_LSL
  | I_LSR
  | I_LT
  | I_MAP
  | I_MEM
  | I_MUL
  | I_NEG
  | I_NEQ
  | I_NIL
  | I_NONE
  | I_NOT
  | I_NOW
  | I_OR
  | I_PAIR
  | I_PUSH
  | I_RIGHT
  | I_SIZE
  | I_SOME
  | I_SOURCE
  | I_SENDER
  | I_SELF
  | I_SLICE
  | I_STEPS_TO_QUOTA
  | I_SUB
  | I_SWAP
  | I_TRANSFER_TOKENS
  | I_SET_DELEGATE
  | I_UNIT
  | I_UPDATE
  | I_XOR
  | I_ITER
  | I_LOOP_LEFT
  | I_ADDRESS
  | I_CONTRACT
  | I_ISNAT
  | I_CAST
  | I_RENAME
  | I_DIG
  | I_DUG
  | I_NEVER
  | I_SELF_ADDRESS
  | I_GET_AND_UPDATE
  | I_UNPAIR
  | I_LEVEL
  | I_VOTING_POWER
  | I_TOTAL_VOTING_POWER
  | I_TICKET
  | I_READ_TICKET
  | I_SPLIT_TICKET
  | I_JOIN_TICKETS
  | I_SAPLING_EMPTY_STATE
  | I_SAPLING_VERIFY_UPDATE
  | I_PAIRING_CHECK
  | T_bool
  | T_contract
  | T_int
  | T_key
  | T_key_hash
  | T_lambda
  | T_list
  | T_map
  | T_big_map
  | T_nat
  | T_option
  | T_or
  | T_pair
  | T_set
  | T_signature
  | T_string
  | T_bytes
  | T_mutez
  | T_timestamp
  | T_unit
  | T_operation
  | T_address
  | T_chain_id
  | T_never
  | T_ticket
  | T_sapling_state
  | T_sapling_transaction
  | T_bls12_381_fr
  | T_bls12_381_g1
  | T_bls12_381_g2.


Definition to_string (p : primitive) : string :=
  match p with
  | K_parameter => "parameter"
  | K_storage => "storage"
  | K_code => "code"
  | D_False => "False"
  | D_Elt => "Elt"
  | D_Left => "Left"
  | D_None => "None"
  | D_Pair => "Pair"
  | D_Right => "Right"
  | D_Some => "Some"
  | D_True => "True"
  | D_Unit => "Unit"
  | I_PACK => "PACK"
  | I_UNPACK => "UNPACK"
  | I_BLAKE2B => "BLAKE2B"
  | I_SHA256 => "SHA256"
  | I_SHA512 => "SHA512"
  | I_KECCAK => "KECCAK"
  | I_SHA3 => "SHA3"
  | I_ABS => "ABS"
  | I_ADD => "ADD"
  | I_AMOUNT => "AMOUNT"
  | I_AND => "AND"
  | I_BALANCE => "BALANCE"
  | I_CAR => "CAR"
  | I_CDR => "CDR"
  | I_CHAIN_ID => "CHAIN_ID"
  | I_CHECK_SIGNATURE => "CHECK_SIGNATURE"
  | I_COMPARE => "COMPARE"
  | I_CONCAT => "CONCAT"
  | I_CONS => "CONS"
  | I_CREATE_ACCOUNT => "CREATE_ACCOUNT"
  | I_CREATE_CONTRACT => "CREATE_CONTRACT"
  | I_IMPLICIT_ACCOUNT => "IMPLICIT_ACCOUNT"
  | I_DIP => "DIP"
  | I_DROP => "DROP"
  | I_DUP => "DUP"
  | I_EDIV => "EDIV"
  | I_EMPTY_BIG_MAP => "EMPTY_BIG_MAP"
  | I_EMPTY_MAP => "EMPTY_MAP"
  | I_EMPTY_SET => "EMPTY_SET"
  | I_EQ => "EQ"
  | I_EXEC => "EXEC"
  | I_APPLY => "APPLY"
  | I_FAILWITH => "FAILWITH"
  | I_GE => "GE"
  | I_GET => "GET"
  | I_GT => "GT"
  | I_HASH_KEY => "HASH_KEY"
  | I_IF => "IF"
  | I_IF_CONS => "IF_CONS"
  | I_IF_LEFT => "IF_LEFT"
  | I_IF_NONE => "IF_NONE"
  | I_INT => "INT"
  | I_LAMBDA => "LAMBDA"
  | I_LE => "LE"
  | I_LEFT => "LEFT"
  | I_LOOP => "LOOP"
  | I_LSL => "LSL"
  | I_LSR => "LSR"
  | I_LT => "LT"
  | I_MAP => "MAP"
  | I_MEM => "MEM"
  | I_MUL => "MUL"
  | I_NEG => "NEG"
  | I_NEQ => "NEQ"
  | I_NIL => "NIL"
  | I_NONE => "NONE"
  | I_NOT => "NOT"
  | I_NOW => "NOW"
  | I_OR => "OR"
  | I_PAIR => "PAIR"
  | I_PUSH => "PUSH"
  | I_RIGHT => "RIGHT"
  | I_SIZE => "SIZE"
  | I_SOME => "SOME"
  | I_SOURCE => "SOURCE"
  | I_SENDER => "SENDER"
  | I_SELF => "SELF"
  | I_SLICE => "SLICE"
  | I_STEPS_TO_QUOTA => "STEPS_TO_QUOTA"
  | I_SUB => "SUB"
  | I_SWAP => "SWAP"
  | I_TRANSFER_TOKENS => "TRANSFER_TOKENS"
  | I_SET_DELEGATE => "SET_DELEGATE"
  | I_UNIT => "UNIT"
  | I_UPDATE => "UPDATE"
  | I_XOR => "XOR"
  | I_ITER => "ITER"
  | I_LOOP_LEFT => "LOOP_LEFT"
  | I_ADDRESS => "ADDRESS"
  | I_CONTRACT => "CONTRACT"
  | I_ISNAT => "ISNAT"
  | I_CAST => "CAST"
  | I_RENAME => "RENAME"
  | I_DIG => "DIG"
  | I_DUG => "DUG"
  | I_NEVER => "NEVER"
  | I_SELF_ADDRESS => "SELF_ADDRESS"
  | I_GET_AND_UPDATE => "GET_AND_UPDATE"
  | I_UNPAIR => "UNPAIR"
  | I_LEVEL => "LEVEL"
  | I_VOTING_POWER => "VOTING_POWER"
  | I_TOTAL_VOTING_POWER => "TOTAL_VOTING_POWER"
  | I_TICKET => "TICKET"
  | I_READ_TICKET => "READ_TICKET"
  | I_SPLIT_TICKET => "SPLIT_TICKET"
  | I_JOIN_TICKETS => "JOIN_TICKETS"
  | I_SAPLING_EMPTY_STATE => "SAPLING_EMPTY_STATE"
  | I_SAPLING_VERIFY_UPDATE => "SAPLING_VERIFY_UPDATE"
  | I_PAIRING_CHECK => "PAIRING_CHECK"
  | T_bool => "bool"
  | T_contract => "contract"
  | T_int => "int"
  | T_key => "key"
  | T_key_hash => "key_hash"
  | T_lambda => "lambda"
  | T_list => "list"
  | T_map => "map"
  | T_big_map => "big_map"
  | T_nat => "nat"
  | T_option => "option"
  | T_or => "or"
  | T_pair => "pair"
  | T_set => "set"
  | T_signature => "signature"
  | T_string => "string"
  | T_bytes => "bytes"
  | T_mutez => "mutez"
  | T_timestamp => "timestamp"
  | T_unit => "unit"
  | T_operation => "operation"
  | T_address => "address"
  | T_chain_id => "chain_id"
  | T_never => "never"
  | T_ticket => "ticket"
  | T_sapling_state => "sapling_state"
  | T_sapling_transaction => "sapling_transaction"
  | T_bls12_381_fr => "bls12_381_fr"
  | T_bls12_381_g1 => "bls12_381_g1"
  | T_bls12_381_g2 => "bls12_381_g2"
  end.

Open Scope string.

Definition of_string (s : string) : M primitive :=
  match s with
  | "parameter" => Return K_parameter
  | "storage" => Return K_storage
  | "code" => Return K_code
  | "False" => Return D_False
  | "Elt" => Return D_Elt
  | "Left" => Return D_Left
  | "None" => Return D_None
  | "Pair" => Return D_Pair
  | "Right" => Return D_Right
  | "Some" => Return D_Some
  | "True" => Return D_True
  | "Unit" => Return D_Unit
  | "PACK" => Return I_PACK
  | "UNPACK" => Return I_UNPACK
  | "BLAKE2B" => Return I_BLAKE2B
  | "SHA256" => Return I_SHA256
  | "SHA512" => Return I_SHA512
  | "KECCAK" => Return I_KECCAK
  | "SHA3" => Return I_SHA3
  | "ABS" => Return I_ABS
  | "ADD" => Return I_ADD
  | "AMOUNT" => Return I_AMOUNT
  | "AND" => Return I_AND
  | "BALANCE" => Return I_BALANCE
  | "CAR" => Return I_CAR
  | "CDR" => Return I_CDR
  | "CHAIN_ID" => Return I_CHAIN_ID
  | "CHECK_SIGNATURE" => Return I_CHECK_SIGNATURE
  | "COMPARE" => Return I_COMPARE
  | "CONCAT" => Return I_CONCAT
  | "CONS" => Return I_CONS
  | "CREATE_ACCOUNT" => Return I_CREATE_ACCOUNT
  | "CREATE_CONTRACT" => Return I_CREATE_CONTRACT
  | "IMPLICIT_ACCOUNT" => Return I_IMPLICIT_ACCOUNT
  | "DIP" => Return I_DIP
  | "DROP" => Return I_DROP
  | "DUP" => Return I_DUP
  | "EDIV" => Return I_EDIV
  | "EMPTY_BIG_MAP" => Return I_EMPTY_BIG_MAP
  | "EMPTY_MAP" => Return I_EMPTY_MAP
  | "EMPTY_SET" => Return I_EMPTY_SET
  | "EQ" => Return I_EQ
  | "EXEC" => Return I_EXEC
  | "APPLY" => Return I_APPLY
  | "FAILWITH" => Return I_FAILWITH
  | "GE" => Return I_GE
  | "GET" => Return I_GET
  | "GT" => Return I_GT
  | "HASH_KEY" => Return I_HASH_KEY
  | "IF" => Return I_IF
  | "IF_CONS" => Return I_IF_CONS
  | "IF_LEFT" => Return I_IF_LEFT
  | "IF_NONE" => Return I_IF_NONE
  | "INT" => Return I_INT
  | "LAMBDA" => Return I_LAMBDA
  | "LE" => Return I_LE
  | "LEFT" => Return I_LEFT
  | "LOOP" => Return I_LOOP
  | "LSL" => Return I_LSL
  | "LSR" => Return I_LSR
  | "LT" => Return I_LT
  | "MAP" => Return I_MAP
  | "MEM" => Return I_MEM
  | "MUL" => Return I_MUL
  | "NEG" => Return I_NEG
  | "NEQ" => Return I_NEQ
  | "NIL" => Return I_NIL
  | "NONE" => Return I_NONE
  | "NOT" => Return I_NOT
  | "NOW" => Return I_NOW
  | "OR" => Return I_OR
  | "PAIR" => Return I_PAIR
  | "PUSH" => Return I_PUSH
  | "RIGHT" => Return I_RIGHT
  | "SIZE" => Return I_SIZE
  | "SOME" => Return I_SOME
  | "SOURCE" => Return I_SOURCE
  | "SENDER" => Return I_SENDER
  | "SELF" => Return I_SELF
  | "SLICE" => Return I_SLICE
  | "STEPS_TO_QUOTA" => Return I_STEPS_TO_QUOTA
  | "SUB" => Return I_SUB
  | "SWAP" => Return I_SWAP
  | "TRANSFER_TOKENS" => Return I_TRANSFER_TOKENS
  | "SET_DELEGATE" => Return I_SET_DELEGATE
  | "UNIT" => Return I_UNIT
  | "UPDATE" => Return I_UPDATE
  | "XOR" => Return I_XOR
  | "ITER" => Return I_ITER
  | "LOOP_LEFT" => Return I_LOOP_LEFT
  | "ADDRESS" => Return I_ADDRESS
  | "CONTRACT" => Return I_CONTRACT
  | "ISNAT" => Return I_ISNAT
  | "DIG" => Return I_DIG
  | "DUG" => Return I_DUG
  | "CAST" => Return I_CAST
  | "RENAME" => Return I_RENAME
  | "NEVER" => Return I_NEVER
  | "SELF_ADDRESS" => Return I_SELF_ADDRESS
  | "GET_AND_UPDATE" => Return I_GET_AND_UPDATE
  | "UNPAIR" => Return I_UNPAIR
  | "LEVEL" => Return I_LEVEL
  | "VOTING_POWER" => Return I_VOTING_POWER
  | "TOTAL_VOTING_POWER" => Return I_TOTAL_VOTING_POWER
  | "TICKET" => Return I_TICKET
  | "READ_TICKET" => Return I_READ_TICKET
  | "SPLIT_TICKET" => Return I_SPLIT_TICKET
  | "JOIN_TICKETS" => Return I_JOIN_TICKETS
  | "SAPLING_EMPTY_STATE" => Return I_SAPLING_EMPTY_STATE
  | "SAPLING_VERIFY_UPDATE" => Return I_SAPLING_VERIFY_UPDATE
  | "PAIRING_CHECK" => Return I_PAIRING_CHECK
  | "bool" => Return T_bool
  | "contract" => Return T_contract
  | "int" => Return T_int
  | "key" => Return T_key
  | "key_hash" => Return T_key_hash
  | "lambda" => Return T_lambda
  | "list" => Return T_list
  | "map" => Return T_map
  | "big_map" => Return T_big_map
  | "nat" => Return T_nat
  | "option" => Return T_option
  | "or" => Return T_or
  | "pair" => Return T_pair
  | "set" => Return T_set
  | "signature" => Return T_signature
  | "string" => Return T_string
  | "bytes" => Return T_bytes
  | "mutez" => Return T_mutez
  | "timestamp" => Return T_timestamp
  | "unit" => Return T_unit
  | "operation" => Return T_operation
  | "address" => Return T_address
  | "chain_id" => Return T_chain_id
  | "never" => Return T_never
  | "ticket" => Return T_ticket
  | "sapling_state" => Return T_sapling_state
  | "sapling_transaction" => Return T_sapling_transaction
  | "bls12_381_fr" => Return T_bls12_381_fr
  | "bls12_381_g1" => Return T_bls12_381_g1
  | "bls12_381_g2" => Return T_bls12_381_g2
  | s => Failed _ (Unknown_primitive s)
  end.

Ltac destruct_char a :=
  destruct a as [b1 b2 b3 b4 b5 b6 b7 b8];
  destruct b1; try discriminate;
  destruct b2; try discriminate;
  destruct b3; try discriminate;
  destruct b4; try discriminate;
  destruct b5; try discriminate;
  destruct b6; try discriminate;
  destruct b7; try discriminate;
  destruct b8; try discriminate.

Ltac consume_char s :=
  destruct s as [|a s];
  [try discriminate|destruct_char a].

Ltac finish H p :=
  apply error.unreturn in H; subst p; reflexivity.

Ltac continue p :=
  match goal with | s : string |- _ => consume_char s | H : _ = _ |- _ => finish H p end.

Lemma of_to_string p s :
  of_string s = Return p <-> to_string p = s.
Proof.
  split; intro H.
  - repeat continue p.
  - subst s.
    destruct p; reflexivity.
Qed.
