Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Receipt_repr.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Compare.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.

Require TezosOfOCaml.Proto_alpha.Proofs.Blinded_public_key_hash.
Require TezosOfOCaml.Proto_alpha.Proofs.Contract_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Tez_repr.

Require TezosOfOCaml.Proto_alpha.Tez_repr.

Lemma balance_encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True) Receipt_repr.balance_encoding.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve balance_encoding_is_valid : Data_encoding_db.

Lemma is_not_zero_false : Receipt_repr.is_not_zero 0 = false.
  reflexivity.
Qed.

Lemma is_not_zero_true : forall x, x <> 0 -> Receipt_repr.is_not_zero x = true.
  intro x; destruct x; easy.
Qed.

Module Balance_update.
  Module Valid.
    Definition t (b : Receipt_repr.balance_update) : Prop :=
      match b with
      | Receipt_repr.Debited (Tez_repr.Tez_tag tz) => 0 < tz <= Int64.max_int
      | Receipt_repr.Credited (Tez_repr.Tez_tag tz) => 0 <= tz <= Int64.max_int
      end.
  End Valid.
End Balance_update.

Lemma balance_update_encoding_is_valid :
  Data_encoding.Valid.t Balance_update.Valid.t Receipt_repr.balance_update_encoding.
  Data_encoding.Valid.data_encoding_auto.
  intros x H; simpl in *; split; trivial; unfold Json.wrap_error.
  { destruct x; simpl in *; autounfold with tezos_z in *;
    destruct t; lia. }
  { destruct x, t; destruct H; autounfold with tezos_z in *;
      unfold normalize_int64, two_pow_63, two_pow_64; simpl;
      repeat match goal with
      | |- context[if ?e then _ else _] => destruct e eqn:?
      end;
      hauto lq: on rew: off solve: lia.
  }
Qed.
#[global] Hint Resolve balance_update_encoding_is_valid : Data_encoding_db.

Definition cuo_index (o_value : Receipt_repr.update_origin) : int :=
    match o_value with
    | Receipt_repr.Block_application => 0
    | Receipt_repr.Protocol_migration => 1
    | Receipt_repr.Subsidy => 2
    | Receipt_repr.Simulation => 3
    end.

Lemma compare_update_origin_is_valid :
  Compare.Valid.t cuo_index Receipt_repr.compare_update_origin.
  apply (Compare.Valid.projection cuo_index id Z.compare).
  exact Compare.Valid.z.
Qed.
 
Lemma update_origin_encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True) Receipt_repr.update_origin_encoding.
  Data_encoding.Valid.data_encoding_auto;
  intro x; destruct x; trivial; tauto.
Qed.
#[global] Hint Resolve update_origin_encoding_is_valid : Data_encoding_db.

Module Balance_updates.
  Module Valid.
    Definition t (x : Receipt_repr.balance_updates) : Prop :=
      List.Forall
        (fun '(_, balance_update, _) => Balance_update.Valid.t balance_update)
        x.
  End Valid.
End Balance_updates.

Lemma balance_updates_encoding_is_valid :
  Data_encoding.Valid.t
    Balance_updates.Valid.t
    Receipt_repr.balance_updates_encoding.
  Data_encoding.Valid.data_encoding_auto.
  apply List.Forall_impl.
  hauto l: on.
Qed.
#[global] Hint Resolve balance_updates_encoding_is_valid : Data_encoding_db.
