Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Require TezosOfOCaml.Proto_2021_01.Raw_context.

Definition current : Raw_context.context -> Int64.t :=
  Raw_context.current_fitness.

Definition increase (ctxt : Raw_context.context) : Raw_context.t :=
  let fitness := current ctxt in
  Raw_context.set_current_fitness ctxt (Int64.succ fitness).
