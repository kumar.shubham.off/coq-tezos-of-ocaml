Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.S.

Axiom Included_HASH_is_valid
  : S.HASH.Valid.t (fun _ => True) Block_hash.Included_HASH.
