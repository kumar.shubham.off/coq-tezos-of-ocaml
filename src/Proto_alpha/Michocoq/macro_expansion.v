(* Open Source License *)
(* Copyright (c) 2021 Nomadic Labs. <contact@nomadic-labs.com> *)

(* Permission is hereby granted, free of charge, to any person obtaining a *)
(* copy of this software and associated documentation files (the "Software"), *)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense, *)
(* and/or sell copies of the Software, and to permit persons to whom the *)
(* Software is furnished to do so, subject to the following conditions: *)

(* The above copyright notice and this permission notice shall be included *)
(* in all copies or substantial portions of the Software. *)

(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR *)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER *)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER *)
(* DEALINGS IN THE SOFTWARE. *)

Require ZArith Coq.Lists.List.
Require Import micheline_syntax primitives macro_lexing.
Require Import error.
Import error.Notations.
Import List.ListNotations.

Definition node := location.location -> location.location -> loc_micheline primitive.

Definition Applied (m : loc_micheline primitive) : node :=
  fun b e => m.

Definition apply_nodes (l : list node) (b e : location.location) :
  list (loc_micheline primitive) :=
  List.map (fun n => n b e) l.

Definition primn p (l : list node) : node := fun b e =>
  Mk_loc_micheline (b, e, PRIM (b, e, p) [] (apply_nodes l b e)).
Definition prim0 p := primn p [].
Definition prim1 p x := primn p [x].
Definition prim2 p x y := primn p [x; y].

Definition seq (l : list node) : node := fun b e =>
  Mk_loc_micheline (b, e, SEQ (apply_nodes l b e)).

Notation "{ A ; .. ; B }" := (seq (cons A .. (cons B nil) ..)).
Notation "{}" := (seq []).

Definition UNIT := prim0 I_UNIT.
Definition FAILWITH := prim0 I_FAILWITH.
Definition IF_ := prim2 I_IF.
Definition IF_NONE := prim2 I_IF_NONE.
Definition IF_LEFT := prim2 I_IF_LEFT.
Definition IF_CONS := prim2 I_IF_CONS.
Definition DUP := prim0 I_DUP.
Definition DUPn := prim1 I_DUP.
Definition DIP := prim1 I_DIP.
Definition DIPn := prim2 I_DIP.
Definition SWAP := prim0 I_SWAP.
Definition CAR := prim0 I_CAR.
Definition CDR := prim0 I_CDR.
Definition PAIR := prim0 I_PAIR.
Definition UNPAIR := prim0 I_UNPAIR.
Definition COMPARE := prim0 I_COMPARE.

Definition FAIL := {UNIT; FAILWITH}.
Definition ASSERT := {IF_ {} {FAIL}}.

Definition ASSERT_NONE := {IF_NONE {} {FAIL}}.
Definition ASSERT_SOME := {IF_NONE {FAIL} {}}.
Definition ASSERT_LEFT := {IF_LEFT {} {FAIL}}.
Definition ASSERT_RIGHT := {IF_LEFT {FAIL} {}}.

Definition IF_SOME bt bf := {IF_NONE bf bt}.
Definition IF_RIGHT bt bf := {IF_LEFT bf bt}.
Definition IF_NIL bt bf := {IF_CONS bf bt}.

Fixpoint expand_cadr (x : cadr) : list node :=
  match x with
  | Cadr_CAR x => CAR :: expand_cadr x
  | Cadr_CDR x => CDR :: expand_cadr x
  | Cadr_nil => []
  end.

Fixpoint expand_set_cadr (x : cadr) : list node :=
  match x with
  | Cadr_CAR Cadr_nil =>
    [CDR; SWAP; PAIR]
  | Cadr_CDR Cadr_nil =>
    [CAR; PAIR]
  | Cadr_CAR x =>
    [DUP; DIP (seq (CAR :: expand_set_cadr x)); CDR; SWAP; PAIR]
  | Cadr_CDR x =>
    [DUP; DIP (seq (CDR :: expand_set_cadr x)); CAR; PAIR]
  | Cadr_nil => [] (* Should not happen *)
  end.

Fixpoint MAP_CADR (x : cadr) (code : list node) : list node :=
  match x with
  | Cadr_CAR Cadr_nil =>
    [DUP; CDR; DIP (seq (CAR :: code)); SWAP; PAIR]
  | Cadr_CDR Cadr_nil =>
    [DUP; CDR] ++ code ++ [SWAP; CAR; PAIR]
  | Cadr_CAR x =>
    [DUP; DIP (seq (CAR :: MAP_CADR x code)); CDR; SWAP; PAIR]
  | Cadr_CDR x =>
    [DUP; DIP (seq (CDR :: MAP_CADR x code)); CAR; PAIR]
  | Cadr_nil => code (* Should not happen *)
  end.

(* PAPAIR stuff *)

Fixpoint expand_papair_d d (x : papair_d d) : list node :=
  match x with
  | Papair_d_P d x y =>
    expand_papair_d macro_lexing.Left x ++
    match (expand_papair_d macro_lexing.Right y) with
    | nil => [PAIR]
    | yi => [DIP (seq yi); PAIR]
    end
  | Papair_d_A => []
  | Papair_d_I => []
  end.

Definition PAPAIR (x : papair) :=
  let 'Papair_P x y := x in
  expand_papair_d
    macro_lexing.Left (Papair_d_P macro_lexing.Left x y).

Goal PAPAIR (Papair_P Papair_d_A Papair_d_I) = [PAIR]%list.
Proof.
  reflexivity.
Qed.

Goal PAPAIR (Papair_P Papair_d_A (Papair_d_P _ Papair_d_A Papair_d_I)) = [ DIP {PAIR}; PAIR ]%list.
Proof.
  reflexivity.
Qed.

Fixpoint expand_unpapair_d d (x : papair_d d) : list node :=
    match x with
    | Papair_d_P d x y =>
      match x, y with
      | Papair_d_A, Papair_d_I => [UNPAIR]
      | _, _ =>
        let xi := expand_unpapair_d macro_lexing.Left x in
        UNPAIR ::
            match (expand_unpapair_d macro_lexing.Right y) with
            | nil => xi
            | yi => DIP (seq yi) :: xi
            end
      end
  | Papair_d_A => []
  | Papair_d_I => []
  end.

Definition UNPAPAIR (x : papair) : list node :=
  let 'Papair_P x y := x in
  expand_unpapair_d
    macro_lexing.Left (Papair_d_P macro_lexing.Left x y).

Goal seq (UNPAPAIR (Papair_P Papair_d_A Papair_d_I)) = { UNPAIR }.
Proof.
  reflexivity.
Qed.

Goal seq (UNPAPAIR (Papair_P Papair_d_A (Papair_d_P _ Papair_d_A Papair_d_I))) =
 {UNPAIR; DIP {UNPAIR}}.
Proof.
  reflexivity.
Qed.

Definition expand_op (op : macro_lexing.op) : node :=
  match op with
  | macro_lexing.EQ => prim0 I_EQ
  | macro_lexing.NEQ => prim0 I_NEQ
  | macro_lexing.GT => prim0 I_GT
  | macro_lexing.LT => prim0 I_LT
  | macro_lexing.GE => prim0 I_GE
  | macro_lexing.LE => prim0 I_LE
  end.

Definition IFop op i1 i2 := {expand_op op; IF_ i1 i2}.
Definition CMPop op := {COMPARE; expand_op op}.
Definition IFCMPop op i1 i2 := {COMPARE; expand_op op; IF_ i1 i2}.
Definition ASSERTop op := {expand_op op; IF_ {} {FAIL}}.
Definition ASSERT_CMPop op := { {COMPARE; expand_op op}; IF_ {} {FAIL}}.
Open Scope list_scope.

Fixpoint expand_macros (bem : loc_micheline macro) : M (loc_micheline primitive) :=
  let 'Mk_loc_micheline ((b, e), m) := bem in
  match m with
  | SEQ l =>
    let! l :=
       (fix expand_macros_l (l : Datatypes.list (loc_micheline macro)) : M (list (loc_micheline primitive)) :=
       match l with
       | [] => Return []
       | i1 :: i2 =>
         let! i1 := expand_macros i1 in
         let! i2 := expand_macros_l i2 in
         Return (i1 :: i2)%list
       end) l in
    Return (Mk_loc_micheline ((b, e), SEQ l))
  | STR _ s => Return (Mk_loc_micheline ((b, e), STR _ s))
  | BYTES _ s => Return (Mk_loc_micheline ((b, e), BYTES _ s))
  | NUMBER _ z => Return (Mk_loc_micheline ((b, e), NUMBER _ z))
  | PRIM ((b', e'), p) annots args =>
    let! args := (fix expand_macros_l (l : Datatypes.list (loc_micheline macro)) : M (list (loc_micheline primitive)) :=
       match l with
       | [] => Return []
       | i1 :: i2 =>
         let! i1 := expand_macros i1 in
         let! i2 := expand_macros_l i2 in
         Return (i1 :: i2)
       end) args in
    match p, args with
    | Prim p, args =>
      Return (Mk_loc_micheline ((b, e), PRIM ((b', e'), p) annots args))
    | macro_lexing.FAIL, [] => Return (FAIL b e)
    | macro_lexing.ASSERT, [] => Return (ASSERT b e)
    | macro_lexing.ASSERT_NONE, [] => Return (ASSERT_NONE b e)
    | macro_lexing.ASSERT_SOME, [] => Return (ASSERT_SOME b e)
    | macro_lexing.ASSERT_LEFT, [] => Return (ASSERT_LEFT b e)
    | macro_lexing.ASSERT_RIGHT, [] => Return (ASSERT_RIGHT b e)
    | macro_lexing.IF_SOME, [x; y] => Return (IF_SOME (Applied x) (Applied y) b e)
    | macro_lexing.IF_RIGHT, [x; y] => Return (IF_RIGHT (Applied x) (Applied y) b e)
    | macro_lexing.IF_NIL, [x; y] => Return (IF_NIL (Applied x) (Applied y) b e)
    | macro_lexing.IFop op, [x; y] => Return (IFop op (Applied x) (Applied y) b e)
    | macro_lexing.CMPop op, [] => Return (CMPop op b e)
    | macro_lexing.IFCMPop op, [x; y] => Return (IFCMPop op (Applied x) (Applied y) b e)
    | macro_lexing.ASSERTop op, [] => Return (ASSERTop op b e)
    | macro_lexing.ASSERT_CMPop op, [] => Return (ASSERT_CMPop op b e)
    | macro_lexing.DUUP n _, [] => Return (DUPn (fun b e => Mk_loc_micheline ((b, e), NUMBER _ (BinInt.Z.of_nat n))) b e)
    | macro_lexing.DIIP n _, [a] => Return (DIPn (fun b e => Mk_loc_micheline ((b, e), NUMBER _ (BinInt.Z.of_nat n))) (Applied a) b e)
    | macro_lexing.CADR cadr _, [] => Return (seq (expand_cadr cadr) b e)
    | macro_lexing.SET_CADR cadr _, [] => Return (seq (expand_set_cadr cadr) b e)
    | macro_lexing.MAP_CADR cadr _, [Mk_loc_micheline (_, _, SEQ a)] =>
      Return (seq (MAP_CADR cadr (List.map Applied a)) b e)
    | macro_lexing.PAPAIR papair _, [] =>
      Return (seq (PAPAIR papair) b e)
    | macro_lexing.UNPAPAIR papair _, [] =>
      Return (seq (UNPAPAIR papair) b e)

    | macro_lexing.FAIL, _
    | macro_lexing.ASSERT, _
    | macro_lexing.ASSERT_NONE, _
    | macro_lexing.ASSERT_SOME, _
    | macro_lexing.ASSERT_LEFT, _
    | macro_lexing.ASSERT_RIGHT, _
    | macro_lexing.IF_SOME, _
    | macro_lexing.IF_RIGHT, _
    | macro_lexing.IF_NIL, _
    | macro_lexing.IFop _, _
    | macro_lexing.CMPop _, _
    | macro_lexing.IFCMPop _, _
    | macro_lexing.ASSERTop _, _
    | macro_lexing.ASSERT_CMPop _, _
    | macro_lexing.DUUP _ _, _
    | macro_lexing.DIIP _ _, _
    | macro_lexing.CADR _ _, _
    | macro_lexing.SET_CADR _ _, _
    | macro_lexing.MAP_CADR _ _, _
    | macro_lexing.PAPAIR _ _, _
    | macro_lexing.UNPAPAIR _ _, _ =>
      Failed _ (Expansion_prim b' e' (macro_lexing.to_string p))
    end end.
