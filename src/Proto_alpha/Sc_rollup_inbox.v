(** File generated by coq-of-ocaml *)
Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Path_encoding.
Require TezosOfOCaml.Proto_alpha.Raw_level_repr.

Definition inbox_hash : string := "\003\250\174\238\208".

Module Inbox_hash.
  Definition prefix : string := "scib1".
  
  Definition encoded_size : int := 55.
  
  Definition H :=
    Blake2B.Make
      {|
        Blake2B.Register.register_encoding _ := Base58.register_encoding
      |}
      (let name := "inbox_hash" in
      let title := "The hash of a the inbox of a smart contract rollup" in
      let b58check_prefix := inbox_hash in
      let size_value {A : Set} : option A :=
        None in
      {|
        Blake2B.PrefixedName.name := name;
        Blake2B.PrefixedName.title := title;
        Blake2B.PrefixedName.size_value := size_value;
        Blake2B.PrefixedName.b58check_prefix := b58check_prefix
      |}).
  
  (** Inclusion of the module [H] *)
  Definition t := H.(S.HASH.t).
  
  Definition name := H.(S.HASH.name).
  
  Definition title := H.(S.HASH.title).
  
  Definition pp := H.(S.HASH.pp).
  
  Definition pp_short := H.(S.HASH.pp_short).
  
  Definition op_eq := H.(S.HASH.op_eq).
  
  Definition op_ltgt := H.(S.HASH.op_ltgt).
  
  Definition op_lt := H.(S.HASH.op_lt).
  
  Definition op_lteq := H.(S.HASH.op_lteq).
  
  Definition op_gteq := H.(S.HASH.op_gteq).
  
  Definition op_gt := H.(S.HASH.op_gt).
  
  Definition compare := H.(S.HASH.compare).
  
  Definition equal := H.(S.HASH.equal).
  
  Definition max := H.(S.HASH.max).
  
  Definition min := H.(S.HASH.min).
  
  Definition hash_bytes := H.(S.HASH.hash_bytes).
  
  Definition hash_string := H.(S.HASH.hash_string).
  
  Definition zero := H.(S.HASH.zero).
  
  Definition size_value := H.(S.HASH.size_value).
  
  Definition to_bytes := H.(S.HASH.to_bytes).
  
  Definition of_bytes_opt := H.(S.HASH.of_bytes_opt).
  
  Definition of_bytes_exn := H.(S.HASH.of_bytes_exn).
  
  Definition to_b58check := H.(S.HASH.to_b58check).
  
  Definition to_short_b58check := H.(S.HASH.to_short_b58check).
  
  Definition of_b58check_exn := H.(S.HASH.of_b58check_exn).
  
  Definition of_b58check_opt := H.(S.HASH.of_b58check_opt).
  
  Definition b58check_encoding := H.(S.HASH.b58check_encoding).
  
  Definition encoding := H.(S.HASH.encoding).
  
  Definition rpc_arg := H.(S.HASH.rpc_arg).
  
  (** Init function; without side-effects in Coq *)
  Definition init_module : unit :=
    Base58.check_encoded_prefix b58check_encoding prefix encoded_size.
  
  Definition Path_encoding_Make_hex_include :=
    Path_encoding.Make_hex
      {|
        Path_encoding.ENCODING.to_bytes := H.(S.HASH.to_bytes);
        Path_encoding.ENCODING.of_bytes_opt := H.(S.HASH.of_bytes_opt)
      |}.
  
  (** Inclusion of the module [Path_encoding_Make_hex_include] *)
  Definition to_path :=
    Path_encoding_Make_hex_include.(Path_encoding.S.to_path).
  
  Definition of_path :=
    Path_encoding_Make_hex_include.(Path_encoding.S.of_path).
  
  Definition path_length :=
    Path_encoding_Make_hex_include.(Path_encoding.S.path_length).
End Inbox_hash.

Definition hash : Set := Inbox_hash.t.

Module t.
  Record record : Set := Build {
    hash : hash;
    inbox_size : int64 }.
  Definition with_hash hash (r : record) :=
    Build hash r.(inbox_size).
  Definition with_inbox_size inbox_size (r : record) :=
    Build r.(hash) inbox_size.
End t.
Definition t := t.record.

Definition pp_hash : Format.formatter -> Inbox_hash.t -> unit := Inbox_hash.pp.

Definition pp (fmtr : Format.formatter) (function_parameter : t) : unit :=
  let '{| t.hash := hash_value; t.inbox_size := inbox_size |} :=
    function_parameter in
  Format.fprintf fmtr
    (CamlinternalFormatBasics.Format
      (CamlinternalFormatBasics.Formatting_gen
        (CamlinternalFormatBasics.Open_box
          (CamlinternalFormatBasics.Format
            (CamlinternalFormatBasics.String_literal "<v 2>"
              CamlinternalFormatBasics.End_of_format) "<v 2>"))
        (CamlinternalFormatBasics.String_literal "{ hash = "
          (CamlinternalFormatBasics.Alpha
            (CamlinternalFormatBasics.Char_literal ";" % char
              (CamlinternalFormatBasics.Formatting_lit
                (CamlinternalFormatBasics.Break "@," 0 0)
                (CamlinternalFormatBasics.String_literal "inbox_size = "
                  (CamlinternalFormatBasics.Int64 CamlinternalFormatBasics.Int_d
                    CamlinternalFormatBasics.No_padding
                    CamlinternalFormatBasics.No_precision
                    (CamlinternalFormatBasics.String_literal " }"
                      (CamlinternalFormatBasics.Formatting_lit
                        CamlinternalFormatBasics.Close_box
                        CamlinternalFormatBasics.End_of_format)))))))))
      "@[<v 2>{ hash = %a;@,inbox_size = %Ld }@]") pp_hash hash_value inbox_size.

Definition hash_encoding : Data_encoding.t Inbox_hash.t := Inbox_hash.encoding.

Definition encoding : Data_encoding.encoding t :=
  Data_encoding.conv
    (fun (function_parameter : t) =>
      let '{| t.hash := hash_value; t.inbox_size := inbox_size |} :=
        function_parameter in
      (hash_value, inbox_size))
    (fun (function_parameter : hash * int64) =>
      let '(hash_value, inbox_size) := function_parameter in
      {| t.hash := hash_value; t.inbox_size := inbox_size |}) None
    (Data_encoding.obj2 (Data_encoding.req None None "hash" hash_encoding)
      (Data_encoding.req None None "inbox_size" Data_encoding.int64_value)).

Definition message_encoding
  : Data_encoding.encoding (Inbox_hash.t * string * Raw_level_repr.raw_level) :=
  Data_encoding.obj3 (Data_encoding.req None None "parent" hash_encoding)
    (Data_encoding.req None None "payload" Data_encoding.string_value)
    (Data_encoding.req None None "level" Raw_level_repr.encoding).

Definition number_of_available_messages (function_parameter : t) : Z.t :=
  let '{| t.hash := _; t.inbox_size := inbox_size |} := function_parameter in
  Z.of_int64 inbox_size.

Definition empty : t := {| t.hash := Inbox_hash.zero; t.inbox_size := 0 |}.

Definition add_message (function_parameter : t)
  : string -> Raw_level_repr.raw_level -> t :=
  let '{| t.hash := hash_value; t.inbox_size := inbox_size |} :=
    function_parameter in
  fun (message : string) =>
    fun (level : Raw_level_repr.raw_level) =>
      let message_bytes :=
        Data_encoding.Binary.to_bytes_exn None message_encoding
          (hash_value, message, level) in
      let hash_value := Inbox_hash.hash_bytes None [ message_bytes ] in
      {| t.hash := hash_value; t.inbox_size := Int64.succ inbox_size |}.

Definition add_messages
  (messages : list string) (level : Raw_level_repr.raw_level) (inbox : t) : t :=
  List.fold_left
    (fun (inbox : t) =>
      fun (message : string) => add_message inbox message level) inbox messages.

Definition consume_n_messages (n_value : int) (function_parameter : t)
  : option t :=
  let '{| t.hash := hash_value; t.inbox_size := inbox_size |} :=
    function_parameter in
  if n_value <i 0 then
    None
  else
    if (Int64.of_int n_value) >i64 inbox_size then
      None
    else
      Some
        {| t.hash := hash_value;
          t.inbox_size := inbox_size -i64 (Int64.of_int n_value) |}.
