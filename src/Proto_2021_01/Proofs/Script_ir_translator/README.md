Here we prove properties about the translator. This file is by far the largest
one in the protocol (more than 6.000 lines of code). It mainly contains
primitives about the parsing, typing and unparsing of Michelson terms. We
attempt to verify various properties relating all these functions, like:

* how parsing and unparsing operations relate to each other;
* what are the pre-conditions required to avoid raising errors for the functions
  in the error monad;
* are these pre-conditions ensured by functions producing Michelson terms;
* the usual properties about the comparison functions (equality, ordering);
* coherence properties about the conversion functions (between comparable types
  and types for example).

## Challenges
Among the challenges encountered by these proofs are:

* the GADTs, which make the translation of the OCaml source code difficult. We
  use [attributes](https://clarus.github.io/coq-of-ocaml/docs/attributes) to
  guide the compilation of `coq-of-ocaml`;
* the non-syntactic termination; for some of the functions we disable the
  termination checker of Coq with `Unset Guard Checking`. We also need to
  disable the guard checking in proofs following the same induction principle as
  functions which are not syntactically terminating. Without disabling
  termination checks some definitions / proofs would not be accepted by the Coq
  checker. However, doing so we may introduce a proof of `False` if we are not
  cautious and use definitions which are actually not terminating. Plus we need
  to be worried about the `simpl` tactic which may diverge doing symbolic
  evaluation, if the `{struct ...}` parameter of a fixpoint does not change
  in the recursive calls;
* the error-monad, which makes the reasoning more complex with the error case.
  We introduce pre-conditions in order to reason about the cases which do not
  fail;
* the gas counting. We do most of our proofs with an unlimited gas value to
  avoid issues with the gas, what we think does not remove too much generality.
