Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Seed_storage.
Require TezosOfOCaml.Proto_alpha.Nonce_storage.

Require TezosOfOCaml.Proto_alpha.Proofs.Cycle_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Seed_repr.

(* @TODO *)
Axiom for_cycle_is_valid : forall {ctxt} {cycle} {seed : Seed_repr.t},
  Cycle_repr.Valid.t cycle ->
  match Seed_storage.for_cycle ctxt cycle with
  | Pervasives.Ok seed => Seed_repr.Seed.Valid.t seed
  | Pervasives.Error _ => True
  end.

(* @TODO can we specify Seed_storage.cycle_end? *)
