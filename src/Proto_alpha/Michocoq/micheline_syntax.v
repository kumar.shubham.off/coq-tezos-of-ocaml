
Require Import Coq.Strings.String ZArith.

Require Import location.
Require error.
Import error.Notations.
Set Implicit Arguments.

Inductive annotation := Mk_annot : location * location * string -> annotation.

Inductive micheline (primitive : Set) : Set :=
| SEQ (_ : list (loc_micheline primitive))
| PRIM (_ : location * location * primitive)
       (_ : list annotation) (_ : list (loc_micheline primitive))
| STR (_ : string)
| BYTES (_ : string)
| NUMBER (_ : Z)
with
  loc_micheline (primitive : Set) : Set :=
| Mk_loc_micheline : location * location * micheline primitive ->
                     loc_micheline primitive.

Fixpoint map {p1 p2 : Set} (f : p1 -> p2) (m : loc_micheline p1){struct m} : loc_micheline p2 :=
  let 'Mk_loc_micheline (l1, l2, m) := m in
  match m with
  | SEQ l =>
    Mk_loc_micheline (l1, l2, SEQ (Lists.List.map (map f) l))
  | PRIM (lp1, lp2, p) annots args =>
    Mk_loc_micheline (l1, l2, PRIM (lp1, lp2, f p) annots (List.map (map f) args))
  | STR _ s => Mk_loc_micheline (l1, l2, STR _ s)
  | BYTES _ s => Mk_loc_micheline (l1, l2, BYTES _ s)
  | NUMBER _ z => Mk_loc_micheline (l1, l2, NUMBER _ z)
  end.

Fixpoint map_M {p1 p2 : Set} (f : p1 -> error.M p2) (m : loc_micheline p1) : error.M (loc_micheline p2) :=
  let 'Mk_loc_micheline (l1, l2, m) := m in
  match m with
  | SEQ l =>
    let! l := error.list_map (map_M f) l in
    error.Return (Mk_loc_micheline (l1, l2, SEQ l))
  | PRIM (lp1, lp2, p) annots args =>
    let! p := f p in
    let! args := error.list_map (map_M f) args in
    error.Return (Mk_loc_micheline (l1, l2, PRIM (lp1, lp2, p) annots args))
  | STR _ s => error.Return (Mk_loc_micheline (l1, l2, STR _ s))
  | BYTES _ s => error.Return (Mk_loc_micheline (l1, l2, BYTES _ s))
  | NUMBER _ z => error.Return (Mk_loc_micheline (l1, l2, NUMBER _ z))
  end.
