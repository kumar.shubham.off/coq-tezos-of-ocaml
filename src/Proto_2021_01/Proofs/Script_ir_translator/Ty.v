(** In this file we verify various properties about the Michelson type in the
    translator file of the protocol. *)
Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Require TezosOfOCaml.Proto_2021_01.Script_ir_translator.

Require Import TezosOfOCaml.Proto_2021_01.Proofs.Utils.
Require TezosOfOCaml.Proto_2021_01.Proofs.Raw_context.
Require TezosOfOCaml.Proto_2021_01.Proofs.Script_ir_translator.Comparable_ty.
Require TezosOfOCaml.Proto_2021_01.Proofs.Script_typed_ir.

(** A version of the [unparse_ty] function which always succeeds. *)
Definition simple_unparse_ty ctxt ty :=
  match Script_ir_translator.unparse_ty ctxt ty with
  | Pervasives.Ok (node, _) => node
  | _ => Micheline.Prim (-1) Michelson_v1_primitives.T_unit [] []
  end.

(** In case of unlimited gas, the [simple_unparse_ty] function behaves like
    [unparse_ty]. *)
Fixpoint simple_unparse_ty_eq ctxt ty
  : let unlimited_ctxt := Raw_context.with_unlimited_gas ctxt in
    (Script_ir_translator.unparse_ty unlimited_ctxt ty =
    return? (simple_unparse_ty unlimited_ctxt ty, unlimited_ctxt)).
  destruct ty; unfold simple_unparse_ty; simpl; trivial;
    repeat (rewrite simple_unparse_ty_eq; simpl);
    reflexivity.
Qed.

(** When we unparse a pair there are at least to parameters (maybe more in case
    of flattening). *)
Fixpoint simple_unparse_ty_pair_at_least_two ctxt ty :
  let unlimited_ctxt := Raw_context.with_unlimited_gas ctxt in
  match simple_unparse_ty unlimited_ctxt ty with
  | Prim _ Michelson_v1_primitives.T_pair ps _ =>
    match ps with
    | [] | [_] => False
    | _ => True
    end
  | _ => True
  end.
  destruct ty; simpl; trivial;
    unfold simple_unparse_ty; simpl;
    repeat (rewrite simple_unparse_ty_eq; simpl);
    trivial.
  assert (H_ty2 := simple_unparse_ty_pair_at_least_two ctxt ty2).
  simpl in H_ty2.
  destruct (simple_unparse_ty _ ty2); trivial.
  destruct p; try destruct a; try destruct l0; trivial.
Qed.

(** When we unparse a Michelson type we always get a [Prim] with a location of
    [(-1)]. *)
Lemma simple_unparse_ty_location ctxt ty :
  let unlimited_ctxt := Raw_context.with_unlimited_gas ctxt in
  match simple_unparse_ty unlimited_ctxt ty with
  | Prim l _ _ _ => l = (-1)
  | _ => False
  end.
  destruct ty; simpl;
    unfold simple_unparse_ty; simpl;
    repeat (rewrite simple_unparse_ty_eq; simpl);
    reflexivity.
Qed.

(** We show that the parsing and unparsing are inverse functions on the
    Michelson types for the simplified case of [simple_unparse_ty]. *)
Fixpoint parse_simple_unparse_ty
  ctxt
  legacy
  allow_lazy_storage allow_operation allow_contract allow_ticket
  ty
  (H_ty
    : Script_typed_ir.Ty.is_valid
        legacy
        allow_lazy_storage allow_operation allow_contract allow_ticket
        ty =
      true
  )
  : let unlimited_ctxt := Raw_context.with_unlimited_gas ctxt in
    (let node := simple_unparse_ty unlimited_ctxt ty in
    Script_ir_translator.parse_ty
      unlimited_ctxt
      legacy
      allow_lazy_storage allow_operation allow_contract allow_ticket
      node) =
    return? (Script_ir_translator.Ex_ty ty, unlimited_ctxt).
  destruct ty; unfold simple_unparse_ty; simpl; try reflexivity;
    repeat (rewrite simple_unparse_ty_eq; simpl);
    repeat (rewrite Comparable_ty.parse_unparse_comparable_ty; simpl);
    repeat (rewrite (parse_simple_unparse_ty ctxt); simpl);
    trivial;
    simpl in H_ty; try (rewrite Bool.andb_true_iff in H_ty; destruct H_ty);
    trivial.
  { step Script_typed_ir.Pair_t.
    assert (H_location := simple_unparse_ty_location ctxt ty2).
    simpl in H_location.
    destruct (simple_unparse_ty _ ty2) eqn:H_eq;
      try contradiction H_location.
    rewrite <- H_eq.
    destruct p;
      try destruct a;
      repeat (rewrite parse_simple_unparse_ty; trivial; simpl).
    assert (H_two := simple_unparse_ty_pair_at_least_two ctxt ty2).
    simpl in H_two; rewrite H_eq in H_two.
    do 2 (destruct l0; try now destruct H_two).
    rewrite H_location in H_eq.
    rewrite <- H_eq.
    now rewrite (parse_simple_unparse_ty ctxt).
  }
  { step Script_typed_ir.Big_map_t.
    now destruct allow_lazy_storage.
  }
  { step Script_typed_ir.Contract_t.
    now destruct allow_contract.
  }
  { step Script_typed_ir.Sapling_transaction_t.
    now rewrite Sapling_repr.Memo_size.parse_unparse.
  }
  { step Script_typed_ir.Sapling_state_t.
    destruct allow_lazy_storage; try easy.
    now rewrite Sapling_repr.Memo_size.parse_unparse.
  }
  { step Script_typed_ir.Operation_t.
    now destruct allow_operation.
  }
  { step Script_typed_ir.Ticket_t.
    now destruct allow_ticket.
  }
Qed.

(** The unparsing followed by a parsing of a Michelson type is the identity. We
    require the invariant [Script_typed_ir.Ty.is_valid] to be [true] for this
    proof to work. We verify this property for the unlimited gas case, what we
    believe does not reduce the generality of the proof as always consume
    operations are done on the gas. *)
Lemma parse_unparse_ty
  ctxt
  legacy
  allow_lazy_storage allow_operation allow_contract allow_ticket
  ty
  (H_ty
  : Script_typed_ir.Ty.is_valid
      legacy
      allow_lazy_storage allow_operation allow_contract allow_ticket
      ty =
    true
  )
  : let unlimited_ctxt := Raw_context.with_unlimited_gas ctxt in
    (let? '(node, ctxt) := Script_ir_translator.unparse_ty unlimited_ctxt ty in
    Script_ir_translator.parse_ty
      ctxt
      legacy
      allow_lazy_storage allow_operation allow_contract allow_ticket
      node) =
    return? (Script_ir_translator.Ex_ty ty, unlimited_ctxt).
  simpl.
  rewrite simple_unparse_ty_eq; simpl.
  now apply parse_simple_unparse_ty.
Qed.

Ltac apply_parse_is_valid parse_is_valid :=
  match goal with
  | |- context [Script_ir_translator.parse_ty
      ?ctxt
      ?legacy
      ?allow_lazy_storage ?allow_operation ?allow_contract ?allow_ticket
      ?node
    ] =>
    let H := fresh "H" in
    assert (H :=
      parse_is_valid
        ctxt
        legacy
        allow_lazy_storage allow_operation allow_contract allow_ticket
        node
    );
    let ty := fresh "ty" in
    destruct (Script_ir_translator.parse_ty _ _ _ _ _ _) as [[[ty]]|];
    simpl in *;
    trivial
  end.

Ltac destruct_gas_consume :=
  destruct (Alpha_context.Gas.consume _ _); simpl; trivial.

Ltac destruct_list l :=
  repeat (destruct l; simpl; trivial).

Ltac destruct_parse_comparable_ty :=
  destruct (Script_ir_translator.parse_comparable_ty _ _) as [[[]]|];
    simpl; trivial.

Ltac destruct_parse_memo_size n :=
  unfold Script_ir_translator.parse_memo_size;
  destruct n; simpl; trivial;
  try (
    unfold Alpha_context.Sapling.Memo_size.parse_z;
    destruct (_ && _) eqn:H; simpl; trivial
  ).

Unset Guard Checking.
(** We show that the parsing operation always produces terms which validate the
    property [Script_ir_translator.parse_ty]. We believe this lemma to be useful
    as it seems that the property [Script_ir_translator.parse_ty] is often
    implicitely assumed to be true on Michelson type terms. *)
Fixpoint parse_is_valid
  ctxt
  legacy
  allow_lazy_storage allow_operation allow_contract allow_ticket
  node
  {struct node}
  : let result :=
      Script_ir_translator.parse_ty
        ctxt
        legacy
        allow_lazy_storage allow_operation allow_contract allow_ticket
        node in
    match result with
    | Pervasives.Ok (Script_ir_translator.Ex_ty ty, _) =>
      Script_typed_ir.Ty.is_valid
        legacy
        allow_lazy_storage allow_operation allow_contract allow_ticket
        ty =
      true
    | _ => True
    end.
  destruct node; simpl; destruct_gas_consume.
  destruct p; simpl; try exact I;
    try now destruct l0.
  { step Script_typed_ir.Contract_t.
    destruct_list l0.
    destruct allow_contract; simpl; trivial.
    apply_parse_is_valid parse_is_valid.
  }
  { step Script_typed_ir.Lambda_t.
    destruct_list l0.
    do 2 apply_parse_is_valid parse_is_valid.
    now rewrite Bool.andb_true_iff.
  }
  { step Script_typed_ir.List_t.
    destruct_list l0.
    apply_parse_is_valid parse_is_valid.
  }
  { step Script_typed_ir.Map_t.
    destruct_list l0.
    destruct_parse_comparable_ty.
    apply_parse_is_valid parse_is_valid.
  }
  { step Script_typed_ir.Big_map_t.
    destruct allow_lazy_storage; simpl; trivial.
    destruct_gas_consume.
    destruct_list l0.
    destruct_parse_comparable_ty.
    apply_parse_is_valid parse_is_valid.
  }
  { step Script_typed_ir.Option_t.
    destruct_list l0.
    apply_parse_is_valid parse_is_valid.
  }
  { step Script_typed_ir.Union_t.
    destruct_list l0.
    do 2 apply_parse_is_valid parse_is_valid.
    now rewrite Bool.andb_true_iff.
  }
  { step Script_typed_ir.Pair_t.
    destruct l0; simpl; trivial.
    do 2 apply_parse_is_valid parse_is_valid.
    now rewrite Bool.andb_true_iff.
  }
  { step Script_typed_ir.Set_t.
    destruct_list l0.
    destruct_parse_comparable_ty.
  }
  { step Script_typed_ir.Operation_t.
    destruct_list l0.
    destruct allow_operation; simpl; trivial.
  }
  { step Script_typed_ir.Sapling_transaction_t.
    destruct_list l0.
    destruct_parse_memo_size n.
  }
  { step Script_typed_ir.Sapling_state_t.
    destruct_list l0.
    destruct allow_lazy_storage; simpl; trivial.
    destruct_parse_memo_size n.
  }
  { step Script_typed_ir.Ticket_t.
    destruct_list l0.
    destruct allow_ticket; simpl; trivial.
    destruct_parse_comparable_ty.
  }
Qed.
Set Guard Checking.

(** The conversion function [ty_of_comparable_ty] produces valid terms. *)
Fixpoint ty_of_comparable_ty_is_valid
  legacy
  allow_lazy_storage allow_operation allow_contract allow_ticket
  ty
  : Script_typed_ir.Ty.is_valid
      legacy
      allow_lazy_storage allow_operation allow_contract allow_ticket
      (Script_ir_translator.ty_of_comparable_ty ty) =
    true.
  destruct ty; simpl; repeat rewrite ty_of_comparable_ty_is_valid; reflexivity.
Qed.

(** The conversion comparable ty -> ty -> comparable ty is the identity. *)
Fixpoint comparable_ty_of_ty_of_comparable_ty ctxt loc comparable_ty
  : let unlimited_ctxt := Raw_context.with_unlimited_gas ctxt in
    (let ty := Script_ir_translator.ty_of_comparable_ty comparable_ty in
    Script_ir_translator.comparable_ty_of_ty unlimited_ctxt loc ty) =
    return? (comparable_ty, unlimited_ctxt).
  destruct comparable_ty; try reflexivity; simpl;
    repeat (rewrite comparable_ty_of_ty_of_comparable_ty; simpl);
    reflexivity.
Qed.

(** The ty -> conversion comparable ty -> ty is the identity. *)
Fixpoint ty_of_comparable_ty_of_ty ctxt loc ty
  : let unlimited_ctxt := Raw_context.with_unlimited_gas ctxt in
    when_success
      (Script_ir_translator.comparable_ty_of_ty unlimited_ctxt loc ty)
      (fun '(comparable_ty, ctxt') =>
        Script_ir_translator.ty_of_comparable_ty comparable_ty = ty /\
        ctxt' = unlimited_ctxt
      ).
  destruct ty; simpl; try easy;
    try (
      destruct (Script_ir_translator.serialize_ty_for_error _ _) as [[]|];
        simpl; exact I
    );
    repeat (eapply when_success_bind; [
      apply ty_of_comparable_ty_of_ty |
      let H := fresh H in
      intros v H;
      destruct v; destruct H as [? H_ctxt];
      rewrite H_ctxt;
      clear H_ctxt
    ]);
    simpl;
    split; congruence.
Qed.
