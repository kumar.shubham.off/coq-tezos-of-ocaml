Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Storage.
Require TezosOfOCaml.Proto_alpha.Commitment_storage.

Require TezosOfOCaml.Proto_alpha.Proofs.Tez_repr.

(** [committed_amount] returns a valid [Tez_repr.t] *)
(* @TODO *)
Axiom committed_amount_is_valid
  : forall {ctxt : Raw_context.t} {bpkh},
  match Commitment_storage.committed_amount ctxt bpkh with
  | Pervasives.Ok tez => Tez_repr.Valid.t tez
  | Pervasives.Error _ => True
  end.

(** increase and decrease functions are inversion of each other *)
(* @TODO*)
Axiom increase_decrease_inverse 
  : forall {ctxt : Raw_context.t} {bpkh} {amount},
  let result := Commitment_storage.increase_commitment_only_call_from_token 
    ctxt bpkh amount in
  match result with
  | Pervasives.Ok ctxt' =>
    let result := Commitment_storage.decrease_commitment_only_call_from_token
      ctxt' bpkh amount in
    match result with
    | Pervasives.Ok ctxt'' => ctxt = ctxt''
    | Pervasives.Error _ => True
    end
  | Pervasives.Error _ => True
  end.
