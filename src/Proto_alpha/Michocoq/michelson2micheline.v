Require Import Ascii Coq.Strings.String Bool.
Require Coq.Lists.List.
Require Import primitives untyped_syntax micheline_syntax error location syntax_type error.
Import List.ListNotations.
Import error.Notations.
Require micheline2michelson.

Open Scope string.

Definition micheline := micheline primitives.primitive.
Definition loc_micheline := loc_micheline primitives.primitive.

Definition dummy_loc : location := Mk_loc 0 0.
Definition dummy_mich (m : micheline) : loc_micheline :=
  Mk_loc_micheline (dummy_loc, dummy_loc, m).
Definition dummy_prim (p : primitives.primitive)
           (l : Datatypes.list loc_micheline) :=
  dummy_mich (PRIM (dummy_loc, dummy_loc, p) [] l).
Definition dummy_annot (s : syntax_type.annotation) : micheline_syntax.annotation
  :=
    let 'Mk_field_annotation s _ := s in
    Mk_annot (dummy_loc, dummy_loc, s).
Definition dummy_seq (m : loc_micheline) : loc_micheline :=
  match m with
  | Mk_loc_micheline (_, _, SEQ _) => m
  | _ => dummy_mich (SEQ [m])
  end.

Definition michelson2micheline_sctype (ct : simple_comparable_type) :=
  match ct with
  | never => dummy_prim T_never []
  | string => dummy_prim T_string []
  | nat => dummy_prim T_nat []
  | int => dummy_prim T_int []
  | bytes => dummy_prim T_bytes []
  | bool => dummy_prim T_bool []
  | mutez => dummy_prim T_mutez []
  | key_hash => dummy_prim T_key_hash []
  | timestamp => dummy_prim T_timestamp []
  | address => dummy_prim T_address []
  | key => dummy_prim T_key []
  | signature => dummy_prim T_signature []
  | unit => dummy_prim T_unit []
  | chain_id => dummy_prim T_chain_id []
  end.

Fixpoint michelson2micheline_ctype (ct: comparable_type) : loc_micheline :=
  match ct with
  | Comparable_type_simple sct => michelson2micheline_sctype sct
  | Cpair cta ctb =>
    dummy_prim T_pair
               [michelson2micheline_ctype cta; michelson2micheline_ctype ctb]
  | Coption cta =>
    dummy_prim T_option [michelson2micheline_ctype cta]
  | Cor t1 t2 =>
    dummy_prim T_or [michelson2micheline_ctype t1;
                     michelson2micheline_ctype t2]
  end.

Definition add_annot (s : annot_o) (m : micheline) :=
  match s with
  | None => m
  | Some s =>
    match m with
    | PRIM prim annots l =>
      PRIM prim (dummy_annot s :: annots) l
    | m => m
    end
  end.

Definition add_annot_loc (s : annot_o) (m : loc_micheline) :=
  let 'Mk_loc_micheline (b, e, m) := m in
  Mk_loc_micheline (b, e, add_annot s m).

Fixpoint michelson2micheline_type (t : type) : loc_micheline :=
  match t with
  | Comparable_type ct => michelson2micheline_sctype ct
  | operation => dummy_prim T_operation []
  | option t' => dummy_prim T_option [michelson2micheline_type t']
  | list t' => dummy_prim T_list [michelson2micheline_type t']
  | set ct => dummy_prim T_set [michelson2micheline_ctype ct]
  | contract t' => dummy_prim T_contract [michelson2micheline_type t']
  | pair t1 t2 =>
    dummy_prim T_pair [michelson2micheline_type t1; michelson2micheline_type t2]
  | or t1 t2 =>
    dummy_prim T_or [michelson2micheline_type t1; michelson2micheline_type t2]
  | lambda t1 t2 =>
    dummy_prim T_lambda [michelson2micheline_type t1; michelson2micheline_type t2]
  | map ct1 t2 =>
    dummy_prim T_map [michelson2micheline_ctype ct1; michelson2micheline_type t2]
  | big_map ct1 t2 =>
    dummy_prim T_big_map [michelson2micheline_ctype ct1; michelson2micheline_type t2]
  | ticket ct => dummy_prim T_ticket [michelson2micheline_ctype ct]
  | sapling_state ms =>
    dummy_prim T_sapling_state [dummy_mich (NUMBER _ (BinInt.Z.of_nat ms))]
  | sapling_transaction ms =>
    dummy_prim T_sapling_transaction [dummy_mich (NUMBER _ (BinInt.Z.of_nat ms))]
  | bls12_381_fr => dummy_prim T_bls12_381_fr []
  | bls12_381_g1 => dummy_prim T_bls12_381_g1 []
  | bls12_381_g2 => dummy_prim T_bls12_381_g2 []
  end.

Fixpoint michelson2micheline_ep (t : entrypoints.entrypoint_tree) : loc_micheline :=
  match t with
  | entrypoints.ep_node t1 n1 t2 n2 =>
    let a := add_annot_loc n1 (michelson2micheline_ep t1) in
    let b := add_annot_loc n2 (michelson2micheline_ep t2) in
    dummy_prim T_or [a; b]
  | entrypoints.ep_leaf a =>
    michelson2micheline_type a
  end.

Definition michelson2micheline_opcode (o : opcode) : loc_micheline :=
  match o with
  | APPLY => dummy_prim I_APPLY []
  | DUP => dummy_prim I_DUP []
  | SWAP => dummy_prim I_SWAP []
  | UNIT => dummy_prim I_UNIT []
  | EQ => dummy_prim I_EQ []
  | NEQ => dummy_prim I_NEQ []
  | LT => dummy_prim I_LT []
  | GT => dummy_prim I_GT []
  | LE => dummy_prim I_LE []
  | GE => dummy_prim I_GE []
  | OR => dummy_prim I_OR []
  | AND => dummy_prim I_AND []
  | XOR => dummy_prim I_XOR []
  | NOT => dummy_prim I_NOT []
  | NEG => dummy_prim I_NEG []
  | ABS => dummy_prim I_ABS []
  | INT => dummy_prim I_INT []
  | ISNAT => dummy_prim I_ISNAT []
  | ADD => dummy_prim I_ADD []
  | SUB => dummy_prim I_SUB []
  | MUL => dummy_prim I_MUL []
  | EDIV => dummy_prim I_EDIV []
  | LSL => dummy_prim I_LSL []
  | LSR => dummy_prim I_LSR []
  | COMPARE => dummy_prim I_COMPARE []
  | CONCAT => dummy_prim I_CONCAT []
  | SIZE => dummy_prim I_SIZE []
  | SLICE => dummy_prim I_SLICE []
  | PAIR => dummy_prim I_PAIR []
  | CAR => dummy_prim I_CAR []
  | CDR => dummy_prim I_CDR []
  | UNPAIR => dummy_prim I_UNPAIR []
  | GET => dummy_prim I_GET []
  | GET_AND_UPDATE => dummy_prim I_GET_AND_UPDATE []
  | SOME => dummy_prim I_SOME []
  | NONE t => dummy_prim I_NONE [michelson2micheline_type t]
  | LEFT t => dummy_prim I_LEFT [michelson2micheline_type t]
  | RIGHT t => dummy_prim I_RIGHT [michelson2micheline_type t]
  | CONS => dummy_prim I_CONS nil
  | NIL t => dummy_prim I_NIL [michelson2micheline_type t]
  | EMPTY_SET ct => dummy_prim I_EMPTY_SET [michelson2micheline_ctype ct]
  | EMPTY_MAP ct t => dummy_prim I_EMPTY_MAP
                                 [michelson2micheline_ctype ct;
                                    michelson2micheline_type t]
  | EMPTY_BIG_MAP ct t => dummy_prim I_EMPTY_BIG_MAP
                                     [michelson2micheline_ctype ct;
                                        michelson2micheline_type t]
  | MEM => dummy_prim I_MEM []
  | UPDATE => dummy_prim I_UPDATE []
  | TRANSFER_TOKENS => dummy_prim I_TRANSFER_TOKENS []
  | SET_DELEGATE => dummy_prim I_SET_DELEGATE []
  | BALANCE => dummy_prim I_BALANCE []
  | ADDRESS => dummy_prim I_ADDRESS []
  | CONTRACT an t =>
    add_annot_loc an (dummy_prim I_CONTRACT [michelson2micheline_type t])
  | SOURCE => dummy_prim I_SOURCE []
  | SENDER => dummy_prim I_SENDER []
  | AMOUNT => dummy_prim I_AMOUNT []
  | IMPLICIT_ACCOUNT => dummy_prim I_IMPLICIT_ACCOUNT []
  | NOW => dummy_prim I_NOW []
  | PACK => dummy_prim I_PACK []
  | UNPACK t => dummy_prim I_UNPACK [michelson2micheline_type t]
  | HASH_KEY => dummy_prim I_HASH_KEY []
  | BLAKE2B => dummy_prim I_BLAKE2B []
  | SHA256 => dummy_prim I_SHA256 []
  | SHA512 => dummy_prim I_SHA512 []
  | KECCAK => dummy_prim I_KECCAK []
  | SHA3 => dummy_prim I_SHA3 []
  | CHECK_SIGNATURE => dummy_prim I_CHECK_SIGNATURE []
  | DIG n => dummy_prim I_DIG [dummy_mich (NUMBER _ (BinInt.Z.of_nat n))]
  | DUG n => dummy_prim I_DUG [dummy_mich (NUMBER _ (BinInt.Z.of_nat n))]
  | DROP n => dummy_prim I_DROP [dummy_mich (NUMBER _ (BinInt.Z.of_nat n))]
  | DUPN n => dummy_prim I_DUP [dummy_mich (NUMBER _ (BinInt.Z.of_nat n))]
  | CHAIN_ID => dummy_prim I_CHAIN_ID []
  | SELF_ADDRESS => dummy_prim I_SELF_ADDRESS []
  | LEVEL => dummy_prim I_LEVEL []
  | VOTING_POWER => dummy_prim I_VOTING_POWER []
  | TOTAL_VOTING_POWER => dummy_prim I_TOTAL_VOTING_POWER []
  | PAIRN n => dummy_prim I_PAIR [dummy_mich (NUMBER _ (BinInt.Z.of_nat n))]
  | UNPAIRN n => dummy_prim I_UNPAIR [dummy_mich (NUMBER _ (BinInt.Z.of_nat n))]
  | GETN n => dummy_prim I_GET [dummy_mich (NUMBER _ (BinInt.Z.of_nat n))]
  | UPDATEN n => dummy_prim I_UPDATE [dummy_mich (NUMBER _ (BinInt.Z.of_nat n))]
  | TICKET => dummy_prim I_TICKET []
  | READ_TICKET => dummy_prim I_READ_TICKET []
  | SPLIT_TICKET => dummy_prim I_SPLIT_TICKET []
  | JOIN_TICKETS => dummy_prim I_JOIN_TICKETS []
  | SAPLING_EMPTY_STATE ms =>
    dummy_prim I_SAPLING_EMPTY_STATE [dummy_mich (NUMBER _ (BinInt.Z.of_nat ms))]
  | SAPLING_VERIFY_UPDATE =>
    dummy_prim I_SAPLING_VERIFY_UPDATE []
  | PAIRING_CHECK => dummy_prim I_PAIRING_CHECK []
  end.

Fixpoint michelson2micheline_instruction (i : instruction) : loc_micheline :=
  match i with
  | Instruction_seq i =>
    dummy_mich (SEQ (michelson2micheline_ins_seq i))
  | FAILWITH => dummy_prim I_FAILWITH []
  | NEVER => dummy_prim I_NEVER []
  | CREATE_CONTRACT t1 t2 an i => dummy_prim I_CREATE_CONTRACT
                                             [michelson2micheline_type t1;
                                             add_annot_loc
                                               an
                                               (michelson2micheline_ep t2);
                                             dummy_mich (SEQ (michelson2micheline_ins_seq i))]
  | IF_ f i1 i2 =>
    let s := match f with
             | IF_bool => I_IF
             | IF_or => I_IF_LEFT
             | IF_option => I_IF_NONE
             | IF_list => I_IF_CONS
             end in
    dummy_prim s [dummy_mich (SEQ (michelson2micheline_ins_seq i1));
                    dummy_mich (SEQ (michelson2micheline_ins_seq i2))]
  | LOOP_ f i =>
    let s := match f with LOOP_bool => I_LOOP | LOOP_or => I_LOOP_LEFT end in
    dummy_prim s [dummy_mich (SEQ (michelson2micheline_ins_seq i))]
  | ITER i =>
    dummy_prim I_ITER [dummy_mich (SEQ (michelson2micheline_ins_seq i))]
  | MAP i =>
    dummy_prim I_MAP [dummy_mich (SEQ (michelson2micheline_ins_seq i))]
  | PUSH t d =>
    let t' := (michelson2micheline_type t) in
    dummy_prim I_PUSH [t'; michelson2micheline_data d]
  | LAMBDA t1 t2 i =>
    dummy_prim I_LAMBDA [ michelson2micheline_type t1;
                            michelson2micheline_type t2;
                            dummy_mich (SEQ (michelson2micheline_ins_seq i))]
  | DIP n i => dummy_prim I_DIP [dummy_mich (NUMBER _ (BinInt.Z.of_nat n));
                                   dummy_mich (SEQ (michelson2micheline_ins_seq i))]
  | SELF an => add_annot_loc an (dummy_prim I_SELF [])
  | EXEC => dummy_prim I_EXEC []
  | instruction_opcode o =>
    michelson2micheline_opcode o
  end
with
michelson2micheline_ins_seq (i : instruction_seq) : Datatypes.list loc_micheline :=
  match i with
  | NOOP => []
  | untyped_syntax.SEQ i1 i2 =>
    michelson2micheline_instruction i1 :: michelson2micheline_ins_seq i2
  end
with michelson2micheline_data (d : concrete_data) : loc_micheline :=
  match d with
  | Int_constant z => dummy_mich (NUMBER _ z)
  | String_constant s => dummy_mich (STR _ s)
  | Bytes_constant b => dummy_mich (BYTES _ b)
  | Unit => dummy_prim D_Unit []
  | True_ => dummy_prim D_True []
  | False_ => dummy_prim D_False []
  | Pair a b =>
    dummy_prim D_Pair [michelson2micheline_data a; michelson2micheline_data b]
  | Left a => dummy_prim D_Left [michelson2micheline_data a]
  | Right a => dummy_prim D_Right [michelson2micheline_data a]
  | Some_ a => dummy_prim D_Some [michelson2micheline_data a]
  | None_ => dummy_prim D_None []
  | Elt a b =>
    dummy_prim D_Elt [michelson2micheline_data a; michelson2micheline_data b]
  | Concrete_seq s => dummy_mich (SEQ (List.map michelson2micheline_data s))
  | Instruction i => michelson2micheline_instruction i
  end.

Lemma success_extract_one_field_annot_ctype (c : comparable_type) :
  base.Is_true (success (micheline2michelson.extract_one_field_annot
             (michelson2micheline_ctype c))).
Proof.
  induction c; try constructor.
  destruct s; constructor.
Qed.

Lemma michelson2micheline2michelson_ctype (c : comparable_type):
  micheline2michelson.micheline2michelson_ctype
    (michelson2micheline_ctype c) = Return c.
Proof.
  induction c; simpl.
  - destruct s; reflexivity.
  - rewrite IHc1.
    rewrite IHc2.
    reflexivity.
  - rewrite IHc.
    reflexivity.
  - rewrite IHc1.
    rewrite IHc2.
    simpl.
    destruct (success_eq_return_rev _ _ (success_extract_one_field_annot_ctype c1))
      as (annot1, H1); rewrite H1.
    destruct (success_eq_return_rev _ _ (success_extract_one_field_annot_ctype c2))
      as (annot2, H2); rewrite H2.
    reflexivity.
Qed.

Lemma michelson2micheline2michelson_type (t : type):
  micheline2michelson.micheline2michelson_type
    (michelson2micheline_type t) = Return t.
Proof.
  induction t; simpl;
    try reflexivity;
    try (destruct s; reflexivity);
    try (rewrite IHt; reflexivity);
    try (rewrite michelson2micheline2michelson_ctype; reflexivity);
    try (rewrite IHt1; rewrite IHt2; reflexivity);
    try (rewrite michelson2micheline2michelson_ctype; rewrite IHt; reflexivity);
    try (rewrite Znat.Nat2Z.id; reflexivity).
Qed.

Lemma michelson2micheline2michelson_opcode (o : opcode) :
  micheline2michelson.micheline2michelson_instruction
    (michelson2micheline_instruction o)
  = Return (untyped_syntax.instruction_opcode o).
Proof.
  destruct o; simpl;
    try reflexivity;
    try (rewrite michelson2micheline2michelson_ctype; reflexivity);
    try (rewrite michelson2micheline2michelson_type; reflexivity);
    try (rewrite michelson2micheline2michelson_ctype;
         rewrite michelson2micheline2michelson_type;
         reflexivity);
    try (rewrite Znat.Nat2Z.id; reflexivity).
  - (* CONTRACT *)
    destruct a as [[s H]|]; simpl; rewrite michelson2micheline2michelson_type.
    + rewrite (assume_some _ H).
      simpl.
      reflexivity.
    + reflexivity.
Qed.
