Require Import Coq.Strings.String ZArith.
Require micheline_parser.
Require Import location.

(** Tokens *)
Inductive token :=
| LBRACE
| RBRACE
| SEMICOLON
| LPAREN
| RPAREN
| PRIM : string -> token
| STR : string -> token
| NUMBER : Z -> token
| BYTES : string -> token
| ANNOTATION : string -> token
| EOF.

Definition parser_token := micheline_parser.Aut.GramDefs.token.

Definition token_to_parser (t : (location * location * token)) : parser_token :=
  let '((b, e), t) := t in
  match t with
  | LBRACE => micheline_parser.LBRACE (b, e)
  | RBRACE => micheline_parser.RBRACE (b, e)
  | SEMICOLON => micheline_parser.SEMICOLON (b, e)
  | LPAREN => micheline_parser.LPAREN (b, e)
  | RPAREN => micheline_parser.RPAREN (b, e)
  | PRIM s => micheline_parser.PRIMt (b, e, s)
  | STR s => micheline_parser.STRt (b, e, s)
  | NUMBER n => micheline_parser.NUMBERt (b, e, n)
  | BYTES s => micheline_parser.BYTESt (b, e, s)
  | ANNOTATION s => micheline_parser.ANNOTATIONt (b, e, s)
  | EOF => micheline_parser.EOF (b, e)
  end.
