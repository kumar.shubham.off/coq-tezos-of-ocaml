Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Fees_storage.
Require TezosOfOCaml.Proto_alpha.Constants_storage.

Require TezosOfOCaml.Proto_alpha.Proofs.Receipt_repr.

(* The mli file says <size> in bytes, so I'm not sure if I have
 * to convert the `size` to `size in bytes` here *)
Module Global_storage_constant_space.
  Module Valid.
    Definition t (size : Z.t) (arg : Raw_context.t * Z.t) := 
      let '(_, cost) := arg in
      cost = size +Z 65.
  End Valid.
End Global_storage_constant_space.

Module Storage_limit.
  Module Valid.
    Definition t x ctxt :=
      0 <= x <= Constants_storage.hard_storage_limit_per_operation ctxt.
  End Valid.
End Storage_limit.

(* @TODO *)
Axiom record_global_constant_storage_space_is_valid
  : forall {ctxt : Raw_context.t} {size : Z.t},
  Global_storage_constant_space.Valid.t size
    (Fees_storage.record_global_constant_storage_space ctxt size).

(* @TODO *)
Axiom record_paid_storage_space_is_valid
  : forall {ctxt : Raw_context.t} {contract},
  let result := Fees_storage.record_paid_storage_space ctxt contract in
  match result with
  | Pervasives.Ok (_, consumed, unpaid) => False (* @TODO define a Valid.t for this *)
  | Pervasives.Error _ => True
  end.

(* @TODO *)
Axiom check_storage_limit : forall {ctxt} {limit},
  Storage_limit.Valid.t limit ctxt ->
  Fees_storage.check_storage_limit ctxt limit = return? tt.

(* @TODO *)
Axiom burn_storage_fees_is_valid
  : forall {ctxt} {limit} {consumed} {source},
  let result := Fees_storage.burn_storage_fees None ctxt limit source consumed in
  match result with
  | Pervasives.Ok (_, limit', updates) =>
    limit' = limit - consumed /\
    Forall (fun '(_, balance_update, _) =>
      Receipt_repr.Balance_update.Valid.t balance_update)
      updates
  | Pervasives.Error _ => True
  end.
