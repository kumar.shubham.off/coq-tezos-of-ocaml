Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Voting_period_storage.

Require TezosOfOCaml.Proto_alpha.Proofs.Voting_period_repr.

(* @TODO *)
Axiom get_current_is_valid : forall {ctxt},
  match Voting_period_storage.get_current ctxt  with
  | Pervasives.Ok v =>
    Voting_period_repr.Valid.t v
  | Pervasives.Error _ => True
  end.
