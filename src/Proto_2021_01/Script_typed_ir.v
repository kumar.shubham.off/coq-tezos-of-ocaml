Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Require TezosOfOCaml.Proto_2021_01.Alpha_context.
Require TezosOfOCaml.Proto_2021_01.Lazy_storage_kind.

Inductive field_annot : Set :=
| Field_annot : string -> field_annot.

Definition address : Set := Alpha_context.Contract.t * string.

Definition pair (a b : Set) : Set := a * b.

Inductive union (a b : Set) : Set :=
| L : a -> union a b
| R : b -> union a b.

Arguments L {_ _}.
Arguments R {_ _}.

Inductive never : Set :=.

Inductive comparable_ty : Set :=
| Unit_key : comparable_ty
| Never_key : comparable_ty
| Int_key : comparable_ty
| Nat_key : comparable_ty
| Signature_key : comparable_ty
| String_key : comparable_ty
| Bytes_key : comparable_ty
| Mutez_key : comparable_ty
| Bool_key : comparable_ty
| Key_hash_key : comparable_ty
| Key_key : comparable_ty
| Timestamp_key : comparable_ty
| Chain_id_key : comparable_ty
| Address_key : comparable_ty
| Pair_key : comparable_ty -> comparable_ty -> comparable_ty
| Union_key : comparable_ty -> comparable_ty -> comparable_ty
| Option_key : comparable_ty -> comparable_ty.

Module Boxed_set.
  Record signature {elt OPS_t : Set} : Set := {
    elt := elt;
    elt_ty : comparable_ty;
    OPS : S.SET (elt := elt) (t := OPS_t);
    boxed : OPS.(S.SET.t);
    size : int;
  }.
End Boxed_set.
Definition Boxed_set := @Boxed_set.signature.
Arguments Boxed_set {_ _}.

Definition set (elt : Set) : Set :=
  {OPS_t : Set @ Boxed_set (elt := elt) (OPS_t := OPS_t)}.

Module Boxed_map.
  Record signature {key value : Set} {OPS_t : Set -> Set} : Set := {
    key := key;
    value := value;
    key_ty : comparable_ty;
    OPS : S.MAP (key := key) (t := OPS_t);
    boxed : OPS.(S.MAP.t) value * int;
  }.
End Boxed_map.
Definition Boxed_map := @Boxed_map.signature.
Arguments Boxed_map {_ _ _}.

Definition map (key value : Set) : Set :=
  {OPS_t : Set -> Set @
    Boxed_map (key := key) (value := value) (OPS_t := OPS_t)}.

Definition operation : Set :=
  Alpha_context.packed_internal_operation *
    option Alpha_context.Lazy_storage.diffs.

Module ticket.
  Record record {a : Set} : Set := Build {
    ticketer : address;
    contents : a;
    amount : Alpha_context.Script_int.num }.
  Arguments record : clear implicits.
  Definition with_ticketer {t_a} ticketer (r : record t_a) :=
    Build t_a ticketer r.(contents) r.(amount).
  Definition with_contents {t_a} contents (r : record t_a) :=
    Build t_a r.(ticketer) contents r.(amount).
  Definition with_amount {t_a} amount (r : record t_a) :=
    Build t_a r.(ticketer) r.(contents) amount.
End ticket.
Definition ticket := ticket.record.

(** Records for the constructor parameters *)
Module ConstructorRecords_nested_entrypoints.
  Module nested_entrypoints.
    Module Entrypoints_Union.
      Record record {_left _right : Set} : Set := Build {
        _left : _left;
        _right : _right }.
      Arguments record : clear implicits.
      Definition with__left {t__left t__right} _left
        (r : record t__left t__right) :=
        Build t__left t__right _left r.(_right).
      Definition with__right {t__left t__right} _right
        (r : record t__left t__right) :=
        Build t__left t__right r.(_left) _right.
    End Entrypoints_Union.
    Definition Entrypoints_Union_skeleton := Entrypoints_Union.record.
  End nested_entrypoints.
End ConstructorRecords_nested_entrypoints.
Import ConstructorRecords_nested_entrypoints.

Module entrypoints.
  Record record {name nested : Set} : Set := Build {
    name : name;
    nested : nested }.
  Arguments record : clear implicits.
  Definition with_name {t_name t_nested} name (r : record t_name t_nested) :=
    Build t_name t_nested name r.(nested).
  Definition with_nested {t_name t_nested} nested
    (r : record t_name t_nested) :=
    Build t_name t_nested r.(name) nested.
End entrypoints.
Definition entrypoints_skeleton := entrypoints.record.

Reserved Notation "'nested_entrypoints.Entrypoints_Union".
Reserved Notation "'entrypoints".

Inductive nested_entrypoints : Set :=
| Entrypoints_Union :
  'nested_entrypoints.Entrypoints_Union -> nested_entrypoints
| Entrypoints_None : nested_entrypoints

where "'entrypoints" :=
  (entrypoints_skeleton (option field_annot) nested_entrypoints)
and "'nested_entrypoints.Entrypoints_Union" :=
  (nested_entrypoints.Entrypoints_Union_skeleton 'entrypoints 'entrypoints).

Module nested_entrypoints.
  Include ConstructorRecords_nested_entrypoints.nested_entrypoints.
  Definition Entrypoints_Union := 'nested_entrypoints.Entrypoints_Union.
End nested_entrypoints.

Definition entrypoints := 'entrypoints.

(** Records for the constructor parameters *)
Module ConstructorRecords_instr.
  Module instr.
    Module Sapling_empty_state.
      Record record {memo_size : Set} : Set := Build {
        memo_size : memo_size }.
      Arguments record : clear implicits.
      Definition with_memo_size {t_memo_size} memo_size
        (r : record t_memo_size) :=
        Build t_memo_size memo_size.
    End Sapling_empty_state.
    Definition Sapling_empty_state_skeleton := Sapling_empty_state.record.
  End instr.
End ConstructorRecords_instr.
Import ConstructorRecords_instr.

Module descr.
  Record record {loc bef aft instr : Set} : Set := Build {
    loc : loc;
    bef : bef;
    aft : aft;
    instr : instr }.
  Arguments record : clear implicits.
  Definition with_loc {t_loc t_bef t_aft t_instr} loc
    (r : record t_loc t_bef t_aft t_instr) :=
    Build t_loc t_bef t_aft t_instr loc r.(bef) r.(aft) r.(instr).
  Definition with_bef {t_loc t_bef t_aft t_instr} bef
    (r : record t_loc t_bef t_aft t_instr) :=
    Build t_loc t_bef t_aft t_instr r.(loc) bef r.(aft) r.(instr).
  Definition with_aft {t_loc t_bef t_aft t_instr} aft
    (r : record t_loc t_bef t_aft t_instr) :=
    Build t_loc t_bef t_aft t_instr r.(loc) r.(bef) aft r.(instr).
  Definition with_instr {t_loc t_bef t_aft t_instr} instr
    (r : record t_loc t_bef t_aft t_instr) :=
    Build t_loc t_bef t_aft t_instr r.(loc) r.(bef) r.(aft) instr.
End descr.
Definition descr_skeleton := descr.record.

Module boxed_list.
  Record record {elements length : Set} : Set := Build {
    elements : elements;
    length : length }.
  Arguments record : clear implicits.
  Definition with_elements {t_elements t_length} elements
    (r : record t_elements t_length) :=
    Build t_elements t_length elements r.(length).
  Definition with_length {t_elements t_length} length
    (r : record t_elements t_length) :=
    Build t_elements t_length r.(elements) length.
End boxed_list.
Definition boxed_list_skeleton := boxed_list.record.

Module big_map.
  Record record {id diff key_type value_type : Set} : Set := Build {
    id : id;
    diff : diff;
    key_type : key_type;
    value_type : value_type }.
  Arguments record : clear implicits.
  Definition with_id {t_id t_diff t_key_type t_value_type} id
    (r : record t_id t_diff t_key_type t_value_type) :=
    Build t_id t_diff t_key_type t_value_type id r.(diff) r.(key_type)
      r.(value_type).
  Definition with_diff {t_id t_diff t_key_type t_value_type} diff
    (r : record t_id t_diff t_key_type t_value_type) :=
    Build t_id t_diff t_key_type t_value_type r.(id) diff r.(key_type)
      r.(value_type).
  Definition with_key_type {t_id t_diff t_key_type t_value_type} key_type
    (r : record t_id t_diff t_key_type t_value_type) :=
    Build t_id t_diff t_key_type t_value_type r.(id) r.(diff) key_type
      r.(value_type).
  Definition with_value_type {t_id t_diff t_key_type t_value_type} value_type
    (r : record t_id t_diff t_key_type t_value_type) :=
    Build t_id t_diff t_key_type t_value_type r.(id) r.(diff) r.(key_type)
      value_type.
End big_map.
Definition big_map_skeleton := big_map.record.

Module script.
  Record record {code arg_type storage storage_type entrypoints : Set} : Set := Build {
    code : code;
    arg_type : arg_type;
    storage : storage;
    storage_type : storage_type;
    entrypoints : entrypoints }.
  Arguments record : clear implicits.
  Definition with_code
    {t_code t_arg_type t_storage t_storage_type t_entrypoints} code
    (r : record t_code t_arg_type t_storage t_storage_type t_entrypoints) :=
    Build t_code t_arg_type t_storage t_storage_type t_entrypoints code
      r.(arg_type) r.(storage) r.(storage_type) r.(entrypoints).
  Definition with_arg_type
    {t_code t_arg_type t_storage t_storage_type t_entrypoints} arg_type
    (r : record t_code t_arg_type t_storage t_storage_type t_entrypoints) :=
    Build t_code t_arg_type t_storage t_storage_type t_entrypoints r.(code)
      arg_type r.(storage) r.(storage_type) r.(entrypoints).
  Definition with_storage
    {t_code t_arg_type t_storage t_storage_type t_entrypoints} storage
    (r : record t_code t_arg_type t_storage t_storage_type t_entrypoints) :=
    Build t_code t_arg_type t_storage t_storage_type t_entrypoints r.(code)
      r.(arg_type) storage r.(storage_type) r.(entrypoints).
  Definition with_storage_type
    {t_code t_arg_type t_storage t_storage_type t_entrypoints} storage_type
    (r : record t_code t_arg_type t_storage t_storage_type t_entrypoints) :=
    Build t_code t_arg_type t_storage t_storage_type t_entrypoints r.(code)
      r.(arg_type) r.(storage) storage_type r.(entrypoints).
  Definition with_entrypoints
    {t_code t_arg_type t_storage t_storage_type t_entrypoints} entrypoints
    (r : record t_code t_arg_type t_storage t_storage_type t_entrypoints) :=
    Build t_code t_arg_type t_storage t_storage_type t_entrypoints r.(code)
      r.(arg_type) r.(storage) r.(storage_type) entrypoints.
End script.
Definition script_skeleton := script.record.

Reserved Notation "'instr.Sapling_empty_state".
Reserved Notation "'script".
Reserved Notation "'end_of_stack".
Reserved Notation "'typed_contract".
Reserved Notation "'big_map".
Reserved Notation "'boxed_list".
Reserved Notation "'descr".

Inductive lambda : Set :=
| Lam : 'descr -> Alpha_context.Script.node -> lambda

with ty : Set :=
| Unit_t : ty
| Int_t : ty
| Nat_t : ty
| Signature_t : ty
| String_t : ty
| Bytes_t : ty
| Mutez_t : ty
| Key_hash_t : ty
| Key_t : ty
| Timestamp_t : ty
| Address_t : ty
| Bool_t : ty
| Pair_t : ty -> ty -> ty
| Union_t : ty -> ty -> ty
| Lambda_t : ty -> ty -> ty
| Option_t : ty -> ty
| List_t : ty -> ty
| Set_t : comparable_ty -> ty
| Map_t : comparable_ty -> ty -> ty
| Big_map_t : comparable_ty -> ty -> ty
| Contract_t : ty -> ty
| Sapling_transaction_t : Alpha_context.Sapling.Memo_size.t -> ty
| Sapling_state_t : Alpha_context.Sapling.Memo_size.t -> ty
| Operation_t : ty
| Chain_id_t : ty
| Never_t : ty
| Bls12_381_g1_t : ty
| Bls12_381_g2_t : ty
| Bls12_381_fr_t : ty
| Ticket_t : comparable_ty -> ty

with stack_ty : Set :=
| Item_t : ty -> stack_ty -> stack_ty
| Empty_t : stack_ty

with instr : Set :=
| Drop : instr
| Dup : instr
| Swap : instr
| Const : forall {ty : Set}, ty -> instr
| Cons_pair : instr
| Car : instr
| Cdr : instr
| Unpair : instr
| Cons_some : instr
| Cons_none : ty -> instr
| If_none : 'descr -> 'descr -> instr
| Cons_left : instr
| Cons_right : instr
| If_left : 'descr -> 'descr -> instr
| Cons_list : instr
| Nil : instr
| If_cons : 'descr -> 'descr -> instr
| List_map : 'descr -> instr
| List_iter : 'descr -> instr
| List_size : instr
| Empty_set : comparable_ty -> instr
| Set_iter : 'descr -> instr
| Set_mem : instr
| Set_update : instr
| Set_size : instr
| Empty_map : comparable_ty -> ty -> instr
| Map_map : 'descr -> instr
| Map_iter : 'descr -> instr
| Map_mem : instr
| Map_get : instr
| Map_update : instr
| Map_get_and_update : instr
| Map_size : instr
| Empty_big_map : comparable_ty -> ty -> instr
| Big_map_mem : instr
| Big_map_get : instr
| Big_map_update : instr
| Big_map_get_and_update : instr
| Concat_string : instr
| Concat_string_pair : instr
| Slice_string : instr
| String_size : instr
| Concat_bytes : instr
| Concat_bytes_pair : instr
| Slice_bytes : instr
| Bytes_size : instr
| Add_seconds_to_timestamp : instr
| Add_timestamp_to_seconds : instr
| Sub_timestamp_seconds : instr
| Diff_timestamps : instr
| Add_tez : instr
| Sub_tez : instr
| Mul_teznat : instr
| Mul_nattez : instr
| Ediv_teznat : instr
| Ediv_tez : instr
| Or : instr
| And : instr
| Xor : instr
| Not : instr
| Is_nat : instr
| Neg_nat : instr
| Neg_int : instr
| Abs_int : instr
| Int_nat : instr
| Add_intint : instr
| Add_intnat : instr
| Add_natint : instr
| Add_natnat : instr
| Sub_int : instr
| Mul_intint : instr
| Mul_intnat : instr
| Mul_natint : instr
| Mul_natnat : instr
| Ediv_intint : instr
| Ediv_intnat : instr
| Ediv_natint : instr
| Ediv_natnat : instr
| Lsl_nat : instr
| Lsr_nat : instr
| Or_nat : instr
| And_nat : instr
| And_int_nat : instr
| Xor_nat : instr
| Not_nat : instr
| Not_int : instr
| Seq : 'descr -> 'descr -> instr
| If : 'descr -> 'descr -> instr
| Loop : 'descr -> instr
| Loop_left : 'descr -> instr
| Dip : 'descr -> instr
| Exec : instr
| Apply : ty -> instr
| Lambda : lambda -> instr
| Failwith : ty -> instr
| Nop : instr
| Compare : comparable_ty -> instr
| Eq : instr
| Neq : instr
| Lt : instr
| Gt : instr
| Le : instr
| Ge : instr
| Address : instr
| Contract : ty -> string -> instr
| Transfer_tokens : instr
| Implicit_account : instr
| Create_contract : ty -> ty -> lambda -> entrypoints -> instr
| Set_delegate : instr
| Now : instr
| Balance : instr
| Level : instr
| Check_signature : instr
| Hash_key : instr
| Pack : ty -> instr
| Unpack : ty -> instr
| Blake2b : instr
| Sha256 : instr
| Sha512 : instr
| Source : instr
| Sender : instr
| Self : ty -> string -> instr
| Self_address : instr
| Amount : instr
| Sapling_empty_state : 'instr.Sapling_empty_state -> instr
| Sapling_verify_update : instr
| Dig : int -> stack_prefix_preservation_witness -> instr
| Dug : int -> stack_prefix_preservation_witness -> instr
| Dipn : int -> stack_prefix_preservation_witness -> 'descr -> instr
| Dropn : int -> stack_prefix_preservation_witness -> instr
| ChainId : instr
| Never : instr
| Voting_power : instr
| Total_voting_power : instr
| Keccak : instr
| Sha3 : instr
| Add_bls12_381_g1 : instr
| Add_bls12_381_g2 : instr
| Add_bls12_381_fr : instr
| Mul_bls12_381_g1 : instr
| Mul_bls12_381_g2 : instr
| Mul_bls12_381_fr : instr
| Mul_bls12_381_z_fr : instr
| Mul_bls12_381_fr_z : instr
| Int_bls12_381_fr : instr
| Neg_bls12_381_g1 : instr
| Neg_bls12_381_g2 : instr
| Neg_bls12_381_fr : instr
| Pairing_check_bls12_381 : instr
| Comb : int -> comb_gadt_witness -> instr
| Uncomb : int -> uncomb_gadt_witness -> instr
| Comb_get : int -> comb_get_gadt_witness -> instr
| Comb_set : int -> comb_set_gadt_witness -> instr
| Dup_n : int -> dup_n_gadt_witness -> instr
| Ticket : instr
| Read_ticket : instr
| Split_ticket : instr
| Join_tickets : comparable_ty -> instr

with comb_gadt_witness : Set :=
| Comb_one : comb_gadt_witness
| Comb_succ : comb_gadt_witness -> comb_gadt_witness

with uncomb_gadt_witness : Set :=
| Uncomb_one : uncomb_gadt_witness
| Uncomb_succ : uncomb_gadt_witness -> uncomb_gadt_witness

with comb_get_gadt_witness : Set :=
| Comb_get_zero : comb_get_gadt_witness
| Comb_get_one : comb_get_gadt_witness
| Comb_get_plus_two : comb_get_gadt_witness -> comb_get_gadt_witness

with comb_set_gadt_witness : Set :=
| Comb_set_zero : comb_set_gadt_witness
| Comb_set_one : comb_set_gadt_witness
| Comb_set_plus_two : comb_set_gadt_witness -> comb_set_gadt_witness

with dup_n_gadt_witness : Set :=
| Dup_n_zero : dup_n_gadt_witness
| Dup_n_succ : dup_n_gadt_witness -> dup_n_gadt_witness

with stack_prefix_preservation_witness : Set :=
| Prefix :
  stack_prefix_preservation_witness -> stack_prefix_preservation_witness
| Rest : stack_prefix_preservation_witness

where "'script" := (fun (t_storage : Set) =>
  script_skeleton lambda ty t_storage ty entrypoints)
and "'end_of_stack" := (unit)
and "'typed_contract" := (ty * address)
and "'big_map" := (fun (t_key t_value : Set) =>
  big_map_skeleton
    (option Alpha_context.Big_map.Id.(Lazy_storage_kind.IdWithTemp.t))
    (map t_key (option t_value)) comparable_ty ty)
and "'boxed_list" := (fun (t_elt : Set) => boxed_list_skeleton (list t_elt) int)
and "'descr" :=
  (descr_skeleton Alpha_context.Script.location stack_ty stack_ty instr)
and "'instr.Sapling_empty_state" :=
  (instr.Sapling_empty_state_skeleton Alpha_context.Sapling.Memo_size.t).

Module instr.
  Include ConstructorRecords_instr.instr.
  Definition Sapling_empty_state := 'instr.Sapling_empty_state.
End instr.

Definition script := 'script.
Definition end_of_stack := 'end_of_stack.
Definition typed_contract := 'typed_contract.
Definition big_map := 'big_map.
Definition boxed_list := 'boxed_list.
Definition descr := 'descr.
