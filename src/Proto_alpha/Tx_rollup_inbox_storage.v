(** File generated by coq-of-ocaml *)
Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Constants_storage.
Require TezosOfOCaml.Proto_alpha.Level_repr.
Require TezosOfOCaml.Proto_alpha.Raw_context.
Require TezosOfOCaml.Proto_alpha.Raw_level_repr.
Require TezosOfOCaml.Proto_alpha.Storage.
Require TezosOfOCaml.Proto_alpha.Storage_sigs.
Require TezosOfOCaml.Proto_alpha.Tx_rollup_errors_repr.
Require TezosOfOCaml.Proto_alpha.Tx_rollup_inbox_repr.
Require TezosOfOCaml.Proto_alpha.Tx_rollup_level_repr.
Require TezosOfOCaml.Proto_alpha.Tx_rollup_message_builder.
Require TezosOfOCaml.Proto_alpha.Tx_rollup_message_repr.
Require TezosOfOCaml.Proto_alpha.Tx_rollup_repr.
Require TezosOfOCaml.Proto_alpha.Tx_rollup_state_repr.
Require TezosOfOCaml.Proto_alpha.Tx_rollup_state_storage.

(** [prepare_metadata ctxt rollup state level] prepares the metadata
    for an inbox at [level], which may imply creating it if it does
    not already exist. *)
Definition prepare_metadata
  (ctxt : Raw_context.t) (rollup : Tx_rollup_repr.t)
  (state_value : Tx_rollup_state_repr.t) (level : Raw_level_repr.t)
  : M?
    (Raw_context.t * Tx_rollup_state_repr.t * Tx_rollup_level_repr.t *
      Tx_rollup_inbox_repr.metadata) :=
  let? '_ :=
    Error_monad.fail_when
      ((Constants_storage.tx_rollup_max_unfinalized_levels ctxt) <=i
      (Tx_rollup_state_repr.inboxes_count state_value))
      (Build_extensible "Too_many_inboxes" unit tt) in
  match
    ((Tx_rollup_state_repr.head_level state_value),
      match Tx_rollup_state_repr.head_level state_value with
      | Some (_, tezos_lvl) => Raw_level_repr.op_lt level tezos_lvl
      | _ => false
      end,
      match Tx_rollup_state_repr.head_level state_value with
      | Some (tx_lvl, tezos_lvl) => Raw_level_repr.op_eq tezos_lvl level
      | _ => false
      end) with
  | (Some (_, tezos_lvl), true, _) =>
    Error_monad.fail
      (Build_extensible "Internal_error" string
        "Trying to write into an inbox from the past")
  | (Some (tx_lvl, tezos_lvl), _, true) =>
    let? '(ctxt, metadata) :=
      Storage.Tx_rollup.Inbox_metadata.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.get)
        (ctxt, tx_lvl) rollup in
    return? (ctxt, state_value, tx_lvl, metadata)
  | (_, _, _) =>
    let? '(state_value, tx_level) :=
      Tx_rollup_state_repr.record_inbox_creation state_value level in
    let metadata := Tx_rollup_inbox_repr.empty_metadata in
    let? '(ctxt, _) :=
      Storage.Tx_rollup.Inbox_metadata.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.init_value)
        (ctxt, tx_level) rollup metadata in
    let ctxt := Raw_context.set_tx_rollup_has_messages ctxt rollup in
    return? (ctxt, state_value, tx_level, metadata)
  end.

(** [update_metadata metadata ~msg_hash ~msg_size] updates [metadata] to account
    for a new message of [msg_size] bytes. *)
Definition update_metadata
  (metadata : Tx_rollup_inbox_repr.metadata)
  (msg_hash : Tx_rollup_message_repr.hash) (msg_size : int)
  : M? Tx_rollup_inbox_repr.metadata :=
  let hash_value :=
    Tx_rollup_inbox_repr.extend_hash
      metadata.(Tx_rollup_inbox_repr.metadata.hash) msg_hash in
  return?
    {|
      Tx_rollup_inbox_repr.metadata.inbox_length :=
        Int32.succ metadata.(Tx_rollup_inbox_repr.metadata.inbox_length);
      Tx_rollup_inbox_repr.metadata.cumulated_size :=
        msg_size +i metadata.(Tx_rollup_inbox_repr.metadata.cumulated_size);
      Tx_rollup_inbox_repr.metadata.hash := hash_value |}.

Definition append_message
  (ctxt : Raw_context.t) (rollup : Tx_rollup_repr.t)
  (state_value : Tx_rollup_state_repr.t) (message : Tx_rollup_message_repr.t)
  : M? (Raw_context.t * Tx_rollup_state_repr.t) :=
  let level := (Raw_context.current_level ctxt).(Level_repr.t.level) in
  let msg_size := Tx_rollup_message_repr.size_value message in
  let? '(ctxt, new_state, tx_level, metadata) :=
    prepare_metadata ctxt rollup state_value level in
  let? '_ :=
    Error_monad.fail_when
      ((Int32.to_int metadata.(Tx_rollup_inbox_repr.metadata.inbox_length)) >=i
      (Constants_storage.tx_rollup_max_messages_per_inbox ctxt))
      (Build_extensible "Inbox_count_would_exceed_limit" Tx_rollup_repr.t rollup)
    in
  let? '(ctxt, msg_hash) := Tx_rollup_message_builder.hash_value ctxt message in
  let? new_metadata := update_metadata metadata msg_hash msg_size in
  let new_size := new_metadata.(Tx_rollup_inbox_repr.metadata.cumulated_size) in
  let inbox_limit := Constants_storage.tx_rollup_hard_size_limit_per_inbox ctxt
    in
  let? '_ :=
    Error_monad.fail_unless (new_size <=i inbox_limit)
      (Build_extensible "Inbox_size_would_exceed_limit" Tx_rollup_repr.t rollup)
    in
  let? '(ctxt, _, _) :=
    Storage.Tx_rollup.Inbox_metadata.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.add)
      (ctxt, tx_level) rollup new_metadata in
  let? '(ctxt, _, _) :=
    Storage.Tx_rollup.Inbox_contents.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.add)
      ((ctxt, tx_level), rollup)
      metadata.(Tx_rollup_inbox_repr.metadata.inbox_length) msg_hash in
  return? (ctxt, new_state).

Definition message_hashes_opt
  (ctxt : Raw_context.t) (level : Tx_rollup_level_repr.t)
  (tx_rollup : Tx_rollup_repr.t)
  : M? (Raw_context.t * option (list Tx_rollup_message_repr.hash)) :=
  let? function_parameter :=
    Storage.Tx_rollup.Inbox_contents.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.list_values)
      None None ((ctxt, level), tx_rollup) in
  match function_parameter with
  | (ctxt, []) =>
    let? ctxt := Tx_rollup_state_storage.assert_exist ctxt tx_rollup in
    return? (ctxt, None)
  | (ctxt, contents) => return? (ctxt, (Some contents))
  end.

Definition message_hashes
  (ctxt : Raw_context.t) (level : Tx_rollup_level_repr.t)
  (tx_rollup : Tx_rollup_repr.t)
  : M? (Raw_context.t * list Tx_rollup_message_repr.hash) :=
  let? function_parameter := message_hashes_opt ctxt level tx_rollup in
  match function_parameter with
  | (ctxt, Some messages) => return? (ctxt, messages)
  | (_, None) =>
    Error_monad.fail
      (Build_extensible "Inbox_does_not_exist"
        (Tx_rollup_repr.t * Tx_rollup_level_repr.t) (tx_rollup, level))
  end.

Definition size_value
  (ctxt : Raw_context.t) (level : Tx_rollup_level_repr.t)
  (tx_rollup : Tx_rollup_repr.t) : M? (Raw_context.t * int) :=
  let? function_parameter :=
    Storage.Tx_rollup.Inbox_metadata.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.find)
      (ctxt, level) tx_rollup in
  match function_parameter with
  |
    (ctxt,
      Some {| Tx_rollup_inbox_repr.metadata.cumulated_size := cumulated_size |})
    => return? (ctxt, cumulated_size)
  | (ctxt, None) =>
    let? _ctxt := Tx_rollup_state_storage.assert_exist ctxt tx_rollup in
    Error_monad.fail
      (Build_extensible "Inbox_does_not_exist"
        (Tx_rollup_repr.t * Tx_rollup_level_repr.t) (tx_rollup, level))
  end.

Definition find
  (ctxt : Raw_context.t) (level : Tx_rollup_level_repr.t)
  (tx_rollup : Tx_rollup_repr.t)
  : M? (Raw_context.t * option Tx_rollup_inbox_repr.t) :=
  let? function_parameter := message_hashes_opt ctxt level tx_rollup in
  match function_parameter with
  | (ctxt, Some contents) =>
    let? '(ctxt, cumulated_size) := size_value ctxt level tx_rollup in
    let hash_value := Tx_rollup_inbox_repr.hash_hashed_inbox contents in
    return?
      (ctxt,
        (Some
          {| Tx_rollup_inbox_repr.t.contents := contents;
            Tx_rollup_inbox_repr.t.cumulated_size := cumulated_size;
            Tx_rollup_inbox_repr.t.hash := hash_value |}))
  | (ctxt, None) => return? (ctxt, None)
  end.

Definition get
  (ctxt : Raw_context.t) (level : Tx_rollup_level_repr.t)
  (tx_rollup : Tx_rollup_repr.t)
  : M? (Raw_context.t * Tx_rollup_inbox_repr.t) :=
  let? function_parameter := find ctxt level tx_rollup in
  match function_parameter with
  | (ctxt, Some res) => return? (ctxt, res)
  | (_, None) =>
    Error_monad.fail
      (Build_extensible "Inbox_does_not_exist"
        (Tx_rollup_repr.t * Tx_rollup_level_repr.t) (tx_rollup, level))
  end.

Definition get_metadata
  (ctxt : Raw_context.t) (level : Tx_rollup_level_repr.t)
  (tx_rollup : Tx_rollup_repr.t)
  : M? (Raw_context.t * Tx_rollup_inbox_repr.metadata) :=
  let? function_parameter :=
    Storage.Tx_rollup.Inbox_metadata.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.find)
      (ctxt, level) tx_rollup in
  match function_parameter with
  | (_, None) =>
    Error_monad.fail
      (Build_extensible "Inbox_does_not_exist"
        (Tx_rollup_repr.t * Tx_rollup_level_repr.t) (tx_rollup, level))
  | (ctxt, Some metadata) => return? (ctxt, metadata)
  end.

#[bypass_check(guard)]
Definition remove
  (ctxt : Raw_context.t) (level : Tx_rollup_level_repr.t)
  (rollup : Tx_rollup_repr.t) : M? Raw_context.t :=
  let fix remove_messages (ctxt : Raw_context.t) (i_value : int32) (len : int32)
    {struct i_value} : M? Raw_context.t :=
    if i_value <i32 len then
      let? '(ctxt, _, _) :=
        Storage.Tx_rollup.Inbox_contents.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.remove)
          ((ctxt, level), rollup) i_value in
      remove_messages ctxt (Int32.succ i_value) len
    else
      return? ctxt in
  let? '(ctxt, metadata) := get_metadata ctxt level rollup in
  let? '(ctxt, _, _) :=
    Storage.Tx_rollup.Inbox_metadata.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.remove)
      (ctxt, level) rollup in
  remove_messages ctxt 0 metadata.(Tx_rollup_inbox_repr.metadata.inbox_length).

Definition check_message_hash
  (ctxt : Raw_context.t) (level : Tx_rollup_level_repr.t)
  (tx_rollup : Tx_rollup_repr.t) (position : int)
  (message : Tx_rollup_message_repr.t) : M? Raw_context.t :=
  let? '(ctxt, messages) :=
    Storage.Tx_rollup.Inbox_contents.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage_with_values.list_values)
      None None ((ctxt, level), tx_rollup) in
  let? expected_hash :=
    (fun x_1 =>
      Option.value_e x_1
        (Error_monad.trace_of_error
          (Build_extensible "Wrong_message_position"
            Tx_rollup_errors_repr.Wrong_message_position
            {| Tx_rollup_errors_repr.Wrong_message_position.level := level;
              Tx_rollup_errors_repr.Wrong_message_position.position := position;
              Tx_rollup_errors_repr.Wrong_message_position.length :=
                List.length messages |}))) (List.nth_opt messages position) in
  let? '(ctxt, actual_hash) := Tx_rollup_message_builder.hash_value ctxt message
    in
  let? '_ :=
    Error_monad.fail_unless
      (Tx_rollup_message_repr.hash_equal actual_hash expected_hash)
      (Build_extensible "Wrong_message_hash" unit tt) in
  return? ctxt.
