Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Contract_storage.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.List.
Require TezosOfOCaml.Proto_alpha.Proofs.Script_expr_hash.
Require TezosOfOCaml.Proto_alpha.Proofs.Script_repr.

Module Legacy_big_map_diff.
  Lemma item_encoding_is_valid
    : Data_encoding.Valid.t (fun _ => True) Contract_storage.Legacy_big_map_diff.item_encoding.
    Data_encoding.Valid.data_encoding_auto.
    intros []; simpl; try tauto.
    destruct u, diff_value; tauto.
  Qed.
  #[global] Hint Resolve item_encoding_is_valid : Data_encoding_db.

  Lemma encoding_is_valid
    : Data_encoding.Valid.t (fun _ => True) Contract_storage.Legacy_big_map_diff.encoding.
    Data_encoding.Valid.data_encoding_auto.
    now intros; apply List.Forall_True.
  Qed.
  #[global] Hint Resolve encoding_is_valid : Data_encoding_db.
End Legacy_big_map_diff.

(* @TODO *)
(* Comment in the code 
  (** [must_exist ctxt contract] fails with the [Non_existing_contract] error if
      [exists ctxt contract] returns [false]. Even though this function is
      gas-free, it is always called in a context where some gas consumption is
      guaranteed whenever necessary. The first context is that of a transfer
      operation, and in that case the base cost of a manager operation
      ([Micheclson_v1_gas.Cost_of.manager_operation]) is consumed. The second
      context is that of an activation operation, and in that case no gas needs to
      be consumed since that operation is not a manager operation. *)
 *)
Axiom exists_implies_must_exists_success : forall {ctxt} {contract},
  Contract_storage._exists ctxt contract = return? true ->
  Contract_storage.must_exist ctxt contract = return? tt.

(* @TODO *)
Axiom allocated_implies_must_be_allocated_success : forall {ctxt} {contract},
  Contract_storage.allocated ctxt contract = return? true ->
  Contract_storage.must_be_allocated ctxt contract = return? tt.
