Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Tx_rollup_commitment_repr.

Require TezosOfOCaml.Proto_alpha.Proofs.Raw_level_repr.

Module Valid.
  Definition t (x : Tx_rollup_level_repr.t) : Prop :=
    Raw_level_repr.Valid.t x.
End Valid.
