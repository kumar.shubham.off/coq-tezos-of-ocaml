Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.
Unset Guard Checking.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Require TezosOfOCaml.Proto_2021_01.Gas_limit_repr.
Require TezosOfOCaml.Proto_2021_01.Michelson_v1_primitives.
Require TezosOfOCaml.Proto_2021_01.Misc.

Definition location : Set := Micheline.canonical_location.

Definition location_encoding
  : Data_encoding.encoding Micheline.canonical_location :=
  Micheline.canonical_location_encoding.

Definition annot : Set := Micheline.annot.

Definition expr : Set := Micheline.canonical Michelson_v1_primitives.prim.

Definition lazy_expr : Set := Data_encoding.lazy_t expr.

Definition node : Set := Micheline.node location Michelson_v1_primitives.prim.

Definition expr_encoding
  : Data_encoding.encoding (Micheline.canonical Michelson_v1_primitives.prim) :=
  Micheline.canonical_encoding_v1 "michelson_v1"
    Michelson_v1_primitives.prim_encoding.

(** Init function; without side-effects in Coq *)
Definition init_module_repr : unit :=
  Error_monad.register_error_kind Error_monad.Permanent "invalid_binary_format"
    "Invalid binary format"
    "Could not deserialize some piece of data from its binary representation"
    None Data_encoding.empty
    (fun (function_parameter : Error_monad._error) =>
      match function_parameter with
      | Build_extensible tag _ payload =>
        if String.eqb tag "Lazy_script_decode" then
          Some tt
        else None
      end)
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      Build_extensible "Lazy_script_decode" unit tt).

Definition lazy_expr_encoding
  : Data_encoding.encoding
    (Data_encoding.lazy_t (Micheline.canonical Michelson_v1_primitives.prim)) :=
  Data_encoding.lazy_encoding expr_encoding.

Definition lazy_expr_value
  (expr : Micheline.canonical Michelson_v1_primitives.prim)
  : Data_encoding.lazy_t (Micheline.canonical Michelson_v1_primitives.prim) :=
  Data_encoding.make_lazy expr_encoding expr.

Module t.
  Record record : Set := Build {
    code : lazy_expr;
    storage : lazy_expr }.
  Definition with_code code (r : record) :=
    Build code r.(storage).
  Definition with_storage storage (r : record) :=
    Build r.(code) storage.
End t.
Definition t := t.record.

Definition encoding : Data_encoding.encoding t :=
  (let arg := Data_encoding.def "scripted.contracts" in
  fun (eta : Data_encoding.encoding t) => arg None None eta)
    (Data_encoding.conv
      (fun (function_parameter : t) =>
        let '{| t.code := code; t.storage := storage_value |} :=
          function_parameter in
        (code, storage_value))
      (fun (function_parameter : lazy_expr * lazy_expr) =>
        let '(code, storage_value) := function_parameter in
        {| t.code := code; t.storage := storage_value |}) None
      (Data_encoding.obj2
        (Data_encoding.req None None "code" lazy_expr_encoding)
        (Data_encoding.req None None "storage" lazy_expr_encoding))).

Definition int_node_size_of_numbits (n : int) : int * int :=
  (1, (1 +i ((n +i 63) /i 64))).

Definition int_node_size (n : Z.t) : int * int :=
  int_node_size_of_numbits (Z.numbits n).

Definition string_node_size_of_length (s : int) : int * int :=
  (1, (1 +i ((s +i 7) /i 8))).

Definition string_node_size (s : string) : int * int :=
  string_node_size_of_length (String.length s).

Definition bytes_node_size_of_length (s : int) : int * int :=
  (2, ((1 +i ((s +i 7) /i 8)) +i 12)).

Definition bytes_node_size (s : bytes) : int * int :=
  bytes_node_size_of_length (Bytes.length s).

Definition prim_node_size_nonrec_of_lengths
  (n_args : int) (annots : list string) : int * int :=
  let annots_length :=
    List.fold_left
      (fun (acc_value : int) =>
        fun (s : string) => acc_value +i (String.length s)) 0 annots in
  if annots_length =i 0 then
    ((1 +i n_args), (2 +i (2 *i n_args)))
  else
    ((2 +i n_args), ((4 +i (2 *i n_args)) +i ((annots_length +i 7) /i 8))).

Definition prim_node_size_nonrec {A : Set}
  (args : list A) (annots : list string) : int * int :=
  let n_args := List.length args in
  prim_node_size_nonrec_of_lengths n_args annots.

Definition seq_node_size_nonrec_of_length (n_args : int) : int * int :=
  ((1 +i n_args), (2 +i (2 *i n_args))).

Definition seq_node_size_nonrec {A : Set} (args : list A) : int * int :=
  let n_args := List.length args in
  seq_node_size_nonrec_of_length n_args.

Definition convert_pair (function_parameter : int * int) : Z.t * Z.t :=
  let '(i1, i2) := function_parameter in
  ((Z.of_int i1), (Z.of_int i2)).

Fixpoint node_size {A B : Set} (node : Micheline.node A B) {struct node}
  : Z.t * Z.t :=
  match node with
  | Micheline.Int _ n => convert_pair (int_node_size n)
  | Micheline.String _ s => convert_pair (string_node_size s)
  | Micheline.Bytes _ s => convert_pair (bytes_node_size s)
  | Micheline.Prim _ _ args annot =>
    List.fold_left
      (fun (function_parameter : Z.t * Z.t) =>
        let '(blocks, words) := function_parameter in
        fun (node : Micheline.node A B) =>
          let '(nblocks, nwords) := node_size node in
          ((blocks +Z nblocks), (words +Z nwords)))
      (convert_pair (prim_node_size_nonrec args annot)) args
  | Micheline.Seq _ args =>
    List.fold_left
      (fun (function_parameter : Z.t * Z.t) =>
        let '(blocks, words) := function_parameter in
        fun (node : Micheline.node A B) =>
          let '(nblocks, nwords) := node_size node in
          ((blocks +Z nblocks), (words +Z nwords)))
      (convert_pair (seq_node_size_nonrec args)) args
  end.

Definition expr_size {A : Set} (expr : Micheline.canonical A) : Z.t * Z.t :=
  node_size (Micheline.root expr).

Definition traversal_cost {A B : Set} (node : Micheline.node A B)
  : Gas_limit_repr.cost :=
  let '(blocks, _words) := node_size node in
  Gas_limit_repr.step_cost blocks.

Definition cost_of_size (function_parameter : Z.t * Z.t)
  : Gas_limit_repr.cost :=
  let '(blocks, words) := function_parameter in
  Gas_limit_repr.op_plusat
    (Gas_limit_repr.op_plusat
      (Gas_limit_repr.op_starat
        (Compare.Z.(Compare.S.max) Z.zero (blocks -Z Z.one))
        (Gas_limit_repr.alloc_cost Z.zero)) (Gas_limit_repr.alloc_cost words))
    (Gas_limit_repr.step_cost blocks).

Definition cost_of_size_int (pair_value : int * int) : Gas_limit_repr.cost :=
  cost_of_size (convert_pair pair_value).

Definition int_node_cost (n : Z.t) : Gas_limit_repr.cost :=
  cost_of_size_int (int_node_size n).

Definition int_node_cost_of_numbits (n : int) : Gas_limit_repr.cost :=
  cost_of_size_int (int_node_size_of_numbits n).

Definition string_node_cost (s : string) : Gas_limit_repr.cost :=
  cost_of_size_int (string_node_size s).

Definition string_node_cost_of_length (s : int) : Gas_limit_repr.cost :=
  cost_of_size_int (string_node_size_of_length s).

Definition bytes_node_cost (s : bytes) : Gas_limit_repr.cost :=
  cost_of_size_int (bytes_node_size s).

Definition bytes_node_cost_of_length (s : int) : Gas_limit_repr.cost :=
  cost_of_size_int (bytes_node_size_of_length s).

Definition prim_node_cost_nonrec {A : Set} (args : list A) (annot : list string)
  : Gas_limit_repr.cost := cost_of_size_int (prim_node_size_nonrec args annot).

Definition seq_node_cost_nonrec {A : Set} (args : list A)
  : Gas_limit_repr.cost := cost_of_size_int (seq_node_size_nonrec args).

Definition seq_node_cost_nonrec_of_length (n_args : int)
  : Gas_limit_repr.cost :=
  cost_of_size_int (seq_node_size_nonrec_of_length n_args).

Definition deserialized_cost {A : Set} (expr : Micheline.canonical A)
  : Gas_limit_repr.cost := cost_of_size (expr_size expr).

Definition serialized_cost (bytes_value : bytes) : Gas_limit_repr.cost :=
  Gas_limit_repr.alloc_bytes_cost (Bytes.length bytes_value).

Definition force_decode {A : Set}
  (lexpr : Data_encoding.lazy_t (Micheline.canonical A))
  : M? (Micheline.canonical A * Gas_limit_repr.cost) :=
  let account_deserialization_cost :=
    Data_encoding.apply_lazy
      (fun (function_parameter : Micheline.canonical A) =>
        let '_ := function_parameter in
        false)
      (fun (function_parameter : bytes) =>
        let '_ := function_parameter in
        true)
      (fun (function_parameter : bool) =>
        let '_ := function_parameter in
        fun (function_parameter : bool) =>
          let '_ := function_parameter in
          false) lexpr in
  match Data_encoding.force_decode lexpr with
  | Some v =>
    if account_deserialization_cost then
      return? (v, (deserialized_cost v))
    else
      return? (v, Gas_limit_repr.free)
  | None =>
    Error_monad.error_value (Build_extensible "Lazy_script_decode" unit tt)
  end.

Definition force_bytes {A : Set}
  (expr : Data_encoding.lazy_t (Micheline.canonical A))
  : M? (bytes * Gas_limit_repr.cost) :=
  let account_serialization_cost :=
    Data_encoding.apply_lazy (fun (v : Micheline.canonical A) => Some v)
      (fun (function_parameter : bytes) =>
        let '_ := function_parameter in
        None)
      (fun (function_parameter : option (Micheline.canonical A)) =>
        let '_ := function_parameter in
        fun (function_parameter : option (Micheline.canonical A)) =>
          let '_ := function_parameter in
          None) expr in
  match
    Misc.result_of_exception
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Data_encoding.force_bytes expr) with
  | Pervasives.Ok bytes_value =>
    match account_serialization_cost with
    | Some v =>
      return?
        (bytes_value,
          (Gas_limit_repr.op_plusat (traversal_cost (Micheline.root v))
            (serialized_cost bytes_value)))
    | None => return? (bytes_value, Gas_limit_repr.free)
    end
  | Pervasives.Error _ =>
    Error_monad.error_value (Build_extensible "Lazy_script_decode" unit tt)
  end.

Definition minimal_deserialize_cost {A : Set} (lexpr : Data_encoding.lazy_t A)
  : Gas_limit_repr.cost :=
  Data_encoding.apply_lazy
    (fun (function_parameter : A) =>
      let '_ := function_parameter in
      Gas_limit_repr.free) (fun (b_value : bytes) => serialized_cost b_value)
    (fun (c_free : Gas_limit_repr.cost) =>
      fun (function_parameter : Gas_limit_repr.cost) =>
        let '_ := function_parameter in
        c_free) lexpr.

Definition unit_value : Micheline.canonical Michelson_v1_primitives.prim :=
  Micheline.strip_locations
    (Micheline.Prim 0 Michelson_v1_primitives.D_Unit nil nil).

Definition unit_parameter
  : Data_encoding.lazy_t (Micheline.canonical Michelson_v1_primitives.prim) :=
  lazy_expr_value unit_value.

Definition is_unit_parameter
  : Data_encoding.lazy_t (Micheline.canonical Michelson_v1_primitives.prim) ->
  bool :=
  let unit_bytes := Data_encoding.force_bytes unit_parameter in
  Data_encoding.apply_lazy
    (fun (v : Micheline.canonical Michelson_v1_primitives.prim) =>
      match Micheline.root v with
      | Micheline.Prim _ Michelson_v1_primitives.D_Unit [] [] => true
      | _ => false
      end)
    (fun (b_value : Compare.Bytes.(Compare.S.t)) =>
      Compare.Bytes.(Compare.S.equal) b_value unit_bytes)
    (fun (res : bool) =>
      fun (function_parameter : bool) =>
        let '_ := function_parameter in
        res).

Fixpoint strip_annotations {A B : Set} (node : Micheline.node A B) {struct node}
  : Micheline.node A B :=
  match node with
  | (Micheline.Int _ _ | Micheline.String _ _ | Micheline.Bytes _ _) as leaf =>
    leaf
  | Micheline.Prim loc name args _ =>
    Micheline.Prim loc name (List.map strip_annotations args) nil
  | Micheline.Seq loc args =>
    Micheline.Seq loc (List.map strip_annotations args)
  end.

Reserved Notation "'micheline_nodes_list".

Fixpoint micheline_nodes_aux {A B C : Set}
  (node : Micheline.node A B) (acc_value : int) (k : int -> C) : C :=
  let micheline_nodes_list {A B C} := 'micheline_nodes_list A B C in
  match node with
  | Micheline.Int _ _ => k (acc_value +i 1)
  | Micheline.String _ _ => k (acc_value +i 1)
  | Micheline.Bytes _ _ => k (acc_value +i 1)
  | Micheline.Prim _ _ subterms _ =>
    micheline_nodes_list subterms (acc_value +i 1) k
  | Micheline.Seq _ subterms => micheline_nodes_list subterms (acc_value +i 1) k
  end

where "'micheline_nodes_list" :=
  (fun (A B C : Set) => fix micheline_nodes_list
    (subterms : list (Micheline.node A B)) (acc_value : int) (k : int -> C)
    {struct subterms} : C :=
    match subterms with
    | [] => k acc_value
    | cons n nodes =>
      micheline_nodes_list nodes acc_value
        (fun (acc_value : int) => micheline_nodes_aux n acc_value k)
    end).

Definition micheline_nodes_list {A B C : Set} := 'micheline_nodes_list A B C.

Definition micheline_nodes {A B : Set} (node : Micheline.node A B) : int :=
  micheline_nodes_aux node 0 (fun (x : int) => x).

Definition cost_MICHELINE_STRIP_LOCATIONS (size : int) : Z.t :=
  (Z.of_int size) *Z (Z.of_int 100).

Definition strip_locations_cost {A B : Set} (node : Micheline.node A B)
  : Gas_limit_repr.cost :=
  let nodes := micheline_nodes node in
  Gas_limit_repr.atomic_step_cost (cost_MICHELINE_STRIP_LOCATIONS nodes).
