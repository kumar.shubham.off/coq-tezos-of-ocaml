Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Delegate_storage.

Require TezosOfOCaml.Proto_alpha.Proofs.Tez_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Receipt_repr.

Module participation_info.
  Module Valid.
    Import Proto_alpha.Delegate_storage.participation_info.

    Record t (x : Delegate_storage.participation_info) : Prop := {
      expected_cycle_activity :
        Pervasives.Int31.Valid.t x.(expected_cycle_activity);
      minimal_cycle_activity :
        Pervasives.Int31.Valid.t x.(minimal_cycle_activity);
      missed_slots :
        Pervasives.Int31.Valid.t x.(missed_slots);
      missed_levels :
        Pervasives.Int31.Valid.t x.(missed_levels);
      remaining_allowed_missed_slots :
        Pervasives.Int31.Valid.t x.(remaining_allowed_missed_slots);
      expected_endorsing_rewards :
        Tez_repr.Valid.t x.(expected_endorsing_rewards);
    }.
  End Valid.
End participation_info.

(* @TODO *)
Axiom frozen_deposits_limit_is_valid : forall {ctxt} {pkh},
  match Delegate_storage.frozen_deposits_limit ctxt pkh with
  | Pervasives.Ok (Some tez) => Tez_repr.Valid.t tez
  | _ => True
  end.

(* @TODO *)
Axiom balance_is_valid : forall {ctxt} {pkh},
  match Delegate_storage.balance ctxt pkh with
  | Pervasives.Ok tez => Tez_repr.Valid.t tez
  | Pervasives.Error _ => True
  end.

(* @TODO Verify *)
(* How to verify the gas burn in this function *)
Axiom record_baking_acitivity_and_pay_rewards_and_fees_is_valid
  : forall {ctxt} {payload_pkh block_pkh : _} {reward bonus : Tez_repr.t},
  let result :=
    Delegate_storage.record_baking_activity_and_pay_rewards_and_fees
      ctxt payload_pkh block_pkh reward (Some bonus) in
  match result with
  | Pervasives.Ok (_, updates) => Receipt_repr.Balance_updates.Valid.t updates
  | Pervasives.Error _ => True
  end.

(* @TODO *)
Axiom full_balance_is_valid
  : forall {ctxt} {pkh},
  match Delegate_storage.full_balance ctxt pkh with
  | Pervasives.Ok tez => Tez_repr.Valid.t tez
  | Pervasives.Error _ => True
  end.

(* @TODO *)
Axiom staking_balance_is_valid
  : forall {ctxt} {pkh},
  match Delegate_storage.staking_balance ctxt pkh with
  | Pervasives.Ok tez => Tez_repr.Valid.t tez
  | Pervasives.Error _ => True
  end.

(* @TODO *)
Axiom delegated_balance_is_valid
  : forall {ctxt} {pkh},
  match Delegate_storage.delegated_balance ctxt pkh with
  | Pervasives.Ok tez => Tez_repr.Valid.t tez
  | Pervasives.Error _ => True
  end.

(* @TODO *)
Axiom freeze_deposits_do_not_call_except_for_migration_is_valid
  : forall {ctxt} {new_cycle} {updates},
  Receipt_repr.Balance_updates.Valid.t updates ->
  let result := Delegate_storage.freeze_deposits_do_not_call_except_for_migration
    ctxt new_cycle updates in
  match result with
  | Pervasives.Ok (_, updates') => Receipt_repr.Balance_updates.Valid.t updates'
  | Pervasives.Error _ => True
  end.
