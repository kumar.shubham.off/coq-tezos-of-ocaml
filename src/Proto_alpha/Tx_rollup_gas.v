(** File generated by coq-of-ocaml *)
Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Gas_limit_repr.
Require TezosOfOCaml.Proto_alpha.Saturation_repr.

Module S := Saturation_repr.

(** The same model as in {!Michelson_v1_gas.N_IBlake2b}. *)
Definition message_hash_cost (msg_size : int) : M? Gas_limit_repr.cost :=
  if 0 <=i msg_size then
    let op_plus := S.add in
    let v0 := Saturation_repr.safe_int msg_size in
    let cost_N_IBlake2b :=
      op_plus (op_plus (S.safe_int 430) v0) (S.shift_right v0 3) in
    return? (Gas_limit_repr.atomic_step_cost cost_N_IBlake2b)
  else
    Error_monad.error_value
      (Build_extensible "Tx_rollup_negative_message_size" unit tt).

(** Init function; without side-effects in Coq *)
Definition init_module : unit :=
  Error_monad.register_error_kind Error_monad.Permanent
    "tx_rollup_negative_message_size"
    "The protocol has computed a negative size for an inbox messages"
    "The protocol has computed a negative size for an inbox messages. This is an internal error, and denotes a bug in the protocol implementation."
    None Data_encoding.unit_value
    (fun (function_parameter : Error_monad._error) =>
      match function_parameter with
      | Build_extensible tag _ payload =>
        if String.eqb tag "Tx_rollup_negative_message_size" then
          Some tt
        else None
      end)
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      Build_extensible "Tx_rollup_negative_message_size" unit tt).
