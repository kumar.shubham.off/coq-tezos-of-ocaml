Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Tx_rollup_l2_address.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.S.
Require Import TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Blake2B.
Require TezosOfOCaml.Proto_alpha.Proofs.Indexable.

Module Indexable.
  Lemma Indexable_Make_include_is_valid :
    Indexable.INDEXABLE.Valid.t
      Tx_rollup_l2_address.Indexable.Indexable_Make_include.
  Proof.
    apply Indexable.Make_is_valid;
    constructor; apply Blake2B.Make_is_valid.
  Qed.

  Lemma encoding_is_valid : Data_encoding.Valid.t Indexable.Valid.t 
    Tx_rollup_l2_address.Indexable.encoding.
  Proof.
    apply Indexable_Make_include_is_valid.
  Qed.
  #[global] Hint Resolve encoding_is_valid : Data_encoding_db.

  Lemma index_encoding_is_valid :
    Data_encoding.Valid.t Indexable.Index.Valid.t
      Tx_rollup_l2_address.Indexable.index_encoding.
  Proof.
    apply Indexable_Make_include_is_valid.
  Qed.
  #[global] Hint Resolve index_encoding_is_valid : Data_encoding_db.

  Lemma value_encoding_is_valid :
    Data_encoding.Valid.t Indexable.Value.Valid.t
      Tx_rollup_l2_address.Indexable.value_encoding.
  Proof.
    apply Indexable_Make_include_is_valid.
  Qed.
  #[global] Hint Resolve value_encoding_is_valid : Data_encoding_db.

  Lemma compare_is_valid :
    Compare.Valid.t id Tx_rollup_l2_address.Indexable.compare.
  Proof.
    apply Indexable_Make_include_is_valid.
  Qed.

  Lemma compare_values_is_valid :
    Compare.Valid.t id Tx_rollup_l2_address.Indexable.compare_values.
  Proof.
    apply Indexable_Make_include_is_valid.
  Qed.
End Indexable.
