Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.
Require Coq.Sorting.Sorted.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require Import TezosOfOCaml.Proto_alpha.Environment.Proofs.Pervasives.
Require Import TezosOfOCaml.Proto_alpha.Proofs.Utils.

Lemma length_destruct {a : Set} (x : a) (l : list a) :
  List.length (x :: l) = List.length l +i 1.
  Utils.tezos_z_autounfold.
  induction l; simpl; try Utils.tezos_z_auto.
Qed.

Lemma length_is_valid {a : Set} (l : list a) :
    Pervasives.Int.Valid.t (List.length l).
  Utils.tezos_z_autounfold.
  induction l; simpl; lia.
Qed.

Fixpoint length_app_sum {a : Set} (l1 l2 : list a)
  : List.length (l1 ++ l2) =
    List.length l1 +i List.length l2.
  assert (H_l1l2 := length_is_valid (l1 ++ l2)).
  destruct l1; simpl.
  { now rewrite Pervasives.normalize_identity. }
  { rewrite length_app_sum.
    Utils.tezos_z_auto.
  }
Qed.

Fixpoint mem_eq_dec {a : Set} {eq_dec : a -> a -> bool}
  (H_eq_dec : forall x y, eq_dec x y = true -> x = y) (v : a) (l : list a)
  : List.mem eq_dec v l = true ->
    List.In v l.
  destruct l; simpl; [congruence|].
  destruct (eq_dec _ _) eqn:H; simpl; [left | right].
  { symmetry.
    now apply H_eq_dec.
  }
  { now apply (mem_eq_dec _ eq_dec). }
Qed.

Definition in_list_string (s : string) (list : list string) : bool :=
  List.mem String.eqb s list.

Fixpoint all_distinct_strings (l : list string) : bool :=
  match l with
  | [] => true
  | h :: tl =>
    if in_list_string h tl then
      false
    else
      all_distinct_strings tl
  end.

Fixpoint find_map {a b : Set} (f : a -> option b) (l : list a) : option b :=
  match l with
  | [] => None
  | x :: l =>
    match f x with
    | None => find_map f l
    | Some y => Some y
    end
  end.

(** [List.Forall] is the trivial case where the property is always true. *)
Fixpoint Forall_True {a : Set} {P : a -> Prop} {l : list a}
  : (forall x, P x) -> List.Forall P l.
  intro; destruct l; constructor; trivial; now apply Forall_True.
Qed.
(* These lemmas are often useful, especially for data-encoding proofs. *)
#[global] Hint Resolve Forall_True Forall_impl : core.

Lemma nil_has_no_last : forall (a : Set), last_opt (nil : List.t a) = None.
  reflexivity.
Qed.

Lemma tz_rev_append_rev {a : Set} : forall (l l' : list a),
  CoqOfOCaml.List.rev_append l l' = (Lists.List.rev l ++ l')%list.
  intro l; induction l; simpl; auto; intros.
  rewrite <- app_assoc; firstorder.
Qed.

Lemma lst_lst : forall (a : Set) (l : list a) (x : a),
  last_opt (l ++ [x])%list = Some x.
  intros.
  induction l; auto.
  simpl; rewrite IHl.
  destruct l; reflexivity.
Qed.

Lemma head_is_last_of_rev : forall (a : Set) (l : list a) (x : a),
  last_opt (rev l) = hd l.
  unfold rev; unfold CoqOfOCaml.List.rev.
  intros.
  induction l; auto.
  simpl; rewrite tz_rev_append_rev; apply lst_lst.
Qed.

Lemma head_of_init : forall {a trace : Set} (f : int -> a) (x : trace),
  init_value x 1 f = Pervasives.Ok [f(0)].
  intros.
  unfold init_value.
  reflexivity.
Qed.

Lemma in_map : forall {a b : Set} (f : a -> b) (xs : list a) (x : a),
  In x xs -> In (f x) (List.map f xs).
  induction xs; intros x Hin; destruct Hin.
  - left; congruence.
  - right; apply IHxs; auto.
Qed.

Lemma fold_right_f_eq {a b : Set} (f g : a -> b -> b) (l : list a) (acc : b) :
  (forall x acc, f x acc = g x acc) ->
  fold_right f l acc = fold_right g l acc.
Proof.
  induction l; sfirstorder.
Qed.

Lemma fold_right_opt_map : forall {a b c : Set} (f : a -> option b)
  (g : b -> c -> c) (xs : list a) (z : c),
  List.fold_right
    (fun x acc =>
      match f x with
      | Some y => g y acc
      | None => acc
      end) xs z =
  List.fold_right g (List.filter_map f xs) z.
  induction xs; hauto lq: on.
Qed.

Lemma fold_right_opt_no_None : forall {a b c : Set} (f : a -> option b)
  (g : b -> c -> c) (xs : list a) (z z_t : c),
  (forall x, In x xs -> exists y, f x = Some y) ->
  List.fold_right
    (fun x acc =>
      match f x with
      | Some y => g y acc
      | None => z_t
      end) xs z =
  List.fold_right g (List.filter_map f xs) z.
  induction xs; qauto l: on.
Qed.

Lemma fold_right_cons_nil : forall {a : Set} (xs : list a),
  fold_right cons xs [] = xs.
  induction xs; intros.
  - reflexivity.
  - simpl; congruence.
Qed.

Lemma In_to_In_fold_add : forall {a b c : Set}
  (f : a -> option b) (g : b -> c) (add : b -> list c -> list c)
  (xs : list a) (x : a) (y : b),
  (forall x xs, In (g x) (add x xs)) ->
  (forall x y xs, In (g x) xs -> In (g x) (add y xs)) ->
  In x xs -> f x = Some y ->
    In (g y) (fold_right
      (fun x' acc =>
        match f x' with
        | Some y' => add y' acc
        | None => acc
        end) xs []).
  induction xs; intros x y add_In1 add_In2 Hxxs Hfx;
  destruct Hxxs; simpl.
  - rewrite H, Hfx.
    apply add_In1.
  - destruct (f a0); [apply add_In2|idtac]; 
    apply (IHxs x); assumption.
Qed.

Lemma In_fold_add_to_In : forall {a b c : Set}
  (f : a -> option b) (g : b -> c) (add : b -> list c -> list c),
  (forall x x' xs, In (g x) (add x' xs) -> x = x' \/ In (g x) xs) ->
  (forall x x' y, f x = Some y -> f x' = Some y -> x = x') ->
  forall (xs : list a) (x : a) (y : b),
  f x = Some y ->
  In (g y) (fold_right
    (fun x' acc =>
          match f x' with
          | Some y' => add y' acc
          | None => acc
          end) xs []) -> In x xs.
  induction xs; intros.
  - exact H2.
  - destruct (f a0) eqn:G.
    + simpl in H2.
      rewrite G in H2.
      destruct (H _ _ _ H2).
      * rewrite H3 in H1; left.
        eapply H0; eassumption.
      * right; eapply IHxs; eassumption.
    + right; eapply IHxs; auto.
      * exact H1.
      * simpl in H2.
        rewrite G in H2.
        exact H2.
Qed.

Lemma hd_Some_to_cons : forall {a : Set} (x : a) xs, hd xs = Some x ->
  exists ys, xs = x :: ys.
  intros X x [|y ys] Hxs.
  - discriminate.
  - exists ys; auto.
    inversion Hxs; reflexivity.
Qed.

Lemma rev_append_last : forall (a : Set) (l l' : list a),
    CoqOfOCaml.List.rev_append l l' =
      (CoqOfOCaml.List.rev_append l [] ++ l')%list.
Proof.
  intros a l l'; revert l';
  induction l as [|x l IHl]; intro l'; [reflexivity |];
  simpl; rewrite IHl, (IHl [x]), <- app_assoc; reflexivity.
Qed.

Lemma rev_head_app_eq : forall (a : Set) (acc l : list a) (x : a),
    (rev (x :: acc) ++ l)%list = (rev acc ++ x :: l)%list.
Proof.
  intros a acc l x;
  unfold rev in *; unfold CoqOfOCaml.List.rev in *;
  simpl;
  rewrite rev_append_last at 1;
  rewrite <- app_assoc; reflexivity.
Qed.

(** We create auxillary definition z_length and prove that 
our z_length equals to length.  

Why do we need z_length? With it we do not have to work with modular arithmetic,
we work with integers and it is much easier to prove something
on integers. 

Recomendation : when you face (length l) in the lemma to be proved,
just rewrite length with z_length by length_z_length_eq. *)

Fixpoint z_length {a : Set} (l : list a) {struct l} : Z.t :=
  match l with
  | [] => 0
  | _ :: l' => ((z_length l') + 1)%Z
  end.

Lemma Z_le_z_length {a : Set} (l : list a) : 0 <= z_length l.
Proof.
  induction l; simpl; lia.
Qed.

Lemma z_length_plus_one : forall {a : Set} (l : list a),
      ((z_length l + 1)%Z <=i 0) = false.
Proof.
  intros a l; assert (0 <= z_length l) by apply Z_le_z_length; lia.
Qed.

Lemma length_z_length_eq : forall (a : Set) (l : list a),
    z_length l <= Pervasives.max_int ->
    List.length l = z_length l.
Proof.
  intros a l H. 
  induction l; simpl; trivial;
  unfold op_plus;
  simpl in H.
  assert (G : z_length l <= Pervasives.max_int) by lia.
  apply IHl in G. rewrite G.
  rewrite Pervasives.normalize_identity. reflexivity.
  split. assert (G' : 0 <= z_length l) by apply Z_le_z_length.
  lia. trivial.
Qed.

Lemma z_length_plus_one_minus_one : forall {a : Set} (l : list a),
    z_length l <= Pervasives.max_int ->
    (z_length l + 1)%Z -i 1 = z_length l.
Proof.
  intros a l H;
  assert (G : 0 <= z_length l) by apply Z_le_z_length;
  unfold op_minus; rewrite Pervasives.normalize_identity; lia. 
Qed.

Module Sorted.
  (* @TODO *)
  Lemma filter_map {a b : Set}
    (R_a : a -> a -> Prop) (R_b : b -> b -> Prop)
    (f : a -> option b) (l : list a) :
    (forall x1 x2,
      match f x1, f x2 with
      | Some y1, Some y2 => R_a x1 x2 -> R_b y1 y2
      | _, _ => True
      end) ->
    Sorting.Sorted.Sorted R_a l ->
    Sorting.Sorted.Sorted R_b (List.filter_map f l).
  Proof.
  Admitted.
End Sorted.
