Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Proto_2021_01.Environment.Format.
Require Proto_2021_01.Environment.Pervasives.

Definition t (a : Set) : Set := option a.

Definition none {a : Set} : option a :=
  None.

Definition some {a : Set} (x : a) : option a :=
  Some x.

Definition value {a : Set} (x : option a) (v : a) : a :=
  match x with
  | None => v
  | Some v => v
  end.

Definition bind {a b : Set} (x : option a) (f : a -> option b) : option b :=
  match x with
  | None => None
  | Some v => f v
  end.

Definition join {a : Set} (x : option (option a)) : option a :=
  match x with
  | None | Some None => None
  | Some (Some v) => Some v
  end.

Definition map {a b : Set} (f : a -> b) (x : option a) : option b :=
  match x with
  | None => None
  | Some v => Some (f v)
  end.

Definition fold {a b : Set} (default : a) (f : b -> a) (x : option b) : a :=
  match x with
  | None => default
  | Some v => f v
  end.

Definition iter {a : Set} (f : a -> unit) (x : option a) : unit :=
  tt.

Definition is_none {a : Set} (x : option a) : bool :=
  match x with
  | None => true
  | Some _ => false
  end.

Definition is_some {a : Set} (x : option a) : bool :=
  match x with
  | None => false
  | Some _ => true
  end.

Definition equal {a : Set} (f : a -> a -> bool) (x1 x2 : option a) : bool :=
  match x1, x2 with
  | None, None => true
  | Some v1, Some v2 => f v1 v2
  | None, Some _ | Some _, None => false
  end.

Definition compare {a : Set} (f : a -> a -> int) (x1 x2 : option a) : int :=
  match x1, x2 with
  | None, None => 0
  | Some v1, Some v2 => f v1 v2
  | None, Some _ => -1
  | Some _, None => 1
  end.

Definition to_result {a e : Set} (error : e) (x : option a)
  : Pervasives.result a e :=
  match x with
  | None => Pervasives.Error error
  | Some v => Pervasives.Ok v
  end.

Definition to_list {a : Set} (x : option a) : list a :=
  match x with
  | None => []
  | Some v => [v]
  end.

Definition op_gtgteq : forall {a b : Set},
  option a -> (a -> option b) -> option b :=
  @bind.

Definition op_gtgtpipe {a b : Set} (x : option a) (f : a -> b) : option b :=
  match x with
  | None => None
  | Some v => Some (f v)
  end.

Definition first_some {a : Set} (x1 x2 : option a) : option a :=
  match x1 with
  | None => x2
  | Some _ => x1
  end.

Parameter pp : forall {a : Set},
  option string -> (Format.formatter -> a -> unit) -> Format.formatter ->
  option a -> unit.

Module Notations.
  Notation "M* X" := (option X) (at level 20).

  Notation "return* X" := (some X) (at level 20).

  Notation "'let*' x ':=' X 'in' Y" :=
    (bind X (fun x => Y))
    (at level 200, x name, X at level 100, Y at level 200).

  Notation "'let*' ' x ':=' X 'in' Y" :=
    (bind X (fun x => Y))
    (at level 200, x pattern, X at level 100, Y at level 200).
End Notations.
Import Notations.
