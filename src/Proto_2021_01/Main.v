Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Require TezosOfOCaml.Proto_2021_01.Alpha_context.
Require TezosOfOCaml.Proto_2021_01.Alpha_services.
Require TezosOfOCaml.Proto_2021_01.Apply.
Require TezosOfOCaml.Proto_2021_01.Apply_results.
Require TezosOfOCaml.Proto_2021_01.Nonce_hash.
Require TezosOfOCaml.Proto_2021_01.Script_ir_translator.
Require TezosOfOCaml.Proto_2021_01.Script_typed_ir.
Require TezosOfOCaml.Proto_2021_01.Services_registration.

Definition block_header_data : Set := Alpha_context.Block_header.protocol_data.

Definition block_header : Set := Alpha_context.Block_header.t.

Definition block_header_data_encoding
  : Data_encoding.encoding Alpha_context.Block_header.protocol_data :=
  Alpha_context.Block_header.protocol_data_encoding.

Definition block_header_metadata : Set := Apply_results.block_metadata.

Definition block_header_metadata_encoding
  : Data_encoding.encoding Apply_results.block_metadata :=
  Apply_results.block_metadata_encoding.

Definition operation_data : Set := Alpha_context.packed_protocol_data.

Definition operation_data_encoding
  : Data_encoding.t Alpha_context.Operation.packed_protocol_data :=
  Alpha_context.Operation.protocol_data_encoding.

Definition operation_receipt : Set := Apply_results.packed_operation_metadata.

Definition operation_receipt_encoding
  : Data_encoding.t Apply_results.packed_operation_metadata :=
  Apply_results.operation_metadata_encoding.

Definition operation_data_and_receipt_encoding
  : Data_encoding.t
    (Alpha_context.Operation.packed_protocol_data *
      Apply_results.packed_operation_metadata) :=
  Apply_results.operation_data_and_metadata_encoding.

Definition operation : Set := Alpha_context.packed_operation.

Definition acceptable_passes : Alpha_context.packed_operation -> list int :=
  Alpha_context.Operation.acceptable_passes.

Definition max_block_length : int :=
  Alpha_context.Block_header.max_header_length.

Definition max_operation_data_length : int :=
  Alpha_context.Constants.max_operation_data_length.

Definition validation_passes : list Updater.quota :=
  [
    {| Updater.quota.max_size := 32 *i 1024; Updater.quota.max_op := Some 32 |};
    {| Updater.quota.max_size := 32 *i 1024; Updater.quota.max_op := None |};
    {|
      Updater.quota.max_size :=
        Alpha_context.Constants.max_anon_ops_per_block *i 1024;
      Updater.quota.max_op :=
        Some Alpha_context.Constants.max_anon_ops_per_block |};
    {| Updater.quota.max_size := 512 *i 1024; Updater.quota.max_op := None |}
  ].

Definition rpc_services : RPC_directory.directory Updater.rpc_context :=
  let '_ := Alpha_services.register tt in
  Services_registration.get_rpc_services tt.

(** Records for the constructor parameters *)
Module ConstructorRecords_validation_mode.
  Module validation_mode.
    Module Application.
      Record record {block_header baker block_delay : Set} : Set := Build {
        block_header : block_header;
        baker : baker;
        block_delay : block_delay }.
      Arguments record : clear implicits.
      Definition with_block_header {t_block_header t_baker t_block_delay}
        block_header (r : record t_block_header t_baker t_block_delay) :=
        Build t_block_header t_baker t_block_delay block_header r.(baker)
          r.(block_delay).
      Definition with_baker {t_block_header t_baker t_block_delay} baker
        (r : record t_block_header t_baker t_block_delay) :=
        Build t_block_header t_baker t_block_delay r.(block_header) baker
          r.(block_delay).
      Definition with_block_delay {t_block_header t_baker t_block_delay}
        block_delay (r : record t_block_header t_baker t_block_delay) :=
        Build t_block_header t_baker t_block_delay r.(block_header) r.(baker)
          block_delay.
    End Application.
    Definition Application_skeleton := Application.record.
    
    Module Partial_application.
      Record record {block_header baker block_delay : Set} : Set := Build {
        block_header : block_header;
        baker : baker;
        block_delay : block_delay }.
      Arguments record : clear implicits.
      Definition with_block_header {t_block_header t_baker t_block_delay}
        block_header (r : record t_block_header t_baker t_block_delay) :=
        Build t_block_header t_baker t_block_delay block_header r.(baker)
          r.(block_delay).
      Definition with_baker {t_block_header t_baker t_block_delay} baker
        (r : record t_block_header t_baker t_block_delay) :=
        Build t_block_header t_baker t_block_delay r.(block_header) baker
          r.(block_delay).
      Definition with_block_delay {t_block_header t_baker t_block_delay}
        block_delay (r : record t_block_header t_baker t_block_delay) :=
        Build t_block_header t_baker t_block_delay r.(block_header) r.(baker)
          block_delay.
    End Partial_application.
    Definition Partial_application_skeleton := Partial_application.record.
    
    Module Partial_construction.
      Record record {predecessor : Set} : Set := Build {
        predecessor : predecessor }.
      Arguments record : clear implicits.
      Definition with_predecessor {t_predecessor} predecessor
        (r : record t_predecessor) :=
        Build t_predecessor predecessor.
    End Partial_construction.
    Definition Partial_construction_skeleton := Partial_construction.record.
    
    Module Full_construction.
      Record record {predecessor protocol_data baker block_delay : Set} : Set := Build {
        predecessor : predecessor;
        protocol_data : protocol_data;
        baker : baker;
        block_delay : block_delay }.
      Arguments record : clear implicits.
      Definition with_predecessor
        {t_predecessor t_protocol_data t_baker t_block_delay} predecessor
        (r : record t_predecessor t_protocol_data t_baker t_block_delay) :=
        Build t_predecessor t_protocol_data t_baker t_block_delay predecessor
          r.(protocol_data) r.(baker) r.(block_delay).
      Definition with_protocol_data
        {t_predecessor t_protocol_data t_baker t_block_delay} protocol_data
        (r : record t_predecessor t_protocol_data t_baker t_block_delay) :=
        Build t_predecessor t_protocol_data t_baker t_block_delay
          r.(predecessor) protocol_data r.(baker) r.(block_delay).
      Definition with_baker
        {t_predecessor t_protocol_data t_baker t_block_delay} baker
        (r : record t_predecessor t_protocol_data t_baker t_block_delay) :=
        Build t_predecessor t_protocol_data t_baker t_block_delay
          r.(predecessor) r.(protocol_data) baker r.(block_delay).
      Definition with_block_delay
        {t_predecessor t_protocol_data t_baker t_block_delay} block_delay
        (r : record t_predecessor t_protocol_data t_baker t_block_delay) :=
        Build t_predecessor t_protocol_data t_baker t_block_delay
          r.(predecessor) r.(protocol_data) r.(baker) block_delay.
    End Full_construction.
    Definition Full_construction_skeleton := Full_construction.record.
  End validation_mode.
End ConstructorRecords_validation_mode.
Import ConstructorRecords_validation_mode.

Reserved Notation "'validation_mode.Application".
Reserved Notation "'validation_mode.Partial_application".
Reserved Notation "'validation_mode.Partial_construction".
Reserved Notation "'validation_mode.Full_construction".

Inductive validation_mode : Set :=
| Application : 'validation_mode.Application -> validation_mode
| Partial_application : 'validation_mode.Partial_application -> validation_mode
| Partial_construction :
  'validation_mode.Partial_construction -> validation_mode
| Full_construction : 'validation_mode.Full_construction -> validation_mode

where "'validation_mode.Application" :=
  (validation_mode.Application_skeleton Alpha_context.Block_header.t
    Alpha_context.public_key_hash Alpha_context.Period.t)
and "'validation_mode.Partial_application" :=
  (validation_mode.Partial_application_skeleton Alpha_context.Block_header.t
    Alpha_context.public_key_hash Alpha_context.Period.t)
and "'validation_mode.Partial_construction" :=
  (validation_mode.Partial_construction_skeleton Block_hash.t)
and "'validation_mode.Full_construction" :=
  (validation_mode.Full_construction_skeleton Block_hash.t
    Alpha_context.Block_header.contents Alpha_context.public_key_hash
    Alpha_context.Period.t).

Module validation_mode.
  Include ConstructorRecords_validation_mode.validation_mode.
  Definition Application := 'validation_mode.Application.
  Definition Partial_application := 'validation_mode.Partial_application.
  Definition Partial_construction := 'validation_mode.Partial_construction.
  Definition Full_construction := 'validation_mode.Full_construction.
End validation_mode.

Module validation_state.
  Record record : Set := Build {
    mode : validation_mode;
    chain_id : Chain_id.t;
    ctxt : Alpha_context.t;
    op_count : int;
    migration_balance_updates : Alpha_context.Receipt.balance_updates }.
  Definition with_mode mode (r : record) :=
    Build mode r.(chain_id) r.(ctxt) r.(op_count) r.(migration_balance_updates).
  Definition with_chain_id chain_id (r : record) :=
    Build r.(mode) chain_id r.(ctxt) r.(op_count) r.(migration_balance_updates).
  Definition with_ctxt ctxt (r : record) :=
    Build r.(mode) r.(chain_id) ctxt r.(op_count) r.(migration_balance_updates).
  Definition with_op_count op_count (r : record) :=
    Build r.(mode) r.(chain_id) r.(ctxt) op_count r.(migration_balance_updates).
  Definition with_migration_balance_updates migration_balance_updates
    (r : record) :=
    Build r.(mode) r.(chain_id) r.(ctxt) r.(op_count) migration_balance_updates.
End validation_state.
Definition validation_state := validation_state.record.

Definition current_context {A : Set} (function_parameter : validation_state)
  : M= (Pervasives.result Context.t A) :=
  let '{| validation_state.ctxt := ctxt |} := function_parameter in
  Error_monad._return
    (Alpha_context.finalize None ctxt).(Updater.validation_result.context).

Definition begin_partial_application
  (chain_id : Chain_id.t) (ctxt : Context.t) (predecessor_timestamp : Time.t)
  (predecessor_fitness : Alpha_context.Fitness.t)
  (block_header : Alpha_context.Block_header.t) : M=? validation_state :=
  let level :=
    block_header.(Alpha_context.Block_header.t.shell).(Block_header.shell_header.level)
    in
  let fitness := predecessor_fitness in
  let timestamp :=
    block_header.(Alpha_context.Block_header.t.shell).(Block_header.shell_header.timestamp)
    in
  let=? '(ctxt, migration_balance_updates) :=
    Alpha_context.prepare ctxt level predecessor_timestamp timestamp fitness in
  let=? '(ctxt, baker, block_delay) :=
    Apply.begin_application ctxt chain_id block_header predecessor_timestamp in
  let mode :=
    Partial_application
      {| validation_mode.Partial_application.block_header := block_header;
        validation_mode.Partial_application.baker :=
          Signature.Public_key.(S.SIGNATURE_PUBLIC_KEY.hash_value) baker;
        validation_mode.Partial_application.block_delay := block_delay |} in
  return=?
    {| validation_state.mode := mode; validation_state.chain_id := chain_id;
      validation_state.ctxt := ctxt; validation_state.op_count := 0;
      validation_state.migration_balance_updates := migration_balance_updates |}.

Definition begin_application
  (chain_id : Chain_id.t) (ctxt : Context.t) (predecessor_timestamp : Time.t)
  (predecessor_fitness : Alpha_context.Fitness.t)
  (block_header : Alpha_context.Block_header.t) : M=? validation_state :=
  let level :=
    block_header.(Alpha_context.Block_header.t.shell).(Block_header.shell_header.level)
    in
  let fitness := predecessor_fitness in
  let timestamp :=
    block_header.(Alpha_context.Block_header.t.shell).(Block_header.shell_header.timestamp)
    in
  let=? '(ctxt, migration_balance_updates) :=
    Alpha_context.prepare ctxt level predecessor_timestamp timestamp fitness in
  let=? '(ctxt, baker, block_delay) :=
    Apply.begin_application ctxt chain_id block_header predecessor_timestamp in
  let mode :=
    Application
      {| validation_mode.Application.block_header := block_header;
        validation_mode.Application.baker :=
          Signature.Public_key.(S.SIGNATURE_PUBLIC_KEY.hash_value) baker;
        validation_mode.Application.block_delay := block_delay |} in
  return=?
    {| validation_state.mode := mode; validation_state.chain_id := chain_id;
      validation_state.ctxt := ctxt; validation_state.op_count := 0;
      validation_state.migration_balance_updates := migration_balance_updates |}.

Definition begin_construction
  (chain_id : Chain_id.t) (ctxt : Context.t) (predecessor_timestamp : Time.t)
  (pred_level : int32) (pred_fitness : Alpha_context.Fitness.t)
  (predecessor : Block_hash.t) (timestamp : Time.t)
  (protocol_data : option block_header_data) (function_parameter : unit)
  : M=? validation_state :=
  let '_ := function_parameter in
  let level := Int32.succ pred_level in
  let fitness := pred_fitness in
  let=? '(ctxt, migration_balance_updates) :=
    Alpha_context.prepare ctxt level predecessor_timestamp timestamp fitness in
  let=? '(mode, ctxt) :=
    match protocol_data with
    | None =>
      let=? ctxt := Apply.begin_partial_construction ctxt in
      let mode :=
        Partial_construction
          {| validation_mode.Partial_construction.predecessor := predecessor |}
        in
      return=? (mode, ctxt)
    | Some proto_header =>
      let=? '(ctxt, protocol_data, baker, block_delay) :=
        Apply.begin_full_construction ctxt predecessor_timestamp
          proto_header.(Alpha_context.Block_header.protocol_data.contents) in
      let mode :=
        let baker :=
          Signature.Public_key.(S.SIGNATURE_PUBLIC_KEY.hash_value) baker in
        Full_construction
          {| validation_mode.Full_construction.predecessor := predecessor;
            validation_mode.Full_construction.protocol_data := protocol_data;
            validation_mode.Full_construction.baker := baker;
            validation_mode.Full_construction.block_delay := block_delay |} in
      return=? (mode, ctxt)
    end in
  return=?
    {| validation_state.mode := mode; validation_state.chain_id := chain_id;
      validation_state.ctxt := ctxt; validation_state.op_count := 0;
      validation_state.migration_balance_updates := migration_balance_updates |}.

Definition apply_operation (function_parameter : validation_state)
  : Alpha_context.packed_operation -> M=? (validation_state * operation_receipt) :=
  let
    '{|
      validation_state.mode := mode;
        validation_state.chain_id := chain_id;
        validation_state.ctxt := ctxt;
        validation_state.op_count := op_count
        |} as data := function_parameter in
  fun (operation : Alpha_context.packed_operation) =>
    match
      (mode,
        match mode with
        | Partial_application _ =>
          Pervasives.not
            (List._exists (Compare.Int.(Compare.S.equal) 0)
              (Alpha_context.Operation.acceptable_passes operation))
        | _ => false
        end) with
    | (Partial_application _, true) =>
      let op_count := op_count +i 1 in
      Error_monad._return
        ((validation_state.with_op_count op_count
          (validation_state.with_ctxt ctxt data)),
          Apply_results.No_operation_metadata)
    | (_, _) =>
      let '{|
        Alpha_context.packed_operation.shell := shell;
          Alpha_context.packed_operation.protocol_data :=
            Alpha_context.Operation_data protocol_data
          |} := operation in
      let operation :=
        {| Alpha_context.operation.shell := shell;
          Alpha_context.operation.protocol_data := protocol_data |} in
      let '(predecessor, baker) :=
        match mode with
        |
          (Partial_application {|
            validation_mode.Partial_application.block_header := {|
              Alpha_context.Block_header.t.shell := {|
                Block_header.shell_header.predecessor := predecessor
                  |}
                |};
              validation_mode.Partial_application.baker := baker
              |} |
          Application {|
            validation_mode.Application.block_header := {|
              Alpha_context.Block_header.t.shell := {|
                Block_header.shell_header.predecessor := predecessor
                  |}
                |};
              validation_mode.Application.baker := baker
              |} |
          Full_construction {|
            validation_mode.Full_construction.predecessor := predecessor;
              validation_mode.Full_construction.baker := baker
              |}) => (predecessor, baker)
        |
          Partial_construction {|
            validation_mode.Partial_construction.predecessor := predecessor
              |} =>
          (predecessor,
            Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.zero))
        end in
      let=? '(ctxt, result_value) :=
        Apply.apply_operation ctxt chain_id Script_ir_translator.Optimized
          predecessor baker (Alpha_context.Operation.hash_value operation)
          operation in
      let op_count := op_count +i 1 in
      return=?
        ((validation_state.with_op_count op_count
          (validation_state.with_ctxt ctxt data)),
          (Apply_results.Operation_metadata result_value))
    end.

Definition finalize_block (function_parameter : validation_state)
  : M=? (Updater.validation_result * Apply_results.block_metadata) :=
  let '{|
    validation_state.mode := mode;
      validation_state.ctxt := ctxt;
      validation_state.op_count := op_count;
      validation_state.migration_balance_updates := migration_balance_updates
      |} := function_parameter in
  match mode with
  | Partial_construction _ =>
    let=? '{|
      Alpha_context.Voting_period.info.voting_period := {|
        Alpha_context.Voting_period.voting_period.kind := kind_value
          |}
        |} := Alpha_context.Voting_period.get_current_info ctxt in
    let=?
      '{|
        Alpha_context.Voting_period.info.voting_period := voting_period;
          Alpha_context.Voting_period.info.position := position
          |} as voting_period_info :=
      Alpha_context.Voting_period.get_rpc_fixed_current_info ctxt in
    let level_info := Alpha_context.Level.current ctxt in
    let baker := Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.zero) in
    let=? ctxt :=
      Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.Map).(S.INDEXES_MAP.fold)
        (fun (delegate : Signature.public_key_hash) =>
          fun (deposit : Alpha_context.Tez.t) =>
            fun (ctxt : M=? Alpha_context.context) =>
              let=? ctxt := ctxt in
              Alpha_context.Delegate.freeze_deposit ctxt delegate deposit)
        (Alpha_context.get_deposits ctxt) (Error_monad._return ctxt) in
    let ctxt := Alpha_context.finalize None ctxt in
    return=?
      (ctxt,
        {| Apply_results.block_metadata.baker := baker;
          Apply_results.block_metadata.level :=
            Alpha_context.Level.to_deprecated_type level_info
              voting_period.(Alpha_context.Voting_period.voting_period.index)
              position; Apply_results.block_metadata.level_info := level_info;
          Apply_results.block_metadata.voting_period_kind := kind_value;
          Apply_results.block_metadata.voting_period_info := voting_period_info;
          Apply_results.block_metadata.nonce_hash := None;
          Apply_results.block_metadata.consumed_gas :=
            Alpha_context.Gas.Arith.zero;
          Apply_results.block_metadata.deactivated := nil;
          Apply_results.block_metadata.balance_updates :=
            migration_balance_updates |})
  |
    Partial_application {|
      validation_mode.Partial_application.block_header := block_header;
        validation_mode.Partial_application.baker := baker;
        validation_mode.Partial_application.block_delay := block_delay
        |} =>
    let included_endorsements := Alpha_context.included_endorsements ctxt in
    let=? '_ :=
      return=
        (Apply.check_minimum_endorsements ctxt
          block_header.(Alpha_context.Block_header.t.protocol_data).(Alpha_context.Block_header.protocol_data.contents)
          block_delay included_endorsements) in
    let=? '{|
      Alpha_context.Voting_period.info.voting_period := {|
        Alpha_context.Voting_period.voting_period.kind := kind_value
          |}
        |} := Alpha_context.Voting_period.get_current_info ctxt in
    let=?
      '{|
        Alpha_context.Voting_period.info.voting_period := voting_period;
          Alpha_context.Voting_period.info.position := position
          |} as voting_period_info :=
      Alpha_context.Voting_period.get_rpc_fixed_current_info ctxt in
    let level_info := Alpha_context.Level.current ctxt in
    let ctxt := Alpha_context.finalize None ctxt in
    return=?
      (ctxt,
        {| Apply_results.block_metadata.baker := baker;
          Apply_results.block_metadata.level :=
            Alpha_context.Level.to_deprecated_type level_info
              voting_period.(Alpha_context.Voting_period.voting_period.index)
              position; Apply_results.block_metadata.level_info := level_info;
          Apply_results.block_metadata.voting_period_kind := kind_value;
          Apply_results.block_metadata.voting_period_info := voting_period_info;
          Apply_results.block_metadata.nonce_hash := None;
          Apply_results.block_metadata.consumed_gas :=
            Alpha_context.Gas.Arith.zero;
          Apply_results.block_metadata.deactivated := nil;
          Apply_results.block_metadata.balance_updates :=
            migration_balance_updates |})
  |
    (Application {|
      validation_mode.Application.block_header := {|
        Alpha_context.Block_header.t.protocol_data := {|
          Alpha_context.Block_header.protocol_data.contents := protocol_data
            |}
          |};
        validation_mode.Application.baker := baker;
        validation_mode.Application.block_delay := block_delay
        |} |
    Full_construction {|
      validation_mode.Full_construction.protocol_data := protocol_data;
        validation_mode.Full_construction.baker := baker;
        validation_mode.Full_construction.block_delay := block_delay
        |}) =>
    let=? '(ctxt, receipt) :=
      Apply.finalize_application ctxt protocol_data baker block_delay
        migration_balance_updates in
    let level := Alpha_context.Level.current ctxt in
    let priority := protocol_data.(Alpha_context.Block_header.contents.priority)
      in
    let raw_level :=
      Alpha_context.Raw_level.to_int32 level.(Alpha_context.Level.t.level) in
    let fitness := Alpha_context.Fitness.current ctxt in
    let commit_message :=
      Format.asprintf
        (CamlinternalFormatBasics.Format
          (CamlinternalFormatBasics.String_literal "lvl "
            (CamlinternalFormatBasics.Int32 CamlinternalFormatBasics.Int_d
              CamlinternalFormatBasics.No_padding
              CamlinternalFormatBasics.No_precision
              (CamlinternalFormatBasics.String_literal ", fit 1:"
                (CamlinternalFormatBasics.Int64 CamlinternalFormatBasics.Int_d
                  CamlinternalFormatBasics.No_padding
                  CamlinternalFormatBasics.No_precision
                  (CamlinternalFormatBasics.String_literal ", prio "
                    (CamlinternalFormatBasics.Int CamlinternalFormatBasics.Int_d
                      CamlinternalFormatBasics.No_padding
                      CamlinternalFormatBasics.No_precision
                      (CamlinternalFormatBasics.String_literal ", "
                        (CamlinternalFormatBasics.Int
                          CamlinternalFormatBasics.Int_d
                          CamlinternalFormatBasics.No_padding
                          CamlinternalFormatBasics.No_precision
                          (CamlinternalFormatBasics.String_literal " ops"
                            CamlinternalFormatBasics.End_of_format)))))))))
          "lvl %ld, fit 1:%Ld, prio %d, %d ops") raw_level fitness priority
        op_count in
    let ctxt := Alpha_context.finalize (Some commit_message) ctxt in
    return=? (ctxt, receipt)
  end.

Definition compare_operations
  (op1 : Alpha_context.packed_operation) (op2 : Alpha_context.packed_operation)
  : int :=
  let 'Alpha_context.Operation_data op1 :=
    op1.(Alpha_context.packed_operation.protocol_data) in
  let 'Alpha_context.Operation_data op2 :=
    op2.(Alpha_context.packed_operation.protocol_data) in
  match
    (op1.(Alpha_context.protocol_data.contents),
      op2.(Alpha_context.protocol_data.contents)) with
  |
    (Alpha_context.Single (Alpha_context.Endorsement _),
      Alpha_context.Single (Alpha_context.Endorsement _)) => 0
  | (_, Alpha_context.Single (Alpha_context.Endorsement _)) => 1
  | (Alpha_context.Single (Alpha_context.Endorsement _), _) => (-1)
  |
    (Alpha_context.Single (Alpha_context.Seed_nonce_revelation _),
      Alpha_context.Single (Alpha_context.Seed_nonce_revelation _)) => 0
  | (_, Alpha_context.Single (Alpha_context.Seed_nonce_revelation _)) => 1
  | (Alpha_context.Single (Alpha_context.Seed_nonce_revelation _), _) => (-1)
  |
    (Alpha_context.Single (Alpha_context.Double_endorsement_evidence _),
      Alpha_context.Single (Alpha_context.Double_endorsement_evidence _)) => 0
  | (_, Alpha_context.Single (Alpha_context.Double_endorsement_evidence _)) => 1
  | (Alpha_context.Single (Alpha_context.Double_endorsement_evidence _), _) =>
    (-1)
  |
    (Alpha_context.Single (Alpha_context.Double_baking_evidence _),
      Alpha_context.Single (Alpha_context.Double_baking_evidence _)) => 0
  | (_, Alpha_context.Single (Alpha_context.Double_baking_evidence _)) => 1
  | (Alpha_context.Single (Alpha_context.Double_baking_evidence _), _) => (-1)
  |
    (Alpha_context.Single (Alpha_context.Activate_account _),
      Alpha_context.Single (Alpha_context.Activate_account _)) => 0
  | (_, Alpha_context.Single (Alpha_context.Activate_account _)) => 1
  | (Alpha_context.Single (Alpha_context.Activate_account _), _) => (-1)
  |
    (Alpha_context.Single (Alpha_context.Proposals _),
      Alpha_context.Single (Alpha_context.Proposals _)) => 0
  | (_, Alpha_context.Single (Alpha_context.Proposals _)) => 1
  | (Alpha_context.Single (Alpha_context.Proposals _), _) => (-1)
  |
    (Alpha_context.Single (Alpha_context.Ballot _),
      Alpha_context.Single (Alpha_context.Ballot _)) => 0
  | (_, Alpha_context.Single (Alpha_context.Ballot _)) => 1
  | (Alpha_context.Single (Alpha_context.Ballot _), _) => (-1)
  |
    (Alpha_context.Single (Alpha_context.Manager_operation op1),
      Alpha_context.Single (Alpha_context.Manager_operation op2)) =>
    Z.compare op1.(Alpha_context.contents.Manager_operation.counter)
      op2.(Alpha_context.contents.Manager_operation.counter)
  |
    (Alpha_context.Cons (Alpha_context.Manager_operation op1) _,
      Alpha_context.Single (Alpha_context.Manager_operation op2)) =>
    Z.compare op1.(Alpha_context.contents.Manager_operation.counter)
      op2.(Alpha_context.contents.Manager_operation.counter)
  |
    (Alpha_context.Single (Alpha_context.Manager_operation op1),
      Alpha_context.Cons (Alpha_context.Manager_operation op2) _) =>
    Z.compare op1.(Alpha_context.contents.Manager_operation.counter)
      op2.(Alpha_context.contents.Manager_operation.counter)
  |
    (Alpha_context.Cons (Alpha_context.Manager_operation op1) _,
      Alpha_context.Cons (Alpha_context.Manager_operation op2) _) =>
    Z.compare op1.(Alpha_context.contents.Manager_operation.counter)
      op2.(Alpha_context.contents.Manager_operation.counter)
  | _ => unreachable_gadt_branch
  end.

Definition init_value
  (ctxt : Context.t) (block_header : Block_header.shell_header)
  : M=? Updater.validation_result :=
  let level := block_header.(Block_header.shell_header.level) in
  let fitness := block_header.(Block_header.shell_header.fitness) in
  let timestamp := block_header.(Block_header.shell_header.timestamp) in
  let typecheck (ctxt : Alpha_context.context) (script : Alpha_context.Script.t)
    : M=?
      ((Alpha_context.Script.t * option Alpha_context.Lazy_storage.diffs) *
        Alpha_context.context) :=
    let allow_forged_in_storage := false in
    let=? '(Script_ir_translator.Ex_script parsed_script, ctxt) :=
      Script_ir_translator.parse_script None ctxt false allow_forged_in_storage
        script in
    let 'existT _ __Ex_script_'b [ctxt, parsed_script] :=
      existT (A := Set)
        (fun __Ex_script_'b =>
          [Alpha_context.context ** Script_typed_ir.script __Ex_script_'b]) _
        [ctxt, parsed_script] in
    let=? '(storage_value, lazy_storage_diff, ctxt) :=
      Script_ir_translator.extract_lazy_storage_diff ctxt
        Script_ir_translator.Optimized false
        Script_ir_translator.no_lazy_storage_id
        Script_ir_translator.no_lazy_storage_id
        parsed_script.(Script_typed_ir.script.storage_type)
        parsed_script.(Script_typed_ir.script.storage) in
    let=? '(storage_value, ctxt) :=
      Script_ir_translator.unparse_data ctxt Script_ir_translator.Optimized
        parsed_script.(Script_typed_ir.script.storage_type) storage_value in
    let storage_value :=
      Alpha_context.Script.lazy_expr_value
        (Micheline.strip_locations storage_value) in
    return=?
      (((Alpha_context.Script.t.with_storage storage_value script),
        lazy_storage_diff), ctxt) in
  let=? ctxt :=
    Alpha_context.prepare_first_block ctxt typecheck level timestamp fitness in
  return=? (Alpha_context.finalize None ctxt).
