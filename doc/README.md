# Doc

In this folder there is the [Docusaurus 2](https://v2.docusaurus.io/) system, the static website generator we use to render the website of the project. See the README file at the root of the project to know how to generate this website.
