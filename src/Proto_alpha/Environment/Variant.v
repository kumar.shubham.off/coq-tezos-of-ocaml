(* TODO: remove this file once this definition is in [coq-of-ocaml]'s ones. *)
Require Import Coq.Strings.String.

(** Polymorphic variants. *)
Inductive t : Set :=
| Build : string -> forall (A : Set), A -> t.
