Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Sc_rollup_repr.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.

Require TezosOfOCaml.Proto_alpha.Proofs.Alpha_context.
Require TezosOfOCaml.Proto_alpha.Proofs.Storage_description.

Module PVM.
  Lemma boot_sector_encoding_is_valid :
    Data_encoding.Valid.t (fun _ => True) Sc_rollup_repr.PVM.boot_sector_encoding.
    Data_encoding.Valid.data_encoding_auto.
  Qed.
  #[global] Hint Resolve boot_sector_encoding_is_valid : Data_encoding_db.
End PVM.

Module Address.
  Lemma encoding_is_valid :
    Data_encoding.Valid.t (fun _ => True) Sc_rollup_repr.Address.encoding.
    apply Blake2B.Make_is_valid.
  Qed.
  #[global] Hint Resolve encoding_is_valid : Data_encoding_db.

  Lemma rpc_arg_valid : RPC_arg.Valid.t (fun _ => True) Sc_rollup_repr.Address.rpc_arg.
    apply Blake2B.Make_is_valid.
  Qed.
End Address.

Axiom of_b58_to_b58_eq : forall s,
    Sc_rollup_repr.of_b58check (Sc_rollup_repr.Address.to_b58check s) = return? s.

Axiom to_b58_of_b58_eq : forall b58,
  match Sc_rollup_repr.of_b58check b58 with
  | Pervasives.Ok v => Sc_rollup_repr.Address.to_b58check v = b58
  | Pervasives.Error _ => True
  end.

Lemma encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True) Sc_rollup_repr.encoding.
  Data_encoding.Valid.data_encoding_auto.
  intros; split; trivial.
  apply of_b58_to_b58_eq.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.

Lemma rpc_arg_valid : RPC_arg.Valid.t (fun _ => True) Sc_rollup_repr.rpc_arg.
  constructor; intros; simpl.
  - rewrite of_b58_to_b58_eq; reflexivity.
  - destruct (Sc_rollup_repr.of_b58check s) eqn:E; simpl; trivial.
    specialize (to_b58_of_b58_eq s); rewrite E; trivial.
Qed.

Lemma index_path_encoding_is_valid :
  Path_encoding.S.Valid.t
    (Storage_description.INDEX.to_Path Sc_rollup_repr.Index).
  constructor;
    unfold
      Sc_rollup_repr.Index,
      Sc_rollup_repr.Index.module,
      Sc_rollup_repr.Index.to_path,
      Sc_rollup_repr.Index.of_path,
      Binary.to_bytes_exn,
      Sc_rollup_repr.Index.path_length; simpl.
  - intros; destruct (Hex.of_bytes _ _); reflexivity.
  - intro v; destruct (Hex.of_bytes _ _) eqn:E;
      rewrite <- E, Hex.to_bytes_of_bytes; clear E; simpl.
    specialize (Data_encoding.Valid.of_bytes_opt_to_bytes_opt encoding_is_valid v I).
    now destruct (Binary.to_bytes_opt _ _ _) eqn:E.
  - intro path; destruct path; trivial; destruct path; trivial.
    destruct (Hex.to_bytes _) eqn:E; simpl; trivial;
    specialize (Hex.of_bytes_to_bytes (Hex s)); rewrite E; intro;
    specialize (Data_encoding.Valid.to_bytes_opt_of_bytes_opt encoding_is_valid b);
    destruct (Binary.of_bytes_opt _ _) eqn:E1; simpl; trivial;
    intro E2; rewrite E2, H; reflexivity.
  - intros; destruct (Hex.of_bytes None _); reflexivity.
Qed.

Module Kind.
  Lemma encoding_is_valid
    : Data_encoding.Valid.t (fun _ => True) Sc_rollup_repr.Kind.encoding.
    Data_encoding.Valid.data_encoding_auto.
  Qed.
  #[global] Hint Resolve encoding_is_valid : Data_encoding_db.
End Kind.
