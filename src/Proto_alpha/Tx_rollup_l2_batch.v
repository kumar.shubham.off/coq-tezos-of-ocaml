(** File generated by coq-of-ocaml *)
Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Indexable.
Require TezosOfOCaml.Proto_alpha.Tx_rollup_l2_address.
Require TezosOfOCaml.Proto_alpha.Tx_rollup_l2_context_sig.
Require TezosOfOCaml.Proto_alpha.Tx_rollup_l2_qty.

Definition tag_size : Variant.t := Variant.Build "Uint8" unit tt.

Definition signer_encoding : Data_encoding.encoding Bls_signature.pk :=
  Data_encoding.conv_with_guard Bls_signature.pk_to_bytes
    (fun (x_value : Bytes.t) =>
      match Bls_signature.pk_of_bytes_opt x_value with
      | Some x_value => return? x_value
      | None => Pervasives.Error "not a BLS public key"
      end) None (Data_encoding.Fixed.bytes_value Bls_signature.pk_size_in_bytes).

Definition Signer_indexable :=
  Indexable.Make
    (let t : Set := Bls_signature.pk in
    let pp {A : Set} (fmt : Format.formatter) (function_parameter : A) : unit :=
      let '_ := function_parameter in
      Format.pp_print_string fmt "<bls_signature>" in
    let compare (x_value : Bls_signature.pk) (y_value : Bls_signature.pk)
      : int :=
      Bytes.compare (Bls_signature.pk_to_bytes x_value)
        (Bls_signature.pk_to_bytes y_value) in
    let encoding := signer_encoding in
    {|
      Indexable.VALUE.encoding := encoding;
      Indexable.VALUE.compare := compare;
      Indexable.VALUE.pp := pp
    |}).

Inductive destination (status : Set) : Set :=
| Layer1 : Signature.public_key_hash -> destination status
| Layer2 : Tx_rollup_l2_address.Indexable.t -> destination status.

Arguments Layer1 {_}.
Arguments Layer2 {_}.

Definition compact_destination
  : Data_encoding.Compact.t (destination Indexable.unknown) :=
  Data_encoding.Compact.union None None
    [
      Data_encoding.Compact.case_value "layer1" None
        (Data_encoding.Compact.payload
          Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.encoding))
        (fun (function_parameter : destination Indexable.unknown) =>
          match function_parameter with
          | Layer1 x_value => Some x_value
          | _ => None
          end) (fun (x_value : Signature.public_key_hash) => Layer1 x_value);
      Data_encoding.Compact.case_value "layer2" None
        (Indexable.compact Tx_rollup_l2_address.encoding)
        (fun (function_parameter : destination Indexable.unknown) =>
          match function_parameter with
          | Layer2 x_value => Some x_value
          | _ => None
          end)
        (fun (x_value : Indexable.t Tx_rollup_l2_address.t) => Layer2 x_value)
    ].

Module V1.
  Module operation_content.
    Record record {status : Set} : Set := Build {
      destination : destination status;
      ticket_hash :
        Tx_rollup_l2_context_sig.Ticket_indexable.(Indexable.INDEXABLE.t);
      qty : Tx_rollup_l2_qty.t }.
    Arguments record : clear implicits.
    Definition with_destination {t_status} destination (r : record t_status) :=
      Build t_status destination r.(ticket_hash) r.(qty).
    Definition with_ticket_hash {t_status} ticket_hash (r : record t_status) :=
      Build t_status r.(destination) ticket_hash r.(qty).
    Definition with_qty {t_status} qty (r : record t_status) :=
      Build t_status r.(destination) r.(ticket_hash) qty.
  End operation_content.
  Definition operation_content := operation_content.record.
  
  Module operation.
    Record record {content : Set} : Set := Build {
      signer : Signer_indexable.(Indexable.INDEXABLE.t);
      counter : int64;
      contents : list (operation_content content) }.
    Arguments record : clear implicits.
    Definition with_signer {t_content} signer (r : record t_content) :=
      Build t_content signer r.(counter) r.(contents).
    Definition with_counter {t_content} counter (r : record t_content) :=
      Build t_content r.(signer) counter r.(contents).
    Definition with_contents {t_content} contents (r : record t_content) :=
      Build t_content r.(signer) r.(counter) contents.
  End operation.
  Definition operation := operation.record.
  
  Definition transaction (content : Set) : Set := list (operation content).
  
  Definition signature : Set := Bls_signature.signature.
  
  Module t.
    Record record {content : Set} : Set := Build {
      contents : list (transaction content);
      aggregated_signature : signature }.
    Arguments record : clear implicits.
    Definition with_contents {t_content} contents (r : record t_content) :=
      Build t_content contents r.(aggregated_signature).
    Definition with_aggregated_signature {t_content} aggregated_signature
      (r : record t_content) :=
      Build t_content r.(contents) aggregated_signature.
  End t.
  Definition t := t.record.
  
  Definition compact_operation_content
    : Data_encoding.Compact.t (operation_content Indexable.unknown) :=
    Data_encoding.Compact.conv None
      (fun (function_parameter : operation_content Indexable.unknown) =>
        let '{|
          operation_content.destination := destination;
            operation_content.ticket_hash := ticket_hash;
            operation_content.qty := qty
            |} := function_parameter in
        (destination, ticket_hash, qty))
      (fun (function_parameter :
        destination Indexable.unknown *
          Tx_rollup_l2_context_sig.Ticket_indexable.(Indexable.INDEXABLE.t) *
          Tx_rollup_l2_qty.t) =>
        let '(destination, ticket_hash, qty) := function_parameter in
        {| operation_content.destination := destination;
          operation_content.ticket_hash := ticket_hash;
          operation_content.qty := qty |})
      (Data_encoding.Compact.obj3
        (Data_encoding.Compact.req "destination" compact_destination)
        (Data_encoding.Compact.req "ticket_hash"
          Tx_rollup_l2_context_sig.Ticket_indexable.(Indexable.INDEXABLE.compact))
        (Data_encoding.Compact.req "qty" Tx_rollup_l2_qty.compact_encoding)).
  
  Definition operation_content_encoding
    : Data_encoding.encoding (operation_content Indexable.unknown) :=
    Data_encoding.Compact.make (Some tag_size) compact_operation_content.
  
  Definition compact_operation_raw
    (encoding_signer :
      Data_encoding.Compact.t Signer_indexable.(Indexable.INDEXABLE.t))
    : Data_encoding.Compact.t (operation Indexable.unknown) :=
    Data_encoding.Compact.conv None
      (fun (function_parameter : operation Indexable.unknown) =>
        let '{|
          operation.signer := signer;
            operation.counter := counter;
            operation.contents := contents
            |} := function_parameter in
        (signer, counter, contents))
      (fun (function_parameter :
        Signer_indexable.(Indexable.INDEXABLE.t) * int64 *
          list (operation_content Indexable.unknown)) =>
        let '(signer, counter, contents) := function_parameter in
        {| operation.signer := signer; operation.counter := counter;
          operation.contents := contents |})
      (Data_encoding.Compact.obj3
        (Data_encoding.Compact.req "signer" encoding_signer)
        (Data_encoding.Compact.req "counter" Data_encoding.Compact.int64_value)
        (Data_encoding.Compact.req "contents"
          (Data_encoding.Compact.list_value 4 operation_content_encoding))).
  
  Definition operation_encoding_raw
    (encoding_signer :
      Data_encoding.Compact.t Signer_indexable.(Indexable.INDEXABLE.t))
    : Data_encoding.encoding (operation Indexable.unknown) :=
    Data_encoding.Compact.make (Some tag_size)
      (compact_operation_raw encoding_signer).
  
  Definition compact_transaction_raw
    (encoding_signer :
      Data_encoding.Compact.t Signer_indexable.(Indexable.INDEXABLE.t))
    : Data_encoding.Compact.t (list (operation Indexable.unknown)) :=
    Data_encoding.Compact.list_value 8 (operation_encoding_raw encoding_signer).
  
  Definition transaction_encoding_raw
    (encoding_signer :
      Data_encoding.Compact.t Signer_indexable.(Indexable.INDEXABLE.t))
    : Data_encoding.t (transaction Indexable.unknown) :=
    Data_encoding.Compact.make (Some tag_size)
      (compact_transaction_raw encoding_signer).
  
  Definition compact_signer_index
    : Data_encoding.Compact.t (Indexable.index Bls_signature.pk) :=
    Data_encoding.Compact.conv None Indexable.to_int32 Indexable.index_exn
      Data_encoding.Compact.int32_value.
  
  Definition compact_signer_either
    : Data_encoding.Compact.t Signer_indexable.(Indexable.INDEXABLE.either) :=
    Signer_indexable.(Indexable.INDEXABLE.compact).
  
  Definition compact_operation
    : Data_encoding.Compact.t (operation Indexable.unknown) :=
    compact_operation_raw compact_signer_either.
  
  Definition compact_transaction_signer_index
    : Data_encoding.Compact.t (list (operation Indexable.unknown)) :=
    compact_transaction_raw compact_signer_index.
  
  Definition compact_transaction
    : Data_encoding.Compact.t (list (operation Indexable.unknown)) :=
    compact_transaction_raw compact_signer_either.
  
  Definition transaction_encoding
    : Data_encoding.t (transaction Indexable.unknown) :=
    transaction_encoding_raw compact_signer_either.
  
  Definition compact (bits : int)
    : Data_encoding.Compact.t (t Indexable.unknown) :=
    Data_encoding.Compact.conv None
      (fun (function_parameter : t Indexable.unknown) =>
        let '{|
          t.contents := contents;
            t.aggregated_signature := aggregated_signature
            |} := function_parameter in
        (aggregated_signature, contents))
      (fun (function_parameter :
        signature * list (transaction Indexable.unknown)) =>
        let '(aggregated_signature, contents) := function_parameter in
        {| t.contents := contents;
          t.aggregated_signature := aggregated_signature |})
      (Data_encoding.Compact.obj2
        (Data_encoding.Compact.req "aggregated_signature"
          (Data_encoding.Compact.payload
            Tx_rollup_l2_context_sig.signature_encoding))
        (Data_encoding.Compact.req "contents"
          (Data_encoding.Compact.list_value bits transaction_encoding))).
End V1.

Inductive t (signer content : Set) : Set :=
| V1 : V1.t content -> t signer content.

Arguments V1 {_ _}.

(** We use two bits for the versioning of the layer-2 batches, which
    leaves six bits in the shared tag of compact encoding. We use
    these six bits to efficiently encode small lists.

    To ensure backward compatibility, the value of the label
    [tag_bits] cannot be modified. To have more than 3 versions of the
    encoding, one would have to use the fourth case to wrap a new
    union.

{[
   union
     ~tag_bits:2
     ~inner_bits:6
     [
       case "V1" ...;
       case "V2" ...;
       case "V3" ...;
       case "V_next" ...
              (union [ case "V4" ... ; ... ]);
     ]
]} *)
Definition compact
  : Data_encoding.Compact.t (t Indexable.unknown Indexable.unknown) :=
  Data_encoding.Compact.union (Some 2) (Some 6)
    [
      Data_encoding.Compact.case_value "V1" None (V1.compact 6)
        (fun (function_parameter : t Indexable.unknown Indexable.unknown) =>
          let 'V1 x_value := function_parameter in
          Some x_value)
        (fun (x_value : V1.t Indexable.unknown) => V1 x_value)
    ].

(** An encoding for [t] that uses a specialized, space-efficient encoding
    for the list of transactions. *)
Definition encoding : Data_encoding.t (t Indexable.unknown Indexable.unknown) :=
  Data_encoding.Compact.make (Some tag_size) compact.
