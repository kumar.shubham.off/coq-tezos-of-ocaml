(* Open Source License *)
(* Copyright (c) 2021 Nomadic Labs. <contact@nomadic-labs.com> *)

(* Permission is hereby granted, free of charge, to any person obtaining a *)
(* copy of this software and associated documentation files (the "Software"), *)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense, *)
(* and/or sell copies of the Software, and to permit persons to whom the *)
(* Software is furnished to do so, subject to the following conditions: *)

(* The above copyright notice and this permission notice shall be included *)
(* in all copies or substantial portions of the Software. *)

(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR *)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER *)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER *)
(* DEALINGS IN THE SOFTWARE. *)

(* Functions and lemmas over basic data types *)

Require Ascii Coq.Strings.String ZArith Coq.Lists.List Bool DecimalString.
Import ZArith Ascii Coq.Strings.String.

(* Logic *)

Lemma forall_ex {A : Set} {phi psi : A -> Prop} :
  (forall x, phi x <-> psi x) -> ((exists x, phi x) <-> (exists x, psi x)).
Proof.
  intro Hall.
  split; intros (x, Hx); exists x; specialize (Hall x); intuition.
Qed.

Lemma and_comm_3 {A B C} : A /\ (B /\ C) <-> B /\ (A /\ C).
Proof.
  tauto.
Qed.

Lemma ex_and_comm {A : Set} {P : Prop} {Q : A -> Prop} :
  P /\ (exists x, Q x) <-> (exists x, P /\ Q x).
Proof.
  split.
  - intros (p, (x, q)).
    exists x.
    auto.
  - intros (x, (p, q)).
    split.
    + auto.
    + exists x.
      auto.
Qed.

Lemma ex_and_comm2 {A B : Set} {P : Prop} {Q : A -> B -> Prop} :
  P /\ (exists x y, Q x y) <-> (exists x y, P /\ Q x y).
Proof.
  rewrite ex_and_comm.
  apply forall_ex; intro x.
  apply ex_and_comm.
Qed.

Lemma and_both {P Q R : Prop} : (Q <-> R) -> (P /\ Q <-> P /\ R).
Proof.
  intuition.
Qed.

Lemma ex_and_comm_both {A : Set} {P Q R}:
  (Q <-> exists x : A, R x) ->
  ((P /\ Q) <-> exists x : A, P /\ R x).
Proof.
  intro H.
  transitivity (P /\ exists x, R x).
  - apply and_both; assumption.
  - apply ex_and_comm.
Qed.

Lemma ex_and_comm_both2 {A B : Set} {P Q R}:
  (Q <-> exists (x : A) (y : B), R x y) ->
  ((P /\ Q) <-> exists x y, P /\ R x y).
Proof.
  intro H.
  transitivity (P /\ exists x y, R x y).
  - apply and_both; assumption.
  - apply ex_and_comm2.
Qed.

Lemma and_both_2 (P Q R : Prop):
  (P -> (Q <-> R)) ->
  ((P /\ Q) <-> (P /\ R)).
Proof.
  intuition.
Qed.

Lemma and_both_0 {P Q R S : Prop} : (P <-> R) -> (Q <-> S) -> (P /\ Q) <-> (R /\ S).
Proof.
  intuition.
Qed.

Lemma and_left {P Q R : Prop} : P -> (Q <-> R) -> ((P /\ Q) <-> R).
Proof.
  intuition.
Qed.

Lemma and_right {P Q R : Prop} : P -> (Q <-> R) -> (Q <-> (P /\ R)).
Proof.
  intuition.
Qed.

Lemma or_both {P Q R S} : P <-> R -> Q <-> S -> ((P \/ Q) <-> (R \/ S)).
Proof.
  intuition.
Qed.

Lemma eq_sym_iff {A : Type} (x y : A) : x = y <-> y = x.
Proof.
  split; apply eq_sym.
Qed.

Lemma ex_order :
  forall A B (P : A -> B -> Prop),
    (exists x y, P x y) <->
    (exists y x, P x y).
Proof. split; intros [x [y HPxy]]; eauto. Qed.

Lemma ex_eq_simpl :
  forall A x P,
    (exists x' : A, x' = x /\ P x') <->
    P x.
Proof.
  intros A x P.
  split.
  - intros [x' [Hx Hp]]. congruence.
  - intros Hp. exists x. intuition.
Qed.

Lemma ex_eq_some_simpl {A : Type} (x : A) (P : A -> Prop) :
  (exists y : A, Some x = Some y /\ P y) <-> P x.
Proof.
  split; intro H.
  - destruct H as (y & Heq & Hp).
    inversion Heq.
    assumption.
  - exists x.
    intuition.
Qed.

Lemma iff_comm :
  forall A B,
    (A <-> B) <-> (B <-> A).
Proof. intuition. Qed.

Lemma and_pair_eq (A B : Type) (a c : A) (b d : B) : (a, b) = (c, d) <-> (a = c) /\ (b = d).
Proof.
  intuition. now inversion H. now inversion H.
  congruence.
Qed.

Lemma ex_2 {A : Set} P Q :
    (exists x : A, (exists y : A, P x y) /\ Q x) <->
    (exists x y : A, P x y /\ Q x).
Proof. intuition; destruct H; destruct H; destruct H; eauto. Qed.

Lemma underex_and_comm :
  forall (A : Set) (P Q : A -> Prop),
    (exists (x : A), P x /\ Q x) <->
    (exists (x : A), Q x /\ P x).
Proof.
  intros A P Q. apply forall_ex. intuition.
Qed.

Lemma forall_ex2 {A : Set} {B : Set} {phi : A -> B -> Prop} {psi : A -> B -> Prop} :
  (forall x y, phi x y <-> psi x y) -> (exists x y, phi x y) <-> (exists x y, psi x y).
Proof. intros H. auto using forall_ex. Qed.

Lemma underex2_and_comm :
  forall (A B : Set) (P Q : A -> B -> Prop),
    (exists (x : A) (y : B), P x y /\ Q x y) <->
    (exists (x : A) (y : B), Q x y /\ P x y ).
Proof. intros A B P Q. apply forall_ex2. intuition. Qed.

Lemma underex2_and_assoc :
  forall (A B : Set) (P Q R : A -> B -> Prop),
    (exists (x : A) (y : B), (P x y /\ Q x y) /\ R x y) <->
    (exists (x : A) (y : B), P x y /\ Q x y /\ R x y ).
Proof.
  intros A B P Q R.
  apply forall_ex2. intros. apply and_assoc.
Qed.

Lemma ex2_and_comm {A : Set} {B : Set} {P : Prop} {Q : A -> B -> Prop} :
  P /\ (exists x y, Q x y) <-> (exists x y, P /\ Q x y).
Proof.
  split.
  intros [Hp [x [y Hq]]]. eauto.
  intros [x [y [Hp Hq]]]. eauto.
Qed.

Lemma underex_and_assoc :
  forall (A : Set) (P Q R : A -> Prop),
    (exists (x : A), (P x /\ Q x) /\ R x) <->
    (exists (x : A), P x /\ Q x /\ R x).
Proof.
  intros A P Q R.
  apply forall_ex. intros. apply and_assoc.
Qed.

Lemma eq_iff_refl :
  forall P Q, P = Q -> P <-> Q.
Proof. intros. rewrite H. apply iff_refl. Qed.

(* Strings *)

Definition string_of_Z z := DecimalString.NilZero.string_of_int (Z.to_int z).

Fixpoint make_string (a : Ascii.ascii) (n : nat) :=
  match n with
  | 0 => EmptyString
  | S n' => String a (make_string a n')
  end.

Definition lf := (String "010" EmptyString).

Definition string_snoc s c := (s ++ String c "")%string.

Lemma append_empty_right : forall s, (String.append s "") = s.
Proof.
  induction s.
  - reflexivity.
  - simpl. rewrite IHs. reflexivity.
Qed.

Lemma append_assoc : forall s1 s2 s3,
    ((s1 ++ s2) ++ s3)%string = (s1 ++ (s2 ++ s3))%string.
Proof.
  induction s1; intros.
  - reflexivity.
  - simpl. rewrite IHs1. reflexivity.
Qed.

Lemma append_string_empty_left : forall a s, (String a "" ++ s)%string = String a s.
Proof. reflexivity. Qed.

(* Booleans *)

Lemma bool_dec_same (a : bool) (H : a = a) : H = eq_refl.
Proof.
  apply Eqdep_dec.UIP_dec.
  apply Bool.bool_dec.
Qed.

Lemma bool_dec_same2 (x y : bool) (H1 H2 : x = y) (HH1 HH2 : H1 = H2) : HH1 = HH2.
Proof.
  apply Eqdep_dec.UIP_dec.
  intros x2 y2.
  left.
  apply Eqdep_dec.UIP_dec.
  apply Bool.bool_dec.
Qed.

Lemma bool_dec_same_same (x : Datatypes.bool) : bool_dec_same x eq_refl = eq_refl.
Proof.
  apply bool_dec_same2.
Qed.

Global Coercion Is_true := Bool.Is_true.

Lemma Is_true_UIP (b : bool) (x y : b) : x = y.
Proof.
  destruct b; destruct x; destruct y; reflexivity.
Defined.

Lemma IT_eq (b : bool) : b -> b = true.
Proof.
  destruct b; auto.
Qed.

Lemma IT_eq_rev (b : bool) : b = true -> b.
Proof.
  intro H; subst b; exact I.
Qed.

Lemma IT_eq_iff (b : bool) : b <-> b = true.
Proof.
  split.
  - apply IT_eq.
  - apply IT_eq_rev.
Qed.

(* Redefinition of stdlib lemmas because we need them to compute *)
Definition andb_prop a b : (a && b)%bool = true -> a = true /\ b = true.
Proof.
  destruct a; destruct b; try discriminate; split; reflexivity.
Defined.

Lemma andb_prop_refl : andb_prop true true eq_refl = conj eq_refl eq_refl.
Proof.
  destruct (andb_prop true true eq_refl).
  f_equal; apply bool_dec_same.
Qed.

Definition andb_prop_elim b1 b2 : (b1 && b2)%bool -> b1 /\ b2.
Proof.
  destruct b1; destruct b2; try contradiction; split; constructor.
Defined.

Lemma Is_true_and_left b1 b2 : (b1 && b2)%bool -> b1.
Proof.
  destruct b1; simpl.
  - intro; constructor.
  - auto.
Qed.

Lemma Is_true_and_right b1 b2 : (b1 && b2)%bool -> b2.
Proof.
  destruct b1; simpl.
  - auto.
  - intro H.
    inversion H.
Qed.

Lemma if_false_is_and (b : bool) A :
  (if b then A else Logic.False) <-> (b = true /\ A).
Proof.
  destruct b.
  - apply and_right.
    + reflexivity.
    + intuition.
  - split.
    + intro H; inversion H.
    + intros (H, _); inversion H.
Qed.

Lemma if_false_not (b : Datatypes.bool) A : (if b then Logic.False else A) <-> (b = false /\ A).
Proof.
  destruct b.
  - split.
    + intro H; inversion H.
    + intros (H, _); inversion H.
  - apply and_right.
    + reflexivity.
    + intuition.
Qed.

Lemma destruct_if (b : Datatypes.bool) P Q :
  (if b then P else Q) <-> ((b = true /\ P ) \/ (b = false /\ Q)).
Proof.
  destruct b; intuition discriminate.
Qed.

Lemma bool_not_false b : b = false <-> ~ b = true.
Proof.
  destruct b; intuition congruence.
Qed.

Lemma match_if_exchange A B (b : Datatypes.bool) (P : A -> Prop) (Q : B -> Prop) u v :
  match (if b then inl u else inr v) with
  | inl x => P x
  | inr y => Q y
  end =
  if b then P u else Q v.
Proof.
  destruct b; reflexivity.
Qed.

(* Lists *)

Lemma uncons {A} (a1 a2 : A) l1 l2 :
  cons a1 l1 = cons a2 l2 -> a1 = a2 /\ l1 = l2.
Proof.
  intro H; injection H; auto.
Qed.

Fixpoint forallb {A} (P : A -> bool) (l : list A) : bool :=
  match l with
  | nil => true
  | cons a l => (P a && forallb P l)%bool
  end.

Lemma app_length_inv {A} : forall (l1 l1' l2 l2' : Datatypes.list A),
    List.length l1 = List.length l1' ->
    (l1 ++ l2)%list = (l1' ++ l2')%list ->
    l1 = l1' /\ l2 = l2'.
Proof.
  induction l1; intros l1' l2 l2' Hlen Happ.
  - destruct l1'; simpl in *.
    + auto.
    + inversion Hlen.
  - destruct l1'; simpl in *.
    + inversion Hlen.
    + injection Happ. intros Happ2 Ha. subst.
      specialize (IHl1 l1' l2 l2' (eq_add_S _ _ Hlen) Happ2) as [Hl1 Hl2].
      subst.
      auto.
Qed.

Lemma fold_right_psi_simpl :
  forall (A B : Type) (psi : A -> Prop) (xs : Datatypes.list B)
         (f  : (A -> Prop) -> B -> A -> Prop)
         (f' : B -> A -> A)
         (H : forall x st psi, f psi x st = psi (f' x st)),
  forall st0,
    List.fold_right (fun x psi st => f psi x st) psi xs st0 =
    psi (List.fold_right f' st0 (List.rev xs)).
Proof.
  intros A B psi xs f f' H.
  induction xs.
  - now simpl.
  - intro st0.
    simpl.
    rewrite H.
    rewrite IHxs.
    now rewrite List.fold_right_app.
Qed.

Fixpoint list_filter_map {A B : Type} (f : A -> option B) (l : list A) : list B :=
  match l with
  | nil => nil
  | cons a l =>
    let tl := list_filter_map f l in
    match f a with
    | None => tl
    | Some b => cons b tl
    end
  end.

(* Sigma types *)

Definition sigT_eq_1 {A} (P : A -> Set) (xa yb : sigT P) : xa = yb -> projT1 xa = projT1 yb.
Proof.
  apply f_equal.
Defined.

Definition sigT_eq_2 {A} (P : A -> Set) (xa yb : sigT P) (H : xa = yb) :
  eq_rec (projT1 xa) P (projT2 xa) (projT1 yb) (sigT_eq_1 P xa yb H) = projT2 yb.
Proof.
  subst xa.
  reflexivity.
Defined.

Definition existT_eq_1 {A} (P : A -> Set) x y a b : existT P x a = existT P y b -> x = y.
Proof.
  apply (f_equal (@projT1 A P)).
Defined.

Definition existT_eq_2 {A} (P : A -> Set) x y a b (H : existT P x a = existT P y b ) :
  eq_rec x P a y (existT_eq_1 P x y a b H) = b.
Proof.
  apply (sigT_eq_2 P (existT P x a) (existT P y b)).
Defined.

Definition existT_eq_3 {A} (P : A -> Set) x y a b :
  existT P x a = existT P y b ->
  sig (fun H : x = y => eq_rec x P a y H = b).
Proof.
  intro H.
  exists (existT_eq_1 P x y a b H).
  apply existT_eq_2.
Defined.

(* Same about sig *)

Definition sig_eq_1 {A} (P : A -> Prop) (xa yb : sig P) : xa = yb -> proj1_sig xa = proj1_sig yb.
Proof.
  apply f_equal.
Defined.

Definition sig_eq_2 {A} (P : A -> Prop) (xa yb : sig P) (H : xa = yb) :
  eq_rec (proj1_sig xa) P (proj2_sig xa) (proj1_sig yb) (sig_eq_1 P xa yb H) = proj2_sig yb.
Proof.
  subst xa.
  reflexivity.
Defined.

Definition exist_eq_1 {A} (P : A -> Prop) x y a b : exist P x a = exist P y b -> x = y.
Proof.
  apply (f_equal (@proj1_sig A P)).
Defined.

Definition exist_eq_2 {A} (P : A -> Prop) x y a b (H : exist P x a = exist P y b ) :
  eq_rec x P a y (exist_eq_1 P x y a b H) = b.
Proof.
  apply (sig_eq_2 P (exist P x a) (exist P y b)).
Defined.

Definition exist_eq_3 {A} (P : A -> Prop) x y a b :
  exist P x a = exist P y b ->
  sig (fun H : x = y => eq_rec x P a y H = b).
Proof.
  intro H.
  exists (exist_eq_1 P x y a b H).
  apply exist_eq_2.
Defined.

(* Arith *)

Lemma lt_proof_irrelevant : forall (n1 n2 : nat) (p q : (n1 ?= n2) = Lt), p = q.
Proof.
  intros n1 n2 p q.
  apply Eqdep_dec.UIP_dec.
  destruct x; destruct y; auto;
    try (right; intro contra; discriminate contra).
Qed.

Lemma le_trans_rev : forall n m p : nat, m <= p -> n <= m -> n <= p.
Proof.
  intros n m p Hmp Hnm. apply le_trans with m. apply Hnm. apply Hmp.
Qed.
