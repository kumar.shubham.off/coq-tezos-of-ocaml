Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Proto_alpha.Environment.S.

Parameter Fr_t : Set.
  
Parameter Fr : S.PRIME_FIELD (t := Fr_t).

Parameter G1_t : Set.

Parameter G1 : S.CURVE (t := G1_t) (Scalar_t := Fr.(S.PRIME_FIELD.t)).

Parameter G2_t : Set.

Parameter G2 : S.CURVE (t := G2_t) (Scalar_t := Fr.(S.PRIME_FIELD.t)).

Parameter pairing_check : list (G1.(S.CURVE.t) * G2.(S.CURVE.t)) -> bool.
