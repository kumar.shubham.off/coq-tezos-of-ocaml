Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Script_interpreter_defs.

Require Import TezosOfOCaml.Proto_alpha.Proofs.Saturation_repr.
Require Import TezosOfOCaml.Proto_alpha.Simulations.Script_typed_ir.
Require TezosOfOCaml.Proto_alpha.Simulations.Script_interpreter_defs.

(** Our definition behaves as the original OCaml code. *)
Lemma dep_cost_of_instr_eq {a s f}
  (i : With_family.kinstr (a :: s) f)
  (accu : Family.Ty.to_Set a) (stack : Family.Stack_ty.to_Set s) :
  Script_interpreter_defs.dep_cost_of_instr i (accu, stack) =
  Script_interpreter_defs.cost_of_instr (With_family.to_kinstr i) accu stack.
  dep_destruct i;
    unfold Script_interpreter_defs.cost_of_instr; simpl;
    try rewrite cast_eval;
    simpl in *;
    destruct_all Script_typed_ir.never;
    Tactics.destruct_pairs;
    try rewrite_cast_exists;
    try match goal with
    | _ : ?t |- context[cast_exists ?T _] =>
      rewrite (cast_exists_eval_1 (T := T) (E1 := t))
    end;
    reflexivity.
Qed.

(** The cost of an instruction is valid in saturated arithmetic. *)
Lemma dep_cost_of_instr_is_valid {s f}
  (i : With_family.kinstr s f) (accu_stack : Family.Stack_ty.to_Set s) :
  Saturation_repr.Valid.t (Script_interpreter_defs.dep_cost_of_instr i accu_stack).
  destruct i; simpl;
    apply axiom. (*TODO: fix this*)
    (* now apply Saturation_repr.Valid.decide.*)
Qed.

(** The cost of an instruction is not zero, except for a few instructions. *)
Lemma dep_cost_of_instr_is_not_zero {s f}
  (i : With_family.kinstr s f) (accu_stack : Family.Stack_ty.to_Set s) :
  Script_interpreter_defs.dep_cost_of_instr i accu_stack <> 0.
Admitted.
  (* TODO: fix, currently getting universe issues *)
  (* match i with *)
  (* | With_family.IFailwith _ _ _ => True *)
  (* | _ => (dep_cost_of_instr i accu_stack) <> 0 *)
  (* end *)
  (* ******* *)
  (* String below eats about 14GB of memory and I kill it *)
  (* I'm not 100% sure but I think it started after changes I made in dep_cost_of_instr *)
  (* ******* *)
  (* destruct i, accu_stack; simpl; cbv; trivial; try congruence; *)
  (* apply axiom. (*TODO*) *)
(* Qed. *)

Lemma dep_consume_instr_eq {a s f}
  (local_gas_counter : Local_gas_counter.local_gas_counter)
  (i : With_family.kinstr (a :: s) f)
  (accu : Family.Ty.to_Set a) (stack : Family.Stack_ty.to_Set s) :
  Script_interpreter_defs.dep_consume_instr local_gas_counter i (accu, stack) =
  Script_interpreter_defs.consume_instr
    local_gas_counter (With_family.to_kinstr i) accu stack.
  unfold Script_interpreter_defs.dep_consume_instr.
  now rewrite dep_cost_of_instr_eq.
Qed.
