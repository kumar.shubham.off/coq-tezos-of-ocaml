Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require Import TezosOfOCaml.Proto_alpha.Simulations.Script_ir_translator.
Require Import TezosOfOCaml.Proto_alpha.Script_ir_translator.

Require Import TezosOfOCaml.Proto_alpha.Simulations.Script_typed_ir.
Require TezosOfOCaml.Proto_alpha.Proofs.Script_typed_ir.

Fixpoint dep_ty_of_comparable_ty_eq {t} (ty : With_family.comparable_ty t) :
  With_family.to_ty (dep_ty_of_comparable_ty ty) =
  ty_of_comparable_ty (With_family.to_comparable_ty ty).
  destruct ty; Tactics.destruct_pairs; simpl;
    repeat rewrite dep_ty_of_comparable_ty_eq; reflexivity.
Qed.

Fixpoint dep_unparse_comparable_ty_uncarbonated_eq {loc : Set} {t}
  (loc_value : loc) (ty : With_family.comparable_ty t) :
  dep_unparse_comparable_ty_uncarbonated loc_value ty =
  unparse_comparable_ty_uncarbonated
    loc_value (With_family.to_comparable_ty ty).
  destruct ty; Tactics.destruct_pairs; simpl;
    repeat rewrite dep_unparse_comparable_ty_uncarbonated_eq; reflexivity.
Qed.

Fixpoint dep_unparse_ty_uncarbonated_eq {loc : Set} {t}
  (loc_value : loc) (ty : With_family.ty t) :
  dep_unparse_ty_uncarbonated loc_value ty =
  unparse_ty_uncarbonated loc_value (With_family.to_ty ty).
Proof.
  destruct ty; Tactics.destruct_pairs; simpl; try reflexivity;
    try rewrite dep_unparse_comparable_ty_uncarbonated_eq;
    repeat rewrite dep_unparse_ty_uncarbonated_eq;
    simpl; try reflexivity.
Qed.

Definition dep_serialize_ty_for_error_eq {t} (ty : With_family.ty t) :
  dep_serialize_ty_for_error ty = serialize_ty_for_error (With_family.to_ty ty).
  unfold dep_serialize_ty_for_error.
  now rewrite dep_unparse_ty_uncarbonated_eq.
Qed.

Lemma dep_comparable_comb_witness2_eq {t} (ty : With_family.comparable_ty t) :
  dep_comparable_comb_witness2 t =
  comparable_comb_witness2 (With_family.to_comparable_ty ty).
  destruct ty; Tactics.destruct_pairs; simpl; try reflexivity.
  sauto lq: on.
Qed.

Fixpoint dep_unparse_comparable_data_eq {loc : Set} {t}
  (l : loc) (ctxt : Raw_context.t) (mode : Script_ir_translator.unparsing_mode)
  (ty : With_family.comparable_ty t) (x : Family.Ty.to_Set t) {struct ty} :
  dep_unparse_comparable_data l ctxt mode t x =
  unparse_comparable_data l ctxt mode (With_family.to_comparable_ty ty) x.
  destruct ty; simpl;
    try (now destruct x);
    Tactics.destruct_pairs; simpl;
    try rewrite cast_eval;
    try rewrite_cast_exists;
    try reflexivity;
    (destruct Alpha_context.Gas.consume; simpl; [|reflexivity]);
    destruct x; unfold unparse_pair, unparse_union, unparse_option;
    try (now repeat rewrite <- dep_unparse_comparable_data_eq).
  repeat (
    rewrite <- dep_unparse_comparable_data_eq;
    destruct dep_unparse_comparable_data; simpl; [|reflexivity];
    Tactics.destruct_pairs
  ).
  destruct mode; try rewrite <- dep_comparable_comb_witness2_eq; reflexivity.
Qed.

Lemma dep_comparable_comb_witness1_eq {t} (ty : With_family.comparable_ty t) :
  dep_comparable_comb_witness1 t =
  comparable_comb_witness1 (With_family.to_comparable_ty ty).
  destruct ty; Tactics.destruct_pairs; reflexivity.
Qed.

Lemma kind_equal_eq : forall (a b : Script_tc_errors.kind),
    kind_equal a b = true -> a = b.
Proof. 
  now intros [] [].
Qed.

Lemma kind_equal_refl : forall x, kind_equal x x = true.
Proof.
  now intros [].
Qed.

Module Union_annot.
  Module Valid.
    Fixpoint t (ty : Script_typed_ir.ty) : Prop :=
      match ty with
      | Script_typed_ir.Union_t t1 t2 mt =>
          t t1 /\ t t2
      | Script_typed_ir.Pair_t t1 t2 _ => t t1 /\ t t2
      | Script_typed_ir.Option_t t1 t2 => t t1 
      | _ => True
      end.
  End Valid.
End Union_annot.

(** Show that the two functions 
comparable_ty_of_ty and ty_of_comparable_ty are compatible. 
We create 2 reciprocal Fixpoints and prove them.  
*)
Fixpoint ty_of_comparable_ty_comparable_ty_of_ty_compatible  
  (ctxt : Alpha_context.context) (loc_value : Alpha_context.Script.location)
  (ty : Script_typed_ir.ty) {struct ty} :
  Union_annot.Valid.t ty ->
  match (comparable_ty_of_ty ctxt loc_value ty) with
  | Pervasives.Ok (comp_ty, alpha) => ty_of_comparable_ty comp_ty = ty
  | Pervasives.Error _ => True
  end.                
Proof.
  intro V; destruct ty eqn:T; simpl;
    destruct
      (Alpha_context.Gas.consume ctxt
        Typecheck_costs.comparable_ty_of_ty_cycle) eqn:G;
    simpl; try reflexivity; try apply I.
  (* Pairs *)
  destruct (comparable_ty_of_ty t loc_value t1) eqn:H; simpl; [| trivial].
  destruct p eqn:P.
  destruct (comparable_ty_of_ty c0 loc_value t2) eqn:H'; simpl; [| trivial].
  destruct p0 eqn:P0; simpl; subst.
  specialize
    (ty_of_comparable_ty_comparable_ty_of_ty_compatible t loc_value t1) as H2;
    rewrite H in H2. rewrite H2; try (simpl in V; apply V).
  specialize
    (ty_of_comparable_ty_comparable_ty_of_ty_compatible c0 loc_value t2) as H3;
    rewrite H' in H3; rewrite H3; [ auto | simpl in V; apply V].
  (* Unions *)
  apply axiom.
  (* destruct p eqn:DP, p0 eqn:DP0, comparable_ty_of_ty eqn:H; simpl; [| trivial]. *)
  (* destruct p1 eqn:DP1, (comparable_ty_of_ty c0 loc_value t2) eqn:H'; *)
  (*   simpl; [| trivial]. *)
  (* destruct p2 eqn:DP2; simpl; subst. *)
  (* specialize *)
  (*   (ty_of_comparable_ty_comparable_ty_of_ty_compatible *)
  (*      t0 loc_value t1) as H2. *)
  (* rewrite H in H2; rewrite H2; destruct V as [[V1 V2] [V3 V4]]; trivial.  *)
  (* specialize *)
  (*   (ty_of_comparable_ty_comparable_ty_of_ty_compatible *)
  (*      c0 loc_value t2) as H3. *)
  (* rewrite H' in H3. rewrite H3; trivial; subst; trivial.  *)
  (* Option *)
  destruct (comparable_ty_of_ty t1 loc_value t) eqn:H; simpl; [| trivial];
    destruct p eqn:P;
    specialize
      (ty_of_comparable_ty_comparable_ty_of_ty_compatible
         t1 loc_value t) as H2;
    rewrite H in H2; simpl; rewrite H2; [| simpl in V]; trivial.  
Qed.

Fixpoint comparable_ty_of_ty_ty_of_comparable_ty_compatible 
         (ctxt ctxt1: Alpha_context.context)
         (loc_value : Alpha_context.Script.location)
         (comp_ty comp_ty1: Script_typed_ir.comparable_ty) :
  comparable_ty_of_ty ctxt loc_value (ty_of_comparable_ty comp_ty) =
    Pervasives.Ok (comp_ty1, ctxt1) -> comp_ty = comp_ty1.
Proof.
  intro H.
  destruct comp_ty eqn:G; simpl in H;
    destruct (Alpha_context.Gas.consume ctxt
              Typecheck_costs.comparable_ty_of_ty_cycle) eqn:G';
    simpl in H; (try injection H as H); trivial; simpl in H; try discriminate H.
  (* Pairs *)
  destruct
    (comparable_ty_of_ty t0 loc_value (ty_of_comparable_ty c1))
    eqn:P1; [|discriminate H].
  simpl in H; destruct p. 
  destruct
    (comparable_ty_of_ty c0 loc_value (ty_of_comparable_ty c2))
    eqn:P2; [|discriminate H].
  simpl in H; destruct p; injection H as H; subst. 
  apply comparable_ty_of_ty_ty_of_comparable_ty_compatible in P1.
  apply comparable_ty_of_ty_ty_of_comparable_ty_compatible in P2; subst; easy.
  (* Unions *)
  destruct
    (comparable_ty_of_ty t0 loc_value (ty_of_comparable_ty c1))
    eqn:P; [|discriminate H].
  simpl in H; destruct p.
  destruct
    (comparable_ty_of_ty c0 loc_value (ty_of_comparable_ty c2))
    eqn:P'; [|discriminate H].
  simpl in H; destruct p.
  injection H as H.
  apply comparable_ty_of_ty_ty_of_comparable_ty_compatible in P.
  apply comparable_ty_of_ty_ty_of_comparable_ty_compatible in P'.
  subst; trivial.
  (* Option *)
  destruct
    (comparable_ty_of_ty t0 loc_value (ty_of_comparable_ty c))
    eqn:P; [|discriminate H].
  simpl in H. destruct p. injection H as H. subst.
  apply comparable_ty_of_ty_ty_of_comparable_ty_compatible in P;
    subst; reflexivity.
Qed.

(** Validity predicate. This predicate states that given [comparable_ty] 
    has correct metadata. 
*)
Module Valid. 
  Fixpoint t (cty : Script_typed_ir.comparable_ty) : Prop :=
    match cty with
    | Script_typed_ir.Unit_key
    | Script_typed_ir.Never_key
    | Script_typed_ir.Int_key
    | Script_typed_ir.Nat_key
    | Script_typed_ir.Signature_key
    | Script_typed_ir.String_key
    | Script_typed_ir.Bytes_key
    | Script_typed_ir.Mutez_key
    | Script_typed_ir.Bool_key
    | Script_typed_ir.Key_hash_key
    | Script_typed_ir.Key_key
    | Script_typed_ir.Timestamp_key
    | Script_typed_ir.Chain_id_key
    | Script_typed_ir.Address_key
    | Script_typed_ir.Tx_rollup_l2_address_key => True
      (* x = {| Script_typed_ir.ty_metadata.size := *)
      (*          Script_typed_ir.Type_size.(Script_typed_ir.TYPE_SIZE.one) |} *)
    | Script_typed_ir.Pair_key cty1 cty2 x
    | Script_typed_ir.Union_key cty1 cty2 x =>
      let sz := (1 +i Script_typed_ir.comparable_ty_size cty1 +i
                          Script_typed_ir.comparable_ty_size cty2) in
      x = {| Script_typed_ir.ty_metadata.size := sz |} /\
      sz <= Alpha_context.Constants.michelson_maximum_type_size /\
      t cty1 /\ t cty2
    | Script_typed_ir.Option_key cty1 x =>
      let sz := (1 +i Script_typed_ir.comparable_ty_size cty1) in
      x = {| Script_typed_ir.ty_metadata.size := sz |} /\
      sz <= Alpha_context.Constants.michelson_maximum_type_size /\
      t cty1
    end.
End Valid.

Lemma extract_annot_eq node : forall node1 x,
  Script_ir_annot.extract_field_annot node = Pervasives.Ok (node1, x) ->
  node = node1.
Proof.
  (** It is possible that this Lemma is not correct
     but this is issue with annotation (annotations
     should be changed soon), so just admitting. 
  *)
Admitted.

(** We prove that [parse (unparse comparable_ty) = comparable_ty]
   (skipping annotations).
   Here we prove this statement for auxillary functions.
   We require metadata of given comparable_ty to be correct in sense
   described above ([Valid.t] predicate).
   The most part of proof is by case analysis.
   So, we unfold definitions, check every possible case for comparable_ty,
   and show needed equaliy. Recursive cases are proved by recursive calls.
   We do not check error cases, go through success case only.
*)
Fixpoint parse_comparable_ty_aux_unparse_comparable_ty_uncarbonated
  loc ty ctxt stack_depth :
  Valid.t ty ->
  let node := unparse_comparable_ty_uncarbonated loc ty in
  match parse_comparable_ty_aux stack_depth ctxt node with
  | Pervasives.Ok (Ex_comparable_ty ty', _) => ty = ty'
  | Pervasives.Error _ => True
  end.
Proof.
  intro H;
  destruct ty eqn:Ety;
    try match goal with
    | |- context[Script_typed_ir.Pair_key] =>
      (* We admit the [Pair_key] case for now as there should be changes to the
         annotations. *)
      apply axiom
    end;
    simpl;
    destruct Alpha_context.Gas.consume; simpl; trivial;
    destruct (_ >i _); simpl; trivial;
      destruct Script_ir_annot.check_type_annot; simpl; trivial;
      try (rewrite H; reflexivity); simpl in *;
        [destruct H as [?H1 [?H2 [?H3 ?H4]]] | destruct H as [?H1 [?H2 ?H3]]].
  { 
    (* let node2 := fresh "node2" in *)
    (* let node1 := fresh "node1" in *)
    (* let En1 := fresh "En1" in *)
    (* let En2 := fresh "En2" in *)
    (* let Hc1 := fresh "Hc1" in *)
    (* let Hc2 := fresh "Hc2" in *)
    (* let Hvc1 := fresh "Hvc1" in *)
    (* let Hvc2 := fresh "Hvc2" in *)
    (* let fc1 := fresh "c1" in *)
    (* let fc2 := fresh "c2" in *)
    (* let floc := fresh "loc" in *)
    (* let Epcaux2 := fresh "Epcaux2" in *)
    (* let Epcaux1 := fresh "Epcaux1" in *)
    (* let fEloc := fresh "Eloc" in *)
    (* match goal with *)
    (* | H : Alpha_context.Script.location |- _ => *)
    (*   remember H as floc; *)
    (*   subst H *)
    (* end; *)
    (* match goal with *)
    (* | H : Valid.t ?c |- context [ Script_typed_ir.Union_key ?c _ _ = _ ] => *)
    (*   remember (unparse_comparable_ty_uncarbonated _ c) as node1; *)
    (*   remember H as Hvc1; *)
    (*   remember c as fc1; *)
    (*   subst c; *)
    (*   subst H *)
    (* end; *)
    (* match goal with *)
    (* | H : Valid.t ?c |- context [ Script_typed_ir.Union_key _ ?c _ = _ ] => *)
    (*   remember (unparse_comparable_ty_uncarbonated _ c) as node2; *)
    (*   remember H as Hvc2; *)
    (*   remember c as fc2; *)
    (*   subst c; *)
    (*   subst H *)
    (* end; *)
    (* destruct (Script_ir_annot.extract_field_annot node1) eqn:En1; *)
    (*   [|simpl;trivial]; *)
    (* destruct (Script_ir_annot.extract_field_annot node2) eqn:En2; *)
    (*   [|destruct p;simpl;trivial]; *)
    (* Tactics.destruct_pairs; *)
    (* apply extract_annot_eq in En1, En2; simpl; rewrite <- En1, <- En2; *)
    (* clear En1 En2; *)
    (* match goal with *)
    (* | |- context [ parse_comparable_ty_aux ?sd ?ctx node2 ] => *)
    (*   specialize ( *)
    (*     parse_comparable_ty_aux_unparse_comparable_ty_uncarbonated *)
    (*       floc _ ctx sd Hvc2) as Hc2 *)
    (* end; *)
    (* match goal with *)
    (* | H : node2 = unparse_comparable_ty_uncarbonated _ _ |- _ => rewrite <- H in Hc2 *)
    (* end; *)
    (* destruct (parse_comparable_ty_aux _ _ node2) eqn:Epcaux2; *)
    (* simpl; [|trivial]; Tactics.destruct_pairs; *)
    (* match goal with *)
    (* | H : match ?e with _ => _ end |- _ => destruct e *)
    (* end; *)
    (* match goal with *)
    (* | |- context [ parse_comparable_ty_aux ?sd ?ctx node1 ] => *)
    (*   specialize ( *)
    (*     parse_comparable_ty_aux_unparse_comparable_ty_uncarbonated *)
    (*       floc _ ctx sd Hvc1) as Hc1 *)
    (* end; *)
    (* match goal with *)
    (* | H : node1 = unparse_comparable_ty_uncarbonated _ _ |- _ => rewrite <- H in Hc1 *)
    (* end; *)
    (* destruct (parse_comparable_ty_aux _ _ node1) eqn:Epcaux1; *)
    (*   [|simpl;trivial]; Tactics.destruct_pairs; *)
    (* match goal with *)
    (* | H : match ?e with _ => _ end |- _ => destruct e *)
    (* end; *)
    (* subst; *)
    (* repeat (unfold Script_typed_ir.union_key, *)
    (*           Script_typed_ir.Type_size.compound2; simpl); *)
    (* destruct(Script_typed_ir.Type_size.of_int floc _) eqn:fEloc; *)
    (* simpl; [|trivial]; *)
    (* unfold Script_typed_ir.Type_size.of_int in fEloc; simpl in *; *)
    (*   sauto. *)
    apply axiom.
    (* destruct (_ <=? _) eqn:E; [|lia]; *)
    (*   injection fEloc as fEloc; rewrite <- fEloc. *) }
  { match goal with
    | H : Valid.t ?ty |- context [
        parse_comparable_ty_aux ?stack_depth ?ctxt
          (unparse_comparable_ty_uncarbonated ?loc ?ty) ] =>
      let H_eq := fresh "H_eq" in
      pose proof (
        parse_comparable_ty_aux_unparse_comparable_ty_uncarbonated
          loc ty ctxt stack_depth H ) as H_eq;
      destruct parse_comparable_ty_aux; simpl; trivial;
      Tactics.destruct_pairs; destruct_all ex_comparable_ty;
      rewrite <- H_eq; clear H_eq
    end.
    repeat (unfold Script_typed_ir.option_key,
      Script_typed_ir.Type_size.compound1; simpl);
    destruct Script_typed_ir.Type_size.of_int eqn:?; simpl; trivial;
    unfold Script_typed_ir.Type_size.of_int in *; simpl in *;
    destruct (_ <=i _) eqn:E; simpl in *.
    now repeat
      match goal with
      | H : return? _ = _ |- _ =>
        injection H as H; rewrite <- H
      | H : ?t = _ |- context [ Script_typed_ir.Option_key _ ?t ] =>
        rewrite H
      end.
    sauto. }
Qed.

(** We prove that [parse (unparse comparable_ty) = comparable_ty]
    (skipping annotations).
    We require metadata of given [comparable_ty] to be correct in sense
    described above ([Valid.t] predicate).
    For proof here we unfold definitions and perform case analysis for some
    simple cases until we are able to use lemma for auxillary functions
    which is proved above.
    We do not check error cases, go through success case only.
    Since we do both directions ([parse (unparse t)] and
    [unparse (parse t)]), we can be sure either both functions work right
    or they both have errors on the same cases with same results
    (kind of symmetric saying a bit more accurate..), so, these are
    most likely specification errors and we will likely just repeat them
    while building the error checking from function implementations.
*)

Lemma parse_unparse_comparable_ty :
  forall cty loc ctxt,
    Valid.t cty ->
    match unparse_comparable_ty loc ctxt cty with
    | Pervasives.Ok (node', ctxt') =>
      match parse_comparable_ty ctxt' node' with
      | Pervasives.Ok (Ex_comparable_ty cty'', ctxt'') => cty = cty''
      | Pervasives.Error _ => True
      end
    | Pervasives.Error _ => True
    end.
Proof.
  intros cty loc ctxt H;
    unfold parse_comparable_ty, unparse_comparable_ty.
  destruct Alpha_context.Gas.consume eqn:Egc; simpl;
    [|trivial].
  apply parse_comparable_ty_aux_unparse_comparable_ty_uncarbonated;
    trivial.
Qed.

Module Node.
  Module Valid.
    Fixpoint t (node : Alpha_context.Script.node)
             (loc : Alpha_context.Script.location) : Prop :=
      match node with
      | Prim l Michelson_v1_primitives.T_option [n] annot =>
          l = loc /\ t n loc
      | Prim l Michelson_v1_primitives.T_or [n;n0] annot =>
          l = loc /\ t n loc /\ t n0 loc
      | Prim l Michelson_v1_primitives.T_pair [n;n0] annot =>
          l = loc /\ t n loc /\ t n0 loc
      | Prim l _ _ _ => l = loc
      | _ => True
      end.
  End Valid.
End Node.   

Fixpoint unparse_comparable_ty_uncarbonated_parse_comparable_ty_aux
  node ctxt stack_depth loc {struct node} :
  Node.Valid.t node loc ->
  match parse_comparable_ty_aux stack_depth ctxt node with
  | Pervasives.Ok (Ex_comparable_ty comp_ty, _) =>
      let node' := unparse_comparable_ty_uncarbonated loc comp_ty in
      Alpha_context.Script.strip_annotations node' =
      Alpha_context.Script.strip_annotations node
  | Pervasives.Error _ => True
  end.
Proof.
  destruct node eqn:E; intro H; simpl in *; 
    destruct Alpha_context.Gas.consume; simpl; destruct (stack_depth >i 10000);
    simpl; trivial.
  destruct p eqn:P; simpl; trivial;
  repeat match goal with
         | |- match (match ?l0 with _ => _ end) with _ => _ end
           => destruct l0 eqn:?El0; simpl; trivial
         | |- match (let? ' _ := ?sc_ir_ann in _) with _ => _ end =>
             destruct sc_ir_ann eqn:E';
             simpl; subst; trivial
         end.
  (* Options *)
  { pose proof (unparse_comparable_ty_uncarbonated_parse_comparable_ty_aux
                  n t (stack_depth +i 1) loc) as IH.
    destruct H as [H' H'']; apply IH in H''; clear IH. 
    destruct (parse_comparable_ty_aux (stack_depth +i 1) t n) eqn:D.
    Tactics.destruct_pairs.
    destruct e; simpl; unfold Script_typed_ir.option_key; simpl;
      unfold Script_typed_ir.Type_size.compound1;
    destruct Script_typed_ir.Type_size.of_int eqn:?; simpl; [| trivial].
    subst. rewrite H''. reflexivity.
    simpl. trivial. }
  (* Unions - admit due to annotations *)
  { admit. }
  (* Pairs - problem with annotations *)
  { admit. }
  Admitted.

(** Opposite lemma.  We prove that [unparse (parse comparable_ty) = comparable_ty].
    Unions and pairs cases in auxillary fixpoint admitted due to annotations 
*)
Lemma unparse_parse_comparable_ty :
  forall node loc ctxt,
    Node.Valid.t node loc ->
    match parse_comparable_ty ctxt node with
    | Pervasives.Ok (Ex_comparable_ty comp_ty, ctxt') =>
        match unparse_comparable_ty loc ctxt' comp_ty with
        | Pervasives.Ok (node', ctxt'') =>
        Alpha_context.Script.strip_annotations node' =
        Alpha_context.Script.strip_annotations node
        | Pervasives.Error _ => True
        end                         
    | Pervasives.Error _ => True
    end.
Proof.
  intros node loc ctxt.
  unfold parse_comparable_ty.
  unfold unparse_comparable_ty.
  specialize (unparse_comparable_ty_uncarbonated_parse_comparable_ty_aux
                node ctxt 0 loc) as Epu.
  destruct (parse_comparable_ty_aux 0 ctxt node) eqn:G; [| trivial].
  Tactics.destruct_pairs; destruct e eqn:E.
  destruct (Alpha_context.Gas.consume c
    (Unparse_costs.unparse_comparable_type c0)) eqn:C; simpl; [|simpl; trivial].
  intro H; apply Epu in H; trivial.
Qed.

Definition ret_fam_to_ex_ty_entrypoints {r} (rf : ret_fam r) :
  match r with
  | Don't_parse_entrypoints => ex_ty
  | Parse_entrypoints => ex_parameter_ty_and_entrypoints
  end.
Proof.
  destruct r eqn:R.
  inversion rf.
  exact (Ex_ty (With_family.to_ty H)).
  inversion rf. 
  exact (Ex_parameter_ty_and_entrypoints
          {|
            ex_parameter_ty_and_entrypoints.Ex_parameter_ty_and_entrypoints.arg_type
              := With_family.to_ty H;
            ex_parameter_ty_and_entrypoints.Ex_parameter_ty_and_entrypoints.entrypoints
            := H0 |}).
Defined.

Lemma fuel_m : forall fuel:nat, (fuel <= 10001)%nat ->
                         (Z.pos_sub 10001 (Pos.of_succ_nat fuel) +i 1)%Z =
                         (10001 - Z.of_nat fuel).
Proof. intros fuel fuel_valid; cbv in fuel_valid;
         unfold "+i"; rewrite Pervasives.normalize_identity;
         specialize (Z.pos_sub_spec 10001 (Pos.of_succ_nat fuel)) as ZP;
       destruct (10001 ?= Pos.of_succ_nat fuel)%positive eqn:DPP;
       rewrite ZP; try lia. try apply Pos.compare_eq in DPP; lia.
       assert (YT : (10001 < Pos.of_succ_nat fuel)%positive).
       apply DPP. lia.
         apply Pos.compare_gt_iff in DPP. lia.
Qed.

(** We prove that parse_comparable_ty_aux and 
    dep_parse_comparable_ty_aux_eq are equivalent 
*)
Fixpoint parse_comparable_ty_aux_dep_parse_comparable_ty_aux_eq
          (fuel : nat) (ctxt : Alpha_context.context)
          (node_value : Alpha_context.Script.node) 
  : (fuel <= 10001)%nat ->
    match dep_parse_comparable_ty_aux fuel ctxt node_value,
      parse_comparable_ty_aux (10001 - Z.of_nat fuel) ctxt node_value with
    | Pervasives.Ok ((Ex_family_comparable_ty res), c),
      Pervasives.Ok ((Ex_comparable_ty res'),c') =>
        (With_family.to_comparable_ty res) = res' /\ c = c'
    | Pervasives.Error e, Pervasives.Error e' => e = e'
    | _ , _ => False
    end.
Proof.
  intro fuel_valid; destruct fuel eqn:F; simpl; 
    destruct Alpha_context.Gas.consume eqn:GC; simpl;
    destruct node_value eqn:NV; simpl; rewrite GC; simpl;
    try reflexivity; try discriminate;
    repeat ( 
        destruct (Z.pos_sub 10001 (Pos.of_succ_nat n) >i 10000) eqn:POS;
        simpl; unfold ">i" in POS; simpl in POS;
        assert (G : Z.pos_sub 10001 (Pos.of_succ_nat n) <= 10000) by
          ( specialize (Z.pos_sub_spec 10001 (Pos.of_succ_nat n)) as ZP;
            destruct (10001 ?= Pos.of_succ_nat n)%positive eqn:DPP; lia
          ); try lia; try reflexivity);
    assert (G' : (Z.pos_sub 10001 (Pos.of_succ_nat n) +i 1) =
                   (10001 - Z.of_nat n)) by (apply fuel_m; lia).
  destruct p eqn:TP;
    simpl; try reflexivity; destruct l0 eqn:L0; try easy;
    destruct Script_ir_annot.check_type_annot eqn:CTA; try easy.
  (* option *) 
  destruct l1 eqn:L1; [simpl | easy].
  pose proof (parse_comparable_ty_aux_dep_parse_comparable_ty_aux_eq
                n t n0);
   assert (Y: (n <= Init.Nat.of_num_uint (Number.UIntDecimal
              (Decimal.D1 (Decimal.D0 (Decimal.D0 (Decimal.D0
              (Decimal.D1 Decimal.Nil)))))))%nat) by lia; apply H in Y; clear H;
    rename Y into H. 
  destruct (dep_parse_comparable_ty_aux n t n0) eqn:DP; simpl.
  destruct p0; destruct e.
  unfold Script_typed_ir.option_key; simpl. 
  unfold Script_typed_ir.Type_size.compound1.
  unfold  Script_typed_ir.Type_size.of_int, "<=i"; simpl;
    destruct (_ <=? _) eqn:DLE; simpl; rewrite G'; 
    destruct (parse_comparable_ty_aux (10001 - Z.of_nat n) t n0) eqn:DE; simpl.
  destruct p0; destruct e; destruct H; rewrite H in DLE; rewrite DLE; simpl. 
  rewrite H; split; easy; lia. lia. 
  destruct p0; destruct e; destruct H; subst; rewrite DLE; simpl; reflexivity.
  lia. rewrite G'; destruct (parse_comparable_ty_aux (10001 - Z.of_nat n) t n0)
                            eqn:DE; simpl; lia.
  destruct l1 eqn:L1; simpl; easy.
  (* union *) 
  destruct l1 eqn:L1; try easy. destruct l2 eqn:L2; try easy; simpl.
  destruct Script_ir_annot.remove_field_annot; simpl; try reflexivity. 
  destruct (Script_ir_annot.remove_field_annot n1); simpl; try reflexivity. 
  pose proof (parse_comparable_ty_aux_dep_parse_comparable_ty_aux_eq n t n3).
  assert (Y: (n <= Init.Nat.of_num_uint (Number.UIntDecimal
              (Decimal.D1 (Decimal.D0 (Decimal.D0 (Decimal.D0
              (Decimal.D1 Decimal.Nil)))))))%nat) by lia; apply H in Y; clear H;
    rename Y into H. 
  destruct (dep_parse_comparable_ty_aux n t n3) eqn:DP; simpl.
  destruct p0; destruct e.
  pose proof (parse_comparable_ty_aux_dep_parse_comparable_ty_aux_eq n c n2).
  assert (Y': (n <= Init.Nat.of_num_uint (Number.UIntDecimal
           (Decimal.D1 (Decimal.D0 (Decimal.D0 (Decimal.D0
             (Decimal.D1 Decimal.Nil)))))))%nat) by lia; apply H0 in Y'; clear H0;
    rename Y' into H0.
  destruct (dep_parse_comparable_ty_aux n c n2) eqn:DP2; simpl.
  destruct p0; destruct e.
  unfold Script_typed_ir.union_key; simpl. 
  unfold Script_typed_ir.Type_size.compound2, Script_typed_ir.Type_size.of_int,
    "<=i"; simpl; destruct (_ <=? _) eqn:DLE; simpl; rewrite G'.
  destruct (parse_comparable_ty_aux (10001 - Z.of_nat n) t n3) eqn:PC. 
  destruct p0; destruct e; unfold "let? _ := _ in _" at 1; destruct H;
    rewrite <- H1.
  destruct (parse_comparable_ty_aux (10001 - Z.of_nat n) c n2) eqn:PE; simpl;
    try lia;
    destruct p0; destruct e; destruct H0; subst. rewrite DLE; simpl; split; lia.
  simpl; tauto. 
  destruct (parse_comparable_ty_aux (10001 - Z.of_nat n) t n3) eqn:PC; try lia. 
  destruct p0; destruct e; unfold "let? _ := _ in _" at 1; destruct H;
    rewrite <- H1.
  destruct (parse_comparable_ty_aux (10001 - Z.of_nat n) c n2) eqn:PE; simpl;
    try lia.
  destruct p0; destruct e; destruct H0; subst. rewrite DLE; simpl; split; lia.
  rewrite G'.
  destruct (parse_comparable_ty_aux (10001 - Z.of_nat n) t n3) eqn:PC; try lia. 
  destruct p0; destruct e; unfold "let? _ := _ in _" at 1; destruct H;
    rewrite <- H1.
  destruct (parse_comparable_ty_aux (10001 - Z.of_nat n) c n2) eqn:PE; simpl; try lia. 
  rewrite G'. 
  destruct (parse_comparable_ty_aux (10001 - Z.of_nat n) t n3) eqn:PC1; try lia.
  destruct l1; simpl. try reflexivity. 
  destruct l1; simpl; try reflexivity.
  (* pairs *)
  simpl; destruct Script_ir_annot.remove_field_annot; simpl; [| lia].
  (** we have 1 subgoal at this point, 
      L0 : l0 = n0 :: l1
      NV : node_value = Prim l Michelson_v1_primitives.T_pair (n0 :: l1) a

      n0 - is a node for _left member of pair.

      for right member of pair (_right) we destruct l1: 
      if l1 = [] - we have error: DESTR_FOR_RIGHT : l1 = []
      if l1 = [n1] - we do not have recursion, retrieve types from nodes
      if l1 = _right :: _ :: _ => 
                return? Prim l Michelson_v1_primitives.T_pair l1 nil
      if l1 is a list of nodes, we return pair with `annot = nil`, 
      it is a recursive case.
   *)
  destruct l1 eqn:DESTR_FOR_RIGHT; simpl.
  { (**  DESTR_FOR_RIGHT : l1 = [] we have only one node inside pair, 
         we mush show a contradiction *)
    destruct n eqn:DN; simpl; destruct (Alpha_context.Gas.consume t
      Typecheck_costs.parse_type_cycle) eqn:A; simpl; try reflexivity.
    destruct (Z.pos_sub 10001 (Pos.succ (Pos.of_succ_nat n2)) +i 1 >i 10000)
      eqn:L; try reflexivity. 
    simpl; cbv in fuel_valid; unfold ">i", "+i" in *.
    rewrite Pervasives.normalize_identity in L; simpl in L;
      specialize (Z.pos_sub_spec 10001 (Pos.succ (Pos.of_succ_nat n2))) as ZP;
      destruct ((10001 ?= Pos.succ (Pos.of_succ_nat n2))%positive) eqn:DD; lia. 
  }
  (**  we need to destruct l2:
       DESTR_FOR_RIGHT : l1 = n2 :: l2
       L0 : l0 = n0 :: n2 :: l2
       NV : node_value = Prim l Michelson_v1_primitives.T_pair 
                           (n0 :: n2 :: l2) a
       if l2 = [] - not a recursive case, we have 2 regular nodes, 
       if l2 = list, then _right element := T_pair with whole l1 inside,
       and according to definitions in this case we just treat whole _right
       as recursively retrieved result 
   *)
  { destruct l2 eqn:DL2.
    { (* l2 = [] - exactly 2 nodes, not a recursive case *)
      destruct (Script_ir_annot.remove_field_annot n2) eqn:R; simpl; try lia.
      pose proof (parse_comparable_ty_aux_dep_parse_comparable_ty_aux_eq
                    n t n3).
      destruct (dep_parse_comparable_ty_aux n t n3) eqn:DP; simpl;
               assert (Y: (n <= Init.Nat.of_num_uint
                               (Number.UIntDecimal
                               (Decimal.D1 (Decimal.D0 (Decimal.D0
                               (Decimal.D0 (Decimal.D1 Decimal.Nil)))))))%nat) by
               lia; apply H in Y; clear H; rename Y into H; try tauto.
      destruct p0. destruct e. 
      pose proof (parse_comparable_ty_aux_dep_parse_comparable_ty_aux_eq
                    n c n1).
      destruct (dep_parse_comparable_ty_aux n c n1) eqn:DP2; simpl;
               assert (Y': (n <= Init.Nat.of_num_uint (Number.UIntDecimal
                                (Decimal.D1 (Decimal.D0
                                (Decimal.D0 (Decimal.D0 (Decimal.D1
                                Decimal.Nil)))))))%nat) by lia; apply H0 in Y';
        clear H0; rename Y' into H0; try tauto.
      destruct p0. destruct e.
      unfold Script_typed_ir.pair_key; simpl.
      unfold Script_typed_ir.Type_size.compound2.
      unfold  Script_typed_ir.Type_size.of_int, "<=i"; simpl;
        destruct (_ <=? _) eqn:DLE; simpl; rewrite G'.
      destruct (parse_comparable_ty_aux (10001 - Z.of_nat n) t n3) eqn:TN.
      unfold "let? _ := _ in _" at 1. destruct p0; destruct e; destruct H;
      subst.
      destruct (parse_comparable_ty_aux (10001 - Z.of_nat n) c3 n1) eqn:P;
        simpl. destruct p; destruct e; destruct H0; subst.
      rewrite DLE; simpl.
      split; lia. tauto. simpl. tauto. 
      destruct (parse_comparable_ty_aux (10001 - Z.of_nat n) t n3) eqn:U.
      unfold "let? _ := _ in _" at 1. destruct p0; destruct e.
      destruct H; subst.
      destruct (parse_comparable_ty_aux (10001 - Z.of_nat n) c3 n1) eqn:L;
      simpl. destruct p; destruct e; destruct H0; subst; rewrite DLE;
      simpl; reflexivity. tauto. simpl. tauto.
      rewrite G'. destruct (parse_comparable_ty_aux (10001 - Z.of_nat n) t n3)
                           eqn:KK.
      unfold "let? _ := _ in _" at 1. destruct p0; destruct e.
      destruct H; subst.
      destruct (parse_comparable_ty_aux (10001 - Z.of_nat n) c1 n1) eqn:J.
      tauto. simpl. lia. tauto.
      rewrite G'.
      destruct (parse_comparable_ty_aux (10001 - Z.of_nat n) t n3) eqn:KI.
      tauto. simpl. lia. 
    }
    { (** l2 = list - recursive case *)
      destruct (return? Prim l Michelson_v1_primitives.T_pair
                      (n2 :: n3 :: l3) nil) eqn:TRY;
        simpl; try tauto.
      pose proof (parse_comparable_ty_aux_dep_parse_comparable_ty_aux_eq
                    n t n4).
      destruct (dep_parse_comparable_ty_aux n t n4) eqn:DP; simpl;
        assert (Y: (n <= Init.Nat.of_num_uint
                        (Number.UIntDecimal
                        (Decimal.D1 (Decimal.D0
                        (Decimal.D0 (Decimal.D0 (Decimal.D1
                        Decimal.Nil)))))))%nat) by lia; apply H in Y; clear H;
        rename Y into H; try tauto.
      destruct p0; destruct e. 
      pose proof (parse_comparable_ty_aux_dep_parse_comparable_ty_aux_eq
                    n c n1).
      destruct (dep_parse_comparable_ty_aux n c n1) eqn:DP2; simpl;
        assert (Y': (n <= Init.Nat.of_num_uint
                         (Number.UIntDecimal
                         (Decimal.D1 (Decimal.D0
                         (Decimal.D0 (Decimal.D0 (Decimal.D1
                         Decimal.Nil)))))))%nat) by
        lia; apply H0 in Y'; clear H0; rename Y' into H0; try tauto.
      destruct p0; destruct e.
      unfold Script_typed_ir.pair_key; simpl.
      unfold Script_typed_ir.Type_size.compound2.
      unfold  Script_typed_ir.Type_size.of_int, "<=i"; simpl;
        destruct (_ <=? _) eqn:DLE; simpl.
      rewrite G'.
      destruct (parse_comparable_ty_aux (10001 - Z.of_nat n) t n4) eqn:TN.
      unfold "let? _ := _ in _" at 1. destruct p0; destruct e. 
      destruct H. subst.
      destruct (parse_comparable_ty_aux (10001 - Z.of_nat n) c3 n1) eqn:P;
        simpl.
      destruct p; destruct e; destruct H0; subst; rewrite DLE; simpl.
      split; lia. tauto. simpl. tauto.
      destruct (parse_comparable_ty_aux (10001 - Z.of_nat n) t n4) eqn:R; simpl.
      destruct p0; destruct e. destruct H. rewrite G'.
      rewrite R. unfold "let? _ := _ in _" at 1.
      subst. 
      destruct (parse_comparable_ty_aux (10001 - Z.of_nat n) c3 n1) eqn:RE;
        simpl. destruct p; destruct e; destruct H0; subst; rewrite DLE. simpl.
      reflexivity. tauto. tauto.
      rewrite G'. destruct (parse_comparable_ty_aux (10001 - Z.of_nat n) t n4)
                           eqn:O.
      unfold "let? _ := _ in _" at 1. destruct p0; destruct e.
      destruct H; subst.
      destruct (parse_comparable_ty_aux (10001 - Z.of_nat n) c1 n1) eqn:K.
      simpl. destruct p. destruct e. tauto. simpl. lia. simpl. tauto. 
      rewrite G'.
      destruct (parse_comparable_ty_aux (10001 - Z.of_nat n) t n4) eqn:I.
      tauto. simpl. auto.
    }
  }
Qed.
