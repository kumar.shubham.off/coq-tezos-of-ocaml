Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Indexable.

Require Import TezosOfOCaml.Proto_alpha.Environment.Proofs.Compare.
Require Import TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.

Module Valid.
  Definition t {a : Set} (x : Indexable.t a) : Prop :=
    match x with
    | Indexable.Value _ | Indexable.Hidden_value _ => True
    | Indexable.Index index | Indexable.Hidden_index index =>
      Int32.Valid.t index
    end.
End Valid.

Module Index.
  Module Valid.
    Definition t {a : Set} (x : Indexable.t a) : Prop :=
      match x with
      | Indexable.Index _ => Valid.t x
      | _ => False
      end.
  End Valid.
End Index.

Module Value.
  Module Valid.
    Definition t {a : Set} (x : Indexable.t a) : Prop :=
      match x with
      | Indexable.Value _ => Valid.t x
      | _ => False
      end.
  End Valid.
End Value.

Lemma encoding_is_valid {a : Set} (val_encoding : Data_encoding.t a) :
  Data_encoding.Valid.t (fun _ => True) val_encoding -> 
  Data_encoding.Valid.t Valid.t (Indexable.encoding val_encoding).
Admitted.

Lemma compare_is_valid {a : Set} (c_value : a -> a -> int) :
  Compare.Valid.t id c_value ->
  Compare.Valid.t id (Indexable.compare c_value).
Proof.
  (* TODO *)
Admitted.

Lemma compare_values_is_valid {a : Set} (c_value : a -> a -> int) :
  Compare.Valid.t id c_value ->
  Compare.Valid.t id (Indexable.compare_values c_value).
Proof.
Admitted.

Module VALUE.
  Module Valid.
    Import TezosOfOCaml.Proto_alpha.Indexable.VALUE.

    Record t {t : Set} (V : Indexable.VALUE (t := t)) : Prop := {
      encoding : Data_encoding.Valid.t (fun _ => True) V.(encoding);
      compare : Compare.Valid.t id V.(compare);
    }.
  End Valid.
End VALUE.

Module INDEXABLE.
  Module Valid.
    Import TezosOfOCaml.Proto_alpha.Indexable.INDEXABLE.

    Record t {v_t : Set} (I : Indexable.INDEXABLE (v_t := v_t)) : Prop := {
      encoding : Data_encoding.Valid.t Valid.t I.(encoding);
      index_encoding : Data_encoding.Valid.t Index.Valid.t I.(index_encoding);
      value_encoding : Data_encoding.Valid.t Value.Valid.t I.(value_encoding);
      compare : Compare.Valid.t id I.(compare);
      compare_values : Compare.Valid.t id I.(compare_values);
    }.
  End Valid.
End INDEXABLE.

Lemma Make_is_valid {t : Set} (V : Indexable.VALUE (t := t)) : 
  VALUE.Valid.t V -> INDEXABLE.Valid.t (Indexable.Make V).
Proof.
  intro H.
  constructor.
  { apply encoding_is_valid.
    apply H.
  }
  { Data_encoding.Valid.data_encoding_auto.
    hauto lq: on.
  }
  { Data_encoding.Valid.data_encoding_auto; try apply H.
    hauto l: on.
  }
  { apply compare_is_valid.
    apply H.
  }
  { apply compare_values_is_valid.
    apply H.
  }
Qed.
