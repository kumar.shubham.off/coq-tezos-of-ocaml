Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Vote_storage.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Signature.
Require TezosOfOCaml.Proto_alpha.Proofs.Storage.
Require Import TezosOfOCaml.Proto_alpha.Proofs.Storage_sigs.
Require TezosOfOCaml.Proto_alpha.Proofs.Utils.

Definition simple_recorded_proposal_count_for_delegate key ctxt proposer : M? int :=
  let? count :=
    Indexed_data_storage.Op.find key 
      (Storage.Eq.Votes.Proposals_count.parse ctxt) proposer in
  return? Option.value_value count 0.

Lemma simple_recorded_proposal_count_for_delegate_eq key ctxt proposer
  : Vote_storage.recorded_proposal_count_for_delegate ctxt proposer =
    (simple_recorded_proposal_count_for_delegate key ctxt proposer).
  unfold simple_recorded_proposal_count_for_delegate,
    Vote_storage.recorded_proposal_count_for_delegate.
  rewrite (Storage.Eq.Votes.Proposals_count.eq key).(
    Indexed_data_storage.Eq.find).
  simpl. reflexivity.
Qed.

Definition simple_record_proposal key ctxt proposal proposer : M? Raw_context.t :=
  let? count := simple_recorded_proposal_count_for_delegate key ctxt proposer in
  let ctxt :=
    Storage.Eq.Votes.Proposals_count.apply ctxt
      (Indexed_data_storage.Op.add proposer (count +i 1)) in
  op_gtpipeeq
  (Storage.Eq.Votes.Proposals.apply ctxt
    (Data_set_storage.Op.add (proposal, proposer))) Error_monad.ok.

Lemma simple_record_proposal_eq key ctxt proposal proposer
  : Vote_storage.record_proposal ctxt proposal proposer =
    simple_record_proposal key ctxt proposal proposer.
  unfold simple_record_proposal, Vote_storage.record_proposal.
  rewrite (simple_recorded_proposal_count_for_delegate_eq key); simpl.
  unfold op_gtgtquestion.
  destruct simple_recorded_proposal_count_for_delegate eqn:?; trivial.
  rewrite (Storage.Eq.Votes.Proposals_count.eq key).(
    Indexed_data_storage.Eq.add); simpl.
  now rewrite Storage.Eq.Votes.Proposals.eq.(
    Data_set_storage.Eq.add).
Qed.

(** We write this proof as an example of the technique of using simulations to
    represent the storage. We apply some lemmas to reduce the problem to
    reasonings over maps. *)
(* TODO: This lemma broke after the changes in Proofs/Storage.v. Fixing it in another MR. *)
Lemma record_proposal_not_idempotent ctxt proposal proposer
  : let key := ["votes"] in
    letP? ctxt' := simple_record_proposal key ctxt proposal proposer in
    letP? ctxt'' := simple_record_proposal key ctxt' proposal proposer in
    ctxt <> ctxt''.
  simpl. unfold simple_record_proposal, simple_recorded_proposal_count_for_delegate.
  simpl. unfold op_gtgtquestion.
  Abort.
  (*
  assert (H_proposals_count :
    Indexed_data_storage.Op.find
      (Storage.Eq.Votes.Proposals_count.parse ctxt)
      proposer <>
    Indexed_data_storage.Op.find
      (Storage.Eq.Votes.Proposals_count.parse ctxt'')
      proposer
  ).
  { unfold ctxt'', ctxt', simple_record_proposal,
      Storage.Eq.Votes.Proposals_count.apply,
      Storage.Eq.Votes.Proposals_count.parse,
      Storage.Eq.Votes.Proposals.apply,
      simple_recorded_proposal_count_for_delegate.
    repeat (rewrite Storage.parse_apply; simpl).
    unfold Storage.Eq.Votes.Proposals_count.parse.
    repeat (rewrite Storage.parse_apply; simpl).
    set (proposals_count := _.(Storage.Simulation.Votes.proposals_count)).
    unfold Indexed_data_storage.Op.find, Indexed_data_storage.State.Map; simpl.
    do 2 rewrite Map.find_add; simpl.
    destruct (Make.find _ _); simpl; try congruence.
    assert (i <> i +i 1 +i 1) by lia.
    congruence.
  }
  { congruence. }
Qed. *)

Module Ballots.
  Module Valid.
    Import Vote_storage.ballots.

    Record t (x : Vote_storage.ballots) : Prop := {
      yay : Int32.Valid.t x.(yay);
      nay : Int32.Valid.t x.(nay);
      pass : Int32.Valid.t x.(pass);
    }.
  End Valid.
End Ballots.

Lemma ballots_encoding_is_valid :
  Data_encoding.Valid.t Ballots.Valid.t Vote_storage.ballots_encoding.
  Data_encoding.Valid.data_encoding_auto.
  intros [] []; simpl in *; repeat split; trivial; lia.
Qed.
#[global] Hint Resolve ballots_encoding_is_valid : Data_encoding_db.

Module Listings.
  Module Valid.
    Definition t (l : list (Signature.public_key_hash * int32)) : Prop :=
      List.Forall (fun '(_, rolls) => Int32.Valid.t rolls) l.
  End Valid.
End Listings.

Lemma listings_encoding_is_valid :
  Data_encoding.Valid.t Listings.Valid.t Vote_storage.listings_encoding.
  Data_encoding.Valid.data_encoding_auto.
  apply List.Forall_impl; intros []; lia.
Qed.
#[global] Hint Resolve listings_encoding_is_valid : Data_encoding_db.
