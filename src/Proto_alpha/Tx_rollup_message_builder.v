(** File generated by coq-of-ocaml *)
Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Gas_limit_repr.
Require TezosOfOCaml.Proto_alpha.Raw_context.
Require TezosOfOCaml.Proto_alpha.Tx_rollup_gas.
Require TezosOfOCaml.Proto_alpha.Tx_rollup_message_repr.

Definition hash_value (ctxt : Raw_context.t) (msg : Tx_rollup_message_repr.t)
  : M? (Raw_context.t * Tx_rollup_message_repr.hash) :=
  let? cost :=
    Tx_rollup_gas.message_hash_cost (Tx_rollup_message_repr.size_value msg) in
  let? ctxt := Raw_context.consume_gas ctxt cost in
  return? (ctxt, (Tx_rollup_message_repr.hash_uncarbonated msg)).
