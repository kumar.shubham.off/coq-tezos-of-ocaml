Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Frozen_deposits_storage.
Require TezosOfOCaml.Proto_alpha.Contract_repr.
Require TezosOfOCaml.Proto_alpha.Storage.

Require TezosOfOCaml.Proto_alpha.Proofs.Tez_repr.

(* @TODO *)
Axiom init_implies_alocated_eq_true : forall {ctxt} {pkh},
  let contract := Contract_repr.Implicit pkh in
  match Frozen_deposits_storage.init_value ctxt pkh with
  | Pervasives.Ok ctxt' =>
    Frozen_deposits_storage.allocated ctxt' contract = true
  | Pervasives.Error _ => True
  end.

(* @TODO *)
Axiom init_implies_get_success : forall {ctxt} {pkh},
  let contract := Contract_repr.Implicit pkh in
  match Frozen_deposits_storage.init_value ctxt pkh with
  | Pervasives.Ok ctxt' =>
    Result.is_ok (Frozen_deposits_storage.get ctxt' contract) = true
  | Pervasives.Error _ => True
  end.

(* @TODO *)
Axiom init_implies_find_success : forall {ctxt} {pkh},
  let contract := Contract_repr.Implicit pkh in
  letP? ctxt' := Frozen_deposits_storage.init_value ctxt pkh in
    Result.is_ok (Frozen_deposits_storage.find ctxt' contract) = true.

(** credit equals to +Z *)
Axiom credit_only_call_from_token_eq : forall {ctxt} {pkh} {amount},
  let contract := Contract_repr.Implicit pkh in
  letP? ctxt' := Frozen_deposits_storage.init_value ctxt pkh in
  letP? ctxt'' := Frozen_deposits_storage.credit_only_call_from_token ctxt' pkh (Tez_repr.Tez_tag amount) in
  letP? frozen_deposits := Storage.Contract.Frozen_deposits.(Storage_sigs.Indexed_data_storage.get) ctxt contract in
  let 'Tez_repr.Tez_tag initial_balance := frozen_deposits.(Storage.deposits.current_amount) in
  let 'Tez_repr.Tez_tag balance := frozen_deposits.(Storage.deposits.current_amount) in
    balance = initial_balance +Z amount.

(** spend equals to -Z *)
Axiom spend_only_call_from_token_eq : forall {ctxt} {pkh} {amount},
  let contract := Contract_repr.Implicit pkh in
  letP? ctxt := Frozen_deposits_storage.init_value ctxt pkh in
  letP? ctxt := Frozen_deposits_storage.update_initial_amount ctxt contract
    (Tez_repr.Tez_tag amount) in
  letP? ctxt := Frozen_deposits_storage.spend_only_call_from_token ctxt pkh (Tez_repr.Tez_tag amount) in
  letP? frozen_deposits := Storage.Contract.Frozen_deposits.(Storage_sigs.Indexed_data_storage.get) ctxt contract in
  let 'Tez_repr.Tez_tag initial_balance := frozen_deposits.(Storage.deposits.current_amount) in
  let 'Tez_repr.Tez_tag balance := frozen_deposits.(Storage.deposits.current_amount) in
    balance = initial_balance -Z amount.

Axiom init_implies_allocated_eq_true : forall {ctxt} {pkh},
  let contract := Contract_repr.Implicit pkh in
  letP? ctxt := Frozen_deposits_storage.init_value ctxt pkh in
  let allocated := Frozen_deposits_storage.allocated ctxt contract in
  allocated = true.
