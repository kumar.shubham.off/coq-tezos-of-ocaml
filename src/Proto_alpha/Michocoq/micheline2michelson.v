Require Import Ascii Coq.Strings.String Coq.Lists.List.
Require Import primitives untyped_syntax micheline_syntax error location.
Require Import syntax_type.
Require Import Lia.
Require Coq.Program.Wf.
Import error.Notations.
Import untyped_syntax.notations.
Open Scope string.

Definition loc_micheline := loc_micheline primitive.

Definition micheline2michelson_sctype (bem : loc_micheline) : M simple_comparable_type :=
  let 'Mk_loc_micheline ((b, e), m) := bem in
  match m with
  | PRIM (_, T_string) _ nil => Return string
  | PRIM (_, T_nat) _ nil => Return nat
  | PRIM (_, T_int) _ nil => Return int
  | PRIM (_, T_bytes) _ nil => Return bytes
  | PRIM (_, T_bool) _ nil => Return bool
  | PRIM (_, T_mutez) _ nil => Return mutez
  | PRIM (_, T_key_hash) _ nil => Return key_hash
  | PRIM (_, T_timestamp) _ nil => Return timestamp
  | PRIM (_, T_address) _ nil => Return address
  | PRIM (_, T_never) _ nil => Return never
  | PRIM (_, T_unit) _ nil => Return unit
  | PRIM (_, T_key) _ nil => Return key
  | PRIM (_, T_signature) _ nil => Return signature
  | PRIM (_, T_chain_id) _ nil => Return chain_id
  | _ => Failed _ (Expansion b e)
  end.

Definition extract_one_field_annot_from_list
           (annots : Datatypes.list (location * location * annotation)) :
  M annot_o :=
  match annots with
  | nil => Return None
  | (_, _, annot) :: nil => Return (Some annot)
  | _ :: (b, e, _) :: _ => Failed _ (Expansion b e)
  end.

Definition field_annot_opt (annot : micheline_syntax.annotation) : Datatypes.option (location * location * annotation) :=
  let 'Mk_annot (b, e, s) := annot in
  let? H := assume_opt (is_field_annotation s) in
  Some (b, e, Mk_field_annotation s H).

Definition extract_one_field_annot (bem : loc_micheline) : M annot_o :=
  let 'Mk_loc_micheline ((b, e), m) := bem in
  match m with
  | PRIM p annots args =>
    let filtered_annots := base.list_filter_map field_annot_opt annots in
    extract_one_field_annot_from_list filtered_annots
  | _ => Return None
  end.

Fixpoint micheline2michelson_ctype_comb a b l {struct l} :=
  Cpair a (match l with
          | nil => b
          | c :: l => micheline2michelson_ctype_comb b c l
          end).

Fixpoint micheline2michelson_ctype (bem : loc_micheline) : M comparable_type :=
  let 'Mk_loc_micheline ((b, e), m) := bem in
  match m with
  | PRIM (_, T_pair) _ (a :: b :: l) =>
    let! a := micheline2michelson_ctype a in
    let! b := micheline2michelson_ctype b in
    let! l := error.list_map micheline2michelson_ctype l in
    Return (micheline2michelson_ctype_comb a b l)
  | PRIM (_, T_option) _ (a :: nil) =>
    let! a := micheline2michelson_ctype a in
    Return (Coption a)
  | PRIM (_, T_or) _ (a :: b :: nil) =>
    let! an := extract_one_field_annot a in
    let! bn := extract_one_field_annot b in
    let! a := micheline2michelson_ctype a in
    let! b := micheline2michelson_ctype b in
    Return (Cor a b)
  | _ =>
    let! a := micheline2michelson_sctype bem in
    Return (Comparable_type_simple a)
  end.

Fixpoint micheline2michelson_type_comb
         a b l {struct l} :=
  pair a (
  match l with
  | nil => b
  | c :: l =>
    micheline2michelson_type_comb b c l
  end).

Fixpoint micheline2michelson_type (bem : loc_micheline) : M type :=
  try (let! ty := micheline2michelson_sctype bem in Return (Comparable_type ty))
      (let 'Mk_loc_micheline ((b, e), m) := bem in
       match m with
       | PRIM (_, T_operation) _ nil => Return operation
       | PRIM (_, T_option) _ (a :: nil) =>
        let! a := micheline2michelson_type a in
        Return (option a)
       | PRIM (_, T_list) _ (a :: nil) =>
        let! a := micheline2michelson_type a in
        Return (list a)
       | PRIM (_, T_contract) _ (a :: nil) =>
        let! a := micheline2michelson_type a in
        Return (contract a)
       | PRIM (_, T_set) _ (a :: nil) =>
        let! a := micheline2michelson_ctype a in
        Return (set a)
       | PRIM (_, T_pair) _ (a :: b :: l) =>
        let! a := micheline2michelson_type a in
        let! b := micheline2michelson_type b in
        let! l := error.list_map micheline2michelson_type l in
        Return (micheline2michelson_type_comb a b l)
       | PRIM (_, T_or) _ (a :: b :: nil) =>
        let! a := micheline2michelson_type a in
        let! b := micheline2michelson_type b in
        Return (or a b)
       | PRIM (_, T_lambda) _ (a :: b :: nil) =>
        let! a := micheline2michelson_type a in
        let! b := micheline2michelson_type b in
        Return (lambda a b)
       | PRIM (_, T_map) _ (a :: b :: nil) =>
        let! a := micheline2michelson_ctype a in
        let! b := micheline2michelson_type b in
        Return (map a b)
       | PRIM (_, T_big_map) _ (a :: b :: nil) =>
        let! a := micheline2michelson_ctype a in
        let! b := micheline2michelson_type b in
        Return (big_map a b)
       | PRIM (_, T_ticket) _ (a :: nil) =>
        let! a := micheline2michelson_ctype a in
        Return (ticket a)
       | PRIM (_, T_sapling_state) _ (Mk_loc_micheline (_, NUMBER _ ms) :: nil) =>
        let ms := BinInt.Z.to_nat ms in
        Return (sapling_state ms)
       | PRIM (_, T_sapling_transaction) _ (Mk_loc_micheline (_, NUMBER _ ms) :: nil) =>
        let ms := BinInt.Z.to_nat ms in
        Return (sapling_transaction ms)
       | PRIM (_, T_bls12_381_fr) _ nil => Return bls12_381_fr
       | PRIM (_, T_bls12_381_g1) _ nil => Return bls12_381_g1
       | PRIM (_, T_bls12_381_g2) _ nil => Return bls12_381_g2
       | _ => Failed _ (Expansion b e)
       end).

Fixpoint micheline2michelson_ep (bem : loc_micheline)
  : M entrypoints.entrypoint_tree :=
  let 'Mk_loc_micheline ((b, e), m) := bem in
  match m with
  | PRIM (_, T_or) _ (a :: b :: nil) =>
    let! an := extract_one_field_annot a in
    let! bn := extract_one_field_annot b in
    let! a := micheline2michelson_ep a in
    let! b := micheline2michelson_ep b in
    Return (entrypoints.ep_node a an b bn)
  | _ =>
    let! a := micheline2michelson_type bem in
    Return (entrypoints.ep_leaf a)
  end.

Definition return_instruction (i : instruction) : M instruction :=
  Return i.

Definition return_opcode (op : opcode) : M instruction :=
  return_instruction (instruction_opcode op).

Definition micheline2michelson_instructions micheline2michelson_instruction :
  Datatypes.list loc_micheline -> M instruction_seq :=
  fix micheline2michelson_instructions (l : Datatypes.list loc_micheline) :=
  match l with
  | nil => Return NOOP
  | i1 :: i2 =>
    let! i1 := micheline2michelson_instruction i1 in
    let! i2 := micheline2michelson_instructions i2 in
    Return (i1 ;; i2)
  end.

Definition micheline2michelson_instruction_seq_aux micheline2michelson_instruction
           (bem : loc_micheline) : M instruction_seq :=
  let 'Mk_loc_micheline ((b, e), m) := bem in
  match m with
  | SEQ l => micheline2michelson_instructions micheline2michelson_instruction l
  | _ => Failed _ (Expansion b e)
  end.

Definition micheline2michelson_instruction_aux
           micheline2michelson_data : loc_micheline -> M instruction :=
  fix micheline2michelson_instruction bem :=
  let micheline2michelson_instruction_seq :=
      micheline2michelson_instruction_seq_aux micheline2michelson_instruction
  in
  let 'Mk_loc_micheline ((b, e), m) := bem in
  match m with
  | SEQ l =>
    let! l := micheline2michelson_instructions micheline2michelson_instruction l in
    Return (Instruction_seq l)
  | PRIM (_, I_FAILWITH) _ nil => return_instruction FAILWITH
  | PRIM (_, I_NEVER) _ nil => return_instruction NEVER
  | PRIM (_, I_EXEC) _ nil => return_instruction EXEC
  | PRIM (_, I_APPLY) _ nil => return_opcode APPLY
  | PRIM (_, I_DROP) _ nil => return_opcode (DROP 1)
  | PRIM (_, I_DROP) _ (Mk_loc_micheline (_, NUMBER _ n) :: nil) =>
    return_opcode (DROP (BinInt.Z.to_nat n))
  | PRIM (_, I_DUP) _ nil => return_opcode DUP
  | PRIM (_, I_DUP) _ (Mk_loc_micheline (_, NUMBER _ n) :: nil) =>
    return_opcode (DUPN (BinInt.Z.to_nat n))
  | PRIM (_, I_SWAP) _ nil => return_opcode SWAP
  | PRIM (_, I_UNIT) _ nil => return_opcode UNIT
  | PRIM (_, I_EQ) _ nil => return_opcode EQ
  | PRIM (_, I_NEQ) _ nil => return_opcode NEQ
  | PRIM (_, I_LT) _ nil => return_opcode LT
  | PRIM (_, I_GT) _ nil => return_opcode GT
  | PRIM (_, I_LE) _ nil => return_opcode LE
  | PRIM (_, I_GE) _ nil => return_opcode GE
  | PRIM (_, I_OR) _ nil => return_opcode OR
  | PRIM (_, I_AND) _ nil => return_opcode AND
  | PRIM (_, I_XOR) _ nil => return_opcode XOR
  | PRIM (_, I_NOT) _ nil => return_opcode NOT
  | PRIM (_, I_NEG) _ nil => return_opcode NEG
  | PRIM (_, I_ABS) _ nil => return_opcode ABS
  | PRIM (_, I_ISNAT) _ nil => return_opcode ISNAT
  | PRIM (_, I_INT) _ nil => return_opcode INT
  | PRIM (_, I_ADD) _ nil => return_opcode ADD
  | PRIM (_, I_SUB) _ nil => return_opcode SUB
  | PRIM (_, I_MUL) _ nil => return_opcode MUL
  | PRIM (_, I_EDIV) _ nil => return_opcode EDIV
  | PRIM (_, I_LSL) _ nil => return_opcode LSL
  | PRIM (_, I_LSR) _ nil => return_opcode LSR
  | PRIM (_, I_COMPARE) _ nil => return_opcode COMPARE
  | PRIM (_, I_CONCAT) _ nil => return_opcode CONCAT
  | PRIM (_, I_SIZE) _ nil => return_opcode SIZE
  | PRIM (_, I_SLICE) _ nil => return_opcode SLICE
  | PRIM (_, I_PAIR) _ nil => return_opcode PAIR
  | PRIM (_, I_PAIR) _ (Mk_loc_micheline (_, NUMBER _ n) :: nil) =>
    return_opcode (PAIRN (BinInt.Z.to_nat n))
  | PRIM (_, I_CAR) _ nil => return_opcode CAR
  | PRIM (_, I_CDR) _ nil => return_opcode CDR
  | PRIM (_, I_UNPAIR) _ nil => return_opcode UNPAIR
  | PRIM (_, I_UNPAIR) _ (Mk_loc_micheline (_, NUMBER _ n) :: nil) =>
    return_opcode (UNPAIRN (BinInt.Z.to_nat n))
  | PRIM (_, I_GET) _ nil => return_opcode GET
  | PRIM (_, I_GET) _ (Mk_loc_micheline (_, NUMBER _ n) :: nil) =>
    return_opcode (GETN (BinInt.Z.to_nat n))
  | PRIM (_, I_GET_AND_UPDATE) _ nil => return_opcode GET_AND_UPDATE
  | PRIM (_, I_SOME) _ nil => return_opcode SOME
  | PRIM (_, I_NONE) _ (ty :: nil) =>
    let! ty := micheline2michelson_type ty in
    return_opcode (NONE ty)
  | PRIM (_, I_LEFT) _ (ty :: nil) =>
    let! ty := micheline2michelson_type ty in
    return_opcode (LEFT ty)
  | PRIM (_, I_RIGHT) _ (ty :: nil) =>
    let! ty := micheline2michelson_type ty in
    return_opcode (RIGHT ty)
  | PRIM (_, I_CONS) _ nil => return_opcode CONS
  | PRIM (_, I_NIL) _ (ty :: nil) =>
    let! ty := micheline2michelson_type ty in
    return_opcode (NIL ty)
  | PRIM (_, I_TRANSFER_TOKENS) _ nil =>
    return_opcode TRANSFER_TOKENS
  | PRIM (_, I_SET_DELEGATE) _ nil => return_opcode SET_DELEGATE
  | PRIM (_, I_BALANCE) _ nil => return_opcode BALANCE
  | PRIM (_, I_ADDRESS) _ nil => return_opcode ADDRESS
  | PRIM (_, I_CONTRACT) _ (ty :: nil) =>
    let! an := extract_one_field_annot bem in
    let! ty := micheline2michelson_type ty in
    return_opcode (CONTRACT an ty)
  | PRIM (_, I_SOURCE) _ nil => return_opcode SOURCE
  | PRIM (_, I_SENDER) _ nil => return_opcode SENDER
  | PRIM (_, I_SELF) _ nil =>
    let! an := extract_one_field_annot bem in
    return_instruction (SELF an)
  | PRIM (_, I_AMOUNT) _ nil => return_opcode AMOUNT
  | PRIM (_, I_IMPLICIT_ACCOUNT) _ nil => return_opcode IMPLICIT_ACCOUNT
  | PRIM (_, I_NOW) _ nil => return_opcode NOW
  | PRIM (_, I_LEVEL) _ nil => return_opcode LEVEL
  | PRIM (_, I_PACK) _ nil => return_opcode PACK
  | PRIM (_, I_UNPACK) _ (ty :: nil) =>
    let! ty := micheline2michelson_type ty in
    return_opcode (UNPACK ty)
  | PRIM (_, I_HASH_KEY) _ nil => return_opcode HASH_KEY
  | PRIM (_, I_BLAKE2B) _ nil => return_opcode BLAKE2B
  | PRIM (_, I_SHA256) _ nil => return_opcode SHA256
  | PRIM (_, I_SHA512) _ nil => return_opcode SHA512
  | PRIM (_, I_KECCAK) _ nil => return_opcode KECCAK
  | PRIM (_, I_SHA3) _ nil => return_opcode SHA3
  | PRIM (_, I_CHECK_SIGNATURE) _ nil =>
    return_opcode CHECK_SIGNATURE
  | PRIM (_, I_MEM) _ nil => return_opcode MEM
  | PRIM (_, I_UPDATE) _ nil => return_opcode UPDATE
  | PRIM (_, I_UPDATE) _ (Mk_loc_micheline (_, NUMBER _ n) :: nil) =>
    return_opcode (UPDATEN (BinInt.Z.to_nat n))
  | PRIM (_, I_CHAIN_ID) _ nil => return_opcode CHAIN_ID
  | PRIM (_, I_SELF_ADDRESS) _ nil => return_opcode SELF_ADDRESS
  | PRIM (_, I_VOTING_POWER) _ nil => return_opcode VOTING_POWER
  | PRIM (_, I_TOTAL_VOTING_POWER) _ nil => return_opcode TOTAL_VOTING_POWER
  | PRIM (_, I_TICKET) _ nil => return_opcode TICKET
  | PRIM (_, I_READ_TICKET) _ nil => return_opcode READ_TICKET
  | PRIM (_, I_SPLIT_TICKET) _ nil => return_opcode SPLIT_TICKET
  | PRIM (_, I_JOIN_TICKETS) _ nil => return_opcode JOIN_TICKETS
  | PRIM (_, I_SAPLING_EMPTY_STATE) _
         (Mk_loc_micheline (_, NUMBER _ ms) :: nil) =>
    return_opcode (SAPLING_EMPTY_STATE (BinInt.Z.to_nat ms))
  | PRIM (_, I_SAPLING_VERIFY_UPDATE) _ nil =>
    return_opcode SAPLING_VERIFY_UPDATE
  | PRIM (_, I_PAIRING_CHECK) _ nil =>
    return_opcode PAIRING_CHECK
  | PRIM (_, I_LOOP) _ (i :: nil) =>
    let! i := micheline2michelson_instruction_seq i in
    return_instruction (LOOP i)
  | PRIM (_, I_LOOP_LEFT) _ (i :: nil) =>
    let! i := micheline2michelson_instruction_seq i in
    return_instruction (LOOP_LEFT i)
  | PRIM (_, I_DIP) _ (i :: nil) =>
    let! i := micheline2michelson_instruction_seq i in
    return_instruction (DIP 1 i)
  | PRIM (_, I_DIP) _ (Mk_loc_micheline (_, NUMBER _ n) :: i :: nil) =>
    let! i := micheline2michelson_instruction_seq i in
    return_instruction (DIP (BinInt.Z.to_nat n) i)
  | PRIM (_, I_DIG) _ (Mk_loc_micheline (_, NUMBER _ n) :: nil) =>
    return_opcode (DIG (BinInt.Z.to_nat n))
  | PRIM (_, I_DUG) _ (Mk_loc_micheline (_, NUMBER _ n) :: nil) =>
    return_opcode (DUG (BinInt.Z.to_nat n))
  | PRIM (_, I_ITER) _ (i :: nil) =>
    let! i := micheline2michelson_instruction_seq i in
    return_instruction (ITER i)
  | PRIM (_, I_MAP) _ (i :: nil) =>
    let! i := micheline2michelson_instruction_seq i in
    return_instruction (MAP i)
  | PRIM (_, I_CREATE_CONTRACT) _
               (Mk_loc_micheline
                  (_, SEQ
                        ((Mk_loc_micheline (_, PRIM (_, K_storage) _ (storage_ty :: nil))) ::
                        (Mk_loc_micheline (_, PRIM (_, K_parameter) _ (params_ty :: nil)) as param) ::
                        (Mk_loc_micheline (_, PRIM (_, K_code) _ (i :: nil))) :: nil)) ::
                  nil) =>
    let! an := extract_one_field_annot param in
    let! i := micheline2michelson_instruction_seq i in
    let! sty := micheline2michelson_type storage_ty in
    let! pty := micheline2michelson_ep params_ty in
    return_instruction (CREATE_CONTRACT sty pty an i)
  | PRIM (_, I_CREATE_CONTRACT) _
               (Mk_loc_micheline
                  (_, SEQ
                        ((Mk_loc_micheline (_, PRIM (_, K_parameter) _ (params_ty :: nil)) as param) ::
                        (Mk_loc_micheline (_, PRIM (_, K_storage) _ (storage_ty :: nil))) ::
                        (Mk_loc_micheline (_, PRIM (_, K_code) _ (i :: nil))) :: nil)) ::
                  nil) =>
    let! an := extract_one_field_annot param in
    let! i := micheline2michelson_instruction_seq i in
    let! sty := micheline2michelson_type storage_ty in
    let! pty := micheline2michelson_ep params_ty in
    return_instruction (CREATE_CONTRACT sty pty an i)
  | PRIM (_, I_EMPTY_SET) _ (cty :: nil) =>
    let! cty := micheline2michelson_ctype cty in
    return_opcode (EMPTY_SET cty)
  | PRIM (_, I_EMPTY_MAP) _ (kty :: vty :: nil) =>
    let! kty := micheline2michelson_ctype kty in
    let! vty := micheline2michelson_type vty in
    return_opcode (EMPTY_MAP kty vty)
  | PRIM (_, I_EMPTY_BIG_MAP) _ (kty :: vty :: nil) =>
    let! kty := micheline2michelson_ctype kty in
    let! vty := micheline2michelson_type vty in
    return_opcode (EMPTY_BIG_MAP kty vty)
  | PRIM (_, I_IF) _ (i1 :: i2 :: nil) =>
    let! i1 := micheline2michelson_instruction_seq i1 in
    let! i2 := micheline2michelson_instruction_seq i2 in
    return_instruction (IF_ IF_bool i1 i2)
  | PRIM (_, I_IF_NONE) _ (i1 :: i2 :: nil) =>
    let! i1 := micheline2michelson_instruction_seq i1 in
    let! i2 := micheline2michelson_instruction_seq i2 in
    return_instruction (IF_NONE i1 i2)
  | PRIM (_, I_IF_LEFT) _ (i1 :: i2 :: nil) =>
    let! i1 := micheline2michelson_instruction_seq i1 in
    let! i2 := micheline2michelson_instruction_seq i2 in
    return_instruction (IF_LEFT i1 i2)
  | PRIM (_, I_IF_CONS) _ (i1 :: i2 :: nil) =>
    let! i1 := micheline2michelson_instruction_seq i1 in
    let! i2 := micheline2michelson_instruction_seq i2 in
    return_instruction (IF_CONS i1 i2)
  | PRIM (_, I_LAMBDA) _ (a :: b :: i :: nil) =>
    let! a := micheline2michelson_type a in
    let! b := micheline2michelson_type b in
    let! i := micheline2michelson_instruction_seq i in
    return_instruction (LAMBDA a b i)
  | PRIM (_, I_PUSH) _ (a :: v :: nil) =>
    let! a := micheline2michelson_type a in
    let! v := micheline2michelson_data v in
    return_instruction (PUSH a v)

  | PRIM (_, I_RENAME) _ _ => Return (Instruction_seq NOOP)
  | PRIM (_, I_CAST) _ _ => Return (Instruction_seq NOOP)

  (* Unknown case *)
  | PRIM (_, s) _ _ => Failed _ (Expansion_prim b e (primitives.to_string s))

  | _ => Failed _ (Expansion b e)
  end.
Fixpoint micheline2michelson_data_comb
         a b l {struct l} :=
  Pair a (match l with
  | nil => b
  | c :: l =>
    micheline2michelson_data_comb b c l
  end).

Fixpoint micheline2michelson_data (bem : loc_micheline) : M concrete_data :=
  let 'Mk_loc_micheline ((b, e), m) := bem in
  match m with
  | SEQ l =>
    let! l :=
      (fix micheline2michelson_data_list (l : Datatypes.list loc_micheline) : M (Datatypes.list concrete_data) :=
        match l with
        | nil => Return nil
        | m :: l =>
          let! d := micheline2michelson_data m in
          let! l := micheline2michelson_data_list l in
          Return (d :: l)
        end
      ) l in
    Return (Concrete_seq l)
  | STR _ s => Return (String_constant s)
  | NUMBER _ z => Return (Int_constant z)
  | BYTES _ s => Return (Bytes_constant s)
  | PRIM (_, D_Unit) _ nil => Return Unit
  | PRIM (_, D_True) _ nil => Return True_
  | PRIM (_, D_False) _ nil => Return False_
  | PRIM (_, D_Pair) _ (a :: b :: l) =>
    let! a := micheline2michelson_data a in
    let! b := micheline2michelson_data b in
    let! l := error.list_map micheline2michelson_data l in
    Return (micheline2michelson_data_comb a b l)
  | PRIM (_, D_Elt) _ (a :: b :: nil) =>
    let! a := micheline2michelson_data a in
    let! b := micheline2michelson_data b in
    Return (Elt a b)
  | PRIM (_, D_Left) _ (a :: nil) =>
    let! a := micheline2michelson_data a in
    Return (Left a)
  | PRIM (_, D_Right) _ (a :: nil) =>
    let! a := micheline2michelson_data a in
    Return (Right a)
  | PRIM (_, D_Some) _ (a :: nil) =>
    let! a := micheline2michelson_data a in
    Return (Some_ a)
  | PRIM (_, D_None) _ nil => Return None_
  | _ =>
    let! i := micheline2michelson_instruction_aux micheline2michelson_data bem in
    Return (Instruction i)
  end.

Definition micheline2michelson_instruction :=
  micheline2michelson_instruction_aux micheline2michelson_data.

Definition micheline2michelson_instruction_seq :=
  micheline2michelson_instruction_seq_aux micheline2michelson_instruction.

Record untyped_michelson_file :=
  Mk_untyped_michelson_file
    { root_annotation : annot_o;
      parameter : entrypoints.entrypoint_tree;
      storage : type;
      code : instruction_seq }.

Record untyped_michelson_file_opt :=
  Mk_untyped_michelson_file_opt
    { root_annot : annot_o;
      parameter_opt : Datatypes.option entrypoints.entrypoint_tree;
      storage_opt : Datatypes.option type;
      code_opt : Datatypes.option instruction_seq }.

Definition read_parameter (ty : entrypoints.entrypoint_tree) (root_annot : annot_o)
           (f : untyped_michelson_file_opt) :=
  match f.(parameter_opt) with
  | None => Return {| root_annot := root_annot;
                      parameter_opt := Some ty;
                      storage_opt := f.(storage_opt);
                      code_opt := f.(code_opt) |}
  | Some _ => Failed _ Parsing
  end.

Definition read_storage (ty : type) (f : untyped_michelson_file_opt) :=
  match f.(storage_opt) with
  | None => Return {| root_annot := f.(root_annot);
                      parameter_opt := f.(parameter_opt);
                      storage_opt := Some ty;
                      code_opt := f.(code_opt) |}
  | Some _ => Failed _ Parsing
  end.

Definition read_code (c : instruction_seq) (f : untyped_michelson_file_opt) :=
  match f.(code_opt) with
  | None => Return {| root_annot := f.(root_annot);
                      parameter_opt := f.(parameter_opt);
                      storage_opt := f.(storage_opt);
                      code_opt := Some c |}
  | Some _ => Failed _ Parsing
  end.

Definition micheline2michelson_file (m : Datatypes.list loc_micheline) : M untyped_michelson_file :=
  let l :=
      match m with
      | Mk_loc_micheline (_, SEQ l) :: nil => l
      | l => l
      end
  in
  let! a :=
    error.list_fold_left
      (fun (a : untyped_michelson_file_opt) (lm : loc_micheline) =>
        let 'Mk_loc_micheline (_, _, m) := lm in
        match m with
        | PRIM (_, _, K_parameter) _ (cons param nil) =>
          let! an := extract_one_field_annot lm in
          let! ty := micheline2michelson_ep param in
          read_parameter ty an a
        | PRIM (_, _, K_storage) _ (cons storage nil) =>
          let! ty := micheline2michelson_type storage in
          read_storage ty a
        | PRIM (_, _, K_code) _ (cons code nil) =>
          let! c := micheline2michelson_instruction_seq code in
          read_code c a
        | _ => Failed _ Parsing
        end)
      l
      {| root_annot := None;
         parameter_opt := None;
         storage_opt := None;
         code_opt := None |} in
    match a.(parameter_opt), a.(storage_opt), a.(code_opt) with
    | Some param, Some storage, Some code =>
      Return {| root_annotation := a.(root_annot);
                parameter := param;
                storage := storage;
                code := code |}
    | _, _, _ => Failed _ Parsing
    end.
