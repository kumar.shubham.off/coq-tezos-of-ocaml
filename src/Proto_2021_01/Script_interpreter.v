Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.
Unset Guard Checking.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Require TezosOfOCaml.Proto_2021_01.Alpha_context.
Require TezosOfOCaml.Proto_2021_01.Michelson_v1_gas.
Require TezosOfOCaml.Proto_2021_01.Michelson_v1_primitives.
Require TezosOfOCaml.Proto_2021_01.Script_ir_translator.
Require TezosOfOCaml.Proto_2021_01.Script_typed_ir.
Require TezosOfOCaml.Proto_2021_01.Tez_repr.

Definition execution_trace : Set :=
  list
    (Alpha_context.Script.location * Alpha_context.Gas.t *
      list (Alpha_context.Script.expr * option string)).

(** Init function; without side-effects in Coq *)
Definition init_module : unit :=
  let trace_encoding :=
    (let arg := Data_encoding.list_value in
    fun (eta :
      Data_encoding.encoding
        (Alpha_context.Script.location * Alpha_context.Gas.t *
          list (Alpha_context.Script.expr * option string))) => arg None eta)
      (Data_encoding.obj3
        (Data_encoding.req None None "location"
          Alpha_context.Script.location_encoding)
        (Data_encoding.req None None "gas" Alpha_context.Gas.encoding)
        (Data_encoding.req None None "stack"
          (Data_encoding.list_value None
            (Data_encoding.obj2
              (Data_encoding.req None None "item"
                Alpha_context.Script.expr_encoding)
              (Data_encoding.opt None None "annot" Data_encoding.string_value)))))
    in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "michelson_v1.script_rejected" "Script failed"
      "A FAILWITH instruction was reached" None
      (Data_encoding.obj3
        (Data_encoding.req None None "location"
          Alpha_context.Script.location_encoding)
        (Data_encoding.req None None "with" Alpha_context.Script.expr_encoding)
        (Data_encoding.opt None None "trace" trace_encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Reject" then
            let '(loc, v, trace_value) :=
              cast
                (Alpha_context.Script.location * Alpha_context.Script.expr *
                  option execution_trace) payload in
            Some (loc, v, trace_value)
          else None
        end)
      (fun (function_parameter :
        Alpha_context.Script.location * Alpha_context.Script.expr *
          option execution_trace) =>
        let '(loc, v, trace_value) := function_parameter in
        Build_extensible "Reject"
          (Alpha_context.Script.location * Alpha_context.Script.expr *
            option execution_trace) (loc, v, trace_value)) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "michelson_v1.script_overflow" "Script failed (overflow error)"
      "A FAIL instruction was reached due to the detection of an overflow" None
      (Data_encoding.obj2
        (Data_encoding.req None None "location"
          Alpha_context.Script.location_encoding)
        (Data_encoding.opt None None "trace" trace_encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Overflow" then
            let '(loc, trace_value) :=
              cast (Alpha_context.Script.location * option execution_trace)
                payload in
            Some (loc, trace_value)
          else None
        end)
      (fun (function_parameter :
        Alpha_context.Script.location * option execution_trace) =>
        let '(loc, trace_value) := function_parameter in
        Build_extensible "Overflow"
          (Alpha_context.Script.location * option execution_trace)
          (loc, trace_value)) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "michelson_v1.runtime_error" "Script runtime error"
      "Toplevel error for all runtime script errors" None
      (Data_encoding.obj2
        (Data_encoding.req None None "contract_handle"
          Alpha_context.Contract.encoding)
        (Data_encoding.req None None "contract_code"
          Alpha_context.Script.expr_encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Runtime_contract_error" then
            let '(contract, expr) :=
              cast (Alpha_context.Contract.t * Alpha_context.Script.expr)
                payload in
            Some (contract, expr)
          else None
        end)
      (fun (function_parameter :
        Alpha_context.Contract.t * Alpha_context.Script.expr) =>
        let '(contract, expr) := function_parameter in
        Build_extensible "Runtime_contract_error"
          (Alpha_context.Contract.t * Alpha_context.Script.expr)
          (contract, expr)) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "michelson_v1.bad_contract_parameter"
      "Contract supplied an invalid parameter"
      "Either no parameter was supplied to a contract with a non-unit parameter type, a non-unit parameter was passed to an account, or a parameter was supplied of the wrong type"
      None
      (Data_encoding.obj1
        (Data_encoding.req None None "contract" Alpha_context.Contract.encoding))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Bad_contract_parameter" then
            let 'c := cast Alpha_context.Contract.t payload in
            Some c
          else None
        end)
      (fun (c : Alpha_context.Contract.t) =>
        Build_extensible "Bad_contract_parameter" Alpha_context.Contract.t c) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "michelson_v1.cannot_serialize_failure"
      "Not enough gas to serialize argument of FAILWITH"
      "Argument of FAILWITH was too big to be serialized with the provided gas"
      None Data_encoding.empty
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Cannot_serialize_failure" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Cannot_serialize_failure" unit tt) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "michelson_v1.cannot_serialize_storage"
      "Not enough gas to serialize execution storage"
      "The returned storage was too big to be serialized with the provided gas"
      None Data_encoding.empty
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Cannot_serialize_storage" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Cannot_serialize_storage" unit tt) in
  Error_monad.register_error_kind Error_monad.Permanent
    "michelson_v1.interp_too_many_recursive_calls"
    "Too many recursive calls during interpretation"
    "Too many recursive calls were needed for interpretation of a Michelson script"
    None Data_encoding.empty
    (fun (function_parameter : Error_monad._error) =>
      match function_parameter with
      | Build_extensible tag _ payload =>
        if String.eqb tag "Michelson_too_many_recursive_calls" then
          Some tt
        else None
      end)
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      Build_extensible "Michelson_too_many_recursive_calls" unit tt).

Module Interp_costs := Michelson_v1_gas.Cost_of.Interpreter.

Fixpoint interp_stack_prefix_preserving_operation {fbef faft result bef aft :
  Set}
  (f : fbef -> M=? (faft * result))
  (n : Script_typed_ir.stack_prefix_preservation_witness) (stk : bef) {struct n}
  : M=? (aft * result) :=
  match (n, stk) with
  |
    (Script_typed_ir.Prefix
      (Script_typed_ir.Prefix
        (Script_typed_ir.Prefix
          (Script_typed_ir.Prefix
            (Script_typed_ir.Prefix
              (Script_typed_ir.Prefix
                (Script_typed_ir.Prefix
                  (Script_typed_ir.Prefix
                    (Script_typed_ir.Prefix
                      (Script_typed_ir.Prefix
                        (Script_typed_ir.Prefix
                          (Script_typed_ir.Prefix
                            (Script_typed_ir.Prefix
                              (Script_typed_ir.Prefix
                                (Script_typed_ir.Prefix
                                  (Script_typed_ir.Prefix n))))))))))))))), v)
    =>
    let 'existT _
      [__0, __12, __15, __18, __21, __24, __27, __3, __30, __33, __36, __39,
        __42, __45, __46, __47, __6, __9] [v, n] :=
      cast_exists
        (Es :=
          [Set ** Set ** Set ** Set ** Set ** Set ** Set ** Set ** Set ** Set **
            Set ** Set ** Set ** Set ** Set ** Set ** Set ** Set])
        (fun
          '[__0, __12, __15, __18, __21, __24, __27, __3, __30, __33, __36,
            __39, __42, __45, __46, __47, __6, __9] =>
          [__0 *
            (__3 *
              (__6 *
                (__9 *
                  (__12 *
                    (__15 *
                      (__18 *
                        (__21 *
                          (__24 *
                            (__27 *
                              (__30 *
                                (__33 * (__36 * (__39 * (__42 * (__45 * __46)))))))))))))))
            ** Script_typed_ir.stack_prefix_preservation_witness]) [v, n] in
    cast (M=? (aft * result))
    (let
      '(v0,
        (v1,
          (v2,
            (v3,
              (v4,
                (v5,
                  (v6,
                    (v7, (v8, (v9, (va, (vb, (vc, (vd, (ve, (vf, rest)))))))))))))))) :=
      v in
    let=? '(rest', result_value) :=
      ((interp_stack_prefix_preserving_operation f n rest) : M=? (__47 * result))
      in
    return=?
      ((v0,
        (v1,
          (v2,
            (v3,
              (v4,
                (v5,
                  (v6,
                    (v7, (v8, (v9, (va, (vb, (vc, (vd, (ve, (vf, rest')))))))))))))))),
        result_value))
  |
    (Script_typed_ir.Prefix
      (Script_typed_ir.Prefix
        (Script_typed_ir.Prefix (Script_typed_ir.Prefix n))), v) =>
    let 'existT _ [__48, __51, __54, __57, __58, __59] [v, n] :=
      cast_exists (Es := [Set ** Set ** Set ** Set ** Set ** Set])
        (fun '[__48, __51, __54, __57, __58, __59] =>
          [__48 * (__51 * (__54 * (__57 * __58))) **
            Script_typed_ir.stack_prefix_preservation_witness]) [v, n] in
    cast (M=? (aft * result))
    (let '(v0, (v1, (v2, (v3, rest)))) := v in
    let=? '(rest', result_value) :=
      ((interp_stack_prefix_preserving_operation f n rest) : M=? (__59 * result))
      in
    return=? ((v0, (v1, (v2, (v3, rest')))), result_value))
  | (Script_typed_ir.Prefix n, v) =>
    let 'existT _ [__60, __61, __62] [v, n] :=
      cast_exists (Es := [Set ** Set ** Set])
        (fun '[__60, __61, __62] =>
          [__60 * __61 ** Script_typed_ir.stack_prefix_preservation_witness])
        [v, n] in
    cast (M=? (aft * result))
    (let '(v, rest) := v in
    let=? '(rest', result_value) :=
      ((interp_stack_prefix_preserving_operation f n rest) : M=? (__62 * result))
      in
    return=? ((v, rest'), result_value))
  | (Script_typed_ir.Rest, v) =>
    let v := cast fbef v in
    cast (M=? (aft * result)) (f v)
  end.

Module step_constants.
  Record record : Set := Build {
    source : Alpha_context.Contract.t;
    payer : Alpha_context.Contract.t;
    self : Alpha_context.Contract.t;
    amount : Alpha_context.Tez.t;
    chain_id : Chain_id.t }.
  Definition with_source source (r : record) :=
    Build source r.(payer) r.(self) r.(amount) r.(chain_id).
  Definition with_payer payer (r : record) :=
    Build r.(source) payer r.(self) r.(amount) r.(chain_id).
  Definition with_self self (r : record) :=
    Build r.(source) r.(payer) self r.(amount) r.(chain_id).
  Definition with_amount amount (r : record) :=
    Build r.(source) r.(payer) r.(self) amount r.(chain_id).
  Definition with_chain_id chain_id (r : record) :=
    Build r.(source) r.(payer) r.(self) r.(amount) chain_id.
End step_constants.
Definition step_constants := step_constants.record.

Module STEP_LOGGER.
  Record signature : Set := {
    log_interp :
      forall {bef : Set},
      Alpha_context.context -> Script_typed_ir.descr -> bef -> unit;
    log_entry :
      forall {bef : Set},
      Alpha_context.context -> Script_typed_ir.descr -> bef -> unit;
    log_exit :
      forall {aft : Set},
      Alpha_context.context -> Script_typed_ir.descr -> aft -> unit;
    get_log : unit -> M=? (option execution_trace);
  }.
End STEP_LOGGER.
Definition STEP_LOGGER := STEP_LOGGER.signature.

Definition logger : Set := {_ : unit @ STEP_LOGGER}.

Module No_trace.
  Definition log_interp {A B C : Set} (_ctxt : A) (_descr : B) (_stack : C)
    : unit := tt.
  
  Definition log_entry {A B C : Set} (_ctxt : A) (_descr : B) (_stack : C)
    : unit := tt.
  
  Definition log_exit {A B C : Set} (_ctxt : A) (_descr : B) (_stack : C)
    : unit := tt.
  
  Definition get_log {A B : Set} (function_parameter : unit)
    : M= (Pervasives.result (option A) B) :=
    let '_ := function_parameter in
    Error_monad.return_none.
  
  Definition module :=
    {|
      STEP_LOGGER.log_interp _ := log_interp;
      STEP_LOGGER.log_entry _ := log_entry;
      STEP_LOGGER.log_exit _ := log_exit;
      STEP_LOGGER.get_log := get_log
    |}.
End No_trace.
Definition No_trace : STEP_LOGGER := No_trace.module.

Definition cost_of_instr {b : Set}
  (descr_value : Script_typed_ir.descr) (stack_value : b)
  : Alpha_context.Gas.cost :=
  match (descr_value.(Script_typed_ir.descr.instr), stack_value) with
  | (Script_typed_ir.Drop, _) => Interp_costs.drop
  
  | (Script_typed_ir.Dup, _) => Interp_costs.dup
  
  | (Script_typed_ir.Swap, _) => Interp_costs.swap
  
  | (Script_typed_ir.Const _, _) => Interp_costs.push
  
  | (Script_typed_ir.Cons_some, _) => Interp_costs.cons_some
  
  | (Script_typed_ir.Cons_none _, _) => Interp_costs.cons_none
  
  | (Script_typed_ir.If_none _ _, _) => Interp_costs.if_none
  
  | (Script_typed_ir.Cons_pair, _) => Interp_costs.cons_pair
  
  | (Script_typed_ir.Unpair, _) => Interp_costs.unpair
  
  | (Script_typed_ir.Car, _) => Interp_costs.car
  
  | (Script_typed_ir.Cdr, _) => Interp_costs.cdr
  
  | (Script_typed_ir.Cons_left, _) => Interp_costs.cons_left
  
  | (Script_typed_ir.Cons_right, _) => Interp_costs.cons_right
  
  | (Script_typed_ir.If_left _ _, _) => Interp_costs.if_left
  
  | (Script_typed_ir.Cons_list, _) => Interp_costs.cons_list
  
  | (Script_typed_ir.Nil, _) => Interp_costs.nil
  
  | (Script_typed_ir.If_cons _ _, _) => Interp_costs.if_cons
  
  | (Script_typed_ir.List_map _, stack_value) =>
    let 'existT _ [__39, __40] stack_value :=
      cast_exists (Es := [Set ** Set])
        (fun '[__39, __40] => Script_typed_ir.boxed_list __39 * __40)
        stack_value in
    let '(list_value, _) := stack_value in
    Interp_costs.list_map list_value
  
  | (Script_typed_ir.List_size, _) => Interp_costs.list_size
  
  | (Script_typed_ir.List_iter _, stack_value) =>
    let 'existT _ [__44, __45] stack_value :=
      cast_exists (Es := [Set ** Set])
        (fun '[__44, __45] => Script_typed_ir.boxed_list __44 * __45)
        stack_value in
    let '(l_value, _) := stack_value in
    Interp_costs.list_iter l_value
  
  | (Script_typed_ir.Empty_set _, _) => Interp_costs.empty_set
  
  | (Script_typed_ir.Set_iter _, stack_value) =>
    let 'existT _ [__47, __48] stack_value :=
      cast_exists (Es := [Set ** Set])
        (fun '[__47, __48] => Script_typed_ir.set __47 * __48) stack_value in
    let '(set, _) := stack_value in
    Interp_costs.set_iter set
  
  | (Script_typed_ir.Set_mem, stack_value) =>
    let 'existT _ [__49, __50] stack_value :=
      cast_exists (Es := [Set ** Set])
        (fun '[__49, __50] => __49 * (Script_typed_ir.set __49 * __50))
        stack_value in
    let '(v, (set, _)) := stack_value in
    Interp_costs.set_mem v set
  
  | (Script_typed_ir.Set_update, stack_value) =>
    let 'existT _ [__51, __52] stack_value :=
      cast_exists (Es := [Set ** Set])
        (fun '[__51, __52] => __51 * (bool * (Script_typed_ir.set __51 * __52)))
        stack_value in
    let '(v, (_, (set, _))) := stack_value in
    Interp_costs.set_update v set
  
  | (Script_typed_ir.Set_size, _) => Interp_costs.set_size
  
  | (Script_typed_ir.Empty_map _ _, _) => Interp_costs.empty_map
  
  | (Script_typed_ir.Map_map _, stack_value) =>
    let 'existT _ [__57, __58, __59] stack_value :=
      cast_exists (Es := [Set ** Set ** Set])
        (fun '[__57, __58, __59] => Script_typed_ir.map __57 __58 * __59)
        stack_value in
    let '(map, _) := stack_value in
    Interp_costs.map_map map
  
  | (Script_typed_ir.Map_iter _, stack_value) =>
    let 'existT _ [__61, __62, __63] stack_value :=
      cast_exists (Es := [Set ** Set ** Set])
        (fun '[__61, __62, __63] => Script_typed_ir.map __61 __62 * __63)
        stack_value in
    let '(map, _) := stack_value in
    Interp_costs.map_iter map
  
  | (Script_typed_ir.Map_mem, stack_value) =>
    let 'existT _ [__64, __65, __66] stack_value :=
      cast_exists (Es := [Set ** Set ** Set])
        (fun '[__64, __65, __66] =>
          __64 * (Script_typed_ir.map __64 __65 * __66)) stack_value in
    let '(v, (map, _rest)) := stack_value in
    Interp_costs.map_mem v map
  
  | (Script_typed_ir.Map_get, stack_value) =>
    let 'existT _ [__67, __68, __69] stack_value :=
      cast_exists (Es := [Set ** Set ** Set])
        (fun '[__67, __68, __69] =>
          __67 * (Script_typed_ir.map __67 __68 * __69)) stack_value in
    let '(v, (map, _rest)) := stack_value in
    Interp_costs.map_get v map
  
  | (Script_typed_ir.Map_update, stack_value) =>
    let 'existT _ [__70, __71, __72] stack_value :=
      cast_exists (Es := [Set ** Set ** Set])
        (fun '[__70, __71, __72] =>
          __70 * (option __71 * (Script_typed_ir.map __70 __71 * __72)))
        stack_value in
    let '(k, (_, (map, _))) := stack_value in
    Interp_costs.map_update k map
  
  | (Script_typed_ir.Map_get_and_update, stack_value) =>
    let 'existT _ [__73, __74, __75] stack_value :=
      cast_exists (Es := [Set ** Set ** Set])
        (fun '[__73, __74, __75] =>
          __73 * (option __74 * (Script_typed_ir.map __73 __74 * __75)))
        stack_value in
    let '(k, (_, (map, _))) := stack_value in
    Interp_costs.map_get_and_update k map
  
  | (Script_typed_ir.Map_size, _) => Interp_costs.map_size
  
  | (Script_typed_ir.Empty_big_map _ _, _) => Interp_costs.empty_map
  
  | (Script_typed_ir.Big_map_mem, stack_value) =>
    let 'existT _ [__81, __82, __83] stack_value :=
      cast_exists (Es := [Set ** Set ** Set])
        (fun '[__81, __82, __83] =>
          __81 * (Script_typed_ir.big_map __81 __82 * __83)) stack_value in
    let '(key_value, (map, _)) := stack_value in
    Interp_costs.map_mem key_value map.(Script_typed_ir.big_map.diff)
  
  | (Script_typed_ir.Big_map_get, stack_value) =>
    let 'existT _ [__84, __85, __86] stack_value :=
      cast_exists (Es := [Set ** Set ** Set])
        (fun '[__84, __85, __86] =>
          __84 * (Script_typed_ir.big_map __84 __85 * __86)) stack_value in
    let '(key_value, (map, _)) := stack_value in
    Interp_costs.map_get key_value map.(Script_typed_ir.big_map.diff)
  
  | (Script_typed_ir.Big_map_update, stack_value) =>
    let 'existT _ [__87, __88, __89] stack_value :=
      cast_exists (Es := [Set ** Set ** Set])
        (fun '[__87, __88, __89] =>
          __87 * (option __88 * (Script_typed_ir.big_map __87 __88 * __89)))
        stack_value in
    let '(key_value, (_, (map, _))) := stack_value in
    Interp_costs.map_update key_value map.(Script_typed_ir.big_map.diff)
  
  | (Script_typed_ir.Big_map_get_and_update, stack_value) =>
    let 'existT _ [__90, __91, __92] stack_value :=
      cast_exists (Es := [Set ** Set ** Set])
        (fun '[__90, __91, __92] =>
          __90 * (option __91 * (Script_typed_ir.big_map __90 __91 * __92)))
        stack_value in
    let '(key_value, (_, (map, _))) := stack_value in
    Interp_costs.map_get_and_update key_value map.(Script_typed_ir.big_map.diff)
  
  | (Script_typed_ir.Add_seconds_to_timestamp, stack_value) =>
    let 'existT _ __93 stack_value :=
      cast_exists (Es := Set)
        (fun __93 =>
          Alpha_context.Script_int.num *
            (Alpha_context.Script_timestamp.t * __93)) stack_value in
    let '(n, (t_value, _)) := stack_value in
    Interp_costs.add_seconds_timestamp n t_value
  
  | (Script_typed_ir.Add_timestamp_to_seconds, stack_value) =>
    let 'existT _ __94 stack_value :=
      cast_exists (Es := Set)
        (fun __94 =>
          Alpha_context.Script_timestamp.t *
            (Alpha_context.Script_int.num * __94)) stack_value in
    let '(t_value, (n, _)) := stack_value in
    Interp_costs.add_seconds_timestamp n t_value
  
  | (Script_typed_ir.Sub_timestamp_seconds, stack_value) =>
    let 'existT _ __95 stack_value :=
      cast_exists (Es := Set)
        (fun __95 =>
          Alpha_context.Script_timestamp.t *
            (Alpha_context.Script_int.num * __95)) stack_value in
    let '(t_value, (n, _)) := stack_value in
    Interp_costs.sub_seconds_timestamp n t_value
  
  | (Script_typed_ir.Diff_timestamps, stack_value) =>
    let 'existT _ __96 stack_value :=
      cast_exists (Es := Set)
        (fun __96 =>
          Alpha_context.Script_timestamp.t *
            (Alpha_context.Script_timestamp.t * __96)) stack_value in
    let '(t1, (t2, _)) := stack_value in
    Interp_costs.diff_timestamps t1 t2
  
  | (Script_typed_ir.Concat_string_pair, stack_value) =>
    let 'existT _ __97 stack_value :=
      cast_exists (Es := Set) (fun __97 => string * (string * __97)) stack_value
      in
    let '(x, (y, _)) := stack_value in
    Interp_costs.concat_string_pair x y
  
  | (Script_typed_ir.Concat_string, stack_value) =>
    let 'existT _ __98 stack_value :=
      cast_exists (Es := Set)
        (fun __98 => Script_typed_ir.boxed_list string * __98) stack_value in
    let '(ss, _) := stack_value in
    Interp_costs.concat_string_precheck ss
  
  | (Script_typed_ir.Slice_string, stack_value) =>
    let 'existT _ __99 stack_value :=
      cast_exists (Es := Set)
        (fun __99 =>
          Alpha_context.Script_int.num *
            (Alpha_context.Script_int.num * (string * __99))) stack_value in
    let '(_offset, (_length, (s, _))) := stack_value in
    Interp_costs.slice_string s
  
  | (Script_typed_ir.String_size, _) => Interp_costs.string_size
  
  | (Script_typed_ir.Concat_bytes_pair, stack_value) =>
    let 'existT _ __101 stack_value :=
      cast_exists (Es := Set) (fun __101 => bytes * (bytes * __101)) stack_value
      in
    let '(x, (y, _)) := stack_value in
    Interp_costs.concat_bytes_pair x y
  
  | (Script_typed_ir.Concat_bytes, stack_value) =>
    let 'existT _ __102 stack_value :=
      cast_exists (Es := Set)
        (fun __102 => Script_typed_ir.boxed_list bytes * __102) stack_value in
    let '(ss, _) := stack_value in
    Interp_costs.concat_string_precheck ss
  
  | (Script_typed_ir.Slice_bytes, stack_value) =>
    let 'existT _ __103 stack_value :=
      cast_exists (Es := Set)
        (fun __103 =>
          Alpha_context.Script_int.num *
            (Alpha_context.Script_int.num * (bytes * __103))) stack_value in
    let '(_offset, (_length, (s, _))) := stack_value in
    Interp_costs.slice_bytes s
  
  | (Script_typed_ir.Bytes_size, _) => Interp_costs.bytes_size
  
  | (Script_typed_ir.Add_tez, _) => Interp_costs.add_tez
  
  | (Script_typed_ir.Sub_tez, _) => Interp_costs.sub_tez
  
  | (Script_typed_ir.Mul_teznat, stack_value) =>
    let 'existT _ __107 stack_value :=
      cast_exists (Es := Set)
        (fun __107 =>
          Alpha_context.Tez.t * (Alpha_context.Script_int.num * __107))
        stack_value in
    let '(_, (n, _)) := stack_value in
    Interp_costs.mul_teznat n
  
  | (Script_typed_ir.Mul_nattez, stack_value) =>
    let 'existT _ __108 stack_value :=
      cast_exists (Es := Set)
        (fun __108 =>
          Alpha_context.Script_int.num * (Alpha_context.Tez.t * __108))
        stack_value in
    let '(n, (_, _)) := stack_value in
    Interp_costs.mul_teznat n
  
  | (Script_typed_ir.Or, _) => Interp_costs.bool_or
  
  | (Script_typed_ir.And, _) => Interp_costs.bool_and
  
  | (Script_typed_ir.Xor, _) => Interp_costs.bool_xor
  
  | (Script_typed_ir.Not, _) => Interp_costs.bool_not
  
  | (Script_typed_ir.Is_nat, _) => Interp_costs.is_nat
  
  | (Script_typed_ir.Abs_int, stack_value) =>
    let 'existT _ __114 stack_value :=
      cast_exists (Es := Set)
        (fun __114 => Alpha_context.Script_int.num * __114) stack_value in
    let '(x, _) := stack_value in
    Interp_costs.abs_int x
  
  | (Script_typed_ir.Int_nat, _) => Interp_costs.int_nat
  
  | (Script_typed_ir.Neg_int, stack_value) =>
    let 'existT _ __116 stack_value :=
      cast_exists (Es := Set)
        (fun __116 => Alpha_context.Script_int.num * __116) stack_value in
    let '(x, _) := stack_value in
    Interp_costs.neg_int x
  
  | (Script_typed_ir.Neg_nat, stack_value) =>
    let 'existT _ __117 stack_value :=
      cast_exists (Es := Set)
        (fun __117 => Alpha_context.Script_int.num * __117) stack_value in
    let '(x, _) := stack_value in
    Interp_costs.neg_nat x
  
  | (Script_typed_ir.Add_intint, stack_value) =>
    let 'existT _ __118 stack_value :=
      cast_exists (Es := Set)
        (fun __118 =>
          Alpha_context.Script_int.num * (Alpha_context.Script_int.num * __118))
        stack_value in
    let '(x, (y, _)) := stack_value in
    Interp_costs.add_bigint x y
  
  | (Script_typed_ir.Add_intnat, stack_value) =>
    let 'existT _ __119 stack_value :=
      cast_exists (Es := Set)
        (fun __119 =>
          Alpha_context.Script_int.num * (Alpha_context.Script_int.num * __119))
        stack_value in
    let '(x, (y, _)) := stack_value in
    Interp_costs.add_bigint x y
  
  | (Script_typed_ir.Add_natint, stack_value) =>
    let 'existT _ __120 stack_value :=
      cast_exists (Es := Set)
        (fun __120 =>
          Alpha_context.Script_int.num * (Alpha_context.Script_int.num * __120))
        stack_value in
    let '(x, (y, _)) := stack_value in
    Interp_costs.add_bigint x y
  
  | (Script_typed_ir.Add_natnat, stack_value) =>
    let 'existT _ __121 stack_value :=
      cast_exists (Es := Set)
        (fun __121 =>
          Alpha_context.Script_int.num * (Alpha_context.Script_int.num * __121))
        stack_value in
    let '(x, (y, _)) := stack_value in
    Interp_costs.add_bigint x y
  
  | (Script_typed_ir.Sub_int, stack_value) =>
    let 'existT _ [__122, __123, __124] stack_value :=
      cast_exists (Es := [Set ** Set ** Set])
        (fun '[__122, __123, __124] =>
          Alpha_context.Script_int.num * (Alpha_context.Script_int.num * __124))
        stack_value in
    let '(x, (y, _)) := stack_value in
    Interp_costs.sub_bigint x y
  
  | (Script_typed_ir.Mul_intint, stack_value) =>
    let 'existT _ __125 stack_value :=
      cast_exists (Es := Set)
        (fun __125 =>
          Alpha_context.Script_int.num * (Alpha_context.Script_int.num * __125))
        stack_value in
    let '(x, (y, _)) := stack_value in
    Interp_costs.mul_bigint x y
  
  | (Script_typed_ir.Mul_intnat, stack_value) =>
    let 'existT _ __126 stack_value :=
      cast_exists (Es := Set)
        (fun __126 =>
          Alpha_context.Script_int.num * (Alpha_context.Script_int.num * __126))
        stack_value in
    let '(x, (y, _)) := stack_value in
    Interp_costs.mul_bigint x y
  
  | (Script_typed_ir.Mul_natint, stack_value) =>
    let 'existT _ __127 stack_value :=
      cast_exists (Es := Set)
        (fun __127 =>
          Alpha_context.Script_int.num * (Alpha_context.Script_int.num * __127))
        stack_value in
    let '(x, (y, _)) := stack_value in
    Interp_costs.mul_bigint x y
  
  | (Script_typed_ir.Mul_natnat, stack_value) =>
    let 'existT _ __128 stack_value :=
      cast_exists (Es := Set)
        (fun __128 =>
          Alpha_context.Script_int.num * (Alpha_context.Script_int.num * __128))
        stack_value in
    let '(x, (y, _)) := stack_value in
    Interp_costs.mul_bigint x y
  
  | (Script_typed_ir.Ediv_teznat, stack_value) =>
    let 'existT _ __129 stack_value :=
      cast_exists (Es := Set)
        (fun __129 =>
          Alpha_context.Tez.t * (Alpha_context.Script_int.num * __129))
        stack_value in
    let '(x, (y, _)) := stack_value in
    Interp_costs.ediv_teznat x y
  
  | (Script_typed_ir.Ediv_tez, _) => Interp_costs.ediv_tez
  
  | (Script_typed_ir.Ediv_intint, stack_value) =>
    let 'existT _ __131 stack_value :=
      cast_exists (Es := Set)
        (fun __131 =>
          Alpha_context.Script_int.num * (Alpha_context.Script_int.num * __131))
        stack_value in
    let '(x, (y, _)) := stack_value in
    Interp_costs.ediv_bigint x y
  
  | (Script_typed_ir.Ediv_intnat, stack_value) =>
    let 'existT _ __132 stack_value :=
      cast_exists (Es := Set)
        (fun __132 =>
          Alpha_context.Script_int.num * (Alpha_context.Script_int.num * __132))
        stack_value in
    let '(x, (y, _)) := stack_value in
    Interp_costs.ediv_bigint x y
  
  | (Script_typed_ir.Ediv_natint, stack_value) =>
    let 'existT _ __133 stack_value :=
      cast_exists (Es := Set)
        (fun __133 =>
          Alpha_context.Script_int.num * (Alpha_context.Script_int.num * __133))
        stack_value in
    let '(x, (y, _)) := stack_value in
    Interp_costs.ediv_bigint x y
  
  | (Script_typed_ir.Ediv_natnat, stack_value) =>
    let 'existT _ __134 stack_value :=
      cast_exists (Es := Set)
        (fun __134 =>
          Alpha_context.Script_int.num * (Alpha_context.Script_int.num * __134))
        stack_value in
    let '(x, (y, _)) := stack_value in
    Interp_costs.ediv_bigint x y
  
  | (Script_typed_ir.Lsl_nat, stack_value) =>
    let 'existT _ __135 stack_value :=
      cast_exists (Es := Set)
        (fun __135 =>
          Alpha_context.Script_int.num * (Alpha_context.Script_int.num * __135))
        stack_value in
    let '(x, _) := stack_value in
    Interp_costs.lsl_nat x
  
  | (Script_typed_ir.Lsr_nat, stack_value) =>
    let 'existT _ __136 stack_value :=
      cast_exists (Es := Set)
        (fun __136 =>
          Alpha_context.Script_int.num * (Alpha_context.Script_int.num * __136))
        stack_value in
    let '(x, _) := stack_value in
    Interp_costs.lsr_nat x
  
  | (Script_typed_ir.Or_nat, stack_value) =>
    let 'existT _ __137 stack_value :=
      cast_exists (Es := Set)
        (fun __137 =>
          Alpha_context.Script_int.num * (Alpha_context.Script_int.num * __137))
        stack_value in
    let '(x, (y, _)) := stack_value in
    Interp_costs.or_nat x y
  
  | (Script_typed_ir.And_nat, stack_value) =>
    let 'existT _ __138 stack_value :=
      cast_exists (Es := Set)
        (fun __138 =>
          Alpha_context.Script_int.num * (Alpha_context.Script_int.num * __138))
        stack_value in
    let '(x, (y, _)) := stack_value in
    Interp_costs.and_nat x y
  
  | (Script_typed_ir.And_int_nat, stack_value) =>
    let 'existT _ __139 stack_value :=
      cast_exists (Es := Set)
        (fun __139 =>
          Alpha_context.Script_int.num * (Alpha_context.Script_int.num * __139))
        stack_value in
    let '(x, (y, _)) := stack_value in
    Interp_costs.and_nat x y
  
  | (Script_typed_ir.Xor_nat, stack_value) =>
    let 'existT _ __140 stack_value :=
      cast_exists (Es := Set)
        (fun __140 =>
          Alpha_context.Script_int.num * (Alpha_context.Script_int.num * __140))
        stack_value in
    let '(x, (y, _)) := stack_value in
    Interp_costs.xor_nat x y
  
  | (Script_typed_ir.Not_int, stack_value) =>
    let 'existT _ __141 stack_value :=
      cast_exists (Es := Set)
        (fun __141 => Alpha_context.Script_int.num * __141) stack_value in
    let '(x, _) := stack_value in
    Interp_costs.not_nat x
  
  | (Script_typed_ir.Not_nat, stack_value) =>
    let 'existT _ __142 stack_value :=
      cast_exists (Es := Set)
        (fun __142 => Alpha_context.Script_int.num * __142) stack_value in
    let '(x, _) := stack_value in
    Interp_costs.not_nat x
  
  | (Script_typed_ir.Seq _ _, _) => Interp_costs.seq
  
  | (Script_typed_ir.If _ _, _) => Interp_costs.if_
  
  | (Script_typed_ir.Loop _, _) => Interp_costs.loop
  
  | (Script_typed_ir.Loop_left _, _) => Interp_costs.loop_left
  
  | (Script_typed_ir.Dip _, _) => Interp_costs.dip
  
  | (Script_typed_ir.Exec, _) => Interp_costs.exec
  
  | (Script_typed_ir.Apply _, _) => Interp_costs.apply
  
  | (Script_typed_ir.Lambda _, _) => Interp_costs.push
  
  | (Script_typed_ir.Failwith _, _) => Alpha_context.Gas.free
  
  | (Script_typed_ir.Nop, _) => Interp_costs.nop
  
  | (Script_typed_ir.Compare ty, stack_value) =>
    let 'existT _ [__162, __163] [stack_value, ty] :=
      cast_exists (Es := [Set ** Set])
        (fun '[__162, __163] =>
          [__162 * (__162 * __163) ** Script_typed_ir.comparable_ty])
        [stack_value, ty] in
    let '(a_value, (b_value, _)) := stack_value in
    Interp_costs.compare ty a_value b_value
  
  | (Script_typed_ir.Eq, _) => Interp_costs.neq
  
  | (Script_typed_ir.Neq, _) => Interp_costs.neq
  
  | (Script_typed_ir.Lt, _) => Interp_costs.neq
  
  | (Script_typed_ir.Le, _) => Interp_costs.neq
  
  | (Script_typed_ir.Gt, _) => Interp_costs.neq
  
  | (Script_typed_ir.Ge, _) => Interp_costs.neq
  
  | (Script_typed_ir.Pack _, _) => Alpha_context.Gas.free
  
  | (Script_typed_ir.Unpack _, _) => Alpha_context.Gas.free
  
  | (Script_typed_ir.Address, _) => Interp_costs.address
  
  | (Script_typed_ir.Contract _ _, _) => Interp_costs.contract
  
  | (Script_typed_ir.Transfer_tokens, _) => Interp_costs.transfer_tokens
  
  | (Script_typed_ir.Implicit_account, _) => Interp_costs.implicit_account
  
  | (Script_typed_ir.Set_delegate, _) => Interp_costs.set_delegate
  
  | (Script_typed_ir.Balance, _) => Interp_costs.balance
  
  | (Script_typed_ir.Level, _) => Interp_costs.level
  
  | (Script_typed_ir.Now, _) => Interp_costs.now
  
  | (Script_typed_ir.Check_signature, stack_value) =>
    let 'existT _ __182 stack_value :=
      cast_exists (Es := Set)
        (fun __182 =>
          Alpha_context.public_key * (Alpha_context.signature * (bytes * __182)))
        stack_value in
    let '(key_value, (_, (message, _))) := stack_value in
    Interp_costs.check_signature key_value message
  
  | (Script_typed_ir.Hash_key, stack_value) =>
    let 'existT _ __183 stack_value :=
      cast_exists (Es := Set) (fun __183 => Alpha_context.public_key * __183)
        stack_value in
    let '(pk, _) := stack_value in
    Interp_costs.hash_key pk
  
  | (Script_typed_ir.Blake2b, stack_value) =>
    let 'existT _ __184 stack_value :=
      cast_exists (Es := Set) (fun __184 => bytes * __184) stack_value in
    let '(bytes_value, _) := stack_value in
    Interp_costs.blake2b bytes_value
  
  | (Script_typed_ir.Sha256, stack_value) =>
    let 'existT _ __185 stack_value :=
      cast_exists (Es := Set) (fun __185 => bytes * __185) stack_value in
    let '(bytes_value, _) := stack_value in
    Interp_costs.sha256 bytes_value
  
  | (Script_typed_ir.Sha512, stack_value) =>
    let 'existT _ __186 stack_value :=
      cast_exists (Es := Set) (fun __186 => bytes * __186) stack_value in
    let '(bytes_value, _) := stack_value in
    Interp_costs.sha512 bytes_value
  
  | (Script_typed_ir.Source, _) => Interp_costs.source
  
  | (Script_typed_ir.Sender, _) => Interp_costs.source
  
  | (Script_typed_ir.Self _ _, _) => Interp_costs.self
  
  | (Script_typed_ir.Self_address, _) => Interp_costs.self
  
  | (Script_typed_ir.Amount, _) => Interp_costs.amount
  
  | (Script_typed_ir.Dig n _, _) =>
    let n := cast int n in
    Interp_costs.dign n
  
  | (Script_typed_ir.Dug n _, _) =>
    let n := cast int n in
    Interp_costs.dugn n
  
  | (Script_typed_ir.Dipn n _ _, _) =>
    let n := cast int n in
    Interp_costs.dipn n
  
  | (Script_typed_ir.Dropn n _, _) =>
    let n := cast int n in
    Interp_costs.dropn n
  
  | (Script_typed_ir.ChainId, _) => Interp_costs.chain_id
  
  | (Script_typed_ir.Create_contract _ _ _ _, _) => Interp_costs.create_contract
  
  | (Script_typed_ir.Voting_power, _) => Interp_costs.voting_power
  
  | (Script_typed_ir.Total_voting_power, _) => Interp_costs.total_voting_power
  
  | (Script_typed_ir.Keccak, stack_value) =>
    let 'existT _ __196 stack_value :=
      cast_exists (Es := Set) (fun __196 => bytes * __196) stack_value in
    let '(bytes_value, _) := stack_value in
    Interp_costs.keccak bytes_value
  
  | (Script_typed_ir.Sha3, stack_value) =>
    let 'existT _ __197 stack_value :=
      cast_exists (Es := Set) (fun __197 => bytes * __197) stack_value in
    let '(bytes_value, _) := stack_value in
    Interp_costs.sha3 bytes_value
  
  | (Script_typed_ir.Add_bls12_381_g1, _) => Interp_costs.add_bls12_381_g1
  
  | (Script_typed_ir.Add_bls12_381_g2, _) => Interp_costs.add_bls12_381_g2
  
  | (Script_typed_ir.Add_bls12_381_fr, _) => Interp_costs.add_bls12_381_fr
  
  | (Script_typed_ir.Mul_bls12_381_g1, _) => Interp_costs.mul_bls12_381_g1
  
  | (Script_typed_ir.Mul_bls12_381_g2, _) => Interp_costs.mul_bls12_381_g2
  
  | (Script_typed_ir.Mul_bls12_381_fr, _) => Interp_costs.mul_bls12_381_fr
  
  | (Script_typed_ir.Mul_bls12_381_fr_z, _) => Interp_costs.mul_bls12_381_fr_z
  
  | (Script_typed_ir.Mul_bls12_381_z_fr, _) => Interp_costs.mul_bls12_381_fr_z
  
  | (Script_typed_ir.Int_bls12_381_fr, _) => Interp_costs.int_bls12_381_fr
  
  | (Script_typed_ir.Neg_bls12_381_g1, _) => Interp_costs.neg_bls12_381_g1
  
  | (Script_typed_ir.Neg_bls12_381_g2, _) => Interp_costs.neg_bls12_381_g2
  
  | (Script_typed_ir.Neg_bls12_381_fr, _) => Interp_costs.neg_bls12_381_fr
  
  | (Script_typed_ir.Pairing_check_bls12_381, stack_value) =>
    let 'existT _ __212 stack_value :=
      cast_exists (Es := Set)
        (fun __212 =>
          Script_typed_ir.boxed_list
            (Script_typed_ir.pair Bls12_381.G1.(S.CURVE.t)
              Bls12_381.G2.(S.CURVE.t)) * __212) stack_value in
    let '(pairs, _) := stack_value in
    Interp_costs.pairing_check_bls12_381 pairs
  
  | (Script_typed_ir.Comb n _, _) =>
    let n := cast int n in
    Interp_costs.comb n
  
  | (Script_typed_ir.Uncomb n _, _) =>
    let n := cast int n in
    Interp_costs.uncomb n
  
  | (Script_typed_ir.Comb_get n _, _) =>
    let n := cast int n in
    Interp_costs.comb_get n
  
  | (Script_typed_ir.Comb_set n _, _) =>
    let n := cast int n in
    Interp_costs.comb_set n
  
  | (Script_typed_ir.Dup_n n _, _) =>
    let n := cast int n in
    Interp_costs.dupn n
  
  | (Script_typed_ir.Sapling_empty_state _, _) =>
    Interp_costs.sapling_empty_state
  
  | (Script_typed_ir.Sapling_verify_update, stack_value) =>
    let 'existT _ __221 stack_value :=
      cast_exists (Es := Set)
        (fun __221 =>
          Alpha_context.Sapling.transaction *
            (Alpha_context.Sapling.state * __221)) stack_value in
    let '(tx, _) := stack_value in
    let inputs := List.length tx.(Sapling.UTXO.transaction.inputs) in
    let outputs := List.length tx.(Sapling.UTXO.transaction.outputs) in
    Interp_costs.sapling_verify_update inputs outputs
  
  | (Script_typed_ir.Ticket, _) => Interp_costs.ticket
  
  | (Script_typed_ir.Read_ticket, _) => Interp_costs.read_ticket
  
  | (Script_typed_ir.Split_ticket, stack_value) =>
    let 'existT _ [__226, __227] stack_value :=
      cast_exists (Es := [Set ** Set])
        (fun '[__226, __227] =>
          Script_typed_ir.ticket __226 *
            ((Alpha_context.Script_int.num * Alpha_context.Script_int.num) *
              __227)) stack_value in
    let '(ticket, ((amount_a, amount_b), _)) := stack_value in
    Interp_costs.split_ticket ticket.(Script_typed_ir.ticket.amount) amount_a
      amount_b
  
  | (Script_typed_ir.Join_tickets ty, stack_value) =>
    let 'existT _ [__228, __229] [stack_value, ty] :=
      cast_exists (Es := [Set ** Set])
        (fun '[__228, __229] =>
          [(Script_typed_ir.ticket __228 * Script_typed_ir.ticket __228) * __229
            ** Script_typed_ir.comparable_ty]) [stack_value, ty] in
    let '((ticket_a, ticket_b), _) := stack_value in
    Interp_costs.join_tickets ty ticket_a ticket_b
  | _ => unreachable_gadt_branch
  end.

Definition unpack {A : Set}
  (ctxt : Alpha_context.context) (ty : Script_typed_ir.ty) (bytes_value : bytes)
  : M=? (option A * Alpha_context.context) :=
  let=? '_ :=
    return=
      (Alpha_context.Gas.check_enough ctxt
        (Alpha_context.Script.serialized_cost bytes_value)) in
  if
    ((Bytes.length bytes_value) >=i 1) &&
    ((TzEndian.get_uint8 bytes_value 0) =i 5)
  then
    let bytes_value := Bytes.sub bytes_value 1 ((Bytes.length bytes_value) -i 1)
      in
    match
      Data_encoding.Binary.of_bytes Alpha_context.Script.expr_encoding
        bytes_value with
    | None =>
      Lwt._return
        (let? ctxt :=
          Alpha_context.Gas.consume ctxt
            (Interp_costs.unpack_failed bytes_value) in
        return? (None, ctxt))
    | Some expr =>
      let=? ctxt :=
        return=
          (Alpha_context.Gas.consume ctxt
            (Alpha_context.Script.deserialized_cost expr)) in
      let= function_parameter :=
        Script_ir_translator.parse_data None ctxt false false ty
          (Micheline.root expr) in
      match function_parameter with
      | Pervasives.Ok (value, ctxt) => return=? ((Some value), ctxt)
      | Pervasives.Error _ignored =>
        return=
          (let? ctxt :=
            Alpha_context.Gas.consume ctxt
              (Interp_costs.unpack_failed bytes_value) in
          return? (None, ctxt))
      end
    end
  else
    Error_monad._return (None, ctxt).

Fixpoint step_bounded {b a : Set}
  (logger : logger) (stack_depth : int) (ctxt : Alpha_context.context)
  (step_constants : step_constants) (function_parameter : Script_typed_ir.descr)
  {struct ctxt} : b -> M=? (a * Alpha_context.context) :=
  let
    '{|
      Script_typed_ir.descr.loc := loc;
        Script_typed_ir.descr.instr := instr
        |} as descr_value := function_parameter in
  fun (stack_value : b) =>
    let gas := cost_of_instr descr_value stack_value in
    let=? ctxt := return= (Alpha_context.Gas.consume ctxt gas) in
    let 'existS _ _ Log := logger in
    let '_ := Log.(STEP_LOGGER.log_entry) ctxt descr_value stack_value in
    let logged_return (function_parameter : a * Alpha_context.context)
      : M=? (a * Alpha_context.context) :=
      let '(ret, ctxt) := function_parameter in
      let '_ := Log.(STEP_LOGGER.log_exit) ctxt descr_value ret in
      Error_monad._return (ret, ctxt) in
    let non_terminal_recursion {C D : Set}
      (ctxt : Alpha_context.context) (op_staroptstar : option int)
      : Script_typed_ir.descr -> C -> M=? (D * Alpha_context.context) :=
      let stack_depth :=
        match op_staroptstar with
        | Some op_starsthstar => op_starsthstar
        | None => stack_depth +i 1
        end in
      fun (descr_value : Script_typed_ir.descr) =>
        fun (stack_value : C) =>
          if stack_depth >=i 10000 then
            Error_monad.fail
              (Build_extensible "Michelson_too_many_recursive_calls" unit tt)
          else
            step_bounded logger stack_depth ctxt step_constants descr_value
              stack_value in
    match (instr, stack_value) with
    | (Script_typed_ir.Drop, stack_value) =>
      let 'existT _ [__0, __1] stack_value :=
        cast_exists (Es := [Set ** Set]) (fun '[__0, __1] => __0 * __1)
          stack_value in
      let '(_, rest) := stack_value in
      logged_return ((cast a rest), ctxt)
    
    | (Script_typed_ir.Dup, stack_value) =>
      let 'existT _ [__2, __3] stack_value :=
        cast_exists (Es := [Set ** Set]) (fun '[__2, __3] => __2 * __3)
          stack_value in
      let '(v, rest) := stack_value in
      logged_return ((cast a (v, (v, rest))), ctxt)
    
    | (Script_typed_ir.Swap, stack_value) =>
      let 'existT _ [__4, __5, __6] stack_value :=
        cast_exists (Es := [Set ** Set ** Set])
          (fun '[__4, __5, __6] => __4 * (__5 * __6)) stack_value in
      let '(vi, (vo, rest)) := stack_value in
      logged_return ((cast a (vo, (vi, rest))), ctxt)
    
    | (Script_typed_ir.Const v, rest) =>
      let 'existT _ __7 [rest, v] :=
        cast_exists (Es := Set) (fun __7 => [b ** __7]) [rest, v] in
      logged_return ((cast a (v, rest)), ctxt)
    
    | (Script_typed_ir.Cons_some, stack_value) =>
      let 'existT _ [__8, __9] stack_value :=
        cast_exists (Es := [Set ** Set]) (fun '[__8, __9] => __8 * __9)
          stack_value in
      let '(v, rest) := stack_value in
      logged_return ((cast a ((Some v), rest)), ctxt)
    
    | (Script_typed_ir.Cons_none _ty, rest) =>
      let 'existT _ __10 [rest, _ty] :=
        cast_exists (Es := Set) (fun __10 => [b ** Script_typed_ir.ty])
          [rest, _ty] in
      logged_return ((cast a ((None : option __10), rest)), ctxt)
    
    | (Script_typed_ir.If_none bt bf, stack_value) =>
      let 'existT _ [__11, __12] [stack_value, bf, bt] :=
        cast_exists (Es := [Set ** Set])
          (fun '[__11, __12] =>
            [option __11 * __12 ** Script_typed_ir.descr **
              Script_typed_ir.descr]) [stack_value, bf, bt] in
      let '(v, rest) := stack_value in
      match v with
      | None => step_bounded logger stack_depth ctxt step_constants bt rest
      | Some v =>
        step_bounded logger stack_depth ctxt step_constants bf (v, rest)
      end
    
    | (Script_typed_ir.Cons_pair, stack_value) =>
      let 'existT _ [__13, __14, __15] stack_value :=
        cast_exists (Es := [Set ** Set ** Set])
          (fun '[__13, __14, __15] => __13 * (__14 * __15)) stack_value in
      let '(a_value, (b_value, rest)) := stack_value in
      logged_return ((cast a ((a_value, b_value), rest)), ctxt)
    
    | (Script_typed_ir.Unpair, stack_value) =>
      let 'existT _ [__16, __17, __18] stack_value :=
        cast_exists (Es := [Set ** Set ** Set])
          (fun '[__16, __17, __18] => (__16 * __17) * __18) stack_value in
      let '((a_value, b_value), rest) := stack_value in
      logged_return ((cast a (a_value, (b_value, rest))), ctxt)
    
    | (Script_typed_ir.Car, stack_value) =>
      let 'existT _ [__19, __20, __21] stack_value :=
        cast_exists (Es := [Set ** Set ** Set])
          (fun '[__19, __20, __21] => (__19 * __20) * __21) stack_value in
      let '((a_value, _), rest) := stack_value in
      logged_return ((cast a (a_value, rest)), ctxt)
    
    | (Script_typed_ir.Cdr, stack_value) =>
      let 'existT _ [__22, __23, __24] stack_value :=
        cast_exists (Es := [Set ** Set ** Set])
          (fun '[__22, __23, __24] => (__22 * __23) * __24) stack_value in
      let '((_, b_value), rest) := stack_value in
      logged_return ((cast a (b_value, rest)), ctxt)
    
    | (Script_typed_ir.Cons_left, stack_value) =>
      let 'existT _ [__25, __26] stack_value :=
        cast_exists (Es := [Set ** Set]) (fun '[__25, __26] => __25 * __26)
          stack_value in
      let '(v, rest) := stack_value in
      logged_return
        ((cast a ((Script_typed_ir.L (b := Set_oracle "r") v), rest)), ctxt)
    
    | (Script_typed_ir.Cons_right, stack_value) =>
      let 'existT _ [__28, __29] stack_value :=
        cast_exists (Es := [Set ** Set]) (fun '[__28, __29] => __28 * __29)
          stack_value in
      let '(v, rest) := stack_value in
      logged_return
        ((cast a ((Script_typed_ir.R (a := Set_oracle "l") v), rest)), ctxt)
    
    | (Script_typed_ir.If_left bt bf, stack_value) =>
      let 'existT _ [__31, __32, __33] [stack_value, bf, bt] :=
        cast_exists (Es := [Set ** Set ** Set])
          (fun '[__31, __32, __33] =>
            [Script_typed_ir.union __31 __32 * __33 ** Script_typed_ir.descr **
              Script_typed_ir.descr]) [stack_value, bf, bt] in
      let '(v, rest) := stack_value in
      match v with
      | Script_typed_ir.L v =>
        step_bounded logger stack_depth ctxt step_constants bt (v, rest)
      | Script_typed_ir.R v =>
        step_bounded logger stack_depth ctxt step_constants bf (v, rest)
      end
    
    | (Script_typed_ir.Cons_list, stack_value) =>
      let 'existT _ [__34, __35] stack_value :=
        cast_exists (Es := [Set ** Set])
          (fun '[__34, __35] => __34 * (Script_typed_ir.boxed_list __34 * __35))
          stack_value in
      let '(hd, (tl, rest)) := stack_value in
      logged_return
        ((cast a ((Script_ir_translator.list_cons hd tl), rest)), ctxt)
    
    | (Script_typed_ir.Nil, rest) =>
      let rest := cast b rest in
      logged_return
        ((cast a ((Script_ir_translator.list_empty (a := Set_oracle "a")), rest)),
          ctxt)
    
    | (Script_typed_ir.If_cons bt bf, v) =>
      let 'existT _ [__37, __38] [v, bf, bt] :=
        cast_exists (Es := [Set ** Set])
          (fun '[__37, __38] =>
            [Script_typed_ir.boxed_list __37 * __38 ** Script_typed_ir.descr **
              Script_typed_ir.descr]) [v, bf, bt] in
      let
        '({|
          Script_typed_ir.boxed_list.elements := elements;
            Script_typed_ir.boxed_list.length := length
            |}, rest) := v in
      match elements with
      | [] => step_bounded logger stack_depth ctxt step_constants bf rest
      | cons hd tl =>
        let tl :=
          {| Script_typed_ir.boxed_list.elements := tl;
            Script_typed_ir.boxed_list.length := length -i 1 |} in
        step_bounded logger stack_depth ctxt step_constants bt (hd, (tl, rest))
      end
    
    | (Script_typed_ir.List_map body, stack_value) =>
      let 'existT _ [__39, __40, __41] [stack_value, body] :=
        cast_exists (Es := [Set ** Set ** Set])
          (fun '[__39, __40, __41] =>
            [Script_typed_ir.boxed_list __39 * __40 ** Script_typed_ir.descr])
          [stack_value, body] in
      let '(list_value, rest) := stack_value in
      let fix loop
        (rest : __40) (ctxt : Alpha_context.context) (l_value : list __39)
        (acc_value : list __41)
        : M=? ((Script_typed_ir.boxed_list __41 * __40) * Alpha_context.context) :=
        match l_value with
        | [] =>
          let result_value :=
            {| Script_typed_ir.boxed_list.elements := List.rev acc_value;
              Script_typed_ir.boxed_list.length :=
                list_value.(Script_typed_ir.boxed_list.length) |} in
          Error_monad._return ((result_value, rest), ctxt)
        | cons hd tl =>
          let=? '((hd, rest), ctxt) :=
            non_terminal_recursion ctxt None body (hd, rest) in
          loop rest ctxt tl (cons hd acc_value)
        end in
      let=? '(res, ctxt) :=
        loop rest ctxt list_value.(Script_typed_ir.boxed_list.elements) nil in
      logged_return ((cast a res), ctxt)
    
    | (Script_typed_ir.List_size, stack_value) =>
      let 'existT _ [__42, __43] stack_value :=
        cast_exists (Es := [Set ** Set])
          (fun '[__42, __43] => Script_typed_ir.boxed_list __42 * __43)
          stack_value in
      let '(list_value, rest) := stack_value in
      logged_return
        ((cast a
          ((Alpha_context.Script_int.abs
            (Alpha_context.Script_int.of_int
              list_value.(Script_typed_ir.boxed_list.length))), rest)), ctxt)
    
    | (Script_typed_ir.List_iter body, stack_value) =>
      let 'existT _ [__44, __45] [stack_value, body] :=
        cast_exists (Es := [Set ** Set])
          (fun '[__44, __45] =>
            [Script_typed_ir.boxed_list __44 * __45 ** Script_typed_ir.descr])
          [stack_value, body] in
      let '(l_value, init_value) := stack_value in
      let fix loop
        (ctxt : Alpha_context.context) (l_value : list __44)
        (stack_value : __45) : M=? (__45 * Alpha_context.context) :=
        match l_value with
        | [] => Error_monad._return (stack_value, ctxt)
        | cons hd tl =>
          let=? '(stack_value, ctxt) :=
            non_terminal_recursion ctxt None body (hd, stack_value) in
          loop ctxt tl stack_value
        end in
      let=? '(res, ctxt) :=
        loop ctxt l_value.(Script_typed_ir.boxed_list.elements) init_value in
      logged_return ((cast a res), ctxt)
    
    | (Script_typed_ir.Empty_set t_value, rest) =>
      let 'existT _ __46 [rest, t_value] :=
        cast_exists (Es := Set)
          (fun __46 => [b ** Script_typed_ir.comparable_ty]) [rest, t_value] in
      logged_return
        ((cast a
          (((Script_ir_translator.empty_set t_value) : Script_typed_ir.set __46),
            rest)), ctxt)
    
    | (Script_typed_ir.Set_iter body, stack_value) =>
      let 'existT _ [__47, __48] [stack_value, body] :=
        cast_exists (Es := [Set ** Set])
          (fun '[__47, __48] =>
            [Script_typed_ir.set __47 * __48 ** Script_typed_ir.descr])
          [stack_value, body] in
      let '(set, init_value) := stack_value in
      let l_value :=
        List.rev
          (Script_ir_translator.set_fold
            (fun (e : __47) => fun (acc_value : list __47) => cons e acc_value)
            set nil) in
      let fix loop
        (ctxt : Alpha_context.context) (l_value : list __47)
        (stack_value : __48) : M=? (__48 * Alpha_context.context) :=
        match l_value with
        | [] => Error_monad._return (stack_value, ctxt)
        | cons hd tl =>
          let=? '(stack_value, ctxt) :=
            non_terminal_recursion ctxt None body (hd, stack_value) in
          loop ctxt tl stack_value
        end in
      let=? '(res, ctxt) := loop ctxt l_value init_value in
      logged_return ((cast a res), ctxt)
    
    | (Script_typed_ir.Set_mem, stack_value) =>
      let 'existT _ [__49, __50] stack_value :=
        cast_exists (Es := [Set ** Set])
          (fun '[__49, __50] => __49 * (Script_typed_ir.set __49 * __50))
          stack_value in
      let '(v, (set, rest)) := stack_value in
      logged_return
        ((cast a ((Script_ir_translator.set_mem v set), rest)), ctxt)
    
    | (Script_typed_ir.Set_update, stack_value) =>
      let 'existT _ [__51, __52] stack_value :=
        cast_exists (Es := [Set ** Set])
          (fun '[__51, __52] =>
            __51 * (bool * (Script_typed_ir.set __51 * __52))) stack_value in
      let '(v, (presence, (set, rest))) := stack_value in
      logged_return
        ((cast a ((Script_ir_translator.set_update v presence set), rest)), ctxt)
    
    | (Script_typed_ir.Set_size, stack_value) =>
      let 'existT _ [__53, __54] stack_value :=
        cast_exists (Es := [Set ** Set])
          (fun '[__53, __54] => Script_typed_ir.set __53 * __54) stack_value in
      let '(set, rest) := stack_value in
      logged_return ((cast a ((Script_ir_translator.set_size set), rest)), ctxt)
    
    | (Script_typed_ir.Empty_map tk _tv, rest) =>
      let 'existT _ [__55, __56] [rest, _tv, tk] :=
        cast_exists (Es := [Set ** Set])
          (fun '[__55, __56] =>
            [b ** Script_typed_ir.ty ** Script_typed_ir.comparable_ty])
          [rest, _tv, tk] in
      logged_return
        ((cast a
          (((Script_ir_translator.empty_map tk) : Script_typed_ir.map __55 __56),
            rest)), ctxt)
    
    | (Script_typed_ir.Map_map body, stack_value) =>
      let 'existT _ [__57, __58, __59, __60] [stack_value, body] :=
        cast_exists (Es := [Set ** Set ** Set ** Set])
          (fun '[__57, __58, __59, __60] =>
            [Script_typed_ir.map __57 __58 * __59 ** Script_typed_ir.descr])
          [stack_value, body] in
      let '(map, rest) := stack_value in
      let l_value :=
        List.rev
          (Script_ir_translator.map_fold
            (fun (k : __57) =>
              fun (v : __58) =>
                fun (acc_value : list (__57 * __58)) => cons (k, v) acc_value)
            map nil) in
      let fix loop
        (rest : __59) (ctxt : Alpha_context.context)
        (l_value : list (__57 * __58))
        (acc_value : Script_typed_ir.map __57 __60)
        : M=? ((Script_typed_ir.map __57 __60 * __59) * Alpha_context.context) :=
        match l_value with
        | [] => Error_monad._return ((acc_value, rest), ctxt)
        | cons ((k, _) as hd) tl =>
          let=? '((hd, rest), ctxt) :=
            non_terminal_recursion ctxt None body (hd, rest) in
          loop rest ctxt tl
            (Script_ir_translator.map_update k (Some hd) acc_value)
        end in
      let=? '(res, ctxt) :=
        loop rest ctxt l_value
          (Script_ir_translator.empty_map (Script_ir_translator.map_key_ty map))
        in
      logged_return ((cast a res), ctxt)
    
    | (Script_typed_ir.Map_iter body, stack_value) =>
      let 'existT _ [__61, __62, __63] [stack_value, body] :=
        cast_exists (Es := [Set ** Set ** Set])
          (fun '[__61, __62, __63] =>
            [Script_typed_ir.map __61 __62 * __63 ** Script_typed_ir.descr])
          [stack_value, body] in
      let '(map, init_value) := stack_value in
      let l_value :=
        List.rev
          (Script_ir_translator.map_fold
            (fun (k : __61) =>
              fun (v : __62) =>
                fun (acc_value : list (__61 * __62)) => cons (k, v) acc_value)
            map nil) in
      let fix loop
        (ctxt : Alpha_context.context) (l_value : list (__61 * __62))
        (stack_value : __63) : M=? (__63 * Alpha_context.context) :=
        match l_value with
        | [] => Error_monad._return (stack_value, ctxt)
        | cons hd tl =>
          let=? '(stack_value, ctxt) :=
            non_terminal_recursion ctxt None body (hd, stack_value) in
          loop ctxt tl stack_value
        end in
      let=? '(res, ctxt) := loop ctxt l_value init_value in
      logged_return ((cast a res), ctxt)
    
    | (Script_typed_ir.Map_mem, stack_value) =>
      let 'existT _ [__64, __65, __66] stack_value :=
        cast_exists (Es := [Set ** Set ** Set])
          (fun '[__64, __65, __66] =>
            __64 * (Script_typed_ir.map __64 __65 * __66)) stack_value in
      let '(v, (map, rest)) := stack_value in
      logged_return
        ((cast a ((Script_ir_translator.map_mem v map), rest)), ctxt)
    
    | (Script_typed_ir.Map_get, stack_value) =>
      let 'existT _ [__67, __68, __69] stack_value :=
        cast_exists (Es := [Set ** Set ** Set])
          (fun '[__67, __68, __69] =>
            __67 * (Script_typed_ir.map __67 __68 * __69)) stack_value in
      let '(v, (map, rest)) := stack_value in
      logged_return
        ((cast a ((Script_ir_translator.map_get v map), rest)), ctxt)
    
    | (Script_typed_ir.Map_update, stack_value) =>
      let 'existT _ [__70, __71, __72] stack_value :=
        cast_exists (Es := [Set ** Set ** Set])
          (fun '[__70, __71, __72] =>
            __70 * (option __71 * (Script_typed_ir.map __70 __71 * __72)))
          stack_value in
      let '(k, (v, (map, rest))) := stack_value in
      logged_return
        ((cast a ((Script_ir_translator.map_update k v map), rest)), ctxt)
    
    | (Script_typed_ir.Map_get_and_update, stack_value) =>
      let 'existT _ [__73, __74, __75] stack_value :=
        cast_exists (Es := [Set ** Set ** Set])
          (fun '[__73, __74, __75] =>
            __73 * (option __74 * (Script_typed_ir.map __73 __74 * __75)))
          stack_value in
      let '(k, (v, (map, rest))) := stack_value in
      let map' := Script_ir_translator.map_update k v map in
      let v' := Script_ir_translator.map_get k map in
      logged_return ((cast a (v', (map', rest))), ctxt)
    
    | (Script_typed_ir.Map_size, stack_value) =>
      let 'existT _ [__76, __77, __78] stack_value :=
        cast_exists (Es := [Set ** Set ** Set])
          (fun '[__76, __77, __78] => Script_typed_ir.map __76 __77 * __78)
          stack_value in
      let '(map, rest) := stack_value in
      logged_return ((cast a ((Script_ir_translator.map_size map), rest)), ctxt)
    
    | (Script_typed_ir.Empty_big_map tk tv, rest) =>
      let 'existT _ [__79, __80] [rest, tv, tk] :=
        cast_exists (Es := [Set ** Set])
          (fun '[__79, __80] =>
            [b ** Script_typed_ir.ty ** Script_typed_ir.comparable_ty])
          [rest, tv, tk] in
      logged_return
        ((cast a
          (((Script_ir_translator.empty_big_map tk tv) :
            Script_typed_ir.big_map __79 __80), rest)), ctxt)
    
    | (Script_typed_ir.Big_map_mem, stack_value) =>
      let 'existT _ [__81, __82, __83] stack_value :=
        cast_exists (Es := [Set ** Set ** Set])
          (fun '[__81, __82, __83] =>
            __81 * (Script_typed_ir.big_map __81 __82 * __83)) stack_value in
      let '(key_value, (map, rest)) := stack_value in
      let=? '(res, ctxt) := Script_ir_translator.big_map_mem ctxt key_value map
        in
      logged_return ((cast a (res, rest)), ctxt)
    
    | (Script_typed_ir.Big_map_get, stack_value) =>
      let 'existT _ [__84, __85, __86] stack_value :=
        cast_exists (Es := [Set ** Set ** Set])
          (fun '[__84, __85, __86] =>
            __84 * (Script_typed_ir.big_map __84 __85 * __86)) stack_value in
      let '(key_value, (map, rest)) := stack_value in
      let=? '(res, ctxt) := Script_ir_translator.big_map_get ctxt key_value map
        in
      logged_return ((cast a (res, rest)), ctxt)
    
    | (Script_typed_ir.Big_map_update, stack_value) =>
      let 'existT _ [__87, __88, __89] stack_value :=
        cast_exists (Es := [Set ** Set ** Set])
          (fun '[__87, __88, __89] =>
            __87 * (option __88 * (Script_typed_ir.big_map __87 __88 * __89)))
          stack_value in
      let '(key_value, (maybe_value, (map, rest))) := stack_value in
      let big_map :=
        Script_ir_translator.big_map_update key_value maybe_value map in
      logged_return ((cast a (big_map, rest)), ctxt)
    
    | (Script_typed_ir.Big_map_get_and_update, stack_value) =>
      let 'existT _ [__90, __91, __92] stack_value :=
        cast_exists (Es := [Set ** Set ** Set])
          (fun '[__90, __91, __92] =>
            __90 * (option __91 * (Script_typed_ir.big_map __90 __91 * __92)))
          stack_value in
      let '(k, (v, (map, rest))) := stack_value in
      let map' := Script_ir_translator.big_map_update k v map in
      let=? '(v', ctxt) := Script_ir_translator.big_map_get ctxt k map in
      logged_return ((cast a (v', (map', rest))), ctxt)
    
    | (Script_typed_ir.Add_seconds_to_timestamp, stack_value) =>
      let 'existT _ __93 stack_value :=
        cast_exists (Es := Set)
          (fun __93 =>
            Alpha_context.Script_int.num *
              (Alpha_context.Script_timestamp.t * __93)) stack_value in
      let '(n, (t_value, rest)) := stack_value in
      let result_value := Alpha_context.Script_timestamp.add_delta t_value n in
      logged_return ((cast a (result_value, rest)), ctxt)
    
    | (Script_typed_ir.Add_timestamp_to_seconds, stack_value) =>
      let 'existT _ __94 stack_value :=
        cast_exists (Es := Set)
          (fun __94 =>
            Alpha_context.Script_timestamp.t *
              (Alpha_context.Script_int.num * __94)) stack_value in
      let '(t_value, (n, rest)) := stack_value in
      let result_value := Alpha_context.Script_timestamp.add_delta t_value n in
      logged_return ((cast a (result_value, rest)), ctxt)
    
    | (Script_typed_ir.Sub_timestamp_seconds, stack_value) =>
      let 'existT _ __95 stack_value :=
        cast_exists (Es := Set)
          (fun __95 =>
            Alpha_context.Script_timestamp.t *
              (Alpha_context.Script_int.num * __95)) stack_value in
      let '(t_value, (s, rest)) := stack_value in
      let result_value := Alpha_context.Script_timestamp.sub_delta t_value s in
      logged_return ((cast a (result_value, rest)), ctxt)
    
    | (Script_typed_ir.Diff_timestamps, stack_value) =>
      let 'existT _ __96 stack_value :=
        cast_exists (Es := Set)
          (fun __96 =>
            Alpha_context.Script_timestamp.t *
              (Alpha_context.Script_timestamp.t * __96)) stack_value in
      let '(t1, (t2, rest)) := stack_value in
      let result_value := Alpha_context.Script_timestamp.diff_value t1 t2 in
      logged_return ((cast a (result_value, rest)), ctxt)
    
    | (Script_typed_ir.Concat_string_pair, stack_value) =>
      let 'existT _ __97 stack_value :=
        cast_exists (Es := Set) (fun __97 => string * (string * __97))
          stack_value in
      let '(x, (y, rest)) := stack_value in
      let s := String.concat "" [ x; y ] in
      logged_return ((cast a (s, rest)), ctxt)
    
    | (Script_typed_ir.Concat_string, stack_value) =>
      let 'existT _ __98 stack_value :=
        cast_exists (Es := Set)
          (fun __98 => Script_typed_ir.boxed_list string * __98) stack_value in
      let '(ss, rest) := stack_value in
      let total_length :=
        List.fold_left
          (fun (acc_value : Z.t) =>
            fun (s : string) => acc_value +Z (Z.of_int (String.length s)))
          Z.zero ss.(Script_typed_ir.boxed_list.elements) in
      let=? ctxt :=
        return=
          (Alpha_context.Gas.consume ctxt
            (Interp_costs.concat_string total_length)) in
      let s := String.concat "" ss.(Script_typed_ir.boxed_list.elements) in
      logged_return ((cast a (s, rest)), ctxt)
    
    | (Script_typed_ir.Slice_string, stack_value) =>
      let 'existT _ __99 stack_value :=
        cast_exists (Es := Set)
          (fun __99 =>
            Alpha_context.Script_int.num *
              (Alpha_context.Script_int.num * (string * __99))) stack_value in
      let '(offset, (length, (s, rest))) := stack_value in
      let s_length := Z.of_int (String.length s) in
      let offset := Alpha_context.Script_int.to_zint offset in
      let length := Alpha_context.Script_int.to_zint length in
      if (offset <Z s_length) && ((offset +Z length) <=Z s_length) then
        logged_return
          ((cast a
            ((Some (String.sub s (Z.to_int offset) (Z.to_int length))), rest)),
            ctxt)
      else
        logged_return ((cast a ((None : option string), rest)), ctxt)
    
    | (Script_typed_ir.String_size, stack_value) =>
      let 'existT _ __100 stack_value :=
        cast_exists (Es := Set) (fun __100 => string * __100) stack_value in
      let '(s, rest) := stack_value in
      logged_return
        ((cast a
          ((Alpha_context.Script_int.abs
            (Alpha_context.Script_int.of_int (String.length s))), rest)), ctxt)
    
    | (Script_typed_ir.Concat_bytes_pair, stack_value) =>
      let 'existT _ __101 stack_value :=
        cast_exists (Es := Set) (fun __101 => bytes * (bytes * __101))
          stack_value in
      let '(x, (y, rest)) := stack_value in
      let s := Bytes.cat x y in
      logged_return ((cast a (s, rest)), ctxt)
    
    | (Script_typed_ir.Concat_bytes, stack_value) =>
      let 'existT _ __102 stack_value :=
        cast_exists (Es := Set)
          (fun __102 => Script_typed_ir.boxed_list bytes * __102) stack_value in
      let '(ss, rest) := stack_value in
      let total_length :=
        List.fold_left
          (fun (acc_value : Z.t) =>
            fun (s : bytes) => acc_value +Z (Z.of_int (Bytes.length s))) Z.zero
          ss.(Script_typed_ir.boxed_list.elements) in
      let=? ctxt :=
        return=
          (Alpha_context.Gas.consume ctxt
            (Interp_costs.concat_string total_length)) in
      let s := Bytes.concat Bytes.empty ss.(Script_typed_ir.boxed_list.elements)
        in
      logged_return ((cast a (s, rest)), ctxt)
    
    | (Script_typed_ir.Slice_bytes, stack_value) =>
      let 'existT _ __103 stack_value :=
        cast_exists (Es := Set)
          (fun __103 =>
            Alpha_context.Script_int.num *
              (Alpha_context.Script_int.num * (bytes * __103))) stack_value in
      let '(offset, (length, (s, rest))) := stack_value in
      let s_length := Z.of_int (Bytes.length s) in
      let offset := Alpha_context.Script_int.to_zint offset in
      let length := Alpha_context.Script_int.to_zint length in
      if (offset <Z s_length) && ((offset +Z length) <=Z s_length) then
        logged_return
          ((cast a
            ((Some (Bytes.sub s (Z.to_int offset) (Z.to_int length))), rest)),
            ctxt)
      else
        logged_return ((cast a ((None : option bytes), rest)), ctxt)
    
    | (Script_typed_ir.Bytes_size, stack_value) =>
      let 'existT _ __104 stack_value :=
        cast_exists (Es := Set) (fun __104 => bytes * __104) stack_value in
      let '(s, rest) := stack_value in
      logged_return
        ((cast a
          ((Alpha_context.Script_int.abs
            (Alpha_context.Script_int.of_int (Bytes.length s))), rest)), ctxt)
    
    | (Script_typed_ir.Add_tez, stack_value) =>
      let 'existT _ __105 stack_value :=
        cast_exists (Es := Set)
          (fun __105 => Alpha_context.Tez.t * (Alpha_context.Tez.t * __105))
          stack_value in
      let '(x, (y, rest)) := stack_value in
      let=? res := return= (Alpha_context.Tez.op_plusquestion x y) in
      logged_return ((cast a (res, rest)), ctxt)
    
    | (Script_typed_ir.Sub_tez, stack_value) =>
      let 'existT _ __106 stack_value :=
        cast_exists (Es := Set)
          (fun __106 => Alpha_context.Tez.t * (Alpha_context.Tez.t * __106))
          stack_value in
      let '(x, (y, rest)) := stack_value in
      let=? res := return= (Alpha_context.Tez.op_minusquestion x y) in
      logged_return ((cast a (res, rest)), ctxt)
    
    | (Script_typed_ir.Mul_teznat, stack_value) =>
      let 'existT _ __107 stack_value :=
        cast_exists (Es := Set)
          (fun __107 =>
            Alpha_context.Tez.t * (Alpha_context.Script_int.num * __107))
          stack_value in
      let '(x, (y, rest)) := stack_value in
      match Alpha_context.Script_int.to_int64 y with
      | None =>
        let=? log := Log.(STEP_LOGGER.get_log) tt in
        Error_monad.fail
          (Build_extensible "Overflow"
            (Alpha_context.Script.location * option execution_trace) (loc, log))
      | Some y =>
        let=? res := return= (Alpha_context.Tez.op_starquestion x y) in
        logged_return ((cast a (res, rest)), ctxt)
      end
    
    | (Script_typed_ir.Mul_nattez, stack_value) =>
      let 'existT _ __108 stack_value :=
        cast_exists (Es := Set)
          (fun __108 =>
            Alpha_context.Script_int.num * (Alpha_context.Tez.t * __108))
          stack_value in
      let '(y, (x, rest)) := stack_value in
      match Alpha_context.Script_int.to_int64 y with
      | None =>
        let=? log := Log.(STEP_LOGGER.get_log) tt in
        Error_monad.fail
          (Build_extensible "Overflow"
            (Alpha_context.Script.location * option execution_trace) (loc, log))
      | Some y =>
        let=? res := return= (Alpha_context.Tez.op_starquestion x y) in
        logged_return ((cast a (res, rest)), ctxt)
      end
    
    | (Script_typed_ir.Or, stack_value) =>
      let 'existT _ __109 stack_value :=
        cast_exists (Es := Set) (fun __109 => bool * (bool * __109)) stack_value
        in
      let '(x, (y, rest)) := stack_value in
      logged_return ((cast a ((x || y), rest)), ctxt)
    
    | (Script_typed_ir.And, stack_value) =>
      let 'existT _ __110 stack_value :=
        cast_exists (Es := Set) (fun __110 => bool * (bool * __110)) stack_value
        in
      let '(x, (y, rest)) := stack_value in
      logged_return ((cast a ((x && y), rest)), ctxt)
    
    | (Script_typed_ir.Xor, stack_value) =>
      let 'existT _ __111 stack_value :=
        cast_exists (Es := Set) (fun __111 => bool * (bool * __111)) stack_value
        in
      let '(x, (y, rest)) := stack_value in
      logged_return
        ((cast a ((Compare.Bool.(Compare.S.op_ltgt) x y), rest)), ctxt)
    
    | (Script_typed_ir.Not, stack_value) =>
      let 'existT _ __112 stack_value :=
        cast_exists (Es := Set) (fun __112 => bool * __112) stack_value in
      let '(x, rest) := stack_value in
      logged_return ((cast a ((Pervasives.not x), rest)), ctxt)
    
    | (Script_typed_ir.Is_nat, stack_value) =>
      let 'existT _ __113 stack_value :=
        cast_exists (Es := Set)
          (fun __113 => Alpha_context.Script_int.num * __113) stack_value in
      let '(x, rest) := stack_value in
      logged_return ((cast a ((Alpha_context.Script_int.is_nat x), rest)), ctxt)
    
    | (Script_typed_ir.Abs_int, stack_value) =>
      let 'existT _ __114 stack_value :=
        cast_exists (Es := Set)
          (fun __114 => Alpha_context.Script_int.num * __114) stack_value in
      let '(x, rest) := stack_value in
      logged_return ((cast a ((Alpha_context.Script_int.abs x), rest)), ctxt)
    
    | (Script_typed_ir.Int_nat, stack_value) =>
      let 'existT _ __115 stack_value :=
        cast_exists (Es := Set)
          (fun __115 => Alpha_context.Script_int.num * __115) stack_value in
      let '(x, rest) := stack_value in
      logged_return
        ((cast a ((Alpha_context.Script_int.int_value x), rest)), ctxt)
    
    | (Script_typed_ir.Neg_int, stack_value) =>
      let 'existT _ __116 stack_value :=
        cast_exists (Es := Set)
          (fun __116 => Alpha_context.Script_int.num * __116) stack_value in
      let '(x, rest) := stack_value in
      logged_return ((cast a ((Alpha_context.Script_int.neg x), rest)), ctxt)
    
    | (Script_typed_ir.Neg_nat, stack_value) =>
      let 'existT _ __117 stack_value :=
        cast_exists (Es := Set)
          (fun __117 => Alpha_context.Script_int.num * __117) stack_value in
      let '(x, rest) := stack_value in
      logged_return ((cast a ((Alpha_context.Script_int.neg x), rest)), ctxt)
    
    | (Script_typed_ir.Add_intint, stack_value) =>
      let 'existT _ __118 stack_value :=
        cast_exists (Es := Set)
          (fun __118 =>
            Alpha_context.Script_int.num *
              (Alpha_context.Script_int.num * __118)) stack_value in
      let '(x, (y, rest)) := stack_value in
      logged_return ((cast a ((Alpha_context.Script_int.add x y), rest)), ctxt)
    
    | (Script_typed_ir.Add_intnat, stack_value) =>
      let 'existT _ __119 stack_value :=
        cast_exists (Es := Set)
          (fun __119 =>
            Alpha_context.Script_int.num *
              (Alpha_context.Script_int.num * __119)) stack_value in
      let '(x, (y, rest)) := stack_value in
      logged_return ((cast a ((Alpha_context.Script_int.add x y), rest)), ctxt)
    
    | (Script_typed_ir.Add_natint, stack_value) =>
      let 'existT _ __120 stack_value :=
        cast_exists (Es := Set)
          (fun __120 =>
            Alpha_context.Script_int.num *
              (Alpha_context.Script_int.num * __120)) stack_value in
      let '(x, (y, rest)) := stack_value in
      logged_return ((cast a ((Alpha_context.Script_int.add x y), rest)), ctxt)
    
    | (Script_typed_ir.Add_natnat, stack_value) =>
      let 'existT _ __121 stack_value :=
        cast_exists (Es := Set)
          (fun __121 =>
            Alpha_context.Script_int.num *
              (Alpha_context.Script_int.num * __121)) stack_value in
      let '(x, (y, rest)) := stack_value in
      logged_return
        ((cast a ((Alpha_context.Script_int.add_n x y), rest)), ctxt)
    
    | (Script_typed_ir.Sub_int, stack_value) =>
      let 'existT _ [__122, __123, __124] stack_value :=
        cast_exists (Es := [Set ** Set ** Set])
          (fun '[__122, __123, __124] =>
            Alpha_context.Script_int.num *
              (Alpha_context.Script_int.num * __124)) stack_value in
      let '(x, (y, rest)) := stack_value in
      logged_return ((cast a ((Alpha_context.Script_int.sub x y), rest)), ctxt)
    
    | (Script_typed_ir.Mul_intint, stack_value) =>
      let 'existT _ __125 stack_value :=
        cast_exists (Es := Set)
          (fun __125 =>
            Alpha_context.Script_int.num *
              (Alpha_context.Script_int.num * __125)) stack_value in
      let '(x, (y, rest)) := stack_value in
      logged_return ((cast a ((Alpha_context.Script_int.mul x y), rest)), ctxt)
    
    | (Script_typed_ir.Mul_intnat, stack_value) =>
      let 'existT _ __126 stack_value :=
        cast_exists (Es := Set)
          (fun __126 =>
            Alpha_context.Script_int.num *
              (Alpha_context.Script_int.num * __126)) stack_value in
      let '(x, (y, rest)) := stack_value in
      logged_return ((cast a ((Alpha_context.Script_int.mul x y), rest)), ctxt)
    
    | (Script_typed_ir.Mul_natint, stack_value) =>
      let 'existT _ __127 stack_value :=
        cast_exists (Es := Set)
          (fun __127 =>
            Alpha_context.Script_int.num *
              (Alpha_context.Script_int.num * __127)) stack_value in
      let '(x, (y, rest)) := stack_value in
      logged_return ((cast a ((Alpha_context.Script_int.mul x y), rest)), ctxt)
    
    | (Script_typed_ir.Mul_natnat, stack_value) =>
      let 'existT _ __128 stack_value :=
        cast_exists (Es := Set)
          (fun __128 =>
            Alpha_context.Script_int.num *
              (Alpha_context.Script_int.num * __128)) stack_value in
      let '(x, (y, rest)) := stack_value in
      logged_return
        ((cast a ((Alpha_context.Script_int.mul_n x y), rest)), ctxt)
    
    | (Script_typed_ir.Ediv_teznat, stack_value) =>
      let 'existT _ __129 stack_value :=
        cast_exists (Es := Set)
          (fun __129 =>
            Alpha_context.Tez.t * (Alpha_context.Script_int.num * __129))
          stack_value in
      let '(x, (y, rest)) := stack_value in
      let x := Alpha_context.Script_int.of_int64 (Alpha_context.Tez.to_mutez x)
        in
      let result_value :=
        match Alpha_context.Script_int.ediv x y with
        | None => None
        | Some (q, r_value) =>
          match
            ((Alpha_context.Script_int.to_int64 q),
              (Alpha_context.Script_int.to_int64 r_value)) with
          | (Some q, Some r_value) =>
            match
              ((Alpha_context.Tez.of_mutez q),
                (Alpha_context.Tez.of_mutez r_value)) with
            | (Some q, Some r_value) => Some (q, r_value)
            | _ =>
              (* ❌ Assert instruction is not handled. *)
              assert (option (Alpha_context.Tez.tez * Alpha_context.Tez.tez))
                false
            end
          | _ =>
            (* ❌ Assert instruction is not handled. *)
            assert (option (Alpha_context.Tez.tez * Alpha_context.Tez.tez))
              false
          end
        end in
      logged_return ((cast a (result_value, rest)), ctxt)
    
    | (Script_typed_ir.Ediv_tez, stack_value) =>
      let 'existT _ __130 stack_value :=
        cast_exists (Es := Set)
          (fun __130 => Alpha_context.Tez.t * (Alpha_context.Tez.t * __130))
          stack_value in
      let '(x, (y, rest)) := stack_value in
      let x :=
        Alpha_context.Script_int.abs
          (Alpha_context.Script_int.of_int64 (Alpha_context.Tez.to_mutez x)) in
      let y :=
        Alpha_context.Script_int.abs
          (Alpha_context.Script_int.of_int64 (Alpha_context.Tez.to_mutez y)) in
      let result_value :=
        match Alpha_context.Script_int.ediv_n x y with
        | None => None
        | Some (q, r_value) =>
          match Alpha_context.Script_int.to_int64 r_value with
          | None =>
            (* ❌ Assert instruction is not handled. *)
            assert
              (option (Alpha_context.Script_int.num * Alpha_context.Tez.tez))
              false
          | Some r_value =>
            match Alpha_context.Tez.of_mutez r_value with
            | None =>
              (* ❌ Assert instruction is not handled. *)
              assert
                (option (Alpha_context.Script_int.num * Alpha_context.Tez.tez))
                false
            | Some r_value => Some (q, r_value)
            end
          end
        end in
      logged_return ((cast a (result_value, rest)), ctxt)
    
    | (Script_typed_ir.Ediv_intint, stack_value) =>
      let 'existT _ __131 stack_value :=
        cast_exists (Es := Set)
          (fun __131 =>
            Alpha_context.Script_int.num *
              (Alpha_context.Script_int.num * __131)) stack_value in
      let '(x, (y, rest)) := stack_value in
      logged_return ((cast a ((Alpha_context.Script_int.ediv x y), rest)), ctxt)
    
    | (Script_typed_ir.Ediv_intnat, stack_value) =>
      let 'existT _ __132 stack_value :=
        cast_exists (Es := Set)
          (fun __132 =>
            Alpha_context.Script_int.num *
              (Alpha_context.Script_int.num * __132)) stack_value in
      let '(x, (y, rest)) := stack_value in
      logged_return ((cast a ((Alpha_context.Script_int.ediv x y), rest)), ctxt)
    
    | (Script_typed_ir.Ediv_natint, stack_value) =>
      let 'existT _ __133 stack_value :=
        cast_exists (Es := Set)
          (fun __133 =>
            Alpha_context.Script_int.num *
              (Alpha_context.Script_int.num * __133)) stack_value in
      let '(x, (y, rest)) := stack_value in
      logged_return ((cast a ((Alpha_context.Script_int.ediv x y), rest)), ctxt)
    
    | (Script_typed_ir.Ediv_natnat, stack_value) =>
      let 'existT _ __134 stack_value :=
        cast_exists (Es := Set)
          (fun __134 =>
            Alpha_context.Script_int.num *
              (Alpha_context.Script_int.num * __134)) stack_value in
      let '(x, (y, rest)) := stack_value in
      logged_return
        ((cast a ((Alpha_context.Script_int.ediv_n x y), rest)), ctxt)
    
    | (Script_typed_ir.Lsl_nat, stack_value) =>
      let 'existT _ __135 stack_value :=
        cast_exists (Es := Set)
          (fun __135 =>
            Alpha_context.Script_int.num *
              (Alpha_context.Script_int.num * __135)) stack_value in
      let '(x, (y, rest)) := stack_value in
      match Alpha_context.Script_int.shift_left_n x y with
      | None =>
        let=? log := Log.(STEP_LOGGER.get_log) tt in
        Error_monad.fail
          (Build_extensible "Overflow"
            (Alpha_context.Script.location * option execution_trace) (loc, log))
      | Some x => logged_return ((cast a (x, rest)), ctxt)
      end
    
    | (Script_typed_ir.Lsr_nat, stack_value) =>
      let 'existT _ __136 stack_value :=
        cast_exists (Es := Set)
          (fun __136 =>
            Alpha_context.Script_int.num *
              (Alpha_context.Script_int.num * __136)) stack_value in
      let '(x, (y, rest)) := stack_value in
      match Alpha_context.Script_int.shift_right_n x y with
      | None =>
        let=? log := Log.(STEP_LOGGER.get_log) tt in
        Error_monad.fail
          (Build_extensible "Overflow"
            (Alpha_context.Script.location * option execution_trace) (loc, log))
      | Some r_value => logged_return ((cast a (r_value, rest)), ctxt)
      end
    
    | (Script_typed_ir.Or_nat, stack_value) =>
      let 'existT _ __137 stack_value :=
        cast_exists (Es := Set)
          (fun __137 =>
            Alpha_context.Script_int.num *
              (Alpha_context.Script_int.num * __137)) stack_value in
      let '(x, (y, rest)) := stack_value in
      logged_return
        ((cast a ((Alpha_context.Script_int.logor x y), rest)), ctxt)
    
    | (Script_typed_ir.And_nat, stack_value) =>
      let 'existT _ __138 stack_value :=
        cast_exists (Es := Set)
          (fun __138 =>
            Alpha_context.Script_int.num *
              (Alpha_context.Script_int.num * __138)) stack_value in
      let '(x, (y, rest)) := stack_value in
      logged_return
        ((cast a ((Alpha_context.Script_int.logand x y), rest)), ctxt)
    
    | (Script_typed_ir.And_int_nat, stack_value) =>
      let 'existT _ __139 stack_value :=
        cast_exists (Es := Set)
          (fun __139 =>
            Alpha_context.Script_int.num *
              (Alpha_context.Script_int.num * __139)) stack_value in
      let '(x, (y, rest)) := stack_value in
      logged_return
        ((cast a ((Alpha_context.Script_int.logand x y), rest)), ctxt)
    
    | (Script_typed_ir.Xor_nat, stack_value) =>
      let 'existT _ __140 stack_value :=
        cast_exists (Es := Set)
          (fun __140 =>
            Alpha_context.Script_int.num *
              (Alpha_context.Script_int.num * __140)) stack_value in
      let '(x, (y, rest)) := stack_value in
      logged_return
        ((cast a ((Alpha_context.Script_int.logxor x y), rest)), ctxt)
    
    | (Script_typed_ir.Not_int, stack_value) =>
      let 'existT _ __141 stack_value :=
        cast_exists (Es := Set)
          (fun __141 => Alpha_context.Script_int.num * __141) stack_value in
      let '(x, rest) := stack_value in
      logged_return ((cast a ((Alpha_context.Script_int.lognot x), rest)), ctxt)
    
    | (Script_typed_ir.Not_nat, stack_value) =>
      let 'existT _ __142 stack_value :=
        cast_exists (Es := Set)
          (fun __142 => Alpha_context.Script_int.num * __142) stack_value in
      let '(x, rest) := stack_value in
      logged_return ((cast a ((Alpha_context.Script_int.lognot x), rest)), ctxt)
    
    | (Script_typed_ir.Seq hd tl, stack_value) =>
      let 'existT _ __Seq_'trans [stack_value, tl, hd] :=
        cast_exists (Es := Set)
          (fun __Seq_'trans =>
            [b ** Script_typed_ir.descr ** Script_typed_ir.descr])
          [stack_value, tl, hd] in
      let=? '(trans, ctxt) :=
        ((non_terminal_recursion ctxt None hd stack_value) :
          M=? (__Seq_'trans * Alpha_context.context)) in
      step_bounded logger stack_depth ctxt step_constants tl trans
    
    | (Script_typed_ir.If bt bf, v) =>
      let 'existT _ __143 [v, bf, bt] :=
        cast_exists (Es := Set)
          (fun __143 =>
            [bool * __143 ** Script_typed_ir.descr ** Script_typed_ir.descr])
          [v, bf, bt] in
      let '(b_value, rest) := v in
      if b_value then
        step_bounded logger stack_depth ctxt step_constants bt rest
      else
        step_bounded logger stack_depth ctxt step_constants bf rest
    
    | (Script_typed_ir.Loop body, v) =>
      let 'existT _ __144 [v, body] :=
        cast_exists (Es := Set)
          (fun __144 => [bool * __144 ** Script_typed_ir.descr]) [v, body] in
      let '(b_value, rest) := v in
      if b_value then
        let=? '(trans, ctxt) :=
          ((non_terminal_recursion ctxt None body rest) :
            M=? (b * Alpha_context.context)) in
        step_bounded logger stack_depth ctxt step_constants descr_value trans
      else
        logged_return ((cast a rest), ctxt)
    
    | (Script_typed_ir.Loop_left body, stack_value) =>
      let 'existT _ [__145, __146, __147] [stack_value, body] :=
        cast_exists (Es := [Set ** Set ** Set])
          (fun '[__145, __146, __147] =>
            [Script_typed_ir.union __145 __146 * __147 ** Script_typed_ir.descr])
          [stack_value, body] in
      let '(v, rest) := stack_value in
      match v with
      | Script_typed_ir.L v =>
        let=? '(trans, ctxt) :=
          ((non_terminal_recursion ctxt None body (v, rest)) :
            M=? (b * Alpha_context.context)) in
        step_bounded logger stack_depth ctxt step_constants descr_value trans
      | Script_typed_ir.R v => logged_return ((cast a (v, rest)), ctxt)
      end
    
    | (Script_typed_ir.Dip b_value, stack_value) =>
      let 'existT _ [__148, __149, __150] [stack_value, b_value] :=
        cast_exists (Es := [Set ** Set ** Set])
          (fun '[__148, __149, __150] =>
            [__148 * __149 ** Script_typed_ir.descr]) [stack_value, b_value] in
      let '(ign, rest) := stack_value in
      let=? '(res, ctxt) :=
        ((non_terminal_recursion ctxt None b_value rest) :
          M=? (__150 * Alpha_context.context)) in
      logged_return ((cast a (ign, res)), ctxt)
    
    | (Script_typed_ir.Exec, stack_value) =>
      let 'existT _ [__151, __152, __153] stack_value :=
        cast_exists (Es := [Set ** Set ** Set])
          (fun '[__151, __152, __153] =>
            __151 * (Script_typed_ir.lambda * __153)) stack_value in
      let '(arg, (Script_typed_ir.Lam code _, rest)) := stack_value in
      let '_ := Log.(STEP_LOGGER.log_interp) ctxt code (arg, tt) in
      let=? '((res, _), ctxt) :=
        ((non_terminal_recursion ctxt None code (arg, tt)) :
          M=? ((__152 * Script_typed_ir.end_of_stack) * Alpha_context.context))
        in
      logged_return ((cast a (res, rest)), ctxt)
    
    | (Script_typed_ir.Apply capture_ty, stack_value) =>
      let 'existT _ [__154, __155, __156, __157] [stack_value, capture_ty] :=
        cast_exists (Es := [Set ** Set ** Set ** Set])
          (fun '[__154, __155, __156, __157] =>
            [__154 * (Script_typed_ir.lambda * __157) ** Script_typed_ir.ty])
          [stack_value, capture_ty] in
      let '(capture, (lam, rest)) := stack_value in
      let 'Script_typed_ir.Lam descr_value expr := lam in
      match descr_value.(Script_typed_ir.descr.bef) with
      | Script_typed_ir.Item_t full_arg_ty _ =>
        let=? '(const_expr, ctxt) :=
          Script_ir_translator.unparse_data ctxt Script_ir_translator.Optimized
            capture_ty capture in
        let=? '(ty_expr, ctxt) :=
          return= (Script_ir_translator.unparse_ty ctxt capture_ty) in
        match full_arg_ty with
        | Script_typed_ir.Pair_t capture_ty arg_ty =>
          let arg_stack_ty :=
            Script_typed_ir.Item_t arg_ty Script_typed_ir.Empty_t in
          let const_descr :=
            {|
              Script_typed_ir.descr.loc :=
                descr_value.(Script_typed_ir.descr.loc);
              Script_typed_ir.descr.bef := arg_stack_ty;
              Script_typed_ir.descr.aft :=
                Script_typed_ir.Item_t capture_ty arg_stack_ty;
              Script_typed_ir.descr.instr := Script_typed_ir.Const capture |} in
          let pair_descr :=
            {|
              Script_typed_ir.descr.loc :=
                descr_value.(Script_typed_ir.descr.loc);
              Script_typed_ir.descr.bef :=
                Script_typed_ir.Item_t capture_ty arg_stack_ty;
              Script_typed_ir.descr.aft :=
                Script_typed_ir.Item_t full_arg_ty Script_typed_ir.Empty_t;
              Script_typed_ir.descr.instr := Script_typed_ir.Cons_pair |} in
          let seq_descr :=
            {|
              Script_typed_ir.descr.loc :=
                descr_value.(Script_typed_ir.descr.loc);
              Script_typed_ir.descr.bef := arg_stack_ty;
              Script_typed_ir.descr.aft :=
                Script_typed_ir.Item_t full_arg_ty Script_typed_ir.Empty_t;
              Script_typed_ir.descr.instr :=
                Script_typed_ir.Seq const_descr pair_descr |} in
          let full_descr :=
            {|
              Script_typed_ir.descr.loc :=
                descr_value.(Script_typed_ir.descr.loc);
              Script_typed_ir.descr.bef := arg_stack_ty;
              Script_typed_ir.descr.aft :=
                descr_value.(Script_typed_ir.descr.aft);
              Script_typed_ir.descr.instr :=
                Script_typed_ir.Seq seq_descr descr_value |} in
          let full_expr :=
            Micheline.Seq 0
              [
                Micheline.Prim 0 Michelson_v1_primitives.I_PUSH
                  [ ty_expr; const_expr ] nil;
                Micheline.Prim 0 Michelson_v1_primitives.I_PAIR nil nil;
                expr
              ] in
          let lam' := Script_typed_ir.Lam full_descr full_expr in
          logged_return ((cast a (lam', rest)), ctxt)
        | _ =>
          (* ❌ Assert instruction is not handled. *)
          assert (M=? (a * Alpha_context.context)) false
        end
      | _ => unreachable_gadt_branch
      end
    
    | (Script_typed_ir.Lambda lam, rest) =>
      let 'existT _ [__158, __159] [rest, lam] :=
        cast_exists (Es := [Set ** Set])
          (fun '[__158, __159] => [b ** Script_typed_ir.lambda]) [rest, lam] in
      logged_return ((cast a (lam, rest)), ctxt)
    
    | (Script_typed_ir.Failwith tv, stack_value) =>
      let 'existT _ [__160, __161] [stack_value, tv] :=
        cast_exists (Es := [Set ** Set])
          (fun '[__160, __161] => [__160 * __161 ** Script_typed_ir.ty])
          [stack_value, tv] in
      let '(v, _) := stack_value in
      let=? '(v, _ctxt) :=
        Error_monad.trace_value
          (Build_extensible "Cannot_serialize_failure" unit tt)
          (Script_ir_translator.unparse_data ctxt Script_ir_translator.Optimized
            tv v) in
      let v := Micheline.strip_locations v in
      let=? log := Log.(STEP_LOGGER.get_log) tt in
      Error_monad.fail
        (Build_extensible "Reject"
          (Alpha_context.Script.location *
            Micheline.canonical Alpha_context.Script.prim *
            option execution_trace) (loc, v, log))
    
    | (Script_typed_ir.Nop, stack_value) =>
      let stack_value := cast b stack_value in
      logged_return ((cast a stack_value), ctxt)
    
    | (Script_typed_ir.Compare ty, stack_value) =>
      let 'existT _ [__162, __163] [stack_value, ty] :=
        cast_exists (Es := [Set ** Set])
          (fun '[__162, __163] =>
            [__162 * (__162 * __163) ** Script_typed_ir.comparable_ty])
          [stack_value, ty] in
      let '(a_value, (b_value, rest)) := stack_value in
      logged_return
        ((cast a
          ((Alpha_context.Script_int.of_int
            (Script_ir_translator.compare_comparable ty a_value b_value)), rest)),
          ctxt)
    
    | (Script_typed_ir.Eq, stack_value) =>
      let 'existT _ __164 stack_value :=
        cast_exists (Es := Set)
          (fun __164 => Alpha_context.Script_int.num * __164) stack_value in
      let '(cmpres, rest) := stack_value in
      let cmpres :=
        Alpha_context.Script_int.compare cmpres Alpha_context.Script_int.zero in
      let cmpres := cmpres =i 0 in
      logged_return ((cast a (cmpres, rest)), ctxt)
    
    | (Script_typed_ir.Neq, stack_value) =>
      let 'existT _ __165 stack_value :=
        cast_exists (Es := Set)
          (fun __165 => Alpha_context.Script_int.num * __165) stack_value in
      let '(cmpres, rest) := stack_value in
      let cmpres :=
        Alpha_context.Script_int.compare cmpres Alpha_context.Script_int.zero in
      let cmpres := cmpres <>i 0 in
      logged_return ((cast a (cmpres, rest)), ctxt)
    
    | (Script_typed_ir.Lt, stack_value) =>
      let 'existT _ __166 stack_value :=
        cast_exists (Es := Set)
          (fun __166 => Alpha_context.Script_int.num * __166) stack_value in
      let '(cmpres, rest) := stack_value in
      let cmpres :=
        Alpha_context.Script_int.compare cmpres Alpha_context.Script_int.zero in
      let cmpres := cmpres <i 0 in
      logged_return ((cast a (cmpres, rest)), ctxt)
    
    | (Script_typed_ir.Le, stack_value) =>
      let 'existT _ __167 stack_value :=
        cast_exists (Es := Set)
          (fun __167 => Alpha_context.Script_int.num * __167) stack_value in
      let '(cmpres, rest) := stack_value in
      let cmpres :=
        Alpha_context.Script_int.compare cmpres Alpha_context.Script_int.zero in
      let cmpres := cmpres <=i 0 in
      logged_return ((cast a (cmpres, rest)), ctxt)
    
    | (Script_typed_ir.Gt, stack_value) =>
      let 'existT _ __168 stack_value :=
        cast_exists (Es := Set)
          (fun __168 => Alpha_context.Script_int.num * __168) stack_value in
      let '(cmpres, rest) := stack_value in
      let cmpres :=
        Alpha_context.Script_int.compare cmpres Alpha_context.Script_int.zero in
      let cmpres := cmpres >i 0 in
      logged_return ((cast a (cmpres, rest)), ctxt)
    
    | (Script_typed_ir.Ge, stack_value) =>
      let 'existT _ __169 stack_value :=
        cast_exists (Es := Set)
          (fun __169 => Alpha_context.Script_int.num * __169) stack_value in
      let '(cmpres, rest) := stack_value in
      let cmpres :=
        Alpha_context.Script_int.compare cmpres Alpha_context.Script_int.zero in
      let cmpres := cmpres >=i 0 in
      logged_return ((cast a (cmpres, rest)), ctxt)
    
    | (Script_typed_ir.Pack t_value, stack_value) =>
      let 'existT _ [__170, __171] [stack_value, t_value] :=
        cast_exists (Es := [Set ** Set])
          (fun '[__170, __171] => [__170 * __171 ** Script_typed_ir.ty])
          [stack_value, t_value] in
      let '(value, rest) := stack_value in
      let=? '(bytes_value, ctxt) :=
        Script_ir_translator.pack_data ctxt t_value value in
      logged_return ((cast a (bytes_value, rest)), ctxt)
    
    | (Script_typed_ir.Unpack ty, stack_value) =>
      let 'existT _ [__172, __173] [stack_value, ty] :=
        cast_exists (Es := [Set ** Set])
          (fun '[__172, __173] => [bytes * __172 ** Script_typed_ir.ty])
          [stack_value, ty] in
      let '(bytes_value, rest) := stack_value in
      let=? '(opt, ctxt) :=
        ((unpack ctxt ty bytes_value) :
          M=? (option __173 * Alpha_context.context)) in
      logged_return ((cast a (opt, rest)), ctxt)
    
    | (Script_typed_ir.Address, stack_value) =>
      let 'existT _ [__174, __175] stack_value :=
        cast_exists (Es := [Set ** Set])
          (fun '[__174, __175] =>
            (Script_typed_ir.ty * Script_typed_ir.address) * __175) stack_value
        in
      let '((_, address), rest) := stack_value in
      logged_return ((cast a (address, rest)), ctxt)
    
    | (Script_typed_ir.Contract t_value entrypoint, stack_value) =>
      let 'existT _ [__176, __177] [stack_value, entrypoint, t_value] :=
        cast_exists (Es := [Set ** Set])
          (fun '[__176, __177] =>
            [Script_typed_ir.address * __176 ** string ** Script_typed_ir.ty])
          [stack_value, entrypoint, t_value] in
      let '((contract, contract_entrypoint), rest) := stack_value in
      let entrypoint :=
        if String.equal contract_entrypoint "default" then
          Some entrypoint
        else
          if String.equal entrypoint "default" then
            Some contract_entrypoint
          else
            None in
      match entrypoint with
      | Some entrypoint =>
        let=? '(ctxt, maybe_contract) :=
          Script_ir_translator.parse_contract_for_script ctxt loc t_value
            contract entrypoint in
        logged_return ((cast a (maybe_contract, rest)), ctxt)
      | None =>
        logged_return
          ((cast a ((None : option Script_typed_ir.typed_contract), rest)), ctxt)
      end
    
    | (Script_typed_ir.Transfer_tokens, stack_value) =>
      let 'existT _ [__178, __179] stack_value :=
        cast_exists (Es := [Set ** Set])
          (fun '[__178, __179] =>
            __178 * (Tez_repr.t * (Script_typed_ir.typed_contract * __179)))
          stack_value in
      let '(p_value, (amount, ((tp, (destination, entrypoint)), rest))) :=
        stack_value in
      let=? '(to_duplicate, ctxt) :=
        return= (Script_ir_translator.collect_lazy_storage ctxt tp p_value) in
      let to_update := Script_ir_translator.no_lazy_storage_id in
      let=? '(p_value, lazy_storage_diff, ctxt) :=
        Script_ir_translator.extract_lazy_storage_diff ctxt
          Script_ir_translator.Optimized true to_duplicate to_update tp p_value
        in
      let=? '(p_value, ctxt) :=
        Script_ir_translator.unparse_data ctxt Script_ir_translator.Optimized tp
          p_value in
      let=? ctxt :=
        return=
          (Alpha_context.Gas.consume ctxt
            (Alpha_context.Script.strip_locations_cost p_value)) in
      let operation :=
        Alpha_context.Transaction
          {| Alpha_context.manager_operation.Transaction.amount := amount;
            Alpha_context.manager_operation.Transaction.parameters :=
              Alpha_context.Script.lazy_expr_value
                (Micheline.strip_locations p_value);
            Alpha_context.manager_operation.Transaction.entrypoint := entrypoint;
            Alpha_context.manager_operation.Transaction.destination :=
              destination |} in
      let=? '(ctxt, nonce_value) :=
        return= (Alpha_context.fresh_internal_nonce ctxt) in
      logged_return
        ((cast a
          (((Alpha_context.Internal_operation
            {|
              Alpha_context.internal_operation.source :=
                step_constants.(step_constants.self);
              Alpha_context.internal_operation.operation := operation;
              Alpha_context.internal_operation.nonce := nonce_value |}),
            lazy_storage_diff), rest)), ctxt)
    
    | (Script_typed_ir.Implicit_account, stack_value) =>
      let 'existT _ __180 stack_value :=
        cast_exists (Es := Set)
          (fun __180 => Alpha_context.public_key_hash * __180) stack_value in
      let '(key_value, rest) := stack_value in
      let contract := Alpha_context.Contract.implicit_contract key_value in
      logged_return
        ((cast a ((Script_typed_ir.Unit_t, (contract, "default")), rest)), ctxt)
    
    |
      (Script_typed_ir.Create_contract storage_type param_type
        (Script_typed_ir.Lam _ code) entrypoints, stack_value) =>
      let 'existT _ [__181, __182, __Create_contract_'p]
        [stack_value, entrypoints, code, param_type, storage_type] :=
        cast_exists (Es := [Set ** Set ** Set])
          (fun '[__181, __182, __Create_contract_'p] =>
            [option Alpha_context.public_key_hash *
              (Tez_repr.t * (__181 * __182)) ** Script_typed_ir.entrypoints **
              Alpha_context.Script.node ** Script_typed_ir.ty **
              Script_typed_ir.ty])
          [stack_value, entrypoints, code, param_type, storage_type] in
      let '(delegate, (credit, (init_value, rest))) := stack_value in
      let=? '(unparsed_param_type, ctxt) :=
        return=
          (Script_ir_translator.unparse_parameter_ty ctxt param_type entrypoints)
        in
      let=? '(unparsed_storage_type, ctxt) :=
        return= (Script_ir_translator.unparse_ty ctxt storage_type) in
      let code :=
        Micheline.strip_locations
          (Micheline.Seq 0
            [
              Micheline.Prim 0 Michelson_v1_primitives.K_parameter
                [ unparsed_param_type ] nil;
              Micheline.Prim 0 Michelson_v1_primitives.K_storage
                [ unparsed_storage_type ] nil;
              Micheline.Prim 0 Michelson_v1_primitives.K_code [ code ] nil
            ]) in
      let=? '(to_duplicate, ctxt) :=
        return=
          (Script_ir_translator.collect_lazy_storage ctxt storage_type
            init_value) in
      let to_update := Script_ir_translator.no_lazy_storage_id in
      let=? '(init_value, lazy_storage_diff, ctxt) :=
        Script_ir_translator.extract_lazy_storage_diff ctxt
          Script_ir_translator.Optimized true to_duplicate to_update
          storage_type init_value in
      let=? '(storage_value, ctxt) :=
        Script_ir_translator.unparse_data ctxt Script_ir_translator.Optimized
          storage_type init_value in
      let=? ctxt :=
        return=
          (Alpha_context.Gas.consume ctxt
            (Alpha_context.Script.strip_locations_cost storage_value)) in
      let storage_value := Micheline.strip_locations storage_value in
      let=? '(ctxt, contract) :=
        return= (Alpha_context.Contract.fresh_contract_from_current_nonce ctxt)
        in
      let operation :=
        Alpha_context.Origination
          {| Alpha_context.manager_operation.Origination.delegate := delegate;
            Alpha_context.manager_operation.Origination.script :=
              {|
                Alpha_context.Script.t.code :=
                  Alpha_context.Script.lazy_expr_value code;
                Alpha_context.Script.t.storage :=
                  Alpha_context.Script.lazy_expr_value storage_value |};
            Alpha_context.manager_operation.Origination.credit := credit;
            Alpha_context.manager_operation.Origination.preorigination :=
              Some contract |} in
      let=? '(ctxt, nonce_value) :=
        return= (Alpha_context.fresh_internal_nonce ctxt) in
      logged_return
        ((cast a
          (((Alpha_context.Internal_operation
            {|
              Alpha_context.internal_operation.source :=
                step_constants.(step_constants.self);
              Alpha_context.internal_operation.operation := operation;
              Alpha_context.internal_operation.nonce := nonce_value |}),
            lazy_storage_diff), ((contract, "default"), rest))), ctxt)
    
    | (Script_typed_ir.Set_delegate, stack_value) =>
      let 'existT _ __183 stack_value :=
        cast_exists (Es := Set)
          (fun __183 => option Alpha_context.public_key_hash * __183)
          stack_value in
      let '(delegate, rest) := stack_value in
      let operation := Alpha_context.Delegation delegate in
      let=? '(ctxt, nonce_value) :=
        return= (Alpha_context.fresh_internal_nonce ctxt) in
      logged_return
        ((cast a
          (((Alpha_context.Internal_operation
            {|
              Alpha_context.internal_operation.source :=
                step_constants.(step_constants.self);
              Alpha_context.internal_operation.operation := operation;
              Alpha_context.internal_operation.nonce := nonce_value |}),
            (None : option Alpha_context.Lazy_storage.diffs)), rest)), ctxt)
    
    | (Script_typed_ir.Balance, rest) =>
      let rest := cast b rest in
      let=? '(ctxt, balance) :=
        Alpha_context.Contract.get_balance_carbonated ctxt
          step_constants.(step_constants.self) in
      logged_return ((cast a (balance, rest)), ctxt)
    
    | (Script_typed_ir.Level, rest) =>
      let rest := cast b rest in
      let level :=
        Alpha_context.Script_int.abs
          (Alpha_context.Script_int.of_int32
            (Alpha_context.Raw_level.to_int32
              (Alpha_context.Level.current ctxt).(Alpha_context.Level.t.level)))
        in
      logged_return ((cast a (level, rest)), ctxt)
    
    | (Script_typed_ir.Now, rest) =>
      let rest := cast b rest in
      let now := Alpha_context.Script_timestamp.now ctxt in
      logged_return ((cast a (now, rest)), ctxt)
    
    | (Script_typed_ir.Check_signature, stack_value) =>
      let 'existT _ __184 stack_value :=
        cast_exists (Es := Set)
          (fun __184 =>
            Alpha_context.public_key *
              (Alpha_context.signature * (bytes * __184))) stack_value in
      let '(key_value, (signature, (message, rest))) := stack_value in
      let res := Signature.check None key_value signature message in
      logged_return ((cast a (res, rest)), ctxt)
    
    | (Script_typed_ir.Hash_key, stack_value) =>
      let 'existT _ __185 stack_value :=
        cast_exists (Es := Set) (fun __185 => Alpha_context.public_key * __185)
          stack_value in
      let '(key_value, rest) := stack_value in
      logged_return
        ((cast a
          ((Signature.Public_key.(S.SIGNATURE_PUBLIC_KEY.hash_value) key_value),
            rest)), ctxt)
    
    | (Script_typed_ir.Blake2b, stack_value) =>
      let 'existT _ __186 stack_value :=
        cast_exists (Es := Set) (fun __186 => bytes * __186) stack_value in
      let '(bytes_value, rest) := stack_value in
      let hash_value := Raw_hashes.blake2b bytes_value in
      logged_return ((cast a (hash_value, rest)), ctxt)
    
    | (Script_typed_ir.Sha256, stack_value) =>
      let 'existT _ __187 stack_value :=
        cast_exists (Es := Set) (fun __187 => bytes * __187) stack_value in
      let '(bytes_value, rest) := stack_value in
      let hash_value := Raw_hashes.sha256 bytes_value in
      logged_return ((cast a (hash_value, rest)), ctxt)
    
    | (Script_typed_ir.Sha512, stack_value) =>
      let 'existT _ __188 stack_value :=
        cast_exists (Es := Set) (fun __188 => bytes * __188) stack_value in
      let '(bytes_value, rest) := stack_value in
      let hash_value := Raw_hashes.sha512 bytes_value in
      logged_return ((cast a (hash_value, rest)), ctxt)
    
    | (Script_typed_ir.Source, rest) =>
      let rest := cast b rest in
      logged_return
        ((cast a ((step_constants.(step_constants.payer), "default"), rest)),
          ctxt)
    
    | (Script_typed_ir.Sender, rest) =>
      let rest := cast b rest in
      logged_return
        ((cast a ((step_constants.(step_constants.source), "default"), rest)),
          ctxt)
    
    | (Script_typed_ir.Self t_value entrypoint, rest) =>
      let 'existT _ __189 [rest, entrypoint, t_value] :=
        cast_exists (Es := Set)
          (fun __189 => [b ** string ** Script_typed_ir.ty])
          [rest, entrypoint, t_value] in
      logged_return
        ((cast a
          ((t_value, (step_constants.(step_constants.self), entrypoint)), rest)),
          ctxt)
    
    | (Script_typed_ir.Self_address, rest) =>
      let rest := cast b rest in
      logged_return
        ((cast a ((step_constants.(step_constants.self), "default"), rest)),
          ctxt)
    
    | (Script_typed_ir.Amount, rest) =>
      let rest := cast b rest in
      logged_return
        ((cast a (step_constants.(step_constants.amount), rest)), ctxt)
    
    | (Script_typed_ir.Dig _n n', stack_value) =>
      let 'existT _ [__190, __191, __Dig_'rest] [stack_value, n', _n] :=
        cast_exists (Es := [Set ** Set ** Set])
          (fun '[__190, __191, __Dig_'rest] =>
            [b ** Script_typed_ir.stack_prefix_preservation_witness ** int])
          [stack_value, n', _n] in
      let=? '(aft, x) :=
        ((interp_stack_prefix_preserving_operation
          (fun (function_parameter : __190 * __Dig_'rest) =>
            let '(v, rest) := function_parameter in
            Error_monad._return (rest, v)) n' stack_value) : M=? (__191 * __190))
        in
      logged_return ((cast a (x, aft)), ctxt)
    
    | (Script_typed_ir.Dug _n n', stack_value) =>
      let 'existT _ [__192, __193, __Dug_'rest] [stack_value, n', _n] :=
        cast_exists (Es := [Set ** Set ** Set])
          (fun '[__192, __193, __Dug_'rest] =>
            [__192 * __193 ** Script_typed_ir.stack_prefix_preservation_witness
              ** int]) [stack_value, n', _n] in
      let '(v, rest) := stack_value in
      let=? '(aft, _) :=
        ((interp_stack_prefix_preserving_operation
          (fun (stk : __Dug_'rest) => Error_monad._return ((v, stk), tt)) n'
          rest) : M=? (a * unit)) in
      logged_return ((cast a aft), ctxt)
    
    | (Script_typed_ir.Dipn n n' b_value, stack_value) =>
      let 'existT _ [__Dipn_'faft, __Dipn_'fbef] [stack_value, b_value, n', n]
        :=
        cast_exists (Es := [Set ** Set])
          (fun '[__Dipn_'faft, __Dipn_'fbef] =>
            [b ** Script_typed_ir.descr **
              Script_typed_ir.stack_prefix_preservation_witness ** int])
          [stack_value, b_value, n', n] in
      let=? '(aft, ctxt') :=
        ((interp_stack_prefix_preserving_operation
          (fun (stk : __Dipn_'fbef) =>
            ((non_terminal_recursion ctxt
              (Some ((stack_depth +i 4) +i (n /i 16))) b_value stk) :
              M=? (__Dipn_'faft * Alpha_context.context))) n' stack_value) :
          M=? (a * Alpha_context.context)) in
      logged_return ((cast a aft), ctxt')
    
    | (Script_typed_ir.Dropn _n n', stack_value) =>
      let 'existT _ __Dropn [stack_value, n', _n] :=
        cast_exists (Es := Set)
          (fun __Dropn =>
            [b ** Script_typed_ir.stack_prefix_preservation_witness ** int])
          [stack_value, n', _n] in
      let=? '(_, rest) :=
        ((interp_stack_prefix_preserving_operation
          (fun (stk : a) => Error_monad._return (stk, stk)) n' stack_value) :
          M=? (__Dropn * a)) in
      logged_return ((cast a rest), ctxt)
    
    |
      (Script_typed_ir.Sapling_empty_state {|
        Script_typed_ir.instr.Sapling_empty_state.memo_size := memo_size |},
        stack_value) =>
      let '[stack_value, memo_size] :=
        cast [b ** Alpha_context.Sapling.Memo_size.t] [stack_value, memo_size]
        in
      logged_return
        ((cast a
          ((Alpha_context.Sapling.empty_state None memo_size tt), stack_value)),
          ctxt)
    
    | (Script_typed_ir.Sapling_verify_update, stack_value) =>
      let 'existT _ __194 stack_value :=
        cast_exists (Es := Set)
          (fun __194 =>
            Alpha_context.Sapling.transaction *
              (Alpha_context.Sapling.state * __194)) stack_value in
      let '(transaction, (state, rest)) := stack_value in
      let address :=
        Alpha_context.Contract.to_b58check step_constants.(step_constants.self)
        in
      let chain_id :=
        Chain_id.to_b58check step_constants.(step_constants.chain_id) in
      let anti_replay := Pervasives.op_caret address chain_id in
      let=? '(ctxt, balance_state_opt) :=
        Alpha_context.Sapling.verify_update ctxt state transaction anti_replay
        in
      match balance_state_opt with
      | Some (balance, state) =>
        logged_return
          ((cast a
            ((Some ((Alpha_context.Script_int.of_int64 balance), state)), rest)),
            ctxt)
      | None =>
        logged_return
          ((cast a
            ((None :
              option
                (Script_typed_ir.pair Alpha_context.Script_int.num
                  Alpha_context.Sapling.state)), rest)), ctxt)
      end
    
    | (Script_typed_ir.ChainId, rest) =>
      let rest := cast b rest in
      logged_return
        ((cast a (step_constants.(step_constants.chain_id), rest)), ctxt)
    
    | (Script_typed_ir.Voting_power, stack_value) =>
      let 'existT _ __196 stack_value :=
        cast_exists (Es := Set)
          (fun __196 => Alpha_context.public_key_hash * __196) stack_value in
      let '(key_hash, rest) := stack_value in
      let=? '(ctxt, rolls) := Alpha_context.Vote.get_voting_power ctxt key_hash
        in
      logged_return
        ((cast a
          ((Alpha_context.Script_int.abs
            (Alpha_context.Script_int.of_int32 rolls)), rest)), ctxt)
    
    | (Script_typed_ir.Total_voting_power, rest) =>
      let rest := cast b rest in
      let=? '(ctxt, rolls) := Alpha_context.Vote.get_total_voting_power ctxt in
      logged_return
        ((cast a
          ((Alpha_context.Script_int.abs
            (Alpha_context.Script_int.of_int32 rolls)), rest)), ctxt)
    
    | (Script_typed_ir.Keccak, stack_value) =>
      let 'existT _ __197 stack_value :=
        cast_exists (Es := Set) (fun __197 => bytes * __197) stack_value in
      let '(bytes_value, rest) := stack_value in
      let hash_value := Raw_hashes.keccak256 bytes_value in
      logged_return ((cast a (hash_value, rest)), ctxt)
    
    | (Script_typed_ir.Sha3, stack_value) =>
      let 'existT _ __198 stack_value :=
        cast_exists (Es := Set) (fun __198 => bytes * __198) stack_value in
      let '(bytes_value, rest) := stack_value in
      let hash_value := Raw_hashes.sha3_256 bytes_value in
      logged_return ((cast a (hash_value, rest)), ctxt)
    
    | (Script_typed_ir.Add_bls12_381_g1, stack_value) =>
      let 'existT _ __199 stack_value :=
        cast_exists (Es := Set)
          (fun __199 =>
            Bls12_381.G1.(S.CURVE.t) * (Bls12_381.G1.(S.CURVE.t) * __199))
          stack_value in
      let '(x, (y, rest)) := stack_value in
      logged_return ((cast a ((Bls12_381.G1.(S.CURVE.add) x y), rest)), ctxt)
    
    | (Script_typed_ir.Add_bls12_381_g2, stack_value) =>
      let 'existT _ __200 stack_value :=
        cast_exists (Es := Set)
          (fun __200 =>
            Bls12_381.G2.(S.CURVE.t) * (Bls12_381.G2.(S.CURVE.t) * __200))
          stack_value in
      let '(x, (y, rest)) := stack_value in
      logged_return ((cast a ((Bls12_381.G2.(S.CURVE.add) x y), rest)), ctxt)
    
    | (Script_typed_ir.Add_bls12_381_fr, stack_value) =>
      let 'existT _ __201 stack_value :=
        cast_exists (Es := Set)
          (fun __201 =>
            Bls12_381.Fr.(S.PRIME_FIELD.t) *
              (Bls12_381.Fr.(S.PRIME_FIELD.t) * __201)) stack_value in
      let '(x, (y, rest)) := stack_value in
      logged_return
        ((cast a ((Bls12_381.Fr.(S.PRIME_FIELD.add) x y), rest)), ctxt)
    
    | (Script_typed_ir.Mul_bls12_381_g1, stack_value) =>
      let 'existT _ __202 stack_value :=
        cast_exists (Es := Set)
          (fun __202 =>
            Bls12_381.G1.(S.CURVE.t) * (Bls12_381.Fr.(S.PRIME_FIELD.t) * __202))
          stack_value in
      let '(x, (y, rest)) := stack_value in
      logged_return ((cast a ((Bls12_381.G1.(S.CURVE.mul) x y), rest)), ctxt)
    
    | (Script_typed_ir.Mul_bls12_381_g2, stack_value) =>
      let 'existT _ __203 stack_value :=
        cast_exists (Es := Set)
          (fun __203 =>
            Bls12_381.G2.(S.CURVE.t) * (Bls12_381.Fr.(S.PRIME_FIELD.t) * __203))
          stack_value in
      let '(x, (y, rest)) := stack_value in
      logged_return ((cast a ((Bls12_381.G2.(S.CURVE.mul) x y), rest)), ctxt)
    
    | (Script_typed_ir.Mul_bls12_381_fr, stack_value) =>
      let 'existT _ __204 stack_value :=
        cast_exists (Es := Set)
          (fun __204 =>
            Bls12_381.Fr.(S.PRIME_FIELD.t) *
              (Bls12_381.Fr.(S.PRIME_FIELD.t) * __204)) stack_value in
      let '(x, (y, rest)) := stack_value in
      logged_return
        ((cast a ((Bls12_381.Fr.(S.PRIME_FIELD.mul) x y), rest)), ctxt)
    
    | (Script_typed_ir.Mul_bls12_381_fr_z, stack_value) =>
      let 'existT _ [__205, __206] stack_value :=
        cast_exists (Es := [Set ** Set])
          (fun '[__205, __206] =>
            Alpha_context.Script_int.num *
              (Bls12_381.Fr.(S.PRIME_FIELD.t) * __206)) stack_value in
      let '(x, (y, rest)) := stack_value in
      let x :=
        Bls12_381.Fr.(S.PRIME_FIELD.of_z) (Alpha_context.Script_int.to_zint x)
        in
      let res := ((Bls12_381.Fr.(S.PRIME_FIELD.mul) x y), rest) in
      logged_return ((cast a res), ctxt)
    
    | (Script_typed_ir.Mul_bls12_381_z_fr, stack_value) =>
      let 'existT _ [__207, __208] stack_value :=
        cast_exists (Es := [Set ** Set])
          (fun '[__207, __208] =>
            Bls12_381.Fr.(S.PRIME_FIELD.t) *
              (Alpha_context.Script_int.num * __208)) stack_value in
      let '(y, (x, rest)) := stack_value in
      let x :=
        Bls12_381.Fr.(S.PRIME_FIELD.of_z) (Alpha_context.Script_int.to_zint x)
        in
      let res := ((Bls12_381.Fr.(S.PRIME_FIELD.mul) x y), rest) in
      logged_return ((cast a res), ctxt)
    
    | (Script_typed_ir.Int_bls12_381_fr, stack_value) =>
      let 'existT _ __209 stack_value :=
        cast_exists (Es := Set)
          (fun __209 => Bls12_381.Fr.(S.PRIME_FIELD.t) * __209) stack_value in
      let '(x, rest) := stack_value in
      logged_return
        ((cast a
          ((Alpha_context.Script_int.of_zint
            (Bls12_381.Fr.(S.PRIME_FIELD.to_z) x)), rest)), ctxt)
    
    | (Script_typed_ir.Neg_bls12_381_g1, stack_value) =>
      let 'existT _ __210 stack_value :=
        cast_exists (Es := Set) (fun __210 => Bls12_381.G1.(S.CURVE.t) * __210)
          stack_value in
      let '(x, rest) := stack_value in
      logged_return ((cast a ((Bls12_381.G1.(S.CURVE.negate) x), rest)), ctxt)
    
    | (Script_typed_ir.Neg_bls12_381_g2, stack_value) =>
      let 'existT _ __211 stack_value :=
        cast_exists (Es := Set) (fun __211 => Bls12_381.G2.(S.CURVE.t) * __211)
          stack_value in
      let '(x, rest) := stack_value in
      logged_return ((cast a ((Bls12_381.G2.(S.CURVE.negate) x), rest)), ctxt)
    
    | (Script_typed_ir.Neg_bls12_381_fr, stack_value) =>
      let 'existT _ __212 stack_value :=
        cast_exists (Es := Set)
          (fun __212 => Bls12_381.Fr.(S.PRIME_FIELD.t) * __212) stack_value in
      let '(x, rest) := stack_value in
      logged_return
        ((cast a ((Bls12_381.Fr.(S.PRIME_FIELD.negate) x), rest)), ctxt)
    
    | (Script_typed_ir.Pairing_check_bls12_381, stack_value) =>
      let 'existT _ __213 stack_value :=
        cast_exists (Es := Set)
          (fun __213 =>
            Script_typed_ir.boxed_list
              (Script_typed_ir.pair Bls12_381.G1.(S.CURVE.t)
                Bls12_381.G2.(S.CURVE.t)) * __213) stack_value in
      let '(pairs, rest) := stack_value in
      let check :=
        match pairs.(Script_typed_ir.boxed_list.elements) with
        | [] => true
        | pairs =>
          Option.value
            (Option.map
              (Bls12_381.Gt.(S.FIELD.eq_value) Bls12_381.Gt.(S.FIELD.one))
              (Bls12_381.final_exponentiation_opt (Bls12_381.miller_loop pairs)))
            false
        end in
      logged_return ((cast a (check, rest)), ctxt)
    
    | (Script_typed_ir.Comb _ witness, stack_value) =>
      let '[stack_value, witness] :=
        cast [b ** Script_typed_ir.comb_gadt_witness] [stack_value, witness] in
      let fix aux {before after : Set}
        (witness : Script_typed_ir.comb_gadt_witness) (stack_value : before)
        {struct witness} : after :=
        match (witness, stack_value) with
        | (Script_typed_ir.Comb_one, stack_value) =>
          let stack_value := cast after stack_value in
          stack_value
        | (Script_typed_ir.Comb_succ witness', stack_value) =>
          let 'existT _ [__232, __233, __234, __235] [stack_value, witness'] :=
            cast_exists (Es := [Set ** Set ** Set ** Set])
              (fun '[__232, __233, __234, __235] =>
                [__232 * __233 ** Script_typed_ir.comb_gadt_witness])
              [stack_value, witness'] in
          let '(a_value, tl) := stack_value in
          let '(b_value, tl') := ((aux witness' tl) : __234 * __235) in
          cast after ((a_value, b_value), tl')
        end in
      logged_return ((aux witness stack_value), ctxt)
    
    | (Script_typed_ir.Uncomb _ witness, stack_value) =>
      let '[stack_value, witness] :=
        cast [b ** Script_typed_ir.uncomb_gadt_witness] [stack_value, witness]
        in
      let fix aux {before after : Set}
        (witness : Script_typed_ir.uncomb_gadt_witness) (stack_value : before)
        {struct witness} : after :=
        match (witness, stack_value) with
        | (Script_typed_ir.Uncomb_one, stack_value) =>
          let stack_value := cast after stack_value in
          stack_value
        | (Script_typed_ir.Uncomb_succ witness', stack_value) =>
          let 'existT _ [__242, __243, __244, __245] [stack_value, witness'] :=
            cast_exists (Es := [Set ** Set ** Set ** Set])
              (fun '[__242, __243, __244, __245] =>
                [(__242 * __243) * __244 ** Script_typed_ir.uncomb_gadt_witness])
              [stack_value, witness'] in
          let '((a_value, b_value), tl) := stack_value in
          cast after (a_value, ((aux witness' (b_value, tl)) : __245))
        end in
      logged_return ((aux witness stack_value), ctxt)
    
    | (Script_typed_ir.Comb_get _ witness, stack_value) =>
      let 'existT _ [__214, __215, __216] [stack_value, witness] :=
        cast_exists (Es := [Set ** Set ** Set])
          (fun '[__214, __215, __216] =>
            [__214 * __215 ** Script_typed_ir.comb_get_gadt_witness])
          [stack_value, witness] in
      let '(comb, stack_value) := stack_value in
      let fix aux {before after : Set}
        (witness : Script_typed_ir.comb_get_gadt_witness) (comb : before)
        {struct witness} : after :=
        match (witness, comb) with
        | (Script_typed_ir.Comb_get_zero, v) =>
          let v := cast after v in
          v
        | (Script_typed_ir.Comb_get_one, comb) =>
          let 'existT _ [__250, __251] comb :=
            cast_exists (Es := [Set ** Set])
              (fun '[__250, __251] => __250 * __251) comb in
          let '(a_value, _) := comb in
          cast after a_value
        | (Script_typed_ir.Comb_get_plus_two witness', comb) =>
          let 'existT _ [__252, __253] [comb, witness'] :=
            cast_exists (Es := [Set ** Set])
              (fun '[__252, __253] =>
                [__252 * __253 ** Script_typed_ir.comb_get_gadt_witness])
              [comb, witness'] in
          let '(_, b_value) := comb in
          aux witness' b_value
        end in
      logged_return ((cast a (((aux witness comb) : __216), stack_value)), ctxt)
    
    | (Script_typed_ir.Comb_set _ witness, stack_value) =>
      let 'existT _ [__217, __218, __219, __220] [stack_value, witness] :=
        cast_exists (Es := [Set ** Set ** Set ** Set])
          (fun '[__217, __218, __219, __220] =>
            [__217 * (__218 * __219) ** Script_typed_ir.comb_set_gadt_witness])
          [stack_value, witness] in
      let '(value, (comb, stack_value)) := stack_value in
      let fix aux {value before after : Set}
        (witness : Script_typed_ir.comb_set_gadt_witness) (value : value)
        (item : before) {struct witness} : after :=
        match (witness, item) with
        | (Script_typed_ir.Comb_set_zero, _) => cast after value
        | (Script_typed_ir.Comb_set_one, item) =>
          let 'existT _ [__258, __259] item :=
            cast_exists (Es := [Set ** Set])
              (fun '[__258, __259] => __258 * __259) item in
          cast after
          (let '(_hd, tl) := item in
          (value, tl))
        | (Script_typed_ir.Comb_set_plus_two witness', item) =>
          let 'existT _ [__260, __261, __262] [item, witness'] :=
            cast_exists (Es := [Set ** Set ** Set])
              (fun '[__260, __261, __262] =>
                [__260 * __261 ** Script_typed_ir.comb_set_gadt_witness])
              [item, witness'] in
          cast after
          (let '(hd, tl) := item in
          (hd, ((aux witness' value tl) : __262)))
        end in
      logged_return
        ((cast a (((aux witness value comb) : __220), stack_value)), ctxt)
    
    | (Script_typed_ir.Dup_n _ witness, stack_value) =>
      let 'existT _ __221 [stack_value, witness] :=
        cast_exists (Es := Set)
          (fun __221 => [b ** Script_typed_ir.dup_n_gadt_witness])
          [stack_value, witness] in
      let fix aux {before after : Set}
        (witness : Script_typed_ir.dup_n_gadt_witness) (stack_value : before)
        {struct witness} : after :=
        match (witness, stack_value) with
        | (Script_typed_ir.Dup_n_zero, stack_value) =>
          let 'existT _ [__268, __269] stack_value :=
            cast_exists (Es := [Set ** Set])
              (fun '[__268, __269] => __268 * __269) stack_value in
          cast after
          (let '(a_value, _) := stack_value in
          a_value)
        | (Script_typed_ir.Dup_n_succ witness', stack_value) =>
          let 'existT _ [__270, __271] [stack_value, witness'] :=
            cast_exists (Es := [Set ** Set])
              (fun '[__270, __271] =>
                [__270 * __271 ** Script_typed_ir.dup_n_gadt_witness])
              [stack_value, witness'] in
          cast after
          (let '(_, tl) := stack_value in
          ((aux witness' tl) : after))
        end in
      logged_return
        ((cast a (((aux witness stack_value) : __221), stack_value)), ctxt)
    
    | (Script_typed_ir.Ticket, stack_value) =>
      let 'existT _ [__222, __223] stack_value :=
        cast_exists (Es := [Set ** Set])
          (fun '[__222, __223] => __222 * (Alpha_context.Script_int.num * __223))
          stack_value in
      let '(contents, (amount, rest)) := stack_value in
      let ticketer := (step_constants.(step_constants.self), "default") in
      logged_return
        ((cast a
          ({| Script_typed_ir.ticket.ticketer := ticketer;
            Script_typed_ir.ticket.contents := contents;
            Script_typed_ir.ticket.amount := amount |}, rest)), ctxt)
    
    | (Script_typed_ir.Read_ticket, stack_value) =>
      let 'existT _ [__224, __225] stack_value :=
        cast_exists (Es := [Set ** Set])
          (fun '[__224, __225] => Script_typed_ir.ticket __224 * __225)
          stack_value in
      let
        '({|
          Script_typed_ir.ticket.ticketer := ticketer;
            Script_typed_ir.ticket.contents := contents;
            Script_typed_ir.ticket.amount := amount
            |}, _) := stack_value in
      logged_return
        ((cast a ((ticketer, (contents, amount)), stack_value)), ctxt)
    
    | (Script_typed_ir.Split_ticket, stack_value) =>
      let 'existT _ [__226, __227] stack_value :=
        cast_exists (Es := [Set ** Set])
          (fun '[__226, __227] =>
            Script_typed_ir.ticket __226 *
              ((Alpha_context.Script_int.num * Alpha_context.Script_int.num) *
                __227)) stack_value in
      let '(ticket, ((amount_a, amount_b), rest)) := stack_value in
      let result_value :=
        if
          (Alpha_context.Script_int.compare
            (Alpha_context.Script_int.add_n amount_a amount_b)
            ticket.(Script_typed_ir.ticket.amount)) =i 0
        then
          Some
            ((Script_typed_ir.ticket.with_amount amount_a ticket),
              (Script_typed_ir.ticket.with_amount amount_b ticket))
        else
          None in
      logged_return ((cast a (result_value, rest)), ctxt)
    
    | (Script_typed_ir.Join_tickets contents_ty, stack_value) =>
      let 'existT _ [__228, __229] [stack_value, contents_ty] :=
        cast_exists (Es := [Set ** Set])
          (fun '[__228, __229] =>
            [(Script_typed_ir.ticket __228 * Script_typed_ir.ticket __228) *
              __229 ** Script_typed_ir.comparable_ty])
          [stack_value, contents_ty] in
      let '((ticket_a, ticket_b), rest) := stack_value in
      let result_value :=
        if
          ((Script_ir_translator.compare_address
            ticket_a.(Script_typed_ir.ticket.ticketer)
            ticket_b.(Script_typed_ir.ticket.ticketer)) =i 0) &&
          ((Script_ir_translator.compare_comparable contents_ty
            ticket_a.(Script_typed_ir.ticket.contents)
            ticket_b.(Script_typed_ir.ticket.contents)) =i 0)
        then
          Some
            {|
              Script_typed_ir.ticket.ticketer :=
                ticket_a.(Script_typed_ir.ticket.ticketer);
              Script_typed_ir.ticket.contents :=
                ticket_a.(Script_typed_ir.ticket.contents);
              Script_typed_ir.ticket.amount :=
                Alpha_context.Script_int.add_n
                  ticket_a.(Script_typed_ir.ticket.amount)
                  ticket_b.(Script_typed_ir.ticket.amount) |}
        else
          None in
      logged_return ((cast a (result_value, rest)), ctxt)
    | _ => unreachable_gadt_branch
    end.

Definition step {b a : Set}
  : logger -> Alpha_context.context -> step_constants ->
  Script_typed_ir.descr -> b -> M=? (a * Alpha_context.context) :=
  fun x_1 => step_bounded x_1 0.

Definition interp {p r : Set}
  (logger : logger) (ctxt : Alpha_context.context)
  (step_constants : step_constants)
  (function_parameter : Script_typed_ir.lambda)
  : p -> M=? (r * Alpha_context.context) :=
  let 'Script_typed_ir.Lam code _ := function_parameter in
  fun (arg : p) =>
    let stack_value := (arg, tt) in
    let 'existS _ _ Log := logger in
    let '_ := Log.(STEP_LOGGER.log_interp) ctxt code stack_value in
    let=? '((ret, _), ctxt) :=
      ((step logger ctxt step_constants code stack_value) :
        M=? ((r * Script_typed_ir.end_of_stack) * Alpha_context.context)) in
    return=? (ret, ctxt).

Definition raw_execute
  (logger : logger) (ctxt : Alpha_context.context)
  (mode : Script_ir_translator.unparsing_mode) (step_constants : step_constants)
  (entrypoint : string) (internal : bool)
  (unparsed_script : Alpha_context.Script.t) (arg : Alpha_context.Script.node)
  : M=?
    (Alpha_context.Script.expr * list Alpha_context.packed_internal_operation *
      Alpha_context.context * option Alpha_context.Lazy_storage.diffs) :=
  let=?
    '(Script_ir_translator.Ex_script {|
      Script_typed_ir.script.code := code;
        Script_typed_ir.script.arg_type := arg_type;
        Script_typed_ir.script.storage := storage_value;
        Script_typed_ir.script.storage_type := storage_type;
        Script_typed_ir.script.entrypoints := entrypoints
        |}, ctxt) :=
    Script_ir_translator.parse_script None ctxt true true unparsed_script in
  let 'existT _ [__Ex_script_'a, __Ex_script_'b]
    [ctxt, entrypoints, storage_type, storage_value, arg_type, code] :=
    cast_exists (Es := [Set ** Set])
      (fun '[__Ex_script_'a, __Ex_script_'b] =>
        [Alpha_context.context ** Script_typed_ir.entrypoints **
          Script_typed_ir.ty ** __Ex_script_'b ** Script_typed_ir.ty **
          Script_typed_ir.lambda])
      [ctxt, entrypoints, storage_type, storage_value, arg_type, code] in
  let=? '(box, _) :=
    return=
      (Error_monad.record_trace
        (Build_extensible "Bad_contract_parameter" Alpha_context.Contract.t
          step_constants.(step_constants.self))
        (Script_ir_translator.find_entrypoint arg_type entrypoints entrypoint))
    in
  let=? '(arg, ctxt) :=
    Error_monad.trace_value
      (Build_extensible "Bad_contract_parameter" Alpha_context.Contract.t
        step_constants.(step_constants.self))
      ((Script_ir_translator.parse_data None ctxt false internal arg_type
        (box arg)) : M=? (__Ex_script_'a * Alpha_context.context)) in
  let=? '(script_code, ctxt) :=
    return=
      (Alpha_context.Script.force_decode_in_context ctxt
        unparsed_script.(Alpha_context.Script.t.code)) in
  let=? '(to_duplicate, ctxt) :=
    return= (Script_ir_translator.collect_lazy_storage ctxt arg_type arg) in
  let=? '(to_update, ctxt) :=
    return=
      (Script_ir_translator.collect_lazy_storage ctxt storage_type storage_value)
    in
  let=? '((ops, storage_value), ctxt) :=
    Error_monad.trace_value
      (Build_extensible "Runtime_contract_error"
        (Alpha_context.Contract.t * Alpha_context.Script.expr)
        (step_constants.(step_constants.self), script_code))
      ((interp logger ctxt step_constants code (arg, storage_value)) :
        M=?
          (Script_typed_ir.pair
            (Script_typed_ir.boxed_list Script_typed_ir.operation)
            __Ex_script_'b * Alpha_context.context)) in
  let=? '(storage_value, lazy_storage_diff, ctxt) :=
    Script_ir_translator.extract_lazy_storage_diff ctxt mode false to_duplicate
      to_update storage_type storage_value in
  let=? '(storage_value, ctxt) :=
    Error_monad.trace_value
      (Build_extensible "Cannot_serialize_storage" unit tt)
      (let=? '(storage_value, ctxt) :=
        Script_ir_translator.unparse_data ctxt mode storage_type storage_value
        in
      Lwt._return
        (let? ctxt :=
          Alpha_context.Gas.consume ctxt
            (Alpha_context.Script.strip_locations_cost storage_value) in
        return? ((Micheline.strip_locations storage_value), ctxt))) in
  let '(ops, op_diffs) := List.split ops.(Script_typed_ir.boxed_list.elements)
    in
  let lazy_storage_diff :=
    match
      List.flatten
        (List.map (fun x_1 => Option.value x_1 nil)
          (Pervasives.op_at op_diffs [ lazy_storage_diff ])) with
    | [] => None
    | diff_value => Some diff_value
    end in
  return=? (storage_value, ops, ctxt, lazy_storage_diff).

Module execution_result.
  Record record : Set := Build {
    ctxt : Alpha_context.context;
    storage : Alpha_context.Script.expr;
    lazy_storage_diff : option Alpha_context.Lazy_storage.diffs;
    operations : list Alpha_context.packed_internal_operation }.
  Definition with_ctxt ctxt (r : record) :=
    Build ctxt r.(storage) r.(lazy_storage_diff) r.(operations).
  Definition with_storage storage (r : record) :=
    Build r.(ctxt) storage r.(lazy_storage_diff) r.(operations).
  Definition with_lazy_storage_diff lazy_storage_diff (r : record) :=
    Build r.(ctxt) r.(storage) lazy_storage_diff r.(operations).
  Definition with_operations operations (r : record) :=
    Build r.(ctxt) r.(storage) r.(lazy_storage_diff) operations.
End execution_result.
Definition execution_result := execution_result.record.

Definition execute (op_staroptstar : option logger)
  : Alpha_context.context -> Script_ir_translator.unparsing_mode ->
  step_constants -> Alpha_context.Script.t -> string ->
  Micheline.canonical Alpha_context.Script.prim -> bool -> M=? execution_result :=
  let logger :=
    match op_staroptstar with
    | Some op_starsthstar => op_starsthstar
    | None => existS (A := unit) (fun _ => _) tt No_trace
    end in
  fun (ctxt : Alpha_context.context) =>
    fun (mode : Script_ir_translator.unparsing_mode) =>
      fun (step_constants : step_constants) =>
        fun (script : Alpha_context.Script.t) =>
          fun (entrypoint : string) =>
            fun (parameter : Micheline.canonical Alpha_context.Script.prim) =>
              fun (internal : bool) =>
                let=? '(storage_value, operations, ctxt, lazy_storage_diff) :=
                  raw_execute logger ctxt mode step_constants entrypoint
                    internal script (Micheline.root parameter) in
                return=?
                  {| execution_result.ctxt := ctxt;
                    execution_result.storage := storage_value;
                    execution_result.lazy_storage_diff := lazy_storage_diff;
                    execution_result.operations := operations |}.
