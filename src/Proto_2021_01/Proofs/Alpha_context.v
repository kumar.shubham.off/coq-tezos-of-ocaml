Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Require TezosOfOCaml.Proto_2021_01.Alpha_context.
Require TezosOfOCaml.Proto_2021_01.Gas_limit_repr.
Require TezosOfOCaml.Proto_2021_01.Raw_context.

(*Module Gas.
  Definition ctxt_with_unlimited_gas ctxt :=
    Raw_context.t.with_operation_gas Gas_limit_repr.Unaccounted ctxt.

  Lemma consume_unlimited ctxt cost :
    let ctxt := ctxt_with_unlimited_gas ctxt in
    Alpha_context.Gas.consume ctxt cost = return? ctxt.
    unfold Alpha_context.Gas.consume, Raw_context.consume_gas.
    destruct ctxt; simpl.
    reflexivity.
  Qed.
End Gas.*)
