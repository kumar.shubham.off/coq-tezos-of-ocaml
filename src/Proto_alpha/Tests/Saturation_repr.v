Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Saturation_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Saturation_repr.

Lemma test_add_commutes (t1 t2 : int) :
  Saturation_repr.Valid.t t1 ->
  Saturation_repr.Valid.t t2 ->
  Saturation_repr.add t1 t2 = Saturation_repr.add t2 t1.
Proof.
  intros.
  do 2 (rewrite Saturation_repr.add_eq; trivial).
  replace ((t1 + t2)%Z) with ((t2 + t1)%Z).
  { reflexivity. }
  { lia. }
Qed.

Lemma test_mul_commutes (t1 t2 : int) :
  Saturation_repr.Small_enough.t t1 ->
  Saturation_repr.Small_enough.t t2 ->
  Saturation_repr.mul t1 t2 = Saturation_repr.mul t2 t1.
Proof.
  intros.
  do 2 (rewrite Saturation_repr.mul_eq; trivial).
  replace ((t1 * t2)%Z) with ((t2 * t1)%Z).
  { reflexivity. }
  { lia. }
Qed.
