Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Require TezosOfOCaml.Proto_2021_01.Lazy_storage_kind.
Require TezosOfOCaml.Proto_2021_01.Michelson_v1_primitives.
Require TezosOfOCaml.Proto_2021_01.Raw_context.
Require TezosOfOCaml.Proto_2021_01.Sapling_repr.
Require TezosOfOCaml.Proto_2021_01.Sapling_storage.
Require TezosOfOCaml.Proto_2021_01.Script_expr_hash.
Require TezosOfOCaml.Proto_2021_01.Script_repr.
Require TezosOfOCaml.Proto_2021_01.Storage.
Require TezosOfOCaml.Proto_2021_01.Storage_sigs.

(** Operations to be defined on a lazy storage type. *)
Module OPS.
  Record signature {Id_t alloc updates : Set} : Set := {
    Id : Lazy_storage_kind.IdWithTemp (t := Id_t);
    alloc := alloc;
    updates := updates;
    title : string;
    alloc_encoding : Data_encoding.t alloc;
    updates_encoding : Data_encoding.t updates;
    bytes_size_for_empty : Z.t;
    alloc_value :
      Raw_context.t -> Id.(Lazy_storage_kind.IdWithTemp.t) -> alloc ->
      M=? Raw_context.t;
    apply_updates :
      Raw_context.t -> Id.(Lazy_storage_kind.IdWithTemp.t) -> updates ->
      M=? (Raw_context.t * Z.t);
    Next : Storage.NEXT (id := Id.(Lazy_storage_kind.IdWithTemp.t));
    Total_bytes :
      Storage_sigs.Indexed_data_storage (t := Raw_context.t)
        (key := Id.(Lazy_storage_kind.IdWithTemp.t)) (value := Z.t);
    (** Deep copy. *)
    copy :
      Raw_context.t -> Id.(Lazy_storage_kind.IdWithTemp.t) ->
      Id.(Lazy_storage_kind.IdWithTemp.t) -> M=? Raw_context.t;
    (** Deep deletion. *)
    remove_rec :
      Raw_context.t -> Id.(Lazy_storage_kind.IdWithTemp.t) -> M= Raw_context.t;
  }.
End OPS.
Definition OPS := @OPS.signature.
Arguments OPS {_ _ _}.

Module Big_map.
  Include Lazy_storage_kind.Big_map.
  
  Definition bytes_size_for_big_map_key : int := 65.
  
  Definition bytes_size_for_empty : Z.t :=
    let bytes_size_for_big_map := 33 in
    Z.of_int bytes_size_for_big_map.
  
  Definition alloc_value
    (ctxt : Raw_context.t)
    (id : Lazy_storage_kind.Big_map.Id.(Lazy_storage_kind.IdWithTemp.t))
    (function_parameter : alloc) : M=? Raw_context.t :=
    let '{|
      Lazy_storage_kind.Big_map.alloc.key_type := key_type;
        Lazy_storage_kind.Big_map.alloc.value_type := value_type
        |} := function_parameter in
    let key_type :=
      Micheline.strip_locations
        (Script_repr.strip_annotations (Micheline.root key_type)) in
    let value_type :=
      Micheline.strip_locations
        (Script_repr.strip_annotations (Micheline.root value_type)) in
    let=? ctxt :=
      Storage.Big_map.Key_type.(Storage_sigs.Indexed_data_storage.init_value)
        ctxt id key_type in
    Storage.Big_map.Value_type.(Storage_sigs.Indexed_data_storage.init_value)
      ctxt id value_type.
  
  Definition apply_update
    (ctxt : Raw_context.t) (id : Storage.Big_map.id)
    (function_parameter : update) : M=? (Raw_context.t * Z.t) :=
    let '{|
      Lazy_storage_kind.Big_map.update.key :=
        _key_is_shown_only_on_the_receipt_in_print_big_map_diff;
        Lazy_storage_kind.Big_map.update.key_hash := key_hash;
        Lazy_storage_kind.Big_map.update.value := value
        |} := function_parameter in
    match value with
    | None =>
      let=? '(ctxt, freed, existed) :=
        Storage.Big_map.Contents.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.remove)
          (ctxt, id) key_hash in
      let freed :=
        if existed then
          freed +i bytes_size_for_big_map_key
        else
          freed in
      return=? (ctxt, (Z.of_int (Pervasives.op_tildeminus freed)))
    | Some v =>
      let=? '(ctxt, size_diff, existed) :=
        Storage.Big_map.Contents.(Storage_sigs.Non_iterable_indexed_carbonated_data_storage.init_set)
          (ctxt, id) key_hash v in
      let size_diff :=
        if existed then
          size_diff
        else
          size_diff +i bytes_size_for_big_map_key in
      return=? (ctxt, (Z.of_int size_diff))
    end.
  
  Definition apply_updates
    (ctxt : Raw_context.t) (id : Storage.Big_map.id) (updates : list update)
    : M=? (Raw_context.t * Z.t) :=
    Error_monad.fold_left_s
      (fun (function_parameter : Raw_context.t * Z.t) =>
        let '(ctxt, size) := function_parameter in
        fun (update : update) =>
          let=? '(ctxt, added_size) := apply_update ctxt id update in
          return=? (ctxt, (size +Z added_size))) (ctxt, Z.zero) updates.
  
  Include Storage.Big_map.
End Big_map.

Definition ops (id alloc updates : Set) : Set :=
  {_ : unit @ OPS (Id_t := id) (alloc := alloc) (updates := updates)}.

Module Sapling_state.
  Include Lazy_storage_kind.Sapling_state.
  
  Definition bytes_size_for_empty : Z.t := Z.of_int 33.
  
  Definition alloc_value
    (ctxt : Raw_context.t) (id : Storage.Sapling.id)
    (function_parameter : alloc) : M=? Raw_context.t :=
    let '{| Lazy_storage_kind.Sapling_state.alloc.memo_size := memo_size |} :=
      function_parameter in
    Sapling_storage.init_value ctxt id memo_size.
  
  Definition apply_updates
    (ctxt : Raw_context.context) (id : Storage.Sapling.id)
    (updates : Sapling_repr.diff) : M=? (Raw_context.t * Z.t) :=
    Sapling_storage.apply_diff ctxt id updates.
  
  Include Storage.Sapling.
End Sapling_state.

Definition get_ops {i a u : Set} (function_parameter : Lazy_storage_kind.t)
  : ops i a u :=
  match function_parameter with
  | Lazy_storage_kind.Big_map =>
    cast (ops i a u)
    (existS (A := unit) (fun _ => _) tt
      {|
        OPS.Id := Big_map.Id;
        OPS.title := Big_map.title;
        OPS.alloc_encoding := Big_map.alloc_encoding;
        OPS.updates_encoding := Big_map.updates_encoding;
        OPS.bytes_size_for_empty := Big_map.bytes_size_for_empty;
        OPS.alloc_value := Big_map.alloc_value;
        OPS.apply_updates := Big_map.apply_updates;
        OPS.Next := Big_map.Next;
        OPS.Total_bytes := Big_map.Total_bytes;
        OPS.copy := Big_map.copy;
        OPS.remove_rec := Big_map.remove_rec
      |})
  | Lazy_storage_kind.Sapling_state =>
    cast (ops i a u)
    (existS (A := unit) (fun _ => _) tt
      {|
        OPS.Id := Sapling_state.Id;
        OPS.title := Sapling_state.title;
        OPS.alloc_encoding := Sapling_state.alloc_encoding;
        OPS.updates_encoding := Sapling_state.updates_encoding;
        OPS.bytes_size_for_empty := Sapling_state.bytes_size_for_empty;
        OPS.alloc_value := Sapling_state.alloc_value;
        OPS.apply_updates := Sapling_state.apply_updates;
        OPS.Next := Sapling_state.Next;
        OPS.Total_bytes := Sapling_state.Total_bytes;
        OPS.copy := Sapling_state.copy;
        OPS.remove_rec := Sapling_state.remove_rec
      |})
  end.

(** Records for the constructor parameters *)
Module ConstructorRecords_init.
  Module init.
    Module Copy.
      Record record {src : Set} : Set := Build {
        src : src }.
      Arguments record : clear implicits.
      Definition with_src {t_src} src (r : record t_src) :=
        Build t_src src.
    End Copy.
    Definition Copy_skeleton := Copy.record.
  End init.
End ConstructorRecords_init.
Import ConstructorRecords_init.

Reserved Notation "'init.Copy".

Inductive init (id alloc : Set) : Set :=
| Existing : init id alloc
| Copy : 'init.Copy id -> init id alloc
| Alloc : alloc -> init id alloc

where "'init.Copy" := (fun (t_id : Set) => init.Copy_skeleton t_id).

Module init.
  Include ConstructorRecords_init.init.
  Definition Copy := 'init.Copy.
End init.

Arguments Existing {_ _}.
Arguments Copy {_ _}.
Arguments Alloc {_ _}.

(** Records for the constructor parameters *)
Module ConstructorRecords_diff.
  Module diff.
    Module Update.
      Record record {init updates : Set} : Set := Build {
        init : init;
        updates : updates }.
      Arguments record : clear implicits.
      Definition with_init {t_init t_updates} init
        (r : record t_init t_updates) :=
        Build t_init t_updates init r.(updates).
      Definition with_updates {t_init t_updates} updates
        (r : record t_init t_updates) :=
        Build t_init t_updates r.(init) updates.
    End Update.
    Definition Update_skeleton := Update.record.
  End diff.
End ConstructorRecords_diff.
Import ConstructorRecords_diff.

Reserved Notation "'diff.Update".

Inductive diff (id alloc updates : Set) : Set :=
| Remove : diff id alloc updates
| Update : 'diff.Update alloc id updates -> diff id alloc updates

where "'diff.Update" := (fun (t_alloc t_id t_updates : Set) =>
  diff.Update_skeleton (init t_id t_alloc) t_updates).

Module diff.
  Include ConstructorRecords_diff.diff.
  Definition Update := 'diff.Update.
End diff.

Arguments Remove {_ _ _}.
Arguments Update {_ _ _}.

Definition diff_encoding {i a u : Set} (OPS : ops i a u)
  : Data_encoding.t (diff i a u) :=
  let 'existS _ _ OPS := OPS in
  Data_encoding.union None
    [
      Data_encoding.case_value "update" None (Data_encoding.Tag 0)
        (Data_encoding.obj2
          (Data_encoding.req None None "action"
            (Data_encoding.constant "update"))
          (Data_encoding.req None None "updates" OPS.(OPS.updates_encoding)))
        (fun (function_parameter : diff i a u) =>
          match function_parameter with
          |
            Update {|
              diff.Update.init := Existing;
                diff.Update.updates := updates
                |} => Some (tt, updates)
          | _ => None
          end)
        (fun (function_parameter : unit * u) =>
          let '(_, updates) := function_parameter in
          Update
            {| diff.Update.init := Existing;
              diff.Update.updates := updates |});
      Data_encoding.case_value "remove" None (Data_encoding.Tag 1)
        (Data_encoding.obj1
          (Data_encoding.req None None "action"
            (Data_encoding.constant "remove")))
        (fun (function_parameter : diff i a u) =>
          match function_parameter with
          | Remove => Some tt
          | _ => None
          end)
        (fun (function_parameter : unit) =>
          let '_ := function_parameter in
          Remove);
      Data_encoding.case_value "copy" None (Data_encoding.Tag 2)
        (Data_encoding.obj3
          (Data_encoding.req None None "action"
            (Data_encoding.constant "copy"))
          (Data_encoding.req None None "source"
            OPS.(OPS.Id).(Lazy_storage_kind.IdWithTemp.encoding))
          (Data_encoding.req None None "updates" OPS.(OPS.updates_encoding)))
        (fun (function_parameter : diff i a u) =>
          match function_parameter with
          |
            Update {|
              diff.Update.init := Copy {| init.Copy.src := src |};
                diff.Update.updates := updates
                |} => Some (tt, src, updates)
          | _ => None
          end)
        (fun (function_parameter : unit * i * u) =>
          let '(_, src, updates) := function_parameter in
          Update
            {| diff.Update.init := Copy {| init.Copy.src := src |};
              diff.Update.updates := updates |});
      Data_encoding.case_value "alloc" None (Data_encoding.Tag 3)
        (Data_encoding.merge_objs
          (Data_encoding.obj2
            (Data_encoding.req None None "action"
              (Data_encoding.constant "alloc"))
            (Data_encoding.req None None "updates"
              OPS.(OPS.updates_encoding))) OPS.(OPS.alloc_encoding))
        (fun (function_parameter : diff i a u) =>
          match function_parameter with
          |
            Update {|
              diff.Update.init := Alloc alloc_value;
                diff.Update.updates := updates
                |} => Some ((tt, updates), alloc_value)
          | _ => None
          end)
        (fun (function_parameter : (unit * u) * a) =>
          let '((_, updates), alloc_value) := function_parameter in
          Update
            {| diff.Update.init := Alloc alloc_value;
              diff.Update.updates := updates |})
    ].

(** [apply_updates ctxt ops ~id init] applies the updates [updates] on lazy
  storage [id] on storage context [ctxt] using operations [ops] and returns the
  updated storage context and the added size in bytes (may be negative). *)
Definition apply_updates {i a u : Set} (ctxt : Raw_context.t) (OPS : ops i a u)
  : i -> u -> M=? (Raw_context.t * Z.t) :=
  let 'existS _ _ OPS := OPS in
  fun (id : i) =>
    fun (updates : u) =>
      let=? '(ctxt, updates_size) := OPS.(OPS.apply_updates) ctxt id updates in
      if Z.equal updates_size Z.zero then
        Error_monad._return (ctxt, updates_size)
      else
        let=? size :=
          OPS.(OPS.Total_bytes).(Storage_sigs.Indexed_data_storage.get) ctxt id
          in
        let=? ctxt :=
          OPS.(OPS.Total_bytes).(Storage_sigs.Indexed_data_storage.set) ctxt id
            (size +Z updates_size) in
        return=? (ctxt, updates_size).

(** [apply_init ctxt ops ~id init] applies the initialization [init] on lazy
  storage [id] on storage context [ctxt] using operations [ops] and returns the
  updated storage context and the added size in bytes (may be negative).

  If [id] represents a temporary lazy storage, the added size may be wrong. *)
Definition apply_init {i a u : Set} (ctxt : Raw_context.t) (OPS : ops i a u)
  : i -> init i a -> M=? (Raw_context.t * Z.t) :=
  let 'existS _ _ OPS := OPS in
  fun (id : i) =>
    fun (init_value : init i a) =>
      match init_value with
      | Existing => Error_monad._return (ctxt, Z.zero)
      | Copy {| init.Copy.src := src |} =>
        let=? ctxt := OPS.(OPS.copy) ctxt src id in
        if OPS.(OPS.Id).(Lazy_storage_kind.IdWithTemp.is_temp) id then
          Error_monad._return (ctxt, Z.zero)
        else
          let=? copy_size :=
            OPS.(OPS.Total_bytes).(Storage_sigs.Indexed_data_storage.get) ctxt
              src in
          Error_monad._return
            (ctxt, (copy_size +Z OPS.(OPS.bytes_size_for_empty)))
      | Alloc alloc_value =>
        let=? ctxt :=
          OPS.(OPS.Total_bytes).(Storage_sigs.Indexed_data_storage.init_value)
            ctxt id Z.zero in
        let=? ctxt := OPS.(OPS.alloc_value) ctxt id alloc_value in
        Error_monad._return (ctxt, OPS.(OPS.bytes_size_for_empty))
      end.

(** [apply_diff ctxt ops ~id diff] applies the diff [diff] on lazy storage [id]
  on storage context [ctxt] using operations [ops] and returns the updated
  storage context and the added size in bytes (may be negative).

  If [id] represents a temporary lazy storage, the added size may be wrong. *)
Definition apply_diff {i a u : Set}
  (ctxt : Raw_context.t) (function_parameter : ops i a u)
  : i -> diff i a u -> M=? (Raw_context.t * Z.t) :=
  let 'OPS as ops := function_parameter in
  let 'existS _ _ OPS := OPS in
  fun (id : i) =>
    fun (diff_value : diff i a u) =>
      match diff_value with
      | Remove =>
        if OPS.(OPS.Id).(Lazy_storage_kind.IdWithTemp.is_temp) id then
          let= ctxt := OPS.(OPS.remove_rec) ctxt id in
          return=? (ctxt, Z.zero)
        else
          let=? size :=
            OPS.(OPS.Total_bytes).(Storage_sigs.Indexed_data_storage.get) ctxt
              id in
          let= ctxt := OPS.(OPS.remove_rec) ctxt id in
          Error_monad._return
            (ctxt, (Z.neg (size +Z OPS.(OPS.bytes_size_for_empty))))
      |
        Update {|
          diff.Update.init := init_value; diff.Update.updates := updates |}
        =>
        let=? '(ctxt, init_size) := apply_init ctxt ops id init_value in
        let=? '(ctxt, updates_size) := apply_updates ctxt ops id updates in
        Error_monad._return (ctxt, (init_size +Z updates_size))
      end.

Inductive diffs_item : Set :=
| Item : forall {i a u : Set},
  Lazy_storage_kind.t -> i -> diff i a u -> diffs_item.

Definition make {i a u : Set}
  (k : Lazy_storage_kind.t) (id : i) (diff_value : diff i a u) : diffs_item :=
  Item k id diff_value.

Definition item_encoding : Data_encoding.encoding diffs_item :=
  (let arg := Data_encoding.union in
  fun (eta : list (Data_encoding.case diffs_item)) => arg None eta)
    (List.map
      (fun (function_parameter : int * Lazy_storage_kind.ex) =>
        let '(tag, Lazy_storage_kind.Ex_Kind k) := function_parameter in
        let 'existT _ [__Ex_Kind, __Ex_Kind1, __Ex_Kind2] [k, tag] :=
          cast_exists (Es := [Set ** Set ** Set])
            (fun '[__Ex_Kind, __Ex_Kind1, __Ex_Kind2] =>
              [Lazy_storage_kind.t ** int]) [k, tag] in
        let ops := get_ops k in
        let OPS := ops in
        let 'existS _ _ OPS := OPS in
        let title := OPS.(OPS.title) in
        Data_encoding.case_value title None (Data_encoding.Tag tag)
          (Data_encoding.obj3
            (Data_encoding.req None None "kind" (Data_encoding.constant title))
            (Data_encoding.req None None "id"
              OPS.(OPS.Id).(Lazy_storage_kind.IdWithTemp.encoding))
            (Data_encoding.req None None "diff" (diff_encoding ops)))
          (fun (function_parameter : diffs_item) =>
            let 'Item kind_value id diff_value := function_parameter in
            let 'existT _ [__Item_'a, __Item_'i, __Item_'u]
              [diff_value, id, kind_value] :=
              existT (A := [Set ** Set ** Set])
                (fun '[__Item_'a, __Item_'i, __Item_'u] =>
                  [diff __Item_'i __Item_'a __Item_'u ** __Item_'i **
                    Lazy_storage_kind.t]) [_, _, _] [diff_value, id, kind_value]
              in
            match Lazy_storage_kind.equal k kind_value with
            | Lazy_storage_kind.Eq =>
              cast
                (option
                  (unit * __Ex_Kind * diff __Ex_Kind __Ex_Kind1 __Ex_Kind2))
                (Some (tt, id, diff_value))
            | Lazy_storage_kind.Neq => None
            end)
          (fun (function_parameter :
            unit * __Ex_Kind * diff __Ex_Kind __Ex_Kind1 __Ex_Kind2) =>
            let '(_, id, diff_value) := function_parameter in
            Item k id diff_value)) Lazy_storage_kind.all).

Definition diffs : Set := list diffs_item.

Definition encoding : Data_encoding.encoding (list diffs_item) :=
  (let arg := Data_encoding.def "lazy_storage_diff" in
  fun (eta : Data_encoding.encoding (list diffs_item)) => arg None None eta)
    (Data_encoding.list_value None item_encoding).

Definition apply (ctxt : Raw_context.t) (diffs : list diffs_item)
  : M=? (Raw_context.t * Z.t) :=
  Error_monad.fold_left_s
    (fun (function_parameter : Raw_context.t * Z.t) =>
      let '(ctxt, total_size) := function_parameter in
      fun (function_parameter : diffs_item) =>
        let 'Item k id diff_value := function_parameter in
        let 'existT _ [__Item_'a, __Item_'i, __Item_'u] [diff_value, id, k] :=
          existT (A := [Set ** Set ** Set])
            (fun '[__Item_'a, __Item_'i, __Item_'u] =>
              [diff __Item_'i __Item_'a __Item_'u ** __Item_'i **
                Lazy_storage_kind.t]) [_, _, _] [diff_value, id, k] in
        let ops := get_ops k in
        let=? '(ctxt, added_size) := apply_diff ctxt ops id diff_value in
        let OPS := ops in
        return=?
          (let 'existS _ _ OPS := OPS in
          (ctxt,
            (if OPS.(OPS.Id).(Lazy_storage_kind.IdWithTemp.is_temp) id then
              total_size
            else
              total_size +Z added_size)))) (ctxt, Z.zero) diffs.

Definition fresh {i : Set}
  (kind_value : Lazy_storage_kind.t) (temporary : bool) (ctxt : Raw_context.t)
  : M=? (Raw_context.t * i) :=
  if temporary then
    Error_monad._return
      (Raw_context.fold_map_temporary_lazy_storage_ids ctxt
        (fun (temp_ids : Lazy_storage_kind.Temp_ids.t) =>
          Lazy_storage_kind.Temp_ids.fresh kind_value temp_ids))
  else
    let OPS := (get_ops (a := Set_oracle "a") (u := Set_oracle "u")) kind_value
      in
    let 'existS _ _ OPS := OPS in
    OPS.(OPS.Next).(Storage.NEXT.incr) ctxt.

Definition init_value (ctxt : Raw_context.t) : M=? Raw_context.t :=
  Error_monad.fold_left_s
    (fun (ctxt : Raw_context.t) =>
      fun (function_parameter : int * Lazy_storage_kind.ex) =>
        let '(_tag, Lazy_storage_kind.Ex_Kind k) := function_parameter in
        let 'existT _ [__Ex_Kind, __Ex_Kind1, __Ex_Kind2] [k, _tag] :=
          cast_exists (Es := [Set ** Set ** Set])
            (fun '[__Ex_Kind, __Ex_Kind1, __Ex_Kind2] =>
              [Lazy_storage_kind.t ** int]) [k, _tag] in
        let OPS := ((get_ops k) : ops __Ex_Kind __Ex_Kind1 __Ex_Kind2) in
        let 'existS _ _ OPS := OPS in
        OPS.(OPS.Next).(Storage.NEXT.init_value) ctxt) ctxt
    Lazy_storage_kind.all.

Definition cleanup_temporaries (ctxt : Raw_context.context)
  : M= Raw_context.context :=
  Raw_context.map_temporary_lazy_storage_ids_s ctxt
    (fun (temp_ids : Lazy_storage_kind.Temp_ids.t) =>
      let= ctxt :=
        Lwt_list.fold_left_s
          (fun (ctxt : Raw_context.t) =>
            fun (function_parameter : int * Lazy_storage_kind.ex) =>
              let '(_tag, Lazy_storage_kind.Ex_Kind k) := function_parameter in
              let 'existT _ [__Ex_Kind, __Ex_Kind1, __Ex_Kind2] [k, _tag] :=
                cast_exists (Es := [Set ** Set ** Set])
                  (fun '[__Ex_Kind, __Ex_Kind1, __Ex_Kind2] =>
                    [Lazy_storage_kind.t ** int]) [k, _tag] in
              let OPS := ((get_ops k) : ops __Ex_Kind __Ex_Kind1 __Ex_Kind2) in
              let 'existS _ _ OPS := OPS in
              Lazy_storage_kind.Temp_ids.fold_s k OPS.(OPS.remove_rec) temp_ids
                ctxt) ctxt Lazy_storage_kind.all in
      return= (ctxt, Lazy_storage_kind.Temp_ids.init_value)).
