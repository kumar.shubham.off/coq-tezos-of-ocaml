Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.
Unset Guard Checking.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Require TezosOfOCaml.Proto_2021_01.Constants_repr.
Require TezosOfOCaml.Proto_2021_01.Contract_repr.
Require TezosOfOCaml.Proto_2021_01.Fitness_repr.
Require TezosOfOCaml.Proto_2021_01.Gas_limit_repr.
Require TezosOfOCaml.Proto_2021_01.Lazy_storage_kind.
Require TezosOfOCaml.Proto_2021_01.Level_repr.
Require TezosOfOCaml.Proto_2021_01.Misc.
Require TezosOfOCaml.Proto_2021_01.Parameters_repr.
Require TezosOfOCaml.Proto_2021_01.Raw_level_repr.
Require TezosOfOCaml.Proto_2021_01.Storage_description.
Require TezosOfOCaml.Proto_2021_01.Tez_repr.

Definition Int_set :=
  _Set.Make
    {|
      Compare.COMPARABLE.compare := Compare.Int.(Compare.S.compare)
    |}.

(** Records for the constructor parameters *)
Module ConstructorRecords_gas_counter_status.
  Module gas_counter_status.
    Module Count_operation_gas.
      Record record {block_gas_delta : Set} : Set := Build {
        block_gas_delta : block_gas_delta }.
      Arguments record : clear implicits.
      Definition with_block_gas_delta {t_block_gas_delta} block_gas_delta
        (r : record t_block_gas_delta) :=
        Build t_block_gas_delta block_gas_delta.
    End Count_operation_gas.
    Definition Count_operation_gas_skeleton := Count_operation_gas.record.
    
    Module Count_block_gas.
      Record record {operation_gas_delta : Set} : Set := Build {
        operation_gas_delta : operation_gas_delta }.
      Arguments record : clear implicits.
      Definition with_operation_gas_delta {t_operation_gas_delta}
        operation_gas_delta (r : record t_operation_gas_delta) :=
        Build t_operation_gas_delta operation_gas_delta.
    End Count_block_gas.
    Definition Count_block_gas_skeleton := Count_block_gas.record.
  End gas_counter_status.
End ConstructorRecords_gas_counter_status.
Import ConstructorRecords_gas_counter_status.

Reserved Notation "'gas_counter_status.Count_operation_gas".
Reserved Notation "'gas_counter_status.Count_block_gas".

Inductive gas_counter_status : Set :=
| Unlimited_operation_gas : gas_counter_status
| Count_operation_gas :
  'gas_counter_status.Count_operation_gas -> gas_counter_status
| Count_block_gas : 'gas_counter_status.Count_block_gas -> gas_counter_status

where "'gas_counter_status.Count_operation_gas" :=
  (gas_counter_status.Count_operation_gas_skeleton Gas_limit_repr.Arith.fp)
and "'gas_counter_status.Count_block_gas" :=
  (gas_counter_status.Count_block_gas_skeleton Gas_limit_repr.Arith.fp).

Module gas_counter_status.
  Include ConstructorRecords_gas_counter_status.gas_counter_status.
  Definition Count_operation_gas := 'gas_counter_status.Count_operation_gas.
  Definition Count_block_gas := 'gas_counter_status.Count_block_gas.
End gas_counter_status.

Module back.
  Record record : Set := Build {
    context : Context.t;
    constants : Constants_repr.parametric;
    first_level : Raw_level_repr.t;
    level : Level_repr.t;
    predecessor_timestamp : Time.t;
    timestamp : Time.t;
    fitness : Int64.t;
    deposits :
      Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.Map).(S.INDEXES_MAP.t)
        Tez_repr.t;
    included_endorsements : int;
    allowed_endorsements :
      Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.Map).(S.INDEXES_MAP.t)
        (Signature.public_key * list int * bool);
    fees : Tez_repr.t;
    rewards : Tez_repr.t;
    storage_space_to_pay : option Z.t;
    allocated_contracts : option int;
    origination_nonce : option Contract_repr.origination_nonce;
    temporary_lazy_storage_ids : Lazy_storage_kind.Temp_ids.t;
    internal_nonce : int;
    internal_nonces_used : Int_set.(S.SET.t);
    gas_counter_status : gas_counter_status }.
  Definition with_context context (r : record) :=
    Build context r.(constants) r.(first_level) r.(level)
      r.(predecessor_timestamp) r.(timestamp) r.(fitness) r.(deposits)
      r.(included_endorsements) r.(allowed_endorsements) r.(fees) r.(rewards)
      r.(storage_space_to_pay) r.(allocated_contracts) r.(origination_nonce)
      r.(temporary_lazy_storage_ids) r.(internal_nonce) r.(internal_nonces_used)
      r.(gas_counter_status).
  Definition with_constants constants (r : record) :=
    Build r.(context) constants r.(first_level) r.(level)
      r.(predecessor_timestamp) r.(timestamp) r.(fitness) r.(deposits)
      r.(included_endorsements) r.(allowed_endorsements) r.(fees) r.(rewards)
      r.(storage_space_to_pay) r.(allocated_contracts) r.(origination_nonce)
      r.(temporary_lazy_storage_ids) r.(internal_nonce) r.(internal_nonces_used)
      r.(gas_counter_status).
  Definition with_first_level first_level (r : record) :=
    Build r.(context) r.(constants) first_level r.(level)
      r.(predecessor_timestamp) r.(timestamp) r.(fitness) r.(deposits)
      r.(included_endorsements) r.(allowed_endorsements) r.(fees) r.(rewards)
      r.(storage_space_to_pay) r.(allocated_contracts) r.(origination_nonce)
      r.(temporary_lazy_storage_ids) r.(internal_nonce) r.(internal_nonces_used)
      r.(gas_counter_status).
  Definition with_level level (r : record) :=
    Build r.(context) r.(constants) r.(first_level) level
      r.(predecessor_timestamp) r.(timestamp) r.(fitness) r.(deposits)
      r.(included_endorsements) r.(allowed_endorsements) r.(fees) r.(rewards)
      r.(storage_space_to_pay) r.(allocated_contracts) r.(origination_nonce)
      r.(temporary_lazy_storage_ids) r.(internal_nonce) r.(internal_nonces_used)
      r.(gas_counter_status).
  Definition with_predecessor_timestamp predecessor_timestamp (r : record) :=
    Build r.(context) r.(constants) r.(first_level) r.(level)
      predecessor_timestamp r.(timestamp) r.(fitness) r.(deposits)
      r.(included_endorsements) r.(allowed_endorsements) r.(fees) r.(rewards)
      r.(storage_space_to_pay) r.(allocated_contracts) r.(origination_nonce)
      r.(temporary_lazy_storage_ids) r.(internal_nonce) r.(internal_nonces_used)
      r.(gas_counter_status).
  Definition with_timestamp timestamp (r : record) :=
    Build r.(context) r.(constants) r.(first_level) r.(level)
      r.(predecessor_timestamp) timestamp r.(fitness) r.(deposits)
      r.(included_endorsements) r.(allowed_endorsements) r.(fees) r.(rewards)
      r.(storage_space_to_pay) r.(allocated_contracts) r.(origination_nonce)
      r.(temporary_lazy_storage_ids) r.(internal_nonce) r.(internal_nonces_used)
      r.(gas_counter_status).
  Definition with_fitness fitness (r : record) :=
    Build r.(context) r.(constants) r.(first_level) r.(level)
      r.(predecessor_timestamp) r.(timestamp) fitness r.(deposits)
      r.(included_endorsements) r.(allowed_endorsements) r.(fees) r.(rewards)
      r.(storage_space_to_pay) r.(allocated_contracts) r.(origination_nonce)
      r.(temporary_lazy_storage_ids) r.(internal_nonce) r.(internal_nonces_used)
      r.(gas_counter_status).
  Definition with_deposits deposits (r : record) :=
    Build r.(context) r.(constants) r.(first_level) r.(level)
      r.(predecessor_timestamp) r.(timestamp) r.(fitness) deposits
      r.(included_endorsements) r.(allowed_endorsements) r.(fees) r.(rewards)
      r.(storage_space_to_pay) r.(allocated_contracts) r.(origination_nonce)
      r.(temporary_lazy_storage_ids) r.(internal_nonce) r.(internal_nonces_used)
      r.(gas_counter_status).
  Definition with_included_endorsements included_endorsements (r : record) :=
    Build r.(context) r.(constants) r.(first_level) r.(level)
      r.(predecessor_timestamp) r.(timestamp) r.(fitness) r.(deposits)
      included_endorsements r.(allowed_endorsements) r.(fees) r.(rewards)
      r.(storage_space_to_pay) r.(allocated_contracts) r.(origination_nonce)
      r.(temporary_lazy_storage_ids) r.(internal_nonce) r.(internal_nonces_used)
      r.(gas_counter_status).
  Definition with_allowed_endorsements allowed_endorsements (r : record) :=
    Build r.(context) r.(constants) r.(first_level) r.(level)
      r.(predecessor_timestamp) r.(timestamp) r.(fitness) r.(deposits)
      r.(included_endorsements) allowed_endorsements r.(fees) r.(rewards)
      r.(storage_space_to_pay) r.(allocated_contracts) r.(origination_nonce)
      r.(temporary_lazy_storage_ids) r.(internal_nonce) r.(internal_nonces_used)
      r.(gas_counter_status).
  Definition with_fees fees (r : record) :=
    Build r.(context) r.(constants) r.(first_level) r.(level)
      r.(predecessor_timestamp) r.(timestamp) r.(fitness) r.(deposits)
      r.(included_endorsements) r.(allowed_endorsements) fees r.(rewards)
      r.(storage_space_to_pay) r.(allocated_contracts) r.(origination_nonce)
      r.(temporary_lazy_storage_ids) r.(internal_nonce) r.(internal_nonces_used)
      r.(gas_counter_status).
  Definition with_rewards rewards (r : record) :=
    Build r.(context) r.(constants) r.(first_level) r.(level)
      r.(predecessor_timestamp) r.(timestamp) r.(fitness) r.(deposits)
      r.(included_endorsements) r.(allowed_endorsements) r.(fees) rewards
      r.(storage_space_to_pay) r.(allocated_contracts) r.(origination_nonce)
      r.(temporary_lazy_storage_ids) r.(internal_nonce) r.(internal_nonces_used)
      r.(gas_counter_status).
  Definition with_storage_space_to_pay storage_space_to_pay (r : record) :=
    Build r.(context) r.(constants) r.(first_level) r.(level)
      r.(predecessor_timestamp) r.(timestamp) r.(fitness) r.(deposits)
      r.(included_endorsements) r.(allowed_endorsements) r.(fees) r.(rewards)
      storage_space_to_pay r.(allocated_contracts) r.(origination_nonce)
      r.(temporary_lazy_storage_ids) r.(internal_nonce) r.(internal_nonces_used)
      r.(gas_counter_status).
  Definition with_allocated_contracts allocated_contracts (r : record) :=
    Build r.(context) r.(constants) r.(first_level) r.(level)
      r.(predecessor_timestamp) r.(timestamp) r.(fitness) r.(deposits)
      r.(included_endorsements) r.(allowed_endorsements) r.(fees) r.(rewards)
      r.(storage_space_to_pay) allocated_contracts r.(origination_nonce)
      r.(temporary_lazy_storage_ids) r.(internal_nonce) r.(internal_nonces_used)
      r.(gas_counter_status).
  Definition with_origination_nonce origination_nonce (r : record) :=
    Build r.(context) r.(constants) r.(first_level) r.(level)
      r.(predecessor_timestamp) r.(timestamp) r.(fitness) r.(deposits)
      r.(included_endorsements) r.(allowed_endorsements) r.(fees) r.(rewards)
      r.(storage_space_to_pay) r.(allocated_contracts) origination_nonce
      r.(temporary_lazy_storage_ids) r.(internal_nonce) r.(internal_nonces_used)
      r.(gas_counter_status).
  Definition with_temporary_lazy_storage_ids temporary_lazy_storage_ids
    (r : record) :=
    Build r.(context) r.(constants) r.(first_level) r.(level)
      r.(predecessor_timestamp) r.(timestamp) r.(fitness) r.(deposits)
      r.(included_endorsements) r.(allowed_endorsements) r.(fees) r.(rewards)
      r.(storage_space_to_pay) r.(allocated_contracts) r.(origination_nonce)
      temporary_lazy_storage_ids r.(internal_nonce) r.(internal_nonces_used)
      r.(gas_counter_status).
  Definition with_internal_nonce internal_nonce (r : record) :=
    Build r.(context) r.(constants) r.(first_level) r.(level)
      r.(predecessor_timestamp) r.(timestamp) r.(fitness) r.(deposits)
      r.(included_endorsements) r.(allowed_endorsements) r.(fees) r.(rewards)
      r.(storage_space_to_pay) r.(allocated_contracts) r.(origination_nonce)
      r.(temporary_lazy_storage_ids) internal_nonce r.(internal_nonces_used)
      r.(gas_counter_status).
  Definition with_internal_nonces_used internal_nonces_used (r : record) :=
    Build r.(context) r.(constants) r.(first_level) r.(level)
      r.(predecessor_timestamp) r.(timestamp) r.(fitness) r.(deposits)
      r.(included_endorsements) r.(allowed_endorsements) r.(fees) r.(rewards)
      r.(storage_space_to_pay) r.(allocated_contracts) r.(origination_nonce)
      r.(temporary_lazy_storage_ids) r.(internal_nonce) internal_nonces_used
      r.(gas_counter_status).
  Definition with_gas_counter_status gas_counter_status (r : record) :=
    Build r.(context) r.(constants) r.(first_level) r.(level)
      r.(predecessor_timestamp) r.(timestamp) r.(fitness) r.(deposits)
      r.(included_endorsements) r.(allowed_endorsements) r.(fees) r.(rewards)
      r.(storage_space_to_pay) r.(allocated_contracts) r.(origination_nonce)
      r.(temporary_lazy_storage_ids) r.(internal_nonce) r.(internal_nonces_used)
      gas_counter_status.
End back.
Definition back := back.record.

Module t.
  Record record : Set := Build {
    gas_counter : Gas_limit_repr.Arith.fp;
    back : back }.
  Definition with_gas_counter gas_counter (r : record) :=
    Build gas_counter r.(back).
  Definition with_back back (r : record) :=
    Build r.(gas_counter) back.
End t.
Definition t := t.record.

Definition context : Set := t.

Definition root_context : Set := t.

Definition context_value (ctxt : t) : Context.t := ctxt.(t.back).(back.context).

Definition current_level (ctxt : t) : Level_repr.t :=
  ctxt.(t.back).(back.level).

Definition storage_space_to_pay (ctxt : t) : option Z.t :=
  ctxt.(t.back).(back.storage_space_to_pay).

Definition predecessor_timestamp (ctxt : t) : Time.t :=
  ctxt.(t.back).(back.predecessor_timestamp).

Definition current_timestamp (ctxt : t) : Time.t :=
  ctxt.(t.back).(back.timestamp).

Definition current_fitness (ctxt : t) : Int64.t := ctxt.(t.back).(back.fitness).

Definition first_level (ctxt : t) : Raw_level_repr.t :=
  ctxt.(t.back).(back.first_level).

Definition constants (ctxt : t) : Constants_repr.parametric :=
  ctxt.(t.back).(back.constants).

Definition recover (ctxt : t) : Context.t := ctxt.(t.back).(back.context).

Definition fees (ctxt : t) : Tez_repr.t := ctxt.(t.back).(back.fees).

Definition origination_nonce (ctxt : t)
  : option Contract_repr.origination_nonce :=
  ctxt.(t.back).(back.origination_nonce).

Definition deposits (ctxt : t)
  : Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.Map).(S.INDEXES_MAP.t)
    Tez_repr.t := ctxt.(t.back).(back.deposits).

Definition allowed_endorsements (ctxt : t)
  : Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.Map).(S.INDEXES_MAP.t)
    (Signature.public_key * list int * bool) :=
  ctxt.(t.back).(back.allowed_endorsements).

Definition included_endorsements (ctxt : t) : int :=
  ctxt.(t.back).(back.included_endorsements).

Definition internal_nonce (ctxt : t) : int :=
  ctxt.(t.back).(back.internal_nonce).

Definition internal_nonces_used (ctxt : t) : Int_set.(S.SET.t) :=
  ctxt.(t.back).(back.internal_nonces_used).

Definition gas_counter_status_value (ctxt : t) : gas_counter_status :=
  ctxt.(t.back).(back.gas_counter_status).

Definition rewards (ctxt : t) : Tez_repr.t := ctxt.(t.back).(back.rewards).

Definition allocated_contracts (ctxt : t) : option int :=
  ctxt.(t.back).(back.allocated_contracts).

Definition temporary_lazy_storage_ids (ctxt : t)
  : Lazy_storage_kind.Temp_ids.t :=
  ctxt.(t.back).(back.temporary_lazy_storage_ids).

Definition gas_counter (ctxt : t) : Gas_limit_repr.Arith.fp :=
  ctxt.(t.gas_counter).

Definition update_gas_counter (ctxt : t) (gas_counter : Gas_limit_repr.Arith.fp)
  : t := t.with_gas_counter gas_counter ctxt.

Definition update_back (ctxt : t) (back : back) : t := t.with_back back ctxt.

Definition update_gas_counter_status
  (ctxt : t) (gas_counter_status_value : gas_counter_status) : t :=
  update_back ctxt
    (back.with_gas_counter_status gas_counter_status_value ctxt.(t.back)).

Definition update_context (ctxt : t) (context_value : Context.t) : t :=
  update_back ctxt (back.with_context context_value ctxt.(t.back)).

Definition update_constants (ctxt : t) (constants : Constants_repr.parametric)
  : t := update_back ctxt (back.with_constants constants ctxt.(t.back)).

Definition update_fitness (ctxt : t) (fitness : Int64.t) : t :=
  update_back ctxt (back.with_fitness fitness ctxt.(t.back)).

Definition update_deposits
  (ctxt : t)
  (deposits :
    Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.Map).(S.INDEXES_MAP.t)
      Tez_repr.t) : t :=
  update_back ctxt (back.with_deposits deposits ctxt.(t.back)).

Definition update_allowed_endorsements
  (ctxt : t)
  (allowed_endorsements :
    Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.Map).(S.INDEXES_MAP.t)
      (Signature.public_key * list int * bool)) : t :=
  update_back ctxt
    (back.with_allowed_endorsements allowed_endorsements ctxt.(t.back)).

Definition update_rewards (ctxt : t) (rewards : Tez_repr.t) : t :=
  update_back ctxt (back.with_rewards rewards ctxt.(t.back)).

Definition raw_update_storage_space_to_pay
  (ctxt : t) (storage_space_to_pay : option Z.t) : t :=
  update_back ctxt
    (back.with_storage_space_to_pay storage_space_to_pay ctxt.(t.back)).

Definition update_allocated_contracts
  (ctxt : t) (allocated_contracts : option int) : t :=
  update_back ctxt
    (back.with_allocated_contracts allocated_contracts ctxt.(t.back)).

Definition update_origination_nonce
  (ctxt : t) (origination_nonce : option Contract_repr.origination_nonce) : t :=
  update_back ctxt (back.with_origination_nonce origination_nonce ctxt.(t.back)).

Definition update_internal_nonce (ctxt : t) (internal_nonce : int) : t :=
  update_back ctxt (back.with_internal_nonce internal_nonce ctxt.(t.back)).

Definition update_internal_nonces_used
  (ctxt : t) (internal_nonces_used : Int_set.(S.SET.t)) : t :=
  update_back ctxt
    (back.with_internal_nonces_used internal_nonces_used ctxt.(t.back)).

Definition update_included_endorsements (ctxt : t) (included_endorsements : int)
  : t :=
  update_back ctxt
    (back.with_included_endorsements included_endorsements ctxt.(t.back)).

Definition update_fees (ctxt : t) (fees : Tez_repr.t) : t :=
  update_back ctxt (back.with_fees fees ctxt.(t.back)).

Definition update_temporary_lazy_storage_ids
  (ctxt : t) (temporary_lazy_storage_ids : Lazy_storage_kind.Temp_ids.t) : t :=
  update_back ctxt
    (back.with_temporary_lazy_storage_ids temporary_lazy_storage_ids
      ctxt.(t.back)).

Definition record_endorsement (ctxt : t) (k : Signature.public_key_hash) : t :=
  match
    Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.Map).(S.INDEXES_MAP.find_opt)
      k (allowed_endorsements ctxt) with
  | None =>
    (* ❌ Assert instruction is not handled. *)
    assert t false
  | Some (_, _, true) =>
    (* ❌ Assert instruction is not handled. *)
    assert t false
  | Some (d, s, false) =>
    let ctxt :=
      update_included_endorsements ctxt
        ((included_endorsements ctxt) +i (List.length s)) in
    update_allowed_endorsements ctxt
      (Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.Map).(S.INDEXES_MAP.add)
        k (d, s, true) (allowed_endorsements ctxt))
  end.

Definition init_endorsements
  (ctxt : t)
  (allowed_endorsements' :
    Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.Map).(S.INDEXES_MAP.t)
      (Signature.public_key * list int * bool)) : t :=
  if
    Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.Map).(S.INDEXES_MAP.is_empty)
      allowed_endorsements'
  then
    (* ❌ Assert instruction is not handled. *)
    assert t false
  else
    if
      Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.Map).(S.INDEXES_MAP.is_empty)
        (allowed_endorsements ctxt)
    then
      update_allowed_endorsements ctxt allowed_endorsements'
    else
      (* ❌ Assert instruction is not handled. *)
      assert t false.

(** Init function; without side-effects in Coq *)
Definition init_module1 : unit :=
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent
      "too_many_internal_operations" "Too many internal operations"
      "A transaction exceeded the hard limit of internal operations it can emit"
      None Data_encoding.empty
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Too_many_internal_operations" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Too_many_internal_operations" unit tt) in
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "gas_exhausted.operation" "Gas quota exceeded for the operation"
      "A script or one of its callee took more time than the operation said it would"
      None Data_encoding.empty
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Operation_quota_exceeded" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Operation_quota_exceeded" unit tt) in
  Error_monad.register_error_kind Error_monad.Temporary "gas_exhausted.block"
    "Gas quota exceeded for the block"
    "The sum of gas consumed by all the operations in the block exceeds the hard gas limit per block"
    None Data_encoding.empty
    (fun (function_parameter : Error_monad._error) =>
      match function_parameter with
      | Build_extensible tag _ payload =>
        if String.eqb tag "Block_quota_exceeded" then
          Some tt
        else None
      end)
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      Build_extensible "Block_quota_exceeded" unit tt).

Definition fresh_internal_nonce (ctxt : t) : M? (t * int) :=
  if (internal_nonce ctxt) >=i 65535 then
    Error_monad.error_value
      (Build_extensible "Too_many_internal_operations" unit tt)
  else
    return?
      ((update_internal_nonce ctxt ((internal_nonce ctxt) +i 1)),
        (internal_nonce ctxt)).

Definition reset_internal_nonce (ctxt : t) : t :=
  let ctxt := update_internal_nonce ctxt 0 in
  update_internal_nonces_used ctxt Int_set.(S.SET.empty).

Definition record_internal_nonce (ctxt : t) (k : int) : t :=
  update_internal_nonces_used ctxt
    (Int_set.(S.SET.add) k (internal_nonces_used ctxt)).

Definition internal_nonce_already_recorded (ctxt : t) (k : int) : bool :=
  Int_set.(S.SET.mem) k (internal_nonces_used ctxt).

Definition set_current_fitness (ctxt : t) (fitness : Int64.t) : t :=
  update_fitness ctxt fitness.

Definition add_fees (ctxt : t) (fees' : Tez_repr.t) : M? t :=
  Error_monad.op_gtpipequestion (Tez_repr.op_plusquestion (fees ctxt) fees')
    (update_fees ctxt).

Definition add_rewards (ctxt : t) (rewards' : Tez_repr.t) : M? t :=
  Error_monad.op_gtpipequestion
    (Tez_repr.op_plusquestion (rewards ctxt) rewards') (update_rewards ctxt).

Definition add_deposit
  (ctxt : t) (delegate : Signature.public_key_hash) (deposit : Tez_repr.t)
  : M? t :=
  let previous :=
    match
      Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.Map).(S.INDEXES_MAP.find_opt)
        delegate (deposits ctxt) with
    | Some tz => tz
    | None => Tez_repr.zero
    end in
  let? deposit := Tez_repr.op_plusquestion previous deposit in
  let deposits :=
    Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.Map).(S.INDEXES_MAP.add)
      delegate deposit (deposits ctxt) in
  return? (update_deposits ctxt deposits).

Definition get_deposits
  : t ->
  Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.Map).(S.INDEXES_MAP.t)
    Tez_repr.t := deposits.

Definition get_rewards : t -> Tez_repr.t := rewards.

Definition get_fees : t -> Tez_repr.t := fees.

(** Init function; without side-effects in Coq *)
Definition init_module2 : unit :=
  Error_monad.register_error_kind Error_monad.Permanent
    "undefined_operation_nonce" "Ill timed access to the origination nonce"
    "An origination was attempted out of the scope of a manager operation" None
    Data_encoding.empty
    (fun (function_parameter : Error_monad._error) =>
      match function_parameter with
      | Build_extensible tag _ payload =>
        if String.eqb tag "Undefined_operation_nonce" then
          Some tt
        else None
      end)
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      Build_extensible "Undefined_operation_nonce" unit tt).

Definition init_origination_nonce (ctxt : t) (operation_hash : Operation_hash.t)
  : t :=
  let origination_nonce :=
    Some (Contract_repr.initial_origination_nonce operation_hash) in
  update_origination_nonce ctxt origination_nonce.

Definition increment_origination_nonce (ctxt : t)
  : M? (t * Contract_repr.origination_nonce) :=
  match origination_nonce ctxt with
  | None =>
    Error_monad.error_value
      (Build_extensible "Undefined_operation_nonce" unit tt)
  | Some cur_origination_nonce =>
    let origination_nonce :=
      Some (Contract_repr.incr_origination_nonce cur_origination_nonce) in
    let ctxt := update_origination_nonce ctxt origination_nonce in
    return? (ctxt, cur_origination_nonce)
  end.

Definition get_origination_nonce (ctxt : t)
  : M? Contract_repr.origination_nonce :=
  match origination_nonce ctxt with
  | None =>
    Error_monad.error_value
      (Build_extensible "Undefined_operation_nonce" unit tt)
  | Some origination_nonce => return? origination_nonce
  end.

Definition unset_origination_nonce (ctxt : t) : t :=
  update_origination_nonce ctxt None.

(** Init function; without side-effects in Coq *)
Definition init_module3 : unit :=
  Error_monad.register_error_kind Error_monad.Permanent "gas_limit_too_high"
    "Gas limit out of protocol hard bounds"
    "A transaction tried to exceed the hard limit on gas" None
    Data_encoding.empty
    (fun (function_parameter : Error_monad._error) =>
      match function_parameter with
      | Build_extensible tag _ payload =>
        if String.eqb tag "Gas_limit_too_high" then
          Some tt
        else None
      end)
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      Build_extensible "Gas_limit_too_high" unit tt).

Definition gas_level (ctxt : t) : Gas_limit_repr.t :=
  match gas_counter_status_value ctxt with
  | Unlimited_operation_gas => Gas_limit_repr.Unaccounted
  |
    Count_block_gas {|
      gas_counter_status.Count_block_gas.operation_gas_delta := operation_gas_delta
        |} =>
    Gas_limit_repr.Limited
      {|
        Gas_limit_repr.t.Limited.remaining :=
          Gas_limit_repr.Arith.add (gas_counter ctxt) operation_gas_delta |}
  | Count_operation_gas _ =>
    Gas_limit_repr.Limited
      {| Gas_limit_repr.t.Limited.remaining := gas_counter ctxt |}
  end.

Definition block_gas_level (ctxt : t) : Gas_limit_repr.Arith.fp :=
  match gas_counter_status_value ctxt with
  | (Unlimited_operation_gas | Count_block_gas _) => gas_counter ctxt
  |
    Count_operation_gas {|
      gas_counter_status.Count_operation_gas.block_gas_delta := block_gas_delta
        |} => Gas_limit_repr.Arith.add (gas_counter ctxt) block_gas_delta
  end.

Definition check_gas_limit (ctxt : t) (remaining : Gas_limit_repr.Arith.t)
  : M? unit :=
  if
    (Gas_limit_repr.Arith.op_gt remaining
      (constants ctxt).(Constants_repr.parametric.hard_gas_limit_per_operation))
    || (Gas_limit_repr.Arith.op_lt remaining Gas_limit_repr.Arith.zero)
  then
    Error_monad.error_value (Build_extensible "Gas_limit_too_high" unit tt)
  else
    Error_monad.ok_unit.

Definition set_gas_limit (ctxt : t) (remaining : Gas_limit_repr.Arith.t) : t :=
  let remaining := Gas_limit_repr.Arith.fp_value remaining in
  let block_gas := block_gas_level ctxt in
  let '(gas_counter_status_value, gas_counter) :=
    if Gas_limit_repr.Arith.op_lt remaining block_gas then
      let block_gas_delta := Gas_limit_repr.Arith.sub block_gas remaining in
      ((Count_operation_gas
        {|
          gas_counter_status.Count_operation_gas.block_gas_delta :=
            block_gas_delta |}), remaining)
    else
      let operation_gas_delta := Gas_limit_repr.Arith.sub remaining block_gas in
      ((Count_block_gas
        {|
          gas_counter_status.Count_block_gas.operation_gas_delta :=
            operation_gas_delta |}), block_gas) in
  let ctxt := update_gas_counter_status ctxt gas_counter_status_value in
  t.with_gas_counter gas_counter ctxt.

Definition set_gas_unlimited (ctxt : t) : t :=
  let block_gas := block_gas_level ctxt in
  let ctxt := t.with_gas_counter block_gas ctxt in
  update_gas_counter_status ctxt Unlimited_operation_gas.

Definition is_gas_unlimited (ctxt : t) : bool :=
  match ctxt.(t.back).(back.gas_counter_status) with
  | Unlimited_operation_gas => true
  | _ => false
  end.

Definition is_counting_block_gas (ctxt : t) : bool :=
  match gas_counter_status_value ctxt with
  | Count_block_gas _ => true
  | _ => false
  end.

Definition consume_gas (ctxt : t) (cost : Gas_limit_repr.cost) : M? t :=
  if is_gas_unlimited ctxt then
    return? ctxt
  else
    match Gas_limit_repr.raw_consume (gas_counter ctxt) cost with
    | Some gas_counter => Pervasives.Ok (update_gas_counter ctxt gas_counter)
    | None =>
      if is_counting_block_gas ctxt then
        Error_monad.error_value
          (Build_extensible "Block_quota_exceeded" unit tt)
      else
        Error_monad.error_value
          (Build_extensible "Operation_quota_exceeded" unit tt)
    end.

Definition check_enough_gas (ctxt : t) (cost : Gas_limit_repr.cost) : M? unit :=
  let? '_ := consume_gas ctxt cost in
  Error_monad.ok_unit.

Definition gas_consumed (since : t) (until : t) : Gas_limit_repr.Arith.t :=
  match ((gas_level since), (gas_level until)) with
  |
    (Gas_limit_repr.Limited {| Gas_limit_repr.t.Limited.remaining := before |},
      Gas_limit_repr.Limited {| Gas_limit_repr.t.Limited.remaining := after |})
    => Gas_limit_repr.Arith.sub before after
  | (_, _) => Gas_limit_repr.Arith.zero
  end.

Definition init_storage_space_to_pay (ctxt : t) : t :=
  match storage_space_to_pay ctxt with
  | Some _ =>
    (* ❌ Assert instruction is not handled. *)
    assert t false
  | None =>
    let ctxt := raw_update_storage_space_to_pay ctxt (Some Z.zero) in
    update_allocated_contracts ctxt (Some 0)
  end.

Definition clear_storage_space_to_pay (ctxt : t) : t * Z.t * int :=
  match ((storage_space_to_pay ctxt), (allocated_contracts ctxt)) with
  | ((None, _) | (_, None)) =>
    (* ❌ Assert instruction is not handled. *)
    assert (t * Z.t * int) false
  | (Some storage_space_to_pay, Some allocated_contracts) =>
    let ctxt := raw_update_storage_space_to_pay ctxt None in
    let ctxt := update_allocated_contracts ctxt None in
    (ctxt, storage_space_to_pay, allocated_contracts)
  end.

Definition update_storage_space_to_pay (ctxt : t) (n : Z.t) : t :=
  match storage_space_to_pay ctxt with
  | None =>
    (* ❌ Assert instruction is not handled. *)
    assert t false
  | Some storage_space_to_pay =>
    raw_update_storage_space_to_pay ctxt (Some (n +Z storage_space_to_pay))
  end.

Definition update_allocated_contracts_count (ctxt : t) : t :=
  match allocated_contracts ctxt with
  | None =>
    (* ❌ Assert instruction is not handled. *)
    assert t false
  | Some allocated_contracts =>
    update_allocated_contracts ctxt (Some (Pervasives.succ allocated_contracts))
  end.

Inductive missing_key_kind : Set :=
| Get : missing_key_kind
| _Set : missing_key_kind
| Del : missing_key_kind
| Copy : missing_key_kind.

Inductive storage_error : Set :=
| Incompatible_protocol_version : string -> storage_error
| Missing_key : list string -> missing_key_kind -> storage_error
| Existing_key : list string -> storage_error
| Corrupted_data : list string -> storage_error.

Definition storage_error_encoding : Data_encoding.encoding storage_error :=
  Data_encoding.union None
    [
      Data_encoding.case_value "Incompatible_protocol_version" None
        (Data_encoding.Tag 0)
        (Data_encoding.obj1
          (Data_encoding.req None None "incompatible_protocol_version"
            Data_encoding.string_value))
        (fun (function_parameter : storage_error) =>
          match function_parameter with
          | Incompatible_protocol_version arg => Some arg
          | _ => None
          end) (fun (arg : string) => Incompatible_protocol_version arg);
      Data_encoding.case_value "Missing_key" None (Data_encoding.Tag 1)
        (Data_encoding.obj2
          (Data_encoding.req None None "missing_key"
            (Data_encoding.list_value None Data_encoding.string_value))
          (Data_encoding.req None None "function"
            (Data_encoding.string_enum
              [
                ("get", Get);
                ("set", _Set);
                ("del", Del);
                ("copy", Copy)
              ])))
        (fun (function_parameter : storage_error) =>
          match function_parameter with
          | Missing_key key_value f => Some (key_value, f)
          | _ => None
          end)
        (fun (function_parameter : list string * missing_key_kind) =>
          let '(key_value, f) := function_parameter in
          Missing_key key_value f);
      Data_encoding.case_value "Existing_key" None (Data_encoding.Tag 2)
        (Data_encoding.obj1
          (Data_encoding.req None None "existing_key"
            (Data_encoding.list_value None Data_encoding.string_value)))
        (fun (function_parameter : storage_error) =>
          match function_parameter with
          | Existing_key key_value => Some key_value
          | _ => None
          end) (fun (key_value : list string) => Existing_key key_value);
      Data_encoding.case_value "Corrupted_data" None (Data_encoding.Tag 3)
        (Data_encoding.obj1
          (Data_encoding.req None None "corrupted_data"
            (Data_encoding.list_value None Data_encoding.string_value)))
        (fun (function_parameter : storage_error) =>
          match function_parameter with
          | Corrupted_data key_value => Some key_value
          | _ => None
          end) (fun (key_value : list string) => Corrupted_data key_value)
    ].

Definition pp_storage_error
  (ppf : Format.formatter) (function_parameter : storage_error) : unit :=
  match function_parameter with
  | Incompatible_protocol_version version =>
    Format.fprintf ppf
      (CamlinternalFormatBasics.Format
        (CamlinternalFormatBasics.String_literal
          "Found a context with an unexpected version '"
          (CamlinternalFormatBasics.String CamlinternalFormatBasics.No_padding
            (CamlinternalFormatBasics.String_literal "'."
              CamlinternalFormatBasics.End_of_format)))
        "Found a context with an unexpected version '%s'.") version
  | Missing_key key_value Get =>
    Format.fprintf ppf
      (CamlinternalFormatBasics.Format
        (CamlinternalFormatBasics.String_literal "Missing key '"
          (CamlinternalFormatBasics.String CamlinternalFormatBasics.No_padding
            (CamlinternalFormatBasics.String_literal "'."
              CamlinternalFormatBasics.End_of_format))) "Missing key '%s'.")
      (String.concat "/" key_value)
  | Missing_key key_value _Set =>
    Format.fprintf ppf
      (CamlinternalFormatBasics.Format
        (CamlinternalFormatBasics.String_literal "Cannot set undefined key '"
          (CamlinternalFormatBasics.String CamlinternalFormatBasics.No_padding
            (CamlinternalFormatBasics.String_literal "'."
              CamlinternalFormatBasics.End_of_format)))
        "Cannot set undefined key '%s'.") (String.concat "/" key_value)
  | Missing_key key_value Del =>
    Format.fprintf ppf
      (CamlinternalFormatBasics.Format
        (CamlinternalFormatBasics.String_literal "Cannot delete undefined key '"
          (CamlinternalFormatBasics.String CamlinternalFormatBasics.No_padding
            (CamlinternalFormatBasics.String_literal "'."
              CamlinternalFormatBasics.End_of_format)))
        "Cannot delete undefined key '%s'.") (String.concat "/" key_value)
  | Missing_key key_value Copy =>
    Format.fprintf ppf
      (CamlinternalFormatBasics.Format
        (CamlinternalFormatBasics.String_literal "Cannot copy undefined key '"
          (CamlinternalFormatBasics.String CamlinternalFormatBasics.No_padding
            (CamlinternalFormatBasics.String_literal "'."
              CamlinternalFormatBasics.End_of_format)))
        "Cannot copy undefined key '%s'.") (String.concat "/" key_value)
  | Existing_key key_value =>
    Format.fprintf ppf
      (CamlinternalFormatBasics.Format
        (CamlinternalFormatBasics.String_literal
          "Cannot initialize defined key '"
          (CamlinternalFormatBasics.String CamlinternalFormatBasics.No_padding
            (CamlinternalFormatBasics.String_literal "'."
              CamlinternalFormatBasics.End_of_format)))
        "Cannot initialize defined key '%s'.") (String.concat "/" key_value)
  | Corrupted_data key_value =>
    Format.fprintf ppf
      (CamlinternalFormatBasics.Format
        (CamlinternalFormatBasics.String_literal "Failed to parse the data at '"
          (CamlinternalFormatBasics.String CamlinternalFormatBasics.No_padding
            (CamlinternalFormatBasics.String_literal "'."
              CamlinternalFormatBasics.End_of_format)))
        "Failed to parse the data at '%s'.") (String.concat "/" key_value)
  end.

(** Init function; without side-effects in Coq *)
Definition init_module4 : unit :=
  Error_monad.register_error_kind Error_monad.Permanent "context.storage_error"
    "Storage error (fatal internal error)"
    "An error that should never happen unless something has been deleted or corrupted in the database."
    (Some
      (fun (ppf : Format.formatter) =>
        fun (err : storage_error) =>
          Format.fprintf ppf
            (CamlinternalFormatBasics.Format
              (CamlinternalFormatBasics.Formatting_gen
                (CamlinternalFormatBasics.Open_box
                  (CamlinternalFormatBasics.Format
                    (CamlinternalFormatBasics.String_literal "<v 2>"
                      CamlinternalFormatBasics.End_of_format) "<v 2>"))
                (CamlinternalFormatBasics.String_literal "Storage error:"
                  (CamlinternalFormatBasics.Formatting_lit
                    (CamlinternalFormatBasics.Break "@ " 1 0)
                    (CamlinternalFormatBasics.Alpha
                      (CamlinternalFormatBasics.Formatting_lit
                        CamlinternalFormatBasics.Close_box
                        CamlinternalFormatBasics.End_of_format)))))
              "@[<v 2>Storage error:@ %a@]") pp_storage_error err))
    storage_error_encoding
    (fun (function_parameter : Error_monad._error) =>
      match function_parameter with
      | Build_extensible tag _ payload =>
        if String.eqb tag "Storage_error" then
          let 'err := cast storage_error payload in
          Some err
        else None
      end)
    (fun (err : storage_error) =>
      Build_extensible "Storage_error" storage_error err).

Definition storage_error_value {A : Set} (err : storage_error) : M? A :=
  Error_monad.error_value (Build_extensible "Storage_error" storage_error err).

Definition version_key : list string := [ "version" ].

Definition version_value : string := "alpha_current".

Definition version : string := "v1".

Definition first_level_key : list string := [ version; "first_level" ].

Definition constants_key : list string := [ version; "constants" ].

Definition protocol_param_key : list string := [ "protocol_parameters" ].

Definition get_first_level (ctxt : Context.t) : M=? Raw_level_repr.raw_level :=
  let= function_parameter := Context.get ctxt first_level_key in
  match function_parameter with
  | None => return= (storage_error_value (Missing_key first_level_key Get))
  | Some bytes_value =>
    match Data_encoding.Binary.of_bytes Raw_level_repr.encoding bytes_value with
    | None => return= (storage_error_value (Corrupted_data first_level_key))
    | Some level => return=? level
    end
  end.

Definition set_first_level {A : Set}
  (ctxt : Context.t) (level : Raw_level_repr.raw_level)
  : M= (Pervasives.result Context.t A) :=
  let bytes_value :=
    Data_encoding.Binary.to_bytes_exn Raw_level_repr.encoding level in
  Error_monad.op_gtpipeeq (Context.set ctxt first_level_key bytes_value)
    Error_monad.ok.

(** Init function; without side-effects in Coq *)
Definition init_module5 : unit :=
  let '_ :=
    Error_monad.register_error_kind Error_monad.Temporary
      "context.failed_to_parse_parameter" "Failed to parse parameter"
      "The protocol parameters are not valid JSON."
      (Some
        (fun (ppf : Format.formatter) =>
          fun (bytes_value : bytes) =>
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.Formatting_gen
                  (CamlinternalFormatBasics.Open_box
                    (CamlinternalFormatBasics.Format
                      (CamlinternalFormatBasics.String_literal "<v 2>"
                        CamlinternalFormatBasics.End_of_format) "<v 2>"))
                  (CamlinternalFormatBasics.String_literal
                    "Cannot parse the protocol parameter:"
                    (CamlinternalFormatBasics.Formatting_lit
                      (CamlinternalFormatBasics.Break "@ " 1 0)
                      (CamlinternalFormatBasics.String
                        CamlinternalFormatBasics.No_padding
                        (CamlinternalFormatBasics.Formatting_lit
                          CamlinternalFormatBasics.Close_box
                          CamlinternalFormatBasics.End_of_format)))))
                "@[<v 2>Cannot parse the protocol parameter:@ %s@]")
              (Bytes.to_string bytes_value)))
      (Data_encoding.obj1
        (Data_encoding.req None None "contents" Data_encoding.bytes_value))
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Failed_to_parse_parameter" then
            let 'data := cast bytes payload in
            Some data
          else None
        end)
      (fun (data : bytes) =>
        Build_extensible "Failed_to_parse_parameter" bytes data) in
  Error_monad.register_error_kind Error_monad.Temporary
    "context.failed_to_decode_parameter" "Failed to decode parameter"
    "Unexpected JSON object."
    (Some
      (fun (ppf : Format.formatter) =>
        fun (function_parameter : Data_encoding.json * string) =>
          let '(json_value, msg) := function_parameter in
          Format.fprintf ppf
            (CamlinternalFormatBasics.Format
              (CamlinternalFormatBasics.Formatting_gen
                (CamlinternalFormatBasics.Open_box
                  (CamlinternalFormatBasics.Format
                    (CamlinternalFormatBasics.String_literal "<v 2>"
                      CamlinternalFormatBasics.End_of_format) "<v 2>"))
                (CamlinternalFormatBasics.String_literal
                  "Cannot decode the protocol parameter:"
                  (CamlinternalFormatBasics.Formatting_lit
                    (CamlinternalFormatBasics.Break "@ " 1 0)
                    (CamlinternalFormatBasics.String
                      CamlinternalFormatBasics.No_padding
                      (CamlinternalFormatBasics.Formatting_lit
                        (CamlinternalFormatBasics.Break "@ " 1 0)
                        (CamlinternalFormatBasics.Alpha
                          (CamlinternalFormatBasics.Formatting_lit
                            CamlinternalFormatBasics.Close_box
                            CamlinternalFormatBasics.End_of_format)))))))
              "@[<v 2>Cannot decode the protocol parameter:@ %s@ %a@]") msg
            Data_encoding.Json.pp json_value))
    (Data_encoding.obj2
      (Data_encoding.req None None "contents" Data_encoding.json_value)
      (Data_encoding.req None None "error" Data_encoding.string_value))
    (fun (function_parameter : Error_monad._error) =>
      match function_parameter with
      | Build_extensible tag _ payload =>
        if String.eqb tag "Failed_to_decode_parameter" then
          let '(json_value, msg) :=
            cast (Data_encoding.json * string) payload in
          Some (json_value, msg)
        else None
      end)
    (fun (function_parameter : Data_encoding.json * string) =>
      let '(json_value, msg) := function_parameter in
      Build_extensible "Failed_to_decode_parameter"
        (Data_encoding.json * string) (json_value, msg)).

Definition get_proto_param (ctxt : Context.t)
  : M=? (Parameters_repr.t * Context.t) :=
  let= function_parameter := Context.get ctxt protocol_param_key in
  match function_parameter with
  | None => Pervasives.failwith "Missing protocol parameters."
  | Some bytes_value =>
    match Data_encoding.Binary.of_bytes Data_encoding.json_value bytes_value
      with
    | None =>
      Error_monad.fail
        (Build_extensible "Failed_to_parse_parameter" Context.value bytes_value)
    | Some json_value =>
      let= ctxt := Context.remove_rec ctxt protocol_param_key in
      match
        Misc.result_of_exception
          (fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Data_encoding.Json.destruct Parameters_repr.encoding json_value)
        with
      | Pervasives.Error exn_value =>
        return=
          match exn_value with
          | Build_extensible tag _ payload =>
            if String.eqb tag "Cannot_destruct" then
              let '_ :=
                cast (Data_encoding.Json.path * extensible_type) payload in
              Format.kasprintf Pervasives.failwith
                (CamlinternalFormatBasics.Format
                  (CamlinternalFormatBasics.String_literal
                    "Invalid protocol_parameters: "
                    (CamlinternalFormatBasics.Alpha
                      (CamlinternalFormatBasics.Char_literal " " % char
                        (CamlinternalFormatBasics.Alpha
                          CamlinternalFormatBasics.End_of_format))))
                  "Invalid protocol_parameters: %a %a")
                (fun (ppf : Format.formatter) =>
                  Data_encoding.Json.print_error None ppf) exn_value
                Data_encoding.Json.pp json_value
            else Pervasives.raise exn_value
          end
      | Pervasives.Ok param => return=? (param, ctxt)
      end
    end
  end.

Definition set_constants
  (ctxt : Context.t) (constants : Constants_repr.parametric) : M= Context.t :=
  let bytes_value :=
    Data_encoding.Binary.to_bytes_exn Constants_repr.parametric_encoding
      constants in
  Context.set ctxt constants_key bytes_value.

Definition get_constants {A : Set} (ctxt : Context.t)
  : M= (Pervasives.result Constants_repr.parametric A) :=
  let= function_parameter := Context.get ctxt constants_key in
  match function_parameter with
  | None =>
    return=
      (Pervasives.failwith "Internal error: cannot read constants in context.")
  | Some bytes_value =>
    match
      Data_encoding.Binary.of_bytes Constants_repr.parametric_encoding
        bytes_value with
    | None =>
      return=
        (Pervasives.failwith
          "Internal error: cannot parse constants in context.")
    | Some constants => return=? constants
    end
  end.

Definition patch_constants
  (ctxt : t) (f : Constants_repr.parametric -> Constants_repr.parametric)
  : M= t :=
  let constants := f (constants ctxt) in
  let= context_value := set_constants (context_value ctxt) constants in
  let ctxt := update_context ctxt context_value in
  return= (update_constants ctxt constants).

Definition check_inited (ctxt : Context.t) : M=? unit :=
  let= function_parameter := Context.get ctxt version_key in
  match function_parameter with
  | None =>
    return= (Pervasives.failwith "Internal error: un-initialized context.")
  | Some bytes_value =>
    let s := Bytes.to_string bytes_value in
    return=
      (if Compare.String.(Compare.S.op_eq) s version_value then
        Error_monad.ok_unit
      else
        storage_error_value (Incompatible_protocol_version s))
  end.

Definition prepare
  (level : int32) (predecessor_timestamp : Time.t) (timestamp : Time.t)
  (fitness : list bytes) (ctxt : Context.t) : M=? t :=
  let=? level := return= (Raw_level_repr.of_int32 level) in
  let=? fitness := return= (Fitness_repr.to_int64 fitness) in
  let=? '_ := check_inited ctxt in
  let=? constants := get_constants ctxt in
  let=? first_level := get_first_level ctxt in
  let level :=
    Level_repr.level_from_raw first_level
      constants.(Constants_repr.parametric.blocks_per_cycle)
      constants.(Constants_repr.parametric.blocks_per_commitment) level in
  return=?
    {|
      t.gas_counter :=
        Gas_limit_repr.Arith.fp_value
          constants.(Constants_repr.parametric.hard_gas_limit_per_block);
      t.back :=
        {| back.context := ctxt; back.constants := constants;
          back.first_level := first_level; back.level := level;
          back.predecessor_timestamp := predecessor_timestamp;
          back.timestamp := timestamp; back.fitness := fitness;
          back.deposits :=
            Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.Map).(S.INDEXES_MAP.empty);
          back.included_endorsements := 0;
          back.allowed_endorsements :=
            Signature.Public_key_hash.(S.SIGNATURE_PUBLIC_KEY_HASH.Map).(S.INDEXES_MAP.empty);
          back.fees := Tez_repr.zero; back.rewards := Tez_repr.zero;
          back.storage_space_to_pay := None; back.allocated_contracts := None;
          back.origination_nonce := None;
          back.temporary_lazy_storage_ids :=
            Lazy_storage_kind.Temp_ids.init_value; back.internal_nonce := 0;
          back.internal_nonces_used := Int_set.(S.SET.empty);
          back.gas_counter_status := Unlimited_operation_gas |} |}.

Inductive previous_protocol : Set :=
| Genesis : Parameters_repr.t -> previous_protocol
| Edo_008 : previous_protocol.

Definition check_and_update_protocol_version (ctxt : Context.t)
  : M=? (previous_protocol * Context.t) :=
  let=? '(previous_proto, ctxt) :=
    let= function_parameter := Context.get ctxt version_key in
    match function_parameter with
    | None =>
      Pervasives.failwith
        "Internal error: un-initialized context in check_first_block."
    | Some bytes_value =>
      let s := Bytes.to_string bytes_value in
      if Compare.String.(Compare.S.op_eq) s version_value then
        Pervasives.failwith "Internal error: previously initialized context."
      else
        if Compare.String.(Compare.S.op_eq) s "genesis" then
          let=? '(param, ctxt) := get_proto_param ctxt in
          return=? ((Genesis param), ctxt)
        else
          if Compare.String.(Compare.S.op_eq) s "edo_008" then
            Error_monad._return (Edo_008, ctxt)
          else
            Lwt._return (storage_error_value (Incompatible_protocol_version s))
    end in
  let= ctxt := Context.set ctxt version_key (Bytes.of_string version_value) in
  return=? (previous_proto, ctxt).

Definition prepare_first_block
  (level : int32) (timestamp : Time.t) (fitness : list bytes) (ctxt : Context.t)
  : M=? (previous_protocol * t) :=
  let=? '(previous_proto, ctxt) := check_and_update_protocol_version ctxt in
  let=? ctxt :=
    match previous_proto with
    | Genesis param =>
      let=? first_level := return= (Raw_level_repr.of_int32 level) in
      let=? ctxt := set_first_level ctxt first_level in
      Error_monad.op_gtpipeeq
        (set_constants ctxt param.(Parameters_repr.t.constants)) Error_monad.ok
    | Edo_008 => Error_monad._return ctxt
    end in
  let=? ctxt := prepare level timestamp timestamp fitness ctxt in
  return=? (previous_proto, ctxt).

Definition activate (ctxt : t) (h : Protocol_hash.t) : M= t :=
  Error_monad.op_gtpipeeq (Updater.activate (context_value ctxt) h)
    (update_context ctxt).

Definition fork_test_chain
  (ctxt : t) (protocol : Protocol_hash.t) (expiration : Time.t) : M= t :=
  Error_monad.op_gtpipeeq
    (Updater.fork_test_chain (context_value ctxt) protocol expiration)
    (update_context ctxt).

Definition key : Set := list string.

Definition value : Set := bytes.

Module T.
  Record signature {t : Set} : Set := {
    t := t;
    context := t;
    mem : context -> key -> M= bool;
    dir_mem : context -> key -> M= bool;
    get : context -> key -> M=? value;
    get_option : context -> key -> M= (option value);
    init_value : context -> key -> value -> M=? context;
    set : context -> key -> value -> M=? context;
    init_set : context -> key -> value -> M= context;
    set_option : context -> key -> option value -> M= context;
    delete : context -> key -> M=? context;
    remove : context -> key -> M= context;
    remove_rec : context -> key -> M= context;
    copy : context -> key -> key -> M=? context;
    fold :
      forall {a : Set},
      context -> key -> a -> (Context.key_or_dir -> a -> M= a) -> M= a;
    keys : context -> key -> M= (list key);
    fold_keys :
      forall {a : Set}, context -> key -> a -> (key -> a -> M= a) -> M= a;
    project : context -> root_context;
    absolute_key : context -> key -> key;
    consume_gas : context -> Gas_limit_repr.cost -> M? context;
    check_enough_gas : context -> Gas_limit_repr.cost -> M? unit;
    description : Storage_description.t context;
  }.
End T.
Definition T := @T.signature.
Arguments T {_}.

Definition mem (ctxt : t) (k : Context.key) : M= bool :=
  Context.mem (context_value ctxt) k.

Definition dir_mem (ctxt : t) (k : Context.key) : M= bool :=
  Context.dir_mem (context_value ctxt) k.

Definition get (ctxt : t) (k : Context.key) : M=? Context.value :=
  let= function_parameter := Context.get (context_value ctxt) k in
  match function_parameter with
  | None => return= (storage_error_value (Missing_key k Get))
  | Some v => return=? v
  end.

Definition get_option (ctxt : t) (k : Context.key)
  : M= (option Context.value) := Context.get (context_value ctxt) k.

Definition set (ctxt : t) (k : Context.key) (v : Context.value) : M=? t :=
  let= function_parameter := Context.mem (context_value ctxt) k in
  match function_parameter with
  | false => Lwt._return (storage_error_value (Missing_key k _Set))
  | true =>
    let= context_value := Context.set (context_value ctxt) k v in
    return=? (update_context ctxt context_value)
  end.

Definition init_value (ctxt : t) (k : Context.key) (v : Context.value)
  : M=? t :=
  let= function_parameter := Context.mem (context_value ctxt) k in
  match function_parameter with
  | true => Lwt._return (storage_error_value (Existing_key k))
  | false =>
    let= context_value := Context.set (context_value ctxt) k v in
    return=? (update_context ctxt context_value)
  end.

Definition init_set (ctxt : t) (k : Context.key) (v : Context.value) : M= t :=
  Error_monad.op_gtpipeeq (Context.set (context_value ctxt) k v)
    (update_context ctxt).

Definition delete (ctxt : t) (k : Context.key) : M=? t :=
  let= function_parameter := Context.mem (context_value ctxt) k in
  match function_parameter with
  | false => Lwt._return (storage_error_value (Missing_key k Del))
  | true =>
    let= context_value := Context.remove_rec (context_value ctxt) k in
    return=? (update_context ctxt context_value)
  end.

Definition remove (ctxt : t) (k : Context.key) : M= t :=
  Error_monad.op_gtpipeeq (Context.remove_rec (context_value ctxt) k)
    (update_context ctxt).

Definition set_option
  (ctxt : t) (k : Context.key) (function_parameter : option Context.value)
  : M= t :=
  match function_parameter with
  | None => remove ctxt k
  | Some v => init_set ctxt k v
  end.

Definition remove_rec (ctxt : t) (k : Context.key) : M= t :=
  Error_monad.op_gtpipeeq (Context.remove_rec (context_value ctxt) k)
    (update_context ctxt).

Definition copy (ctxt : t) (from : Context.key) (to_ : Context.key) : M=? t :=
  let= function_parameter := Context.copy (context_value ctxt) from to_ in
  match function_parameter with
  | None => return= (storage_error_value (Missing_key from Copy))
  | Some context_value => return=? (update_context ctxt context_value)
  end.

Definition fold {A : Set}
  (ctxt : t) (k : Context.key) (init_value : A)
  (f : Context.key_or_dir -> A -> M= A) : M= A :=
  Context.fold (context_value ctxt) k init_value f.

Definition keys (ctxt : t) (k : Context.key) : M= (list Context.key) :=
  Context.keys (context_value ctxt) k.

Definition fold_keys {A : Set}
  (ctxt : t) (k : Context.key) (init_value : A) (f : Context.key -> A -> M= A)
  : M= A := Context.fold_keys (context_value ctxt) k init_value f.

Definition project {A : Set} (x : A) : A := x.

Definition absolute_key {A B : Set} (function_parameter : A) : B -> B :=
  let '_ := function_parameter in
  fun (k : B) => k.

Definition description {A : Set} : Storage_description.t A :=
  Storage_description.create tt.

Definition fold_map_temporary_lazy_storage_ids {A : Set}
  (ctxt : t)
  (f : Lazy_storage_kind.Temp_ids.t -> Lazy_storage_kind.Temp_ids.t * A)
  : t * A :=
  (fun (function_parameter : Lazy_storage_kind.Temp_ids.t * A) =>
    let '(temporary_lazy_storage_ids, x) := function_parameter in
    ((update_temporary_lazy_storage_ids ctxt temporary_lazy_storage_ids), x))
    (f (temporary_lazy_storage_ids ctxt)).

Definition map_temporary_lazy_storage_ids_s
  (ctxt : t)
  (f : Lazy_storage_kind.Temp_ids.t -> M= (t * Lazy_storage_kind.Temp_ids.t))
  : M= t :=
  let= '(ctxt, temporary_lazy_storage_ids) :=
    f (temporary_lazy_storage_ids ctxt) in
  return= (update_temporary_lazy_storage_ids ctxt temporary_lazy_storage_ids).
