Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Tx_rollup_message_repr.

Require Import TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Blake2B.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Int64.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.S.
Require TezosOfOCaml.Proto_alpha.Proofs.Script_expr_hash.
Require TezosOfOCaml.Proto_alpha.Proofs.Tx_rollup_l2_address.
Require TezosOfOCaml.Proto_alpha.Proofs.Tx_rollup_l2_qty.

Module Deposit.
  Module Valid.
    Import Tx_rollup_message_repr.deposit.

    Record t (p : Tx_rollup_message_repr.deposit) : Prop := {
      destination : Indexable.Value.Valid.t p.(destination);
      amount : Tx_rollup_l2_qty.Valid.t p.(amount);
    }.
  End Valid.
End Deposit.

Module Valid.
  Definition t (x : Tx_rollup_message_repr.t) : Prop :=
    match x with
    | Tx_rollup_message_repr.Batch _ => True
    | Tx_rollup_message_repr.Deposit d => Deposit.Valid.t d
    end.
End Valid.

Lemma Message_hash_is_valid :
  S.HASH.Valid.t (fun _ => True) Tx_rollup_message_repr.Message_hash.
Proof.
  apply Blake2B.Make_is_valid.
Qed.

Lemma hash_encoding_is_valid : Data_encoding.Valid.t (fun _ => True) 
  Tx_rollup_message_repr.hash_encoding.
Proof.
  apply Message_hash_is_valid.
Qed.
#[global] Hint Resolve hash_encoding_is_valid : Data_encoding_db.

Lemma deposit_encoding_is_valid : Data_encoding.Valid.t Deposit.Valid.t
  Tx_rollup_message_repr.deposit_encoding.
Proof.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve deposit_encoding_is_valid : Data_encoding_db.

Lemma encoding_is_valid : Data_encoding.Valid.t Valid.t
  Tx_rollup_message_repr.encoding.
Proof.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.