Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require Import TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.

Axiom Block_header_Included_HASHABLE_HASHABLE_encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True)  Block_header.Included_HASHABLE.(HASHABLE.encoding).
#[global] Hint Resolve Block_header_Included_HASHABLE_HASHABLE_encoding_is_valid : Data_encoding_db.

Axiom Block_header_shell_header_encoding :
  Data_encoding.Valid.t (fun _ => True) Block_header.shell_header_encoding.
#[global] Hint Resolve Block_header_shell_header_encoding : Data_encoding_db.

Lemma block_header_encoding_is_valid : 
    Data_encoding.Valid.t (fun _ => True) Block_header.encoding.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve block_header_encoding_is_valid : Data_encoding_db.

