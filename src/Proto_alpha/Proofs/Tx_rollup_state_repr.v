Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Tx_rollup_state_repr.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.
Require TezosOfOCaml.Proto_alpha.Proofs.Raw_level_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Tez_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Tx_rollup_commitment_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Tx_rollup_level_repr.

Module Valid.
  Import Tx_rollup_state_repr.t.

  Record t (x : Tx_rollup_state_repr.t) : Prop := {
    commitment_tail_level :
      Option.Forall Tx_rollup_level_repr.Valid.t x.(commitment_tail_level);
    oldest_inbox_level :
      Option.Forall Tx_rollup_level_repr.Valid.t x.(oldest_inbox_level);
    commitment_head_level :
      Option.Forall (fun '(l, _) => Tx_rollup_level_repr.Valid.t l)
        x.(commitment_head_level);
    head_level :
      Option.Forall
        (fun '(l, r) =>
          Tx_rollup_level_repr.Valid.t l /\ Raw_level_repr.Valid.t r)
        x.(head_level);
    burn_per_byte : Tez_repr.Valid.t x.(burn_per_byte);
    inbox_ema : Pervasives.Int31.Valid.t x.(inbox_ema);
  }.
End Valid.

Lemma encoding_is_valid : Data_encoding.Valid.t Valid.t
  Tx_rollup_state_repr.encoding.
Proof.
  Data_encoding.Valid.data_encoding_auto.
  intros [] []; hauto l: on.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.
