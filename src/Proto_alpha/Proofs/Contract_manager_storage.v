Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Contract_manager_storage.

(* @TODO *)
Axiom reveal_implies_is_manager_key_revelead_to_be_true : forall {ctxt} {pkh} {pk},
  match Contract_manager_storage.reveal_manager_key
    ctxt pkh pk with
  | Pervasives.Ok ctxt' =>
    match Contract_manager_storage.is_manager_key_revealed 
      ctxt' pkh with
    | Pervasives.Ok true => True
    | _ => False
    end
  | Pervasives.Error _ => True
  end.

(* @TODO *)
Axiom reveal_implies_get_manager_key_success : forall {ctxt} {pkh} {pk},
  match Contract_manager_storage.reveal_manager_key
    ctxt pkh pk with
  | Pervasives.Ok ctxt' =>
    match Contract_manager_storage.get_manager_key
      None ctxt' pkh with
    | Pervasives.Ok _ => True
    | _ => False
    end
  | Pervasives.Error _ => True
  end.

(* @TODO *)
Axiom get_manager_key_implies_remove_exists_success : forall {ctxt} {pkh : public_key_hash},
  let contract := Contract_repr.Implicit pkh in
  letP? _ := Contract_manager_storage.get_manager_key None ctxt pkh in
  match Contract_manager_storage.remove_existing ctxt contract with
  | Pervasives.Ok _ => True
  | Pervasives.Error _  => False
  end.
