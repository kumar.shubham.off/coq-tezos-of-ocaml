Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Sc_rollup_inbox.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Blake2B.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.
Require TezosOfOCaml.Proto_alpha.Proofs.Raw_level_repr.

Module Inbox_hash.
  Lemma H_is_valid :
    S.HASH.Valid.t (fun _ => True) Sc_rollup_inbox.Inbox_hash.H.
    apply Blake2B.Make_is_valid.
  Qed.

  Lemma encoding_is_valid :
    Data_encoding.Valid.t (fun _ => True) Sc_rollup_inbox.Inbox_hash.encoding.
    apply H_is_valid.
  Qed.
  #[global] Hint Resolve encoding_is_valid : Data_encoding_db.
End Inbox_hash.

Module Valid.
  Import Sc_rollup_inbox.t.

  Record t (x : Sc_rollup_inbox.t) : Prop := {
    inbox_size : Int64.Valid.t x.(inbox_size);
  }.
End Valid.

Lemma hash_encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True) Sc_rollup_inbox.hash_encoding.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve hash_encoding_is_valid : Data_encoding_db.

Lemma encoding_is_valid :
  Data_encoding.Valid.t Valid.t Sc_rollup_inbox.encoding.
  Data_encoding.Valid.data_encoding_auto.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.

Module Message.
  Module Valid.
    Record t
      (x : Sc_rollup_inbox.Inbox_hash.t * string * Raw_level_repr.raw_level) :
      Prop := {
      level :
        let '(_, _, level) := x in
        Raw_level_repr.Valid.t level;
    }.
  End Valid.
End Message.

Lemma message_encoding_is_valid :
  Data_encoding.Valid.t Message.Valid.t Sc_rollup_inbox.message_encoding.
  Data_encoding.Valid.data_encoding_auto.
  sauto lq: on.
Qed.
#[global] Hint Resolve message_encoding_is_valid : Data_encoding_db.
