Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Script_typed_ir.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Chain_id.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Signature.

Module Ty_metadata.
  (** A default empty metadata we use to make a canonical form of a type. *)
  Definition default : Script_typed_ir.ty_metadata :=
    {|
      Script_typed_ir.ty_metadata.size := 0;
    |}.
End Ty_metadata.

(** ** Michelson AST with dependent types 🌲
    We define a dependently typed version of the Michelson AST. We
    attempt to be complete with respect to the OCaml implementation. We follow
    the namings of the OCaml definitions.

    We parametrize our types using type families, which are values describing a
    class of types.In the GADT OCaml version we paramterize instructions by
    types. The advantage of using values rather than types in Coq is that we can
    pattern-match on it.

    For the [kinstr] type we merge the head and stack types. This may simplify
    the code, especially if we encounter the case of [EmptyCell] values. Then we
    can use shorter lists instead of optional cells.

    For the [Set] of values of the types, as in Mi-Cho-Coq, we cannot define then
    as a function of [ty] as it would lead to too many mutual recursions.
    Instead, we use the type [ty_value] to represent the values using
    constructors.
*)
Module Family.
  Module Ty.
    Module Num.
      Inductive t : Set :=
      | Int : t
      | Nat : t.
    End Num.

    Inductive t : Set :=
    | Unit : t
    | Num : Num.t -> t
    | Signature : t
    | String : t
    | Bytes : t
    | Mutez : t
    | Key_hash : t
    | Key : t
    | Timestamp : t
    | Address : t
    | Tx_rollup_l2_address : t
    | Bool : t
    | Pair : t -> t -> t
    | Union : t -> t -> t
    | Lambda : t -> t -> t
    | Option : t -> t
    | List : t -> t
    | Set_ : t -> t
    | Map : t -> t -> t
    | Big_map : t -> t -> t
    | Contract : t -> t
    | Sapling_transaction : t
    | Sapling_state : t
    | Operation : t
    | Chain_id : t
    | Never : t
    | Bls12_381_g1 : t
    | Bls12_381_g2 : t
    | Bls12_381_fr : t
    | Ticket : t -> t
    | Chest_key : t
    | Chest : t.

    Fixpoint is_Comparable (ty : Ty.t) : Prop :=
      match ty with
      | Unit
      | Never
      | Num _
      | Signature
      | String
      | Bytes
      | Mutez
      | Bool
      | Key_hash
      | Key
      | Timestamp
      | Chain_id
      | Address
      | Tx_rollup_l2_address => True
      | Pair t1 t2 =>
             is_Comparable t1
          /\ is_Comparable t2
      | Union t1 t2 =>
             is_Comparable t1
          /\ is_Comparable t2
      | Option t' =>
          is_Comparable t'
      | _ => False
      end.

    Fixpoint to_Set (ty : Ty.t) : Set :=
      match ty with
      | Unit => unit
      | Num _ => Script_int_repr.num
      | Signature => Script_typed_ir.Script_signature.t
      | String => Script_string_repr.t
      | Bytes => Bytes.t
      | Mutez => Tez_repr.t
      | Key_hash => public_key_hash
      | Key => public_key
      | Timestamp => Script_timestamp_repr.t
      | Address => Script_typed_ir.address
      | Tx_rollup_l2_address => Script_typed_ir.tx_rollup_l2_address
      | Bool => bool
      | Pair ty1 ty2 => to_Set ty1 * to_Set ty2
      | Union ty1 ty2 => Script_typed_ir.union (to_Set ty1) (to_Set ty2)
      | Lambda _ _ =>
        (* We loose information there as we do not have the argument or return
           type constraints anymore on the lambda. *)
        Script_typed_ir.lambda
      | Option ty => option (to_Set ty)
      | List ty => Script_typed_ir.boxed_list (to_Set ty)
      | Set_ k => Script_typed_ir.set (to_Set k)
      | Map k v => Script_typed_ir.map (to_Set k) (to_Set v)
      | Big_map k v => Script_typed_ir.big_map (to_Set k) (to_Set v)
      | Contract _ =>
        (* Same here, we loose information about the type of the contract. *)
        Script_typed_ir.typed_contract
      | Sapling_transaction => Alpha_context.Sapling.transaction
      | Sapling_state => Alpha_context.Sapling.state
      | Operation => Script_typed_ir.operation
      | Chain_id => Script_typed_ir.Script_chain_id.t
      | Never => Script_typed_ir.never
      | Bls12_381_g1 => Script_typed_ir.Script_bls.G1.t
      | Bls12_381_g2 => Script_typed_ir.Script_bls.G2.t
      | Bls12_381_fr => Script_typed_ir.Script_bls.Fr.t
      | Ticket ty => Script_typed_ir.ticket (to_Set ty)
      | Chest_key => Script_typed_ir.Script_timelock.chest_key
      | Chest => Script_typed_ir.Script_timelock.chest
      end.
  End Ty.

  Module Stack_ty.
    Definition t : Set := list Ty.t.

    Fixpoint to_Set (tys : t) : Set :=
        match tys with
        | [] => unit
        | ty :: tys => Ty.to_Set ty * to_Set tys
        end.
  End Stack_ty.
End Family.

Module With_family.
  Import Family.

  Inductive comparable_ty : Ty.t -> Set :=
  | Unit_key :
    Script_typed_ir.ty_metadata ->
    comparable_ty Ty.Unit
  | Never_key :
    Script_typed_ir.ty_metadata ->
    comparable_ty Ty.Never
  | Int_key :
    Script_typed_ir.ty_metadata ->
    comparable_ty (Ty.Num Ty.Num.Int)
  | Nat_key :
    Script_typed_ir.ty_metadata ->
    comparable_ty (Ty.Num Ty.Num.Nat)
  | Signature_key :
    Script_typed_ir.ty_metadata ->
    comparable_ty Ty.Signature
  | String_key :
    Script_typed_ir.ty_metadata ->
    comparable_ty Ty.String
  | Bytes_key :
    Script_typed_ir.ty_metadata ->
    comparable_ty Ty.Bytes
  | Mutez_key :
    Script_typed_ir.ty_metadata ->
    comparable_ty Ty.Mutez
  | Bool_key :
    Script_typed_ir.ty_metadata ->
    comparable_ty Ty.Bool
  | Key_hash_key :
    Script_typed_ir.ty_metadata ->
    comparable_ty Ty.Key_hash
  | Key_key :
    Script_typed_ir.ty_metadata ->
    comparable_ty Ty.Key
  | Timestamp_key :
    Script_typed_ir.ty_metadata ->
    comparable_ty Ty.Timestamp
  | Chain_id_key :
    Script_typed_ir.ty_metadata ->
    comparable_ty Ty.Chain_id
  | Address_key :
    Script_typed_ir.ty_metadata ->
    comparable_ty Ty.Address
  | Tx_rollup_l2_address_key :
    comparable_ty Family.Ty.Tx_rollup_l2_address
  | Pair_key {ty1 ty2} :
    comparable_ty ty1 ->
    comparable_ty ty2 ->
    Script_typed_ir.ty_metadata ->
    comparable_ty (Ty.Pair ty1 ty2)
  | Union_key {ty1 ty2} :
    comparable_ty ty1 ->
    comparable_ty ty2 ->
    Script_typed_ir.ty_metadata ->
    comparable_ty (Ty.Union ty1 ty2)
  | Option_key {ty} :
    comparable_ty ty ->
    Script_typed_ir.ty_metadata ->
    comparable_ty (Ty.Option ty).

  Fixpoint to_comparable_ty {t : Family.Ty.t}
    (v : With_family.comparable_ty t) : Script_typed_ir.comparable_ty :=
    match v with
    | With_family.Unit_key md => Script_typed_ir.Unit_key
    | With_family.Never_key md => Script_typed_ir.Never_key
    | With_family.Int_key md => Script_typed_ir.Int_key
    | With_family.Nat_key md => Script_typed_ir.Nat_key
    | With_family.Signature_key md => Script_typed_ir.Signature_key
    | With_family.String_key md => Script_typed_ir.String_key
    | With_family.Bytes_key md => Script_typed_ir.Bytes_key
    | With_family.Mutez_key md => Script_typed_ir.Mutez_key
    | With_family.Bool_key md => Script_typed_ir.Bool_key
    | With_family.Key_hash_key md => Script_typed_ir.Key_hash_key
    | With_family.Key_key md => Script_typed_ir.Key_key
    | With_family.Timestamp_key md => Script_typed_ir.Timestamp_key
    | With_family.Chain_id_key md => Script_typed_ir.Chain_id_key
    | With_family.Address_key md => Script_typed_ir.Address_key
    | With_family.Tx_rollup_l2_address_key =>
      Script_typed_ir.Tx_rollup_l2_address_key
    | With_family.Pair_key t1 t2 md =>
      Script_typed_ir.Pair_key (to_comparable_ty t1) (to_comparable_ty t2) md
    | With_family.Union_key t1 t2 md =>
      Script_typed_ir.Union_key (to_comparable_ty t1) (to_comparable_ty t2) md
    | With_family.Option_key t md =>
      Script_typed_ir.Option_key (to_comparable_ty t) md
    end.

  Inductive comb_gadt_witness : Stack_ty.t -> Stack_ty.t -> Set :=
  | Comb_one : forall {s}, comb_gadt_witness s s
  | Comb_succ : forall {a b s s'}, comb_gadt_witness s (b :: s') ->
      comb_gadt_witness (a :: s) (Ty.Pair a b :: s').

  Fixpoint to_comb_gadt_witness {s f} (w : comb_gadt_witness s f)
    : Script_typed_ir.comb_gadt_witness :=
    match w with
    | Comb_one => Script_typed_ir.Comb_one
    | Comb_succ w' => Script_typed_ir.Comb_succ (to_comb_gadt_witness w')
    end.

  Inductive uncomb_gadt_witness : Stack_ty.t -> Stack_ty.t -> Set :=
  | Uncomb_one : forall {s}, uncomb_gadt_witness s s
  | Uncomb_succ : forall {a b s s'}, uncomb_gadt_witness (b :: s) s' ->
      uncomb_gadt_witness (Ty.Pair a b :: s) (a :: s').

  Fixpoint to_uncomb_gadt_witness {s f} (w : uncomb_gadt_witness s f)
    : Script_typed_ir.uncomb_gadt_witness :=
    match w with
    | Uncomb_one => Script_typed_ir.Uncomb_one
    | Uncomb_succ w' => Script_typed_ir.Uncomb_succ (to_uncomb_gadt_witness w')
    end.

  Inductive comb_get_gadt_witness : Ty.t -> Ty.t -> Set :=
  | Comb_get_zero : forall {b}, comb_get_gadt_witness b b
  | Comb_get_one : forall {a b}, comb_get_gadt_witness (Ty.Pair a b) a
  | Comb_get_plus_two : forall {a b c},
      comb_get_gadt_witness b c ->
      comb_get_gadt_witness (Ty.Pair a b) c.

  Fixpoint to_comb_get_gadt_witness {a b} (w : comb_get_gadt_witness a b)
    : Script_typed_ir.comb_get_gadt_witness :=
    match w with
    | Comb_get_zero => Script_typed_ir.Comb_get_zero
    | Comb_get_one => Script_typed_ir.Comb_get_one
    | Comb_get_plus_two w' => Script_typed_ir.Comb_get_plus_two
      (to_comb_get_gadt_witness w')
    end.

  Inductive comb_set_gadt_witness : Ty.t -> Ty.t -> Ty.t -> Set :=
  | Comb_set_zero : forall {v a}, comb_set_gadt_witness v a v
  | Comb_set_one : forall {v a b},
      comb_set_gadt_witness v (Ty.Pair a b) (Ty.Pair v b)
  | Comb_set_plus_two : forall {v a b c},
      comb_set_gadt_witness v b c ->
      comb_set_gadt_witness v (Ty.Pair a b) (Ty.Pair a c).

  Fixpoint to_comb_set_gadt_witness {a b c} (w : comb_set_gadt_witness a b c)
    : Script_typed_ir.comb_set_gadt_witness :=
    match w with
    | Comb_set_zero => Script_typed_ir.Comb_set_zero
    | Comb_set_one => Script_typed_ir.Comb_set_one
    | Comb_set_plus_two w' => Script_typed_ir.Comb_set_plus_two
      (to_comb_set_gadt_witness w')
    end.

  Inductive dup_n_gadt_witness : Stack_ty.t -> Ty.t -> Set :=
  | Dup_n_zero : forall {a s}, dup_n_gadt_witness (a :: s) a
  | Dup_n_succ : forall {a b s}, dup_n_gadt_witness s b ->
      dup_n_gadt_witness (a :: s) b.

  Fixpoint to_dup_n_gadt_witness {b s} (w : dup_n_gadt_witness s b)
    : Script_typed_ir.dup_n_gadt_witness :=
    match w with
    | Dup_n_zero => Script_typed_ir.Dup_n_zero
    | Dup_n_succ w' => Script_typed_ir.Dup_n_succ (to_dup_n_gadt_witness w')
    end.

  Inductive stack_prefix_preservation_witness :
    Stack_ty.t -> Stack_ty.t -> Stack_ty.t -> Stack_ty.t -> Set :=
  | KRest : forall {s t}, stack_prefix_preservation_witness s t s t
  | KPrefix : forall {a s t u v},
      stack_prefix_preservation_witness s t u v ->
      stack_prefix_preservation_witness s t (a :: u) (a :: v).

  Fixpoint to_stack_prefix_preservation_witness {s t u v}
    (w : stack_prefix_preservation_witness s t u v) :
    Script_typed_ir.stack_prefix_preservation_witness :=
    match w with
    | KRest => Script_typed_ir.KRest
    | KPrefix w' => Script_typed_ir.KPrefix
      axiom (*TODO*)
      (to_stack_prefix_preservation_witness w')
    end.

  Inductive ty : Ty.t -> Set :=
  | Unit_t : Script_typed_ir.ty_metadata -> ty Ty.Unit
  | Int_t : Script_typed_ir.ty_metadata -> ty (Ty.Num Ty.Num.Int)
  | Nat_t : Script_typed_ir.ty_metadata -> ty (Ty.Num Ty.Num.Nat)
  | Signature_t : Script_typed_ir.ty_metadata -> ty Ty.Signature
  | String_t : Script_typed_ir.ty_metadata -> ty Ty.String
  | Bytes_t : Script_typed_ir.ty_metadata -> ty Ty.Bytes
  | Mutez_t : Script_typed_ir.ty_metadata -> ty Ty.Mutez
  | Key_hash_t : Script_typed_ir.ty_metadata -> ty Ty.Key_hash
  | Key_t : Script_typed_ir.ty_metadata -> ty Ty.Key
  | Timestamp_t : Script_typed_ir.ty_metadata -> ty Ty.Timestamp
  | Address_t : Script_typed_ir.ty_metadata -> ty Ty.Address
  | Tx_rollup_l2_address_t : ty Family.Ty.Tx_rollup_l2_address
  | Bool_t : Script_typed_ir.ty_metadata -> ty Ty.Bool
  | Pair_t {t1 t2} :
    ty t1 ->
    ty t2 ->
    Script_typed_ir.ty_metadata ->
    ty (Ty.Pair t1 t2)
  | Union_t {t1 t2} :
    ty t1 ->
    ty t2 ->
    Script_typed_ir.ty_metadata ->
    ty (Ty.Union t1 t2)
  | Lambda_t {arg ret} :
    ty arg ->
    ty ret ->
    Script_typed_ir.ty_metadata ->
    ty (Ty.Lambda arg ret)
  | Option_t {t} :
    ty t ->
    Script_typed_ir.ty_metadata ->
    ty (Ty.Option t)
  | List_t {t} :
    ty t ->
    Script_typed_ir.ty_metadata ->
    ty (Ty.List t)
  | Set_t {t} :
    comparable_ty t ->
    Script_typed_ir.ty_metadata ->
    ty (Ty.Set_ t)
  | Map_t {key value} :
    comparable_ty key ->
    ty value ->
    Script_typed_ir.ty_metadata ->
    ty (Ty.Map key value)
  | Big_map_t {key value} :
    comparable_ty key ->
    ty value ->
    Script_typed_ir.ty_metadata ->
    ty (Ty.Big_map key value)
  | Contract_t {arg} :
    ty arg ->
    Script_typed_ir.ty_metadata ->
    ty (Ty.Contract arg)
  | Sapling_transaction_t :
    Sapling_repr.Memo_size.t ->
    Script_typed_ir.ty_metadata ->
    ty Ty.Sapling_transaction
  | Sapling_state_t :
    Sapling_repr.Memo_size.t ->
    Script_typed_ir.ty_metadata ->
    ty Ty.Sapling_state
  | Operation_t : Script_typed_ir.ty_metadata -> ty Ty.Operation
  | Chain_id_t : Script_typed_ir.ty_metadata -> ty Ty.Chain_id
  | Never_t : Script_typed_ir.ty_metadata -> ty Ty.Never
  | Bls12_381_g1_t : Script_typed_ir.ty_metadata -> ty Ty.Bls12_381_g1
  | Bls12_381_g2_t : Script_typed_ir.ty_metadata -> ty Ty.Bls12_381_g2
  | Bls12_381_fr_t : Script_typed_ir.ty_metadata -> ty Ty.Bls12_381_fr
  | Ticket_t {a} :
    comparable_ty a ->
    Script_typed_ir.ty_metadata ->
    ty (Ty.Ticket a)
  | Chest_key_t : Script_typed_ir.ty_metadata -> ty Ty.Chest_key
  | Chest_t : Script_typed_ir.ty_metadata -> ty Ty.Chest.

  Fixpoint to_ty {a} (t : ty a) : Script_typed_ir.ty :=
    match t with
    | Unit_t m => Script_typed_ir.Unit_t
    | Int_t m => Script_typed_ir.Int_t
    | Nat_t m => Script_typed_ir.Nat_t
    | Signature_t m => Script_typed_ir.Signature_t
    | String_t m => Script_typed_ir.String_t
    | Bytes_t m => Script_typed_ir.Bytes_t
    | Mutez_t m => Script_typed_ir.Mutez_t
    | Key_hash_t m => Script_typed_ir.Key_hash_t
    | Key_t m => Script_typed_ir.Key_t
    | Timestamp_t m => Script_typed_ir.Timestamp_t
    | Address_t m => Script_typed_ir.Address_t
    | Tx_rollup_l2_address_t => Script_typed_ir.Tx_rollup_l2_address_t
    | Bool_t m => Script_typed_ir.Bool_t
    | Pair_t t1 t2 m =>
      Script_typed_ir.Pair_t (to_ty t1) (to_ty t2) m
    | Union_t t1 t2 m =>
      Script_typed_ir.Union_t (to_ty t1) (to_ty t2) m
    | Lambda_t arg ret m => Script_typed_ir.Lambda_t (to_ty arg) (to_ty ret) m
    | Option_t t m => Script_typed_ir.Option_t (to_ty t) m
    | List_t t m => Script_typed_ir.List_t (to_ty t) m
    | Set_t t m => Script_typed_ir.Set_t (to_comparable_ty t) m
    | Map_t k v m => Script_typed_ir.Map_t (to_comparable_ty k) (to_ty v) m
    | Big_map_t key value m =>
      Script_typed_ir.Big_map_t (to_comparable_ty key) (to_ty value) m
    | Contract_t t m => Script_typed_ir.Contract_t (to_ty t) m
    | Sapling_transaction_t x m => Script_typed_ir.Sapling_transaction_t x
    | Sapling_state_t x m => Script_typed_ir.Sapling_state_t x
    | Operation_t m => Script_typed_ir.Operation_t
    | Chain_id_t m => Script_typed_ir.Chain_id_t
    | Never_t m => Script_typed_ir.Never_t
    | Bls12_381_g1_t m => Script_typed_ir.Bls12_381_g1_t
    | Bls12_381_g2_t m => Script_typed_ir.Bls12_381_g2_t
    | Bls12_381_fr_t m => Script_typed_ir.Bls12_381_fr_t
    | Ticket_t ty m => Script_typed_ir.Ticket_t (to_comparable_ty ty) m
    | Chest_key_t m => Script_typed_ir.Chest_key_t
    | Chest_t m => Script_typed_ir.Chest_t
    end.

  Inductive stack_ty : Stack_ty.t -> Set :=
  | Item_t {ty_ rest} :
    ty ty_ ->
    stack_ty rest ->
    stack_ty (ty_ :: rest)
  | Bot_t :
    stack_ty [].

  Fixpoint to_stack_ty {s} (ts : stack_ty s) : Script_typed_ir.stack_ty :=
    match ts with
    | Item_t t rest =>
      Script_typed_ir.Item_t (to_ty t) (to_stack_ty rest)
    | Bot_t => Script_typed_ir.Bot_t
    end.

  Inductive kinfo : Stack_ty.t -> Set :=
  | Kinfo {s} : Alpha_context.Script.location -> stack_ty s -> kinfo s.

  Definition to_kinfo {s} (info : kinfo s) : Script_typed_ir.kinfo :=
    let 'Kinfo iloc kstack_ty := info in
    {|
      Script_typed_ir.kinfo.iloc := iloc;
      Script_typed_ir.kinfo.kstack_ty := to_stack_ty kstack_ty;
    |}.

  Inductive view_signature : Ty.t -> Ty.t -> Set :=
  | View_signature {a b} :
    (* name *)
    Script_string_repr.t ->
    (* input_ty *)
    ty a ->
    (* output_ty *)
    ty b ->
    view_signature a b.

  Definition to_view_signature {a b} (sign : view_signature a b) :
    Script_typed_ir.view_signature :=
    let 'View_signature name i_ty o_ty := sign in
    Script_typed_ir.View_signature
    {|
      Script_typed_ir.view_signature.View_signature.name := name;
      Script_typed_ir.view_signature.View_signature.input_ty := (to_ty i_ty);
      Script_typed_ir.view_signature.View_signature.output_ty := (to_ty o_ty);
    |}.

  Inductive kinstr : Stack_ty.t -> Stack_ty.t -> Set :=
  (*
    Stack
    -----
  *)
  | IDrop {a s f} :
    kinfo (a :: s) ->
    kinstr s f ->
    kinstr (a :: s) f
  | IDup {a s f} :
    kinfo (a :: s) ->
    kinstr (a :: a :: s) f  ->
    kinstr (a :: s) f
  | ISwap {a b s f} :
    kinfo (a :: b :: s) ->
    kinstr (b :: a :: s) f ->
    kinstr (a :: b :: s) f
  | IConst {s ty f} :
    kinfo s ->
    Ty.to_Set ty ->
    kinstr (ty :: s) f ->
    kinstr s f
  (*
    Pairs
    -----
  *)
  | ICons_pair {a b s f} :
    kinfo (a :: b :: s) ->
    kinstr (Ty.Pair a b :: s) f ->
    kinstr (a :: b :: s) f
  | ICar {a b s f} :
    kinfo (a :: b :: s) ->
    kinstr (a :: s) f ->
    kinstr (Ty.Pair a b :: s) f
  | ICdr {a b s f} :
    kinfo (Ty.Pair a b :: s) ->
    kinstr (b :: s) f ->
    kinstr (Ty.Pair a b :: s) f
  | IUnpair {a b s f} :
    kinfo (Ty.Pair a b :: s) ->
    kinstr (a :: b :: s) f ->
    kinstr (Ty.Pair a b :: s) f
  (*
    Options
    -------
  *)
  | ICons_some {v s f} :
    kinfo (v :: s) ->
    kinstr (Ty.Option v :: s) f ->
    kinstr (v :: s) f
  | ICons_none {s b f} :
    kinfo s ->
    kinstr (Ty.Option b :: s) f ->
    kinstr s f
  | IIf_none {a b s t f} :
    kinfo (Ty.Option a :: b :: s) ->
    kinstr s t ->
    kinstr (a :: s) t ->
    kinstr t f ->
    kinstr (Ty.Option a :: s) f
  | IOpt_map {a b s t} :
    kinfo (Ty.Option a :: s) ->
    kinstr (a :: s) (b :: s) ->
    kinstr (Ty.Option b :: s) t ->
    kinstr (Ty.Option a :: s) t
  (*
    Unions
    ------
  *)
  | ICons_left {a b s f} :
    kinfo (a :: s) ->
    kinstr (Ty.Union a b :: s) f ->
    kinstr (a :: s) f
  | ICons_right {a b s f} :
    kinfo (b :: s) ->
    kinstr (Ty.Union a b :: s) f ->
    kinstr (b :: s) f
  | IIf_left {a b s t f} :
    kinfo (Ty.Union a b :: s) ->
    kinstr (a :: s) t ->
    kinstr (b :: s) t ->
    kinstr t f ->
    kinstr (Ty.Union a b :: s) f
  (*
    Lists
    -----
  *)
  | ICons_list {a s f} :
    kinfo (a :: Ty.List a :: s) ->
    kinstr (Ty.List a :: s) f ->
    kinstr (a :: Ty.List a :: s) f
  | INil {b s f} :
    kinfo s ->
    kinstr (Ty.List b :: s) f ->
    kinstr s f
  | IIf_cons {a s f t} :
    kinfo (Ty.List a :: s) ->
    kinstr (a :: Ty.List a :: s) t ->
    kinstr s t ->
    kinstr t f ->
    kinstr (Ty.List a :: s) f
  | IList_map {a b s f} :
    kinfo (Ty.List a :: s) ->
    kinstr (a :: s) (b :: s) ->
    kinstr (Ty.List b :: s) f ->
    kinstr (Ty.List a :: s) f
  | IList_iter {a s f} :
    kinfo (Ty.List a :: s) ->
    kinstr (a :: s) s ->
    kinstr s f ->
    kinstr (Ty.List a :: s) f
  | IList_size {a s f} :
    kinfo (Ty.List a :: s) ->
    kinstr (Ty.Num Ty.Num.Nat :: s) f ->
    kinstr (Ty.List a :: s) f
  (*
    Sets
    ----
  *)
  | IEmpty_set {b s f} :
    kinfo s ->
    comparable_ty b ->
    kinstr (Ty.Set_ b :: s) f ->
    kinstr s f
  | ISet_iter {a s f} :
    kinfo (Ty.Set_ a :: s) ->
    kinstr (a :: s) s ->
    kinstr s f ->
    kinstr (Ty.Set_ a :: s) f
  | ISet_mem {a s f} :
    kinfo (a :: Ty.Set_ a :: s) ->
    kinstr (Ty.Bool :: s) f ->
    kinstr (a :: Ty.Set_ a :: s) f
  | ISet_update {a s f} :
    kinfo (a :: Ty.Bool :: Ty.Set_ a :: s) ->
    kinstr (Ty.Set_ a :: s) f ->
    kinstr (a :: Ty.Bool :: Ty.Set_ a :: s) f
  | ISet_size {a s f} :
    kinfo (Ty.Set_ a :: s) ->
    kinstr (Ty.Num Ty.Num.Nat :: s) f ->
    kinstr (Ty.Set_ a :: s) f
  (*
     Maps
     ----
   *)
  | IEmpty_map {b c s f} :
    kinfo s ->
    comparable_ty b ->
    kinstr (Ty.Map b c :: s) f ->
    kinstr s f
  | IMap_map {a b c s f} :
    kinfo (Ty.Map a b :: s) ->
    kinstr (a :: b :: s) (c :: s) ->
    kinstr (Ty.Map a c :: s) f ->
    kinstr (Ty.Map a b :: s) f
  | IMap_iter {a b s f} :
    kinfo (Ty.Map a b :: s) ->
    kinstr (a :: b :: s) s ->
    kinstr s f ->
    kinstr (Ty.Map a b :: s) f
  | IMap_mem {a b s f} :
    kinfo (a :: Ty.Map a b :: s) ->
    kinstr (Ty.Bool :: s) f ->
    kinstr (a :: Ty.Map a b :: s) f
  | IMap_get {a b s f} :
    kinfo (a :: Ty.Map a b :: s) ->
    kinstr (Ty.Option b :: s) f ->
    kinstr (a :: Ty.Map a b :: s) f
  | IMap_update {a b s f} :
    kinfo (a :: Ty.Option b :: Ty.Map a b :: s) ->
    kinstr (Ty.Map a b :: s) f ->
    kinstr (a :: Ty.Option b :: Ty.Map a b :: s) f
  | IMap_get_and_update {a b s f} :
    kinfo (a :: Ty.Option b :: Ty.Map a b :: s) ->
    kinstr (Ty.Option b :: Ty.Map a b :: s) f ->
    kinstr (a :: Ty.Option b :: Ty.Map a b :: s) f
  | IMap_size {a b s f} :
    kinfo (Ty.Map a b :: s) ->
    kinstr (Ty.Num Ty.Num.Nat :: s) f ->
    kinstr (Ty.Map a b :: s) f
  (*
     Big maps
     --------
  *)
  | IEmpty_big_map {b c s f} :
    kinfo s ->
    comparable_ty b ->
    ty c ->
    kinstr (Ty.Big_map b c :: s) f ->
    kinstr s f
  | IBig_map_mem {a b s f} :
    kinfo (a :: Ty.Big_map a b :: s) ->
    kinstr (Ty.Bool :: s) f ->
    kinstr (a :: (Ty.Big_map a b) :: s) f
  | IBig_map_get {a b s f} :
    kinfo (a :: Ty.Big_map a b :: s) ->
    kinstr (Ty.Option b :: s) f ->
    kinstr (a :: Ty.Big_map a b :: s) f
  | IBig_map_update {a b s f} :
    kinfo (a :: Ty.Option b :: Ty.Big_map a b :: s) ->
    kinstr (Ty.Big_map a b :: s) f ->
    kinstr (a :: Ty.Option b :: Ty.Big_map a b :: s) f
  | IBig_map_get_and_update {a b s f} :
    kinfo (a :: Ty.Option b :: Ty.Big_map a b :: s) ->
    kinstr (Ty.Option b :: Ty.Big_map a b :: s) f ->
    kinstr (a :: Ty.Option b :: Ty.Big_map a b :: s) f
  (*
     Strings
     -------
  *)
  | IConcat_string {s f} :
    kinfo (Ty.List Ty.String :: s) ->
    kinstr (Ty.String :: s) f ->
    kinstr (Ty.List Ty.String :: s) f
  | IConcat_string_pair {s f} :
    kinfo (Ty.String :: Ty.String :: s) ->
    kinstr (Ty.String :: s) f ->
    kinstr (Ty.String :: Ty.String :: s) f
  | ISlice_string {s f} :
    kinfo (Ty.Num Ty.Num.Nat :: Ty.Num Ty.Num.Nat :: Ty.String :: s) ->
    kinstr (Ty.Option Ty.String :: s) f ->
    kinstr (Ty.Num Ty.Num.Nat :: Ty.Num Ty.Num.Nat :: Ty.String :: s) f
  | IString_size {s f} :
    kinfo (Ty.String :: s) ->
    kinstr (Ty.Num Ty.Num.Nat :: s) f ->
    kinstr (Ty.String :: s) f
  (*
     Bytes
     -----
  *)
  | IConcat_bytes {s f} :
    kinfo (Ty.List Ty.Bytes :: s) ->
    kinstr (Ty.Bytes :: s) f ->
    kinstr (Ty.List Ty.Bytes :: s) f
  | IConcat_bytes_pair {s f} :
    kinfo (Ty.Bytes :: Ty.Bytes :: s) ->
    kinstr (Ty.Bytes :: s) f ->
    kinstr (Ty.Bytes :: Ty.Bytes :: s) f
  | ISlice_bytes {s f} :
    kinfo (Ty.Num Ty.Num.Nat :: Ty.Num Ty.Num.Nat :: Ty.Bytes :: s) ->
    kinstr (Ty.Option Ty.Bytes :: s) f ->
    kinstr (Ty.Num Ty.Num.Nat :: Ty.Num Ty.Num.Nat :: Ty.Bytes :: s) f
  | IBytes_size {s f} :
    kinfo (Ty.Bytes :: s) ->
    kinstr (Ty.Num Ty.Num.Nat :: s) f ->
    kinstr (Ty.Bytes :: s) f
  (*
     Timestamps
     ----------
  *)
  | IAdd_seconds_to_timestamp {s f} :
    kinfo (Ty.Num Ty.Num.Int :: Ty.Timestamp :: s) ->
    kinstr (Ty.Timestamp :: s) f ->
    kinstr (Ty.Num Ty.Num.Int :: Ty.Timestamp :: s) f
  | IAdd_timestamp_to_seconds {s f} :
    kinfo (Ty.Timestamp :: Ty.Num Ty.Num.Int :: s) ->
    kinstr (Ty.Timestamp :: s) f ->
    kinstr (Ty.Timestamp :: Ty.Num Ty.Num.Int :: s) f
  | ISub_timestamp_seconds {s f} :
    kinfo (Ty.Timestamp :: Ty.Num Ty.Num.Int :: s) ->
    kinstr (Ty.Timestamp :: s) f ->
    kinstr (Ty.Timestamp :: Ty.Num Ty.Num.Int :: s) f
  | IDiff_timestamps {s f} :
    kinfo (Ty.Timestamp :: Ty.Timestamp :: s) ->
    kinstr (Ty.Num Ty.Num.Int :: s) f ->
    kinstr (Ty.Timestamp :: Ty.Timestamp :: s) f
  (*
     Tez
     ---
  *)
  | IAdd_tez {s f} :
    kinfo (Ty.Mutez :: Ty.Mutez :: s) ->
    kinstr (Ty.Mutez :: s) f ->
    kinstr (Ty.Mutez :: Ty.Mutez :: s) f
  | ISub_tez {s f} :
    kinfo (Ty.Mutez :: Ty.Mutez :: s) ->
    kinstr (Ty.Option Ty.Mutez :: s) f ->
    kinstr (Ty.Mutez :: Ty.Mutez :: s) f
  | ISub_tez_legacy {s f} :
    kinfo (Ty.Mutez :: Ty.Mutez :: s) ->
    kinstr (Ty.Mutez :: s) f ->
    kinstr (Ty.Mutez :: Ty.Mutez :: s) f
  | IMul_teznat {s f} :
    kinfo (Ty.Mutez :: Ty.Num Ty.Num.Nat :: s) ->
    kinstr (Ty.Mutez :: s) f ->
    kinstr (Ty.Mutez :: Ty.Num Ty.Num.Nat :: s) f
  | IMul_nattez {s f} :
    kinfo (Ty.Num Ty.Num.Nat :: Ty.Mutez :: s) ->
    kinstr (Ty.Mutez :: s) f ->
    kinstr (Ty.Num Ty.Num.Nat :: Ty.Mutez :: s) f
  | IEdiv_teznat {s f} :
    kinfo (Ty.Mutez :: Ty.Num Ty.Num.Nat :: s) ->
    kinstr (Ty.Option (Ty.Pair Ty.Mutez Ty.Mutez) :: s) f ->
    kinstr (Ty.Mutez :: Ty.Num Ty.Num.Nat :: s) f
  | IEdiv_tez {s f} :
    kinfo (Ty.Mutez :: Ty.Mutez :: s) ->
    kinstr (Ty.Option (Ty.Pair (Ty.Num Ty.Num.Nat) Ty.Mutez) :: s) f ->
    kinstr (Ty.Mutez :: Ty.Mutez :: s) f
  (*
    Booleans
    --------
  *)
  | IOr {s f} :
    kinfo (Ty.Bool :: Ty.Bool :: s) ->
    kinstr (Ty.Bool :: s) f ->
    kinstr (Ty.Bool :: Ty.Bool :: s) f
  | IAnd {s f} :
    kinfo (Ty.Bool :: Ty.Bool :: s) ->
    kinstr (Ty.Bool :: s) f ->
    kinstr (Ty.Bool :: Ty.Bool :: s) f
  | IXor {s f} :
    kinfo (Ty.Bool :: Ty.Bool :: s) ->
    kinstr (Ty.Bool :: s) f ->
    kinstr (Ty.Bool :: Ty.Bool :: s) f
  | INot {s f} :
    kinfo (Ty.Bool :: s) ->
    kinstr (Ty.Bool :: s) f ->
    kinstr (Ty.Bool :: s) f
  (*
     Integers
     --------
  *)
  | IIs_nat {s f} :
    kinfo (Ty.Num Ty.Num.Int :: s) ->
    kinstr (Ty.Option (Ty.Num Ty.Num.Nat) :: s) f ->
    kinstr (Ty.Num Ty.Num.Int :: s) f
  | INeg {a s f} :
    kinfo (Ty.Num a :: s) ->
    kinstr (Ty.Num Ty.Num.Int :: s) f ->
    kinstr (Ty.Num a :: s) f
  | IAbs_int {s f} :
    kinfo (Ty.Num Ty.Num.Int :: s) ->
    kinstr (Ty.Num Ty.Num.Nat :: s) f ->
    kinstr (Ty.Num Ty.Num.Int :: s) f
  | IInt_nat {s f} :
    kinfo (Ty.Num Ty.Num.Nat :: s) ->
    kinstr (Ty.Num Ty.Num.Int :: s) f ->
    kinstr (Ty.Num Ty.Num.Nat :: s) f
  | IAdd_int {a b s f} :
    kinfo (Ty.Num a :: Ty.Num b :: s) ->
    kinstr (Ty.Num Ty.Num.Int :: s) f ->
    kinstr (Ty.Num a :: Ty.Num b :: s) f
  | IAdd_nat {s f} :
    kinfo (Ty.Num Ty.Num.Nat :: Ty.Num Ty.Num.Nat :: s) ->
    kinstr (Ty.Num Ty.Num.Nat :: s) f ->
    kinstr (Ty.Num Ty.Num.Nat :: Ty.Num Ty.Num.Nat :: s) f
  | ISub_int {a b s f} :
    kinfo (Ty.Num a :: Ty.Num b:: s) ->
    kinstr (Ty.Num Ty.Num.Int :: s) f ->
    kinstr (Ty.Num a :: Ty.Num b :: s) f
  | IMul_int {a b s f} :
    kinfo (Ty.Num a :: Ty.Num b :: s) ->
    kinstr (Ty.Num Ty.Num.Int :: s) f ->
    kinstr (Ty.Num a :: Ty.Num b :: s) f
  | IMul_nat {a s f} :
    kinfo (Ty.Num Ty.Num.Nat :: Ty.Num a :: s) ->
    kinstr (Ty.Num a :: s) f ->
    kinstr (Ty.Num Ty.Num.Nat :: Ty.Num a :: s) f
  | IEdiv_int {a b s f} :
    kinfo (Ty.Num a :: Ty.Num b :: s) ->
    kinstr
      (Ty.Option (Ty.Pair (Ty.Num Ty.Num.Int) (Ty.Num Ty.Num.Nat)) :: s) f ->
    kinstr (Ty.Num a :: Ty.Num b :: s) f
  | IEdiv_nat {a s f} :
    kinfo (Ty.Num Ty.Num.Nat :: Ty.Num a :: s) ->
    kinstr (Ty.Option (Ty.Pair (Ty.Num a) (Ty.Num Ty.Num.Nat)) :: s) f ->
    kinstr (Ty.Num Ty.Num.Nat :: Ty.Num a :: s) f
  | ILsl_nat {s f} :
    kinfo (Ty.Num Ty.Num.Nat :: Ty.Num Ty.Num.Nat :: s) ->
    kinstr (Ty.Num Ty.Num.Nat :: s) f ->
    kinstr (Ty.Num Ty.Num.Nat :: Ty.Num Ty.Num.Nat :: s) f
  | ILsr_nat {s f} :
    kinfo (Ty.Num Ty.Num.Nat :: Ty.Num Ty.Num.Nat :: s) ->
    kinstr (Ty.Num Ty.Num.Nat :: s) f ->
    kinstr (Ty.Num Ty.Num.Nat :: Ty.Num Ty.Num.Nat :: s) f
  | IOr_nat {s f} :
    kinfo (Ty.Num Ty.Num.Nat :: Ty.Num Ty.Num.Nat :: s) ->
    kinstr (Ty.Num Ty.Num.Nat :: s) f ->
    kinstr (Ty.Num Ty.Num.Nat :: Ty.Num Ty.Num.Nat :: s) f
  | IAnd_nat {s f} :
    kinfo (Ty.Num Ty.Num.Nat :: Ty.Num Ty.Num.Nat :: s) ->
    kinstr (Ty.Num Ty.Num.Nat :: s) f ->
    kinstr (Ty.Num Ty.Num.Nat :: Ty.Num Ty.Num.Nat :: s) f
  | IAnd_int_nat {s f} :
    kinfo (Ty.Num Ty.Num.Int :: Ty.Num Ty.Num.Nat :: s) ->
    kinstr (Ty.Num Ty.Num.Nat :: s) f ->
    kinstr (Ty.Num Ty.Num.Int :: Ty.Num Ty.Num.Nat :: s) f
  | IXor_nat {s f} :
    kinfo (Ty.Num Ty.Num.Nat :: Ty.Num Ty.Num.Nat :: s) ->
    kinstr (Ty.Num Ty.Num.Nat :: s) f ->
    kinstr (Ty.Num Ty.Num.Nat :: Ty.Num Ty.Num.Nat :: s) f
  | INot_int {a s f} :
    kinfo (Ty.Num a :: s) ->
    kinstr (Ty.Num Ty.Num.Int :: s) f ->
    kinstr (Ty.Num a :: s) f
  (*
    Control
    -------
  *)
  | IIf {s u f} :
    kinfo (Ty.Bool :: s) ->
    kinstr s u ->
    kinstr s u ->
    kinstr u f ->
    kinstr (Ty.Bool :: s) f
  | ILoop {s f} :
    kinfo (Ty.Bool :: s) ->
    kinstr s (Ty.Bool :: s) ->
    kinstr s f ->
    kinstr (Ty.Bool :: s) f
  | ILoop_left {a b s f} :
    kinfo (Ty.Union a b :: s) ->
    kinstr (a :: s) (Ty.Union a b :: s) ->
    kinstr (b :: s) f ->
    kinstr (Ty.Union a b :: s) f
  | IDip {a s t f} :
    kinfo (a :: s) ->
    kinstr s t ->
    kinstr (a :: t) f ->
    kinstr (a :: s) f
  | IExec {a b s f} :
    kinfo (a :: Ty.Lambda a b :: s) ->
    kinstr (b :: s) f ->
    kinstr (a :: Ty.Lambda a b :: s) f
  | IApply {a b c s f} :
    kinfo (a :: Ty.Lambda (Ty.Pair a b) c :: s) ->
    ty a ->
    kinstr (Ty.Lambda b c :: s) f ->
    kinstr (a :: Ty.Lambda (Ty.Pair a b) c :: s) f
  | ILambda {s b c f} :
    kinfo s ->
    lambda b c ->
    kinstr (Ty.Lambda b c :: s) f ->
    kinstr s f
  | IFailwith {a s f} :
    kinfo (a :: s) ->
    Alpha_context.Script.location ->
    ty a ->
    kinstr (a :: s) f
  (*
     Comparison
     ----------
  *)
  | ICompare {a s f} :
    kinfo (a :: a :: s) ->
    comparable_ty a ->
    kinstr (Ty.Num Ty.Num.Int :: s) f ->
    kinstr (a :: a :: s) f
  (*
     Comparators
     -----------
  *)
  | IEq {s f} :
    kinfo (Ty.Num Ty.Num.Int :: s) ->
    kinstr (Ty.Bool :: s) f ->
    kinstr (Ty.Num Ty.Num.Int :: s) f
  | INeq {s f} :
    kinfo (Ty.Num Ty.Num.Int :: s) ->
    kinstr (Ty.Bool :: s) f ->
    kinstr (Ty.Num Ty.Num.Int :: s) f
  | ILt {s f} :
    kinfo (Ty.Num Ty.Num.Int :: s) ->
    kinstr (Ty.Bool :: s) f ->
    kinstr (Ty.Num Ty.Num.Int :: s) f
  | IGt {s f} :
    kinfo (Ty.Num Ty.Num.Int :: s) ->
    kinstr (Ty.Bool :: s) f ->
    kinstr (Ty.Num Ty.Num.Int :: s) f
  | ILe {s f} :
    kinfo (Ty.Num Ty.Num.Int :: s) ->
    kinstr (Ty.Bool :: s) f ->
    kinstr (Ty.Num Ty.Num.Int :: s) f
  | IGe {s f} :
    kinfo (Ty.Num Ty.Num.Int :: s) ->
    kinstr (Ty.Bool :: s) f ->
    kinstr (Ty.Num Ty.Num.Int :: s) f
  (*
     Protocol
     --------
  *)
  | IAddress {a s f} :
    kinfo (Ty.Contract a :: s) ->
    kinstr (Ty.Address :: s) f ->
    kinstr (Ty.Contract a :: s) f
  | IContract {a s f} :
    kinfo (Ty.Address :: s) ->
    ty a ->
    string ->
    kinstr (Ty.Option (Ty.Contract a) :: s) f ->
    kinstr (Ty.Address :: s) f
  | IView {a b s f} :
    kinfo (a :: Ty.Address :: s) ->
    view_signature a b ->
    kinstr (Ty.Option b :: s) f ->
    kinstr (a :: Ty.Address :: s) f
  | ITransfer_tokens {a s f} :
    kinfo (a :: Ty.Mutez :: Ty.Contract a :: s) ->
    kinstr (Ty.Operation :: s) f ->
    kinstr (a :: Ty.Mutez :: Ty.Contract a :: s) f
  | IImplicit_account {s f} :
    kinfo (Ty.Key_hash :: s) ->
    kinstr (Ty.Contract Ty.Unit :: s) f ->
    kinstr (Ty.Key_hash :: s) f
  | ICreate_contract {a b s f} :
    kinfo (Ty.Option Ty.Key_hash :: Ty.Mutez :: a :: s) ->
    ty a ->
    ty b ->
    lambda (Ty.Pair b a) (Ty.Pair (Ty.List Ty.Operation) a) ->
    Script_typed_ir.view_map ->
    Script_typed_ir.entrypoints ->
    kinstr (Ty.Operation :: Ty.Address :: s) f ->
    kinstr (Ty.Option Ty.Key_hash :: Ty.Mutez :: a :: s) f
  | ISet_delegate {s f} :
    kinfo (Ty.Option Ty.Key_hash :: s) ->
    kinstr (Ty.Operation :: s) f ->
    kinstr (Ty.Option Ty.Key_hash :: s) f
  | INow {s f} :
    kinfo s ->
    kinstr (Ty.Timestamp :: s) f ->
    kinstr s f
  | IBalance {s f} :
    kinfo s ->
    kinstr (Ty.Mutez :: s) f ->
    kinstr s f
  | ILevel {s f} :
    kinfo s ->
    kinstr (Ty.Num Ty.Num.Nat :: s) f ->
    kinstr s f
  | ICheck_signature {s f} :
    kinfo (Ty.Key :: Ty.Signature :: Ty.Bytes :: s) ->
    kinstr (Ty.Bool :: s) f ->
    kinstr (Ty.Key :: Ty.Signature :: Ty.Bytes :: s) f
  | IHash_key {s f} :
    kinfo (Ty.Key :: s) ->
    kinstr (Ty.Key_hash :: s) f ->
    kinstr (Ty.Key :: s) f
  | IPack {a s f} :
    kinfo (a :: s) ->
    ty a ->
    kinstr (Ty.Bytes :: s) f ->
    kinstr (a :: s) f
  | IUnpack {a s f} :
    kinfo (Ty.Bytes :: s) ->
    ty a ->
    kinstr (Ty.Option a :: s) f ->
    kinstr (Ty.Bytes :: s) f
  | IBlake2b {s f} :
    kinfo (Ty.Bytes :: s) ->
    kinstr (Ty.Bytes :: s) f ->
    kinstr (Ty.Bytes :: s) f
  | ISha256 {s f} :
    kinfo (Ty.Bytes :: s) ->
    kinstr (Ty.Bytes :: s) f ->
    kinstr (Ty.Bytes :: s) f
  | ISha512 {s f} :
    kinfo (Ty.Bytes :: s) ->
    kinstr (Ty.Bytes :: s) f ->
    kinstr (Ty.Bytes :: s) f
  | ISource {s f} :
    kinfo s ->
    kinstr (Ty.Address :: s) f ->
    kinstr s f
  | ISender {s f} :
    kinfo s ->
    kinstr (Ty.Address :: s) f ->
    kinstr s f
  | ISelf {b s f} :
    kinfo s ->
    ty b ->
    string ->
    kinstr (Ty.Contract b :: s) f ->
    kinstr s f
  | ISelf_address {s f} :
    kinfo s ->
    kinstr (Ty.Address :: s) f ->
    kinstr s f
  | IAmount {s f} :
    kinfo s ->
    kinstr (Ty.Mutez :: s) f ->
    kinstr s f
  | ISapling_empty_state {s f} :
    kinfo s ->
    Alpha_context.Sapling.Memo_size.t ->
    kinstr (Ty.Sapling_state :: s) f ->
    kinstr s f
  | ISapling_verify_update {s f} :
    kinfo (Ty.Sapling_transaction :: Ty.Sapling_state :: s) ->
    kinstr
      (Ty.Option (Ty.Pair (Ty.Num Ty.Num.Int) Ty.Sapling_state) :: s) f ->
    kinstr (Ty.Sapling_transaction :: Ty.Sapling_state :: s) f
  | IDig {a s t u f} :
    kinfo t ->
    int ->
    stack_prefix_preservation_witness (a :: s) s t u ->
    kinstr (a :: u) f  ->
    kinstr t f
  | IDug {a s t u f} :
    kinfo (a :: t) ->
    int ->
    stack_prefix_preservation_witness s (a :: s) t u ->
    kinstr u f ->
    kinstr (a :: t) f
  | IDipn {s t u v f} :
    kinfo s ->
    int ->
    stack_prefix_preservation_witness t v s u ->
    kinstr t v ->
    kinstr u f ->
    kinstr s f
  | IDropn {s u f} :
    kinfo s ->
    int ->
    stack_prefix_preservation_witness u u s s ->
    kinstr u f ->
    kinstr s f
  | IChainId {s f} :
    kinfo s ->
    kinstr (Ty.Chain_id :: s) f ->
    kinstr s f
  | INever {s f} :
    kinfo (Ty.Never :: s) ->
    kinstr (Ty.Never :: s) f
  | IVoting_power {s f} :
    kinfo (Ty.Key_hash :: s) ->
    kinstr (Ty.Num Ty.Num.Nat :: s) f ->
    kinstr (Ty.Key_hash :: s) f
  | ITotal_voting_power {s f} :
    kinfo s ->
    kinstr (Ty.Num Ty.Num.Nat :: s) f ->
    kinstr s f
  | IKeccak {s f} :
    kinfo (Ty.Bytes :: s) ->
    kinstr (Ty.Bytes :: s) f ->
    kinstr (Ty.Bytes :: s) f
  | ISha3 {s f} :
    kinfo (Ty.Bytes :: s) ->
    kinstr (Ty.Bytes :: s) f ->
    kinstr (Ty.Bytes :: s) f
  | IAdd_bls12_381_g1 {s f} :
    kinfo (Ty.Bls12_381_g1 :: Ty.Bls12_381_g1 :: s) ->
    kinstr (Ty.Bls12_381_g1 :: s) f ->
    kinstr (Ty.Bls12_381_g1 :: Ty.Bls12_381_g1 :: s) f
  | IAdd_bls12_381_g2 {s f} :
    kinfo (Ty.Bls12_381_g2 :: Ty.Bls12_381_g2 :: s) ->
    kinstr (Ty.Bls12_381_g2 :: s) f ->
    kinstr (Ty.Bls12_381_g2 :: Ty.Bls12_381_g2 :: s) f
  | IAdd_bls12_381_fr {s f} :
    kinfo (Ty.Bls12_381_fr :: Ty.Bls12_381_fr :: s) ->
    kinstr (Ty.Bls12_381_fr :: s) f ->
    kinstr (Ty.Bls12_381_fr :: Ty.Bls12_381_fr :: s) f
  | IMul_bls12_381_g1 {s f} :
    kinfo (Ty.Bls12_381_g1 :: Ty.Bls12_381_fr :: s) ->
    kinstr (Ty.Bls12_381_g1 :: s) f ->
    kinstr (Ty.Bls12_381_g1 :: Ty.Bls12_381_fr :: s) f
  | IMul_bls12_381_g2 {s f} :
    kinfo (Ty.Bls12_381_g2 :: Ty.Bls12_381_fr :: s) ->
    kinstr (Ty.Bls12_381_g2 :: s) f ->
    kinstr (Ty.Bls12_381_g2 :: Ty.Bls12_381_fr :: s) f
  | IMul_bls12_381_fr {s f} :
    kinfo (Ty.Bls12_381_fr :: Ty.Bls12_381_fr :: s) ->
    kinstr (Ty.Bls12_381_fr :: s) f ->
    kinstr (Ty.Bls12_381_fr :: Ty.Bls12_381_fr :: s) f
  | IMul_bls12_381_z_fr {a s f} :
    kinfo (Ty.Bls12_381_fr :: Ty.Num a :: s) ->
    kinstr (Ty.Bls12_381_fr :: s) f ->
    kinstr (Ty.Bls12_381_fr :: Ty.Num a :: s) f
  | IMul_bls12_381_fr_z {a s f} :
    kinfo (Ty.Num a :: Ty.Bls12_381_fr :: s) ->
    kinstr (Ty.Bls12_381_fr :: s) f ->
    kinstr (Ty.Num a :: Ty.Bls12_381_fr :: s) f
  | IInt_bls12_381_fr {s f} :
    kinfo (Ty.Bls12_381_fr :: s) ->
    kinstr (Ty.Num Ty.Num.Int :: s) f ->
    kinstr (Ty.Bls12_381_fr :: s) f
  | INeg_bls12_381_g1 {s f} :
    kinfo (Ty.Bls12_381_g1 :: s) ->
    kinstr (Ty.Bls12_381_g1 :: s) f ->
    kinstr (Ty.Bls12_381_g1 :: s) f
  | INeg_bls12_381_g2 {s f} :
    kinfo (Ty.Bls12_381_g2 :: s) ->
    kinstr (Ty.Bls12_381_g2 :: s) f ->
    kinstr (Ty.Bls12_381_g2 :: s) f
  | INeg_bls12_381_fr {s f} :
    kinfo (Ty.Bls12_381_fr :: s) ->
    kinstr (Ty.Bls12_381_fr :: s) f ->
    kinstr (Ty.Bls12_381_fr :: s) f
  | IPairing_check_bls12_381 {s f} :
    kinfo (Ty.List (Ty.Pair Ty.Bls12_381_g1 Ty.Bls12_381_g2) :: s) ->
    kinstr (Ty.Bool :: s) f ->
    kinstr (Ty.List (Ty.Pair Ty.Bls12_381_g1 Ty.Bls12_381_g2) :: s) f
  | IComb {s u f} :
    kinfo s ->
    int ->
    comb_gadt_witness s u ->
    kinstr u f ->
    kinstr s f
  | IUncomb {s u f} :
    kinfo s ->
    int ->
    uncomb_gadt_witness s u ->
    kinstr u f ->
    kinstr s f
  | IComb_get {s t v f} :
    kinfo (t :: s) ->
    int ->
    comb_get_gadt_witness t v ->
    kinstr (v :: s) f ->
    kinstr (t :: s) f
  | IComb_set {a b c s f} :
    kinfo (a :: b :: s) ->
    int ->
    comb_set_gadt_witness a b c ->
    kinstr (c :: s) f ->
    kinstr (a :: b :: s) f
  | IDup_n {s t f} :
    kinfo s ->
    int ->
    dup_n_gadt_witness s t ->
    kinstr (t :: s) f ->
    kinstr s f
  | ITicket {a s f} :
    kinfo (a :: Ty.Num Ty.Num.Nat :: s) ->
    kinstr (Ty.Ticket a :: s) f ->
    kinstr (a :: Ty.Num Ty.Num.Nat :: s) f
  | IRead_ticket {a s f} :
    kinfo (Ty.Ticket a :: s) ->
    kinstr
      (Ty.Address :: (Ty.Pair a (Ty.Num Ty.Num.Nat)) :: Ty.Ticket a :: s) f ->
    kinstr (Ty.Ticket a :: s) f
  | ISplit_ticket {a s f} :
    kinfo (Ty.Ticket a :: (Ty.Pair (Ty.Num Ty.Num.Nat) (Ty.Num Ty.Num.Nat)) :: s) ->
    kinstr (Ty.Option (Ty.Pair (Ty.Ticket a) (Ty.Ticket a)) :: s) f ->
    kinstr (Ty.Ticket a :: (Ty.Pair (Ty.Num Ty.Num.Nat) (Ty.Num Ty.Num.Nat)) :: s) f
  | IJoin_tickets {a s f} :
    kinfo (Ty.Pair (Ty.Ticket a) (Ty.Ticket a) :: s) ->
    comparable_ty a ->
    kinstr (Ty.Option (Ty.Ticket a) :: s) f ->
    kinstr (Ty.Pair (Ty.Ticket a) (Ty.Ticket a) :: s) f
  | IOpen_chest {s f} : 
    kinfo (Ty.Chest_key :: Ty.Chest :: Ty.Num Ty.Num.Nat :: s) ->
    kinstr (Ty.Union Ty.Bytes Ty.Bool :: s) f ->
    kinstr (Ty.Chest_key :: Ty.Chest :: Ty.Num Ty.Num.Nat :: s) f
  (*
    Internal control instructions
    -----------------------------
  *)
  | IHalt {s} :
    kinfo s ->
    kinstr s s
  (* We choose to ignore the [ILog] function. *)

  with lambda : Ty.t -> Ty.t -> Set :=
  | Lam {arg ret} :
    kdescr [arg] [ret] ->
    Alpha_context.Script.node ->
    lambda arg ret

  (* We do not define [logger] and [logging_function] as we choose to ignore
     the logging. *)

  with kdescr : Stack_ty.t -> Stack_ty.t -> Set :=
  | Kdescr {s f} :
    Alpha_context.Script.location ->
    stack_ty s ->
    stack_ty f ->
    kinstr s f ->
    kdescr s f.

  Fixpoint to_kinstr {s f} (i : kinstr s f) : Script_typed_ir.kinstr :=
    match i with
    (*
      Stack
      -----
    *)
    | IDrop info k =>
      Script_typed_ir.IDrop (to_kinfo info) (to_kinstr k)
    | IDup info k =>
      Script_typed_ir.IDup (to_kinfo info) (to_kinstr k)
    | ISwap info k =>
      Script_typed_ir.ISwap (to_kinfo info) (to_kinstr k)
    | IConst info v k =>
      Script_typed_ir.IConst (to_kinfo info) v (to_kinstr k)
    (*
      Pairs
      -----
    *)
    | ICons_pair info k =>
      Script_typed_ir.ICons_pair (to_kinfo info) (to_kinstr k)
    | ICar info k =>
      Script_typed_ir.ICar (to_kinfo info) (to_kinstr k)
    | ICdr info k =>
      Script_typed_ir.ICdr (to_kinfo info) (to_kinstr k)
    | IUnpair info k =>
      Script_typed_ir.IUnpair (to_kinfo info) (to_kinstr k)
    (*
      Options
      -------
    *)
    | ICons_some info k =>
      Script_typed_ir.ICons_some (to_kinfo info) (to_kinstr k)
    | ICons_none info k =>
      Script_typed_ir.ICons_none (to_kinfo info) (to_kinstr k)
    | IIf_none info branch_if_none branch_if_some k =>
      Script_typed_ir.IIf_none {|
        Script_typed_ir.kinstr.IIf_none.kinfo := to_kinfo info;
        Script_typed_ir.kinstr.IIf_none.branch_if_none := to_kinstr branch_if_none;
        Script_typed_ir.kinstr.IIf_none.branch_if_some := to_kinstr branch_if_some;
        Script_typed_ir.kinstr.IIf_none.k := to_kinstr k;
      |}
    | IOpt_map info body k =>
      Script_typed_ir.IOpt_map {|
        Script_typed_ir.kinstr.IOpt_map.kinfo := to_kinfo info;
        Script_typed_ir.kinstr.IOpt_map.body := to_kinstr body;
        Script_typed_ir.kinstr.IOpt_map.k := to_kinstr k;
      |}
    (*
      Unions
      ------
    *)
    | ICons_left info k =>
      Script_typed_ir.ICons_left (to_kinfo info) (to_kinstr k)
    | ICons_right info k =>
      Script_typed_ir.ICons_right (to_kinfo info) (to_kinstr k)
    | IIf_left info branch_if_left branch_if_right k =>
      Script_typed_ir.IIf_left {|
        Script_typed_ir.kinstr.IIf_left.kinfo := to_kinfo info;
        Script_typed_ir.kinstr.IIf_left.branch_if_left := to_kinstr branch_if_left;
        Script_typed_ir.kinstr.IIf_left.branch_if_right := to_kinstr branch_if_right;
        Script_typed_ir.kinstr.IIf_left.k := to_kinstr k;
      |}
    (*
      Lists
      -----
    *)
    | ICons_list info k =>
      Script_typed_ir.ICons_list (to_kinfo info) (to_kinstr k)
    | INil info k =>
      Script_typed_ir.INil (to_kinfo info) (to_kinstr k)
    | IIf_cons info branch_if_cons branch_if_nil k =>
      Script_typed_ir.IIf_cons {|
        Script_typed_ir.kinstr.IIf_cons.kinfo := to_kinfo info;
        Script_typed_ir.kinstr.IIf_cons.branch_if_cons := to_kinstr branch_if_cons;
        Script_typed_ir.kinstr.IIf_cons.branch_if_nil := to_kinstr branch_if_nil;
        Script_typed_ir.kinstr.IIf_cons.k := to_kinstr k;
      |}
    | IList_map info body k =>
      Script_typed_ir.IList_map (to_kinfo info) (to_kinstr body) (to_kinstr k)
    | IList_iter info body k =>
      Script_typed_ir.IList_iter (to_kinfo info) (to_kinstr body) (to_kinstr k)
    | IList_size info k =>
      Script_typed_ir.IList_size (to_kinfo info) (to_kinstr k)
    (*
    Sets
    ----
     *)
    | IEmpty_set info ty instr =>
      Script_typed_ir.IEmpty_set
        (to_kinfo info) (to_comparable_ty ty) (to_kinstr instr)
    | ISet_iter info instr1 instr2 =>
      Script_typed_ir.ISet_iter
        (to_kinfo info) (to_kinstr instr1) (to_kinstr instr2)
    | ISet_mem info instr =>
      Script_typed_ir.ISet_mem (to_kinfo info) (to_kinstr instr)
    | ISet_update info instr =>
      Script_typed_ir.ISet_update (to_kinfo info) (to_kinstr instr)
    | ISet_size info instr =>
      Script_typed_ir.ISet_size (to_kinfo info) (to_kinstr instr)
    (*
     Maps
     ----
     *)
    | IEmpty_map info ty instr =>
      Script_typed_ir.IEmpty_map
        (to_kinfo info) (to_comparable_ty ty) (to_kinstr instr)
    | IMap_map info instr1 instr2 =>
      Script_typed_ir.IMap_map
        (to_kinfo info) (to_kinstr instr1) (to_kinstr instr2)
    | IMap_iter info instr1 instr2 =>
      Script_typed_ir.IMap_iter
        (to_kinfo info) (to_kinstr instr1) (to_kinstr instr2)
    | IMap_mem info instr =>
      Script_typed_ir.IMap_mem (to_kinfo info) (to_kinstr instr)
    | IMap_get info instr =>
      Script_typed_ir.IMap_get (to_kinfo info) (to_kinstr instr)
    | IMap_update info instr =>
      Script_typed_ir.IMap_update (to_kinfo info) (to_kinstr instr)
    | IMap_get_and_update info instr =>
      Script_typed_ir.IMap_get_and_update (to_kinfo info) (to_kinstr instr)
    | IMap_size info instr =>
      Script_typed_ir.IMap_size (to_kinfo info) (to_kinstr instr)
    (*
     Big maps
     --------
     *)
    | IEmpty_big_map info cty ty instr =>
      Script_typed_ir.IEmpty_big_map
        (to_kinfo info) (to_comparable_ty cty) (to_ty ty) (to_kinstr instr)
    | IBig_map_mem info instr =>
      Script_typed_ir.IBig_map_mem (to_kinfo info) (to_kinstr instr)
    | IBig_map_get info instr =>
      Script_typed_ir.IBig_map_get (to_kinfo info) (to_kinstr instr)
    | IBig_map_update info instr =>
      Script_typed_ir.IBig_map_update (to_kinfo info) (to_kinstr instr)
    | IBig_map_get_and_update info instr =>
      Script_typed_ir.IBig_map_get_and_update (to_kinfo info) (to_kinstr instr)
    (*
     Strings
     -------
     *)
    | IConcat_string info instr =>
      Script_typed_ir.IConcat_string (to_kinfo info) (to_kinstr instr)
    | IConcat_string_pair info instr =>
      Script_typed_ir.IConcat_string_pair (to_kinfo info) (to_kinstr instr)
    | ISlice_string info instr =>
      Script_typed_ir.ISlice_string (to_kinfo info) (to_kinstr instr)
    | IString_size info instr =>
      Script_typed_ir.IString_size (to_kinfo info) (to_kinstr instr)
    (*
     Bytes
     -----
     *)
    | IConcat_bytes info instr =>
      Script_typed_ir.IConcat_bytes (to_kinfo info) (to_kinstr instr)
    | IConcat_bytes_pair info instr =>
      Script_typed_ir.IConcat_bytes_pair (to_kinfo info) (to_kinstr instr)
    | ISlice_bytes info instr =>
      Script_typed_ir.ISlice_bytes (to_kinfo info) (to_kinstr instr)
    | IBytes_size info instr =>
      Script_typed_ir.IBytes_size (to_kinfo info) (to_kinstr instr)
    (*
     Timestamps
     ----------
     *)
    | IAdd_seconds_to_timestamp info instr =>
      Script_typed_ir.IAdd_seconds_to_timestamp
        (to_kinfo info) (to_kinstr instr)
    | IAdd_timestamp_to_seconds info instr =>
      Script_typed_ir.IAdd_timestamp_to_seconds
        (to_kinfo info) (to_kinstr instr)
    | ISub_timestamp_seconds info instr =>
      Script_typed_ir.ISub_timestamp_seconds (to_kinfo info) (to_kinstr instr)
    | IDiff_timestamps info instr =>
      Script_typed_ir.IDiff_timestamps (to_kinfo info) (to_kinstr instr)
    (*
     Tez
     ---
     *)
    | IAdd_tez info instr =>
      Script_typed_ir.IAdd_tez (to_kinfo info) (to_kinstr instr)
    | ISub_tez info instr =>
      Script_typed_ir.ISub_tez (to_kinfo info) (to_kinstr instr)
    | ISub_tez_legacy info instr =>
      Script_typed_ir.ISub_tez_legacy (to_kinfo info) (to_kinstr instr)
    | IMul_teznat info instr =>
      Script_typed_ir.IMul_teznat (to_kinfo info) (to_kinstr instr)
    | IMul_nattez info instr =>
      Script_typed_ir.IMul_nattez (to_kinfo info) (to_kinstr instr)
    | IEdiv_teznat info instr =>
      Script_typed_ir.IEdiv_teznat (to_kinfo info) (to_kinstr instr)
    | IEdiv_tez info instr =>
      Script_typed_ir.IEdiv_tez (to_kinfo info) (to_kinstr instr)
    (*
      Booleans
      --------
    *)
    | IOr info k =>
      Script_typed_ir.IOr (to_kinfo info) (to_kinstr k)
    | IAnd info k =>
      Script_typed_ir.IAnd (to_kinfo info) (to_kinstr k)
    | IXor info k =>
      Script_typed_ir.IXor (to_kinfo info) (to_kinstr k)
    | INot info k =>
      Script_typed_ir.INot (to_kinfo info) (to_kinstr k)
    (*
     Integers
     --------
     *)
    | IIs_nat info instr =>
      Script_typed_ir.IIs_nat (to_kinfo info) (to_kinstr instr)
    | INeg info instr =>
      Script_typed_ir.INeg (to_kinfo info) (to_kinstr instr)
    | IAbs_int info instr =>
      Script_typed_ir.IAbs_int (to_kinfo info) (to_kinstr instr)
    | IInt_nat info instr =>
      Script_typed_ir.IInt_nat (to_kinfo info) (to_kinstr instr)
    | IAdd_int info instr =>
      Script_typed_ir.IAdd_int (to_kinfo info) (to_kinstr instr)
    | IAdd_nat info instr =>
      Script_typed_ir.IAdd_nat (to_kinfo info) (to_kinstr instr)
    | ISub_int info instr =>
      Script_typed_ir.ISub_int (to_kinfo info) (to_kinstr instr)
    | IMul_int info instr =>
      Script_typed_ir.IMul_int (to_kinfo info) (to_kinstr instr)
    | IMul_nat info instr =>
      Script_typed_ir.IMul_nat (to_kinfo info) (to_kinstr instr)
    | IEdiv_int info instr =>
      Script_typed_ir.IEdiv_int (to_kinfo info) (to_kinstr instr)
    | IEdiv_nat info instr =>
      Script_typed_ir.IEdiv_nat (to_kinfo info) (to_kinstr instr)
    | ILsl_nat info instr =>
      Script_typed_ir.ILsl_nat (to_kinfo info) (to_kinstr instr)
    | ILsr_nat info instr =>
      Script_typed_ir.ILsr_nat (to_kinfo info) (to_kinstr instr)
    | IOr_nat info instr =>
      Script_typed_ir.IOr_nat (to_kinfo info) (to_kinstr instr)
    | IAnd_nat info instr =>
      Script_typed_ir.IAnd_nat (to_kinfo info) (to_kinstr instr)
    | IAnd_int_nat info instr =>
      Script_typed_ir.IAnd_int_nat (to_kinfo info) (to_kinstr instr)
    | IXor_nat info instr =>
      Script_typed_ir.IXor_nat (to_kinfo info) (to_kinstr instr)
    | INot_int info instr =>
      Script_typed_ir.INot_int (to_kinfo info) (to_kinstr instr)
    (*
      Control
      -------
    *)
    | IIf info branch_if_true branch_if_false k =>
      Script_typed_ir.IIf {|
        Script_typed_ir.kinstr.IIf.kinfo := to_kinfo info;
        Script_typed_ir.kinstr.IIf.branch_if_true := to_kinstr branch_if_true;
        Script_typed_ir.kinstr.IIf.branch_if_false := to_kinstr branch_if_false;
        Script_typed_ir.kinstr.IIf.k := to_kinstr k;
      |}
    | ILoop info body k =>
      Script_typed_ir.ILoop (to_kinfo info) (to_kinstr body) (to_kinstr k)
    | ILoop_left info bl br =>
      Script_typed_ir.ILoop_left (to_kinfo info) (to_kinstr bl) (to_kinstr br)
    | IDip info b k =>
      Script_typed_ir.IDip (to_kinfo info) (to_kinstr b) (to_kinstr k)
    | IExec info k =>
      Script_typed_ir.IExec (to_kinfo info) (to_kinstr k)
    | IApply info capture_ty k =>
      Script_typed_ir.IApply (to_kinfo info) (to_ty capture_ty) (to_kinstr k)
    | ILambda info lam k =>
      Script_typed_ir.ILambda (to_kinfo info) (to_lambda lam) (to_kinstr k)
    | IFailwith info kloc tv =>
      Script_typed_ir.IFailwith (to_kinfo info) kloc (to_ty tv)
    (*
     Comparison
     ----------
     *)
    | ICompare info ty instr =>
      Script_typed_ir.ICompare
        (to_kinfo info) (to_comparable_ty ty) (to_kinstr instr)
    (*
     Comparators
     -----------
     *)
    | IEq info instr =>
      Script_typed_ir.IEq (to_kinfo info) (to_kinstr instr)
    | INeq info instr =>
      Script_typed_ir.INeq (to_kinfo info) (to_kinstr instr)
    | ILt info instr =>
      Script_typed_ir.ILt (to_kinfo info) (to_kinstr instr)
    | IGt info instr =>
      Script_typed_ir.IGt (to_kinfo info) (to_kinstr instr)
    | ILe info instr =>
      Script_typed_ir.ILe (to_kinfo info) (to_kinstr instr)
    | IGe info instr =>
      Script_typed_ir.IGe (to_kinfo info) (to_kinstr instr)
    (*
     Protocol
     --------
     *)
    | IAddress info instr =>
      Script_typed_ir.IAddress (to_kinfo info) (to_kinstr instr)
    | IContract info ty str instr =>
      Script_typed_ir.IContract
        (to_kinfo info) (to_ty ty) str (to_kinstr instr)
    | IView info sign instr =>
      Script_typed_ir.IView (to_kinfo info) (to_view_signature sign) (to_kinstr instr)
    | ITransfer_tokens info instr =>
      Script_typed_ir.ITransfer_tokens (to_kinfo info) (to_kinstr instr)
    | IImplicit_account info instr =>
      Script_typed_ir.IImplicit_account (to_kinfo info) (to_kinstr instr)
    | ICreate_contract info tya tyb lam map_view entrypoints instr =>
      Script_typed_ir.ICreate_contract {|
          Script_typed_ir.kinstr.ICreate_contract.kinfo := to_kinfo info;
          Script_typed_ir.kinstr.ICreate_contract.storage_type := to_ty tya;
          Script_typed_ir.kinstr.ICreate_contract.arg_type := to_ty tyb;
          Script_typed_ir.kinstr.ICreate_contract.lambda := to_lambda lam;
          Script_typed_ir.kinstr.ICreate_contract.views := map_view;
          Script_typed_ir.kinstr.ICreate_contract.entrypoints := entrypoints;
          Script_typed_ir.kinstr.ICreate_contract.k := to_kinstr instr
        |}
    | ISet_delegate info instr =>
      Script_typed_ir.ISet_delegate (to_kinfo info) (to_kinstr instr)
    | INow info instr =>
      Script_typed_ir.INow (to_kinfo info) (to_kinstr instr)
    | IBalance info instr =>
      Script_typed_ir.IBalance (to_kinfo info) (to_kinstr instr)
    | ILevel info instr =>
      Script_typed_ir.ILevel (to_kinfo info) (to_kinstr instr)
    | ICheck_signature info instr =>
      Script_typed_ir.ICheck_signature (to_kinfo info) (to_kinstr instr)
    | IHash_key info instr =>
      Script_typed_ir.IHash_key (to_kinfo info) (to_kinstr instr)
    | IPack info ty instr =>
      Script_typed_ir.IPack
        (to_kinfo info) (to_ty ty) (to_kinstr instr)
    | IUnpack info ty instr =>
      Script_typed_ir.IUnpack
        (to_kinfo info) (to_ty ty) (to_kinstr instr)
    | IBlake2b info instr =>
      Script_typed_ir.IBlake2b (to_kinfo info) (to_kinstr instr)
    | ISha256 info instr =>
      Script_typed_ir.ISha256 (to_kinfo info) (to_kinstr instr)
    | ISha512 info instr =>
      Script_typed_ir.ISha512 (to_kinfo info) (to_kinstr instr)
    | ISource info instr =>
      Script_typed_ir.ISource (to_kinfo info) (to_kinstr instr)
    | ISender info instr =>
      Script_typed_ir.ISender (to_kinfo info) (to_kinstr instr)
    | ISelf info ty str instr =>
      Script_typed_ir.ISelf
        (to_kinfo info) (to_ty ty) str (to_kinstr instr)
    | ISelf_address info instr =>
      Script_typed_ir.ISelf_address (to_kinfo info) (to_kinstr instr)
    | IAmount info instr =>
      Script_typed_ir.IAmount (to_kinfo info) (to_kinstr instr)
    | ISapling_empty_state info sz instr =>
      Script_typed_ir.ISapling_empty_state (to_kinfo info) sz (to_kinstr instr)
    | ISapling_verify_update info instr =>
      Script_typed_ir.ISapling_verify_update (to_kinfo info) (to_kinstr instr)
    | IDig info v spref instr =>
      Script_typed_ir.IDig (to_kinfo info) v (to_stack_prefix_preservation_witness spref) (to_kinstr instr)
    | IDug info v spref instr =>
      Script_typed_ir.IDug (to_kinfo info) v (to_stack_prefix_preservation_witness spref) (to_kinstr instr)
    | IDipn info v spref instr1 instr2 =>
      Script_typed_ir.IDipn
        (to_kinfo info) v (to_stack_prefix_preservation_witness spref) (to_kinstr instr1) (to_kinstr instr2)                   
    | IDropn info v spref instr =>
      Script_typed_ir.IDropn (to_kinfo info) v (to_stack_prefix_preservation_witness spref) (to_kinstr instr)
    | IChainId info instr =>
      Script_typed_ir.IChainId (to_kinfo info) (to_kinstr instr)
    | INever info => Script_typed_ir.INever (to_kinfo info)
    | IVoting_power info instr =>
      Script_typed_ir.IVoting_power (to_kinfo info) (to_kinstr instr)
    | ITotal_voting_power info instr =>
      Script_typed_ir.ITotal_voting_power (to_kinfo info) (to_kinstr instr)
    | IKeccak info instr =>
      Script_typed_ir.IKeccak (to_kinfo info) (to_kinstr instr)
    | ISha3 info instr =>
      Script_typed_ir.ISha3 (to_kinfo info) (to_kinstr instr)
    | IAdd_bls12_381_g1 info instr =>
      Script_typed_ir.IAdd_bls12_381_g1 (to_kinfo info) (to_kinstr instr)
    | IAdd_bls12_381_g2 info instr =>
      Script_typed_ir.IAdd_bls12_381_g2 (to_kinfo info) (to_kinstr instr)
    | IAdd_bls12_381_fr info instr =>
      Script_typed_ir.IAdd_bls12_381_fr (to_kinfo info) (to_kinstr instr)
    | IMul_bls12_381_g1 info instr =>
      Script_typed_ir.IMul_bls12_381_g1 (to_kinfo info) (to_kinstr instr)
    | IMul_bls12_381_g2 info instr =>
      Script_typed_ir.IMul_bls12_381_g2 (to_kinfo info) (to_kinstr instr)
    | IMul_bls12_381_fr info instr =>
      Script_typed_ir.IMul_bls12_381_fr (to_kinfo info) (to_kinstr instr)
    | IMul_bls12_381_z_fr info instr =>
      Script_typed_ir.IMul_bls12_381_z_fr (to_kinfo info) (to_kinstr instr)
    | IMul_bls12_381_fr_z info instr =>
      Script_typed_ir.IMul_bls12_381_fr_z (to_kinfo info) (to_kinstr instr)
    | IInt_bls12_381_fr info instr =>
      Script_typed_ir.IInt_bls12_381_fr (to_kinfo info) (to_kinstr instr)
    | INeg_bls12_381_g1 info instr =>
      Script_typed_ir.INeg_bls12_381_g1 (to_kinfo info) (to_kinstr instr)
    | INeg_bls12_381_g2 info instr =>
      Script_typed_ir.INeg_bls12_381_g2 (to_kinfo info) (to_kinstr instr)
    | INeg_bls12_381_fr info instr =>
      Script_typed_ir.INeg_bls12_381_fr (to_kinfo info) (to_kinstr instr)
    | IPairing_check_bls12_381 info instr =>
      Script_typed_ir.IPairing_check_bls12_381 (to_kinfo info) (to_kinstr instr)
    | IComb info v comb instr =>
      Script_typed_ir.IComb (to_kinfo info) v
        (to_comb_gadt_witness comb)
        (to_kinstr instr)
    | IUncomb info v comb instr =>
      Script_typed_ir.IUncomb (to_kinfo info) v
      (to_uncomb_gadt_witness comb)
      (to_kinstr instr)
    | IComb_get info v comb instr =>
      Script_typed_ir.IComb_get (to_kinfo info) v
      (to_comb_get_gadt_witness comb)
      (to_kinstr instr)
    | IComb_set info v comb instr =>
      Script_typed_ir.IComb_set (to_kinfo info) v
      (to_comb_set_gadt_witness comb)
      (to_kinstr instr)
    | IDup_n info v dup_n instr =>
      Script_typed_ir.IDup_n (to_kinfo info) v
      (to_dup_n_gadt_witness dup_n)
      (to_kinstr instr)
    | ITicket info instr =>
      Script_typed_ir.ITicket (to_kinfo info) (to_kinstr instr)
    | IRead_ticket info instr =>
      Script_typed_ir.IRead_ticket (to_kinfo info) (to_kinstr instr)
    | ISplit_ticket info instr =>
      Script_typed_ir.ISplit_ticket (to_kinfo info) (to_kinstr instr)
    | IJoin_tickets info ty instr =>
      Script_typed_ir.IJoin_tickets
        (to_kinfo info) (to_comparable_ty ty) (to_kinstr instr)
    | IOpen_chest info instr =>
      Script_typed_ir.IOpen_chest (to_kinfo info) (to_kinstr instr)
    (*
      Internal control instructions
      -----------------------------
    *)
    | IHalt info =>
      Script_typed_ir.IHalt (to_kinfo info)
    end

  with to_lambda {arg ret} (lam : lambda arg ret) : Script_typed_ir.lambda :=
    match lam with
    | Lam descr node => Script_typed_ir.Lam (to_kdescr descr) node
    end
    
  with to_kdescr {s f} (descr : kdescr s f) : Script_typed_ir.kdescr :=
    let 'Kdescr kloc kbef kaft kinstr := descr in
    {|
      Script_typed_ir.kdescr.kloc := kloc;
      Script_typed_ir.kdescr.kbef := to_stack_ty kbef;
      Script_typed_ir.kdescr.kaft := to_stack_ty kaft;
      Script_typed_ir.kdescr.kinstr := to_kinstr kinstr;
    |}.

  Inductive continuation : Stack_ty.t -> Stack_ty.t -> Set :=
  | KNil {f} :
    continuation f f
  | KCons {s t f} :
    kinstr s t -> continuation t f -> continuation s f
  | KReturn {a s f} :
    Stack_ty.to_Set s -> continuation (a :: s) f -> continuation [a] f
  | KMap_head {a b s f} :
      (Ty.to_Set a -> Ty.to_Set b) ->
      continuation (b :: s) f ->
      continuation (a :: s) f
  | KUndip {a b s f} :
      Ty.to_Set b ->
      continuation (b :: a :: s) f ->
      continuation (a :: s) f
  | KLoop_in {s f} :
      kinstr s (Ty.Bool :: s) ->
      continuation s f ->
      continuation (Ty.Bool :: s) f
  | KLoop_in_left {a b s f} :
      kinstr (a :: s) (Ty.Union a b :: s) ->
      continuation (b :: s) f ->
      continuation (Ty.Union a b :: s) f
  | KIter {a b s f} :
      kinstr (a :: b :: s) (b :: s) ->
      list (Ty.to_Set a) ->
      continuation (b :: s) f ->
      continuation (b :: s) f
  | KList_enter_body {a b s f} :
      kinstr (a :: s) (b :: s) ->
      list (Ty.to_Set a) ->
      list (Ty.to_Set b) ->
      int ->
      continuation (Ty.List b :: s) f ->
      continuation s f
  | KList_exit_body {a b s f} :
      kinstr (a :: s) (b :: s) ->
      list (Ty.to_Set a) ->
      list (Ty.to_Set b) ->
      int ->
      continuation (Ty.List b :: s) f ->
      continuation (b :: s) f
  | KMap_enter_body {a b c s f} :
      kinstr (Ty.Pair a b :: s) (c :: s) ->
      list (Ty.to_Set (Ty.Pair a b)) ->
      Ty.to_Set (Ty.Map a c) ->
      continuation (Ty.Map a c :: s) f ->
      continuation s f
  | KMap_exit_body {a b c s f} :
      kinstr (Ty.Pair a b :: s) (c :: s) ->
      list (Ty.to_Set (Ty.Pair a b)) ->
      Ty.to_Set (Ty.Map a c) ->
      Ty.to_Set a ->
      continuation (Ty.Map a c :: s) f ->
      continuation (c :: s) f
  | KView_exit {s f} :
    Script_typed_ir.step_constants -> continuation s f -> continuation s f
  (* We choose to ignore the [KLog] instruction. *).

  Fixpoint to_continuation {arg ret} (c : continuation arg ret) :
    Script_typed_ir.continuation :=
    match c with 
    | KNil => Script_typed_ir.KNil
    | KCons instr c =>
      Script_typed_ir.KCons (to_kinstr instr) (to_continuation c)
    | KReturn s c =>
      Script_typed_ir.KReturn s (to_continuation c)
    | KMap_head g c =>
      Script_typed_ir.KMap_head g (to_continuation c)
    | KUndip b c =>
      Script_typed_ir.KUndip b (to_continuation c)
    | KLoop_in i c =>
      Script_typed_ir.KLoop_in (to_kinstr i) (to_continuation c)
    | KLoop_in_left i c =>
      Script_typed_ir.KLoop_in_left (to_kinstr i) (to_continuation c)
    | KIter i xs c =>
      Script_typed_ir.KIter (to_kinstr i) xs (to_continuation c)
    | KList_enter_body i xs ys n c =>
      Script_typed_ir.KList_enter_body (to_kinstr i) xs ys n (to_continuation c)
    | KList_exit_body i xs ys n c =>
      Script_typed_ir.KList_exit_body (to_kinstr i) xs ys n (to_continuation c)
    | KMap_enter_body i xs m c =>
      Script_typed_ir.KMap_enter_body (to_kinstr i) xs m (to_continuation c)
    | KMap_exit_body i xs m x c =>
      Script_typed_ir.KMap_exit_body (to_kinstr i) xs m x (to_continuation c)
    | KView_exit step c =>
      Script_typed_ir.KView_exit step (to_continuation c)
    end.
End With_family.
