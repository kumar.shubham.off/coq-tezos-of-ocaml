Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Storage.
Require TezosOfOCaml.Proto_alpha.Constants_storage.

Require TezosOfOCaml.Proto_alpha.Proofs.Gas_limit_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Saturation_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Tez_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Constants_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Raw_context.

Lemma hard_has_limit_per_operation_is_valid
  : forall {ctxt : Raw_context.t},
  Raw_context.Valid.t ctxt ->
  Saturation_repr.Valid.t
    ctxt.(Raw_context.t.back)
      .(Raw_context.back.constants)
        .(Constants_repr.parametric.hard_gas_limit_per_operation).
Proof.
  intros.
  destruct H, back, constants. easy.
Qed.

Lemma hard_gas_limit_per_block_is_valid
  : forall {ctxt : Raw_context.t},
  Raw_context.Valid.t ctxt ->
  Saturation_repr.Valid.t
    ctxt.(Raw_context.t.back)
      .(Raw_context.back.constants)
        .(Constants_repr.parametric.hard_gas_limit_per_block).
Proof.
  intros.
  destruct H, back, constants. easy.
Qed.

(* @TODO *)
Axiom cost_per_byte_is_valid
  : forall {ctxt : Raw_context.t},
  Raw_context.Valid.t ctxt ->
  Tez_repr.Valid.t 
    (Constants_storage.cost_per_byte ctxt).

(* @TODO *)
Axiom tokens_per_roll_is_valid
  : forall {ctxt : Raw_context.t},
  Raw_context.Valid.t ctxt ->
  Tez_repr.Valid.t 
    (Constants_storage.tokens_per_roll ctxt).

(* @TODO *)
Axiom seed_nonce_revelation_tip_is_valid
  : forall {ctxt : Raw_context.t},
  Raw_context.Valid.t ctxt ->
  Tez_repr.Valid.t 
    (Constants_storage.seed_nonce_revelation_tip ctxt).

(* @TODO *)
Axiom baking_reward_fixed_portion_is_valid
  : forall {ctxt : Raw_context.t},
  Raw_context.Valid.t ctxt ->
  Tez_repr.Valid.t 
    (Constants_storage.baking_reward_fixed_portion ctxt).

(* @TODO *)
Axiom baking_reward_bonus_per_slot_is_valid
  : forall {ctxt : Raw_context.t},
  Raw_context.Valid.t ctxt ->
  Tez_repr.Valid.t 
    (Constants_storage.baking_reward_bonus_per_slot ctxt).

(* @TODO *)
Axiom endorsing_reward_per_slot_is_valid
  : forall {ctxt : Raw_context.t},
  Raw_context.Valid.t ctxt ->
  Tez_repr.Valid.t 
    (Constants_storage.endorsing_reward_per_slot ctxt).

(* @TODO define a valid.t for quorum *)
(* val quorum_min : Raw_context.t -> int32 (* quorum min should be 0? *)
 * val quorum_max : Raw_context.t -> int32 (* should this be max_int? *)
 * val min_proposal_quorum : Raw_context.t -> int32
 *)

(* @TODO *)
Axiom liquidity_baking_subsidy_is_valid
  : forall {ctxt : Raw_context.t},
  Raw_context.Valid.t ctxt ->
  Tez_repr.Valid.t 
    (Constants_storage.liquidity_baking_subsidy ctxt).

(* @TODO *)
Axiom parametric_is_valid
  : forall {ctxt : Raw_context.t},
  Raw_context.Valid.t ctxt ->
  Constants_repr.Parametric.Valid.t 
    (Constants_storage.parametric_value ctxt).

(* @TODO *)
Axiom minimal_participation_ratio_is_valid
  : forall {ctxt : Raw_context.t},
  Raw_context.Valid.t ctxt ->
  Constants_repr.Ratio.Valid.t 
    (Constants_storage.minimal_participation_ratio ctxt).

(* @TODO *)
Axiom double_baking_punishment_is_valid
  : forall {ctxt : Raw_context.t},
  Raw_context.Valid.t ctxt ->
  Tez_repr.Valid.t 
    (Constants_storage.double_baking_punishment ctxt).

(* @TODO *)
Axiom ratio_of_frozen_deposits_slashed_per_double_endorsement_is_valid
  : forall {ctxt : Raw_context.t},
  Raw_context.Valid.t ctxt ->
  Constants_repr.Ratio.Valid.t 
    (Constants_storage.ratio_of_frozen_deposits_slashed_per_double_endorsement ctxt).

(* @TODO *)
Axiom minimal_block_delay_is_valid
  : forall {ctxt : Raw_context.t},
  Raw_context.Valid.t ctxt ->
  Period_repr.Valid.t 
    (Constants_storage.minimal_block_delay ctxt).

(* @TODO *)
Axiom delay_increment_per_round_is_valid
  : forall {ctxt : Raw_context.t},
  Raw_context.Valid.t ctxt ->
  Period_repr.Valid.t 
    (Constants_storage.delay_increment_per_round ctxt).
