# Run this script after coqdoc to populate the Docusaurus documentation in doc/.
require 'fileutils'
require 'json'
require 'pathname'

$base_url = "/coq-tezos-of-ocaml"
$coqdoc_path = "html"
$output_directory = File.join("doc", "docs")
$source_directory = File.join("src", "Proto_alpha")

$nb_documentation_files = 0

def get_relative_path(path)
  source = Pathname.new($source_directory)
  target = Pathname.new(path)
  target.relative_path_from(source).to_s
end

# Fix the links in the generated coqdoc files.
def fix_links(content)
  content
    .gsub(/href="TezosOfOCaml.Proto_alpha.((\w|\.)+).html/) {
      "href=\"#{$base_url}/docs/#{$1.downcase.gsub(".", "/").gsub(/\/_+/, "/")}/"
    }
    .gsub("#\"", "\"")
end

# We treat the coqdoc as raw HTML, as it would slow-down React otherwise (too
# many links).
def to_jsx(content)
  "<div id=\"main\" dangerouslySetInnerHTML={#{JSON.generate({"__html" => content})}} />"
end

def write_if_different(path, content)
  if !File.exist?(path) || File.read(path, :encoding => 'utf-8') != content then
    FileUtils.mkdir_p(File.dirname(path))
    File.open(path, "w") do |file|
      file << content
    end
  end
end

def list_directories(path)
  Dir.glob(File.join(path, "*")).select {|path| File.directory?(path)}
end

def list_md_files(path)
  Dir.glob(File.join(path, "*.md"))
end

$v_files_black_list = File.read("blacklist.txt").split("\n")

def list_v_files(path)
  Dir.glob(File.join(path, "*.v")) - $v_files_black_list
end

def get_target_path_for_sidebar(relative_path, suffix)
  Pathname.new(File.join(
    File.dirname(relative_path),
    File.basename(relative_path, suffix).sub(/\A_+/, "")
  )).cleanpath.to_s
end

def external_link_svg
  '<svg width="13.5" height="13.5" aria-hidden="true" viewBox="0 0 24 24"><path fill="currentColor" d="M21 13v10h-21v-19h12v2h-10v15h17v-8h2zm3-12h-10.988l4.035 4-6.977 7.07 2.828 2.828 6.977-7.07 4.125 4.172v-11z" /></svg>'
end

def md_of_md(path)
  base_name = File.basename(path, ".md")
  content = <<-END
---
id: #{base_name.downcase}
title: #{base_name.gsub("_", " ").strip.capitalize}
---

[Gitlab #{external_link_svg}](https://gitlab.com/nomadic-labs/coq-tezos-of-ocaml/-/blob/master/#{path})

#{File.read(path, :encoding => 'utf-8')}
  END
  relative_path = get_relative_path(path).downcase
  target_path = File.join($output_directory, relative_path)
  write_if_different(target_path, content)
  $nb_documentation_files += 1
  get_target_path_for_sidebar(relative_path, ".md")
end

def link_of_path(path)
  File.join(
    "/docs",
    File.dirname(path).sub("src/Proto_alpha", "").downcase,
    File.basename(path, ".v").downcase.sub(/\A_+/, "")
  )
end

def code_link_of_path(path)
  code_path = path.sub("/Proofs/", "/")
  if code_path != path && File.exist?(code_path)
    "[See code](#{link_of_path(code_path)}), "
  end
end

def proofs_link_of_path(path)
  proofs_path = File.join(File.dirname(path), "Proofs", File.basename(path))
  if File.exist?(proofs_path) && !$v_files_black_list.include?(proofs_path)
    "[See proofs](#{link_of_path(proofs_path)}), "
  end
end

def ocaml_link_of_path(path)
  if File.dirname(path) == "src/Proto_alpha" && File.basename(path) != "Environment.v"
    ", [OCaml #{external_link_svg}](https://gitlab.com/clarus1/tezos/-/blob/guillaume-claret@proto_alpha-coq-of-ocaml/src/proto_alpha/lib_protocol/#{File.basename(path, ".v").downcase}.ml)"
  elsif File.dirname(path) == "src/Proto_alpha/Environment"
    basename = File.basename(path, ".v")
    basename[0] = basename[0].downcase
    basename.sub!("rPC", "RPC")
    basename = "set" if basename == "_Set"
    ", [OCaml #{external_link_svg}](https://gitlab.com/clarus1/tezos/-/blob/guillaume-claret@proto_alpha-coq-of-ocaml/src/lib_protocol_environment/sigs/v5/#{basename}.mli)"
  end
end

def nbsp
  [160].pack('U*')
end

def title_for_v(path)
  name = File.basename(path)
  emoji =
    path.include?("Environment") ? "🍃" :
    {
      "Alpha_context.v" => "🌌",
      "Alpha_services.v" => "🌌",
      "Amendment.v" => "🧑‍⚖️",
      "Apply_results.v" => "🏗️",
      "Apply.v" => "🏗️",
      "Baking.v" => "🥖",
      "Blinded_public_key_hash.v" => "🕶️",
      "Block_header_repr.v" => "🧱",
      "Block_payload_hash.v" => "🧱",
      "Block_payload_repr.v" => "🧱",
      "Bootstrap_storage.v" => "🐣",
      "Cache_memory_helpers.v" => "🧠",
      "Cache_repr.v" => "🧠",
      "Carbonated_map_costs.v" => "🔥",
      "Carbonated_map.v" => "🔥",
      "Commitment_repr.v" => "💍",
      "Commitment_storage.v" => "💍",
      "Constants_repr.v" => "🎛️",
      "Constants_services.v" => "🎛️",
      "Constants_storage.v" => "🎛️",
      "Contract_delegate_storage.v" => "✒️",
      "Contract_hash.v" => "✒️",
      "Contract_manager_storage.v" => "✒️",
      "Contract_repr.v" => "✒️",
      "Contract_services.v" => "✒️",
      "Contract_storage.v" => "✒️",
      "Cycle_repr.v" => "➰",
      "Delegate_activation_storage.v" => "👥",
      "Delegate_services.v" => "👥",
      "Delegate_storage.v" => "👥",
      "Destination_repr.v" => "✒️",
      "Entrypoint_repr.v" => "🚪",
      "Fees_storage.v" => "💰",
      "Fitness_repr.v" => "🏋️",
      "Fixed_point_repr.v" => "🧮",
      "Frozen_deposits_storage.v" => "⛄",
      "Gas_limit_repr.v" => "⛽",
      "Gas_monad.v" => "⛽",
      "Global_constants_costs.v" => "🌍",
      "Global_constants_storage.v" => "🌍",
      "Indexable.v" => "🗂️",
      "Init_storage.v" => "🏁",
      "Lazy_storage_diff.v" => "🦥",
      "Lazy_storage_kind.v" => "🦥",
      "Level_repr.v" => "🪜",
      "Level_storage.v" => "🪜",
      "Liquidity_baking_cpmm.v" => "⚗️",
      "Liquidity_baking_lqt.v" => "⚗️",
      "Liquidity_baking_migration.v" => "⚗️",
      "Liquidity_baking_repr.v" => "⚗️",
      "Local_gas_counter.v" => "⛽",
      "Main.v" => "🧑",
      "Manager_repr.v" => "👔",
      "Michelson_v1_gas.v" => "🍬",
      "Michelson_v1_primitives.v" => "🍬",
      "Migration_repr.v" => "🚗",
      "Misc.v" => "🃏",
      "Non_empty_string.v" => "🔡",
      "Nonce_hash.v" => "💎",
      "Nonce_storage.v" => "💎",
      "Operation_repr.v" => "💸",
      "Origination_nonce.v" => "💎",
      "Parameters_repr.v" => "🕹️",
      "Path_encoding.v" => "🐾",
      "Period_repr.v" => "🕰️",
      "Raw_context_intf.v" => "🖼️",
      "Raw_context.v" => "🖼️",
      "Raw_level_repr.v" => "🪜",
      "Receipt_repr.v" => "🧾",
      "Roll_repr_legacy.v" => "📜",
      "Roll_storage_legacy.v" => "📜",
      "Round_repr.v" => "🥊",
      "Sampler.v" => "🎲",
      "Sapling_repr.v" => "🥷",
      "Sapling_services.v" => "🥷",
      "Sapling_storage.v" => "🥷",
      "Sapling_validator.v" => "🥷",
      "Saturation_repr.v" => "🧮",
      "Sc_rollup_arith.v" => "🦏",
      "Sc_rollup_inbox.v" => "🦏",
      "Sc_rollup_operations.v" => "🦏",
      "Sc_rollup_repr.v" => "🦏",
      "Sc_rollup_storage.v" => "🦏",
      "Sc_rollups.v" => "🦏",
      "Script_cache.v" => "🍬",
      "Script_comparable.v" => "🍬",
      "Script_expr_hash.v" => "🍬",
      "Script_int_repr.v" => "🍬",
      "Script_interpreter_defs.v" => "🍬",
      "Script_interpreter.v" => "🍬",
      "Script_ir_annot.v" => "🍬",
      "Script_ir_translator_mli.v" => "🍬",
      "Script_ir_translator.v" => "🍬",
      "Script_list.v" => "🍬",
      "Script_map.v" => "🍬",
      "Script_repr.v" => "🍬",
      "Script_set.v" => "🍬",
      "Script_string_repr.v" => "🍬",
      "Script_tc_context.v" => "🍬",
      "Script_tc_errors_registration.v" => "🍬",
      "Script_tc_errors.v" => "🍬",
      "Script_timestamp_repr.v" => "🍬",
      "Script_typed_ir_size_costs.v" => "🍬",
      "Script_typed_ir_size.v" => "🍬",
      "Script_typed_ir.v" => "🍬",
      "Seed_repr.v" => "🌱",
      "Seed_storage.v" => "🌱",
      "Services_registration.v" => "🏪",
      "Slot_repr.v" => "🎰",
      "Stake_storage.v" => "🪤",
      "State_hash.v" => "📸",
      "Storage_costs.v" => "💾",
      "Storage_description.v" => "💾",
      "Storage_functors.v" => "💾",
      "Storage_sigs.v" => "💾",
      "Storage.v" => "💾",
      "Tez_repr.v" => "🪙",
      "Ticket_balance_key.v" => "🎫",
      "Ticket_balance_migration_for_j.v" => "🎫",
      "Ticket_costs.v" => "🎫",
      "Ticket_hash_builder.v" => "🎫",
      "Ticket_hash_repr.v" => "🎫",
      "Ticket_lazy_storage_diff.v" => "🎫",
      "Ticket_operations_diff.v" => "🎫",
      "Ticket_scanner.v" => "🎫",
      "Ticket_storage.v" => "🎫",
      "Ticket_token.v" => "🎫",
      "Ticket_token_map.v" => "🎫",
      "Time_repr.v" => "🕰️",
      "Token.v" => "🪅",
      "Tx_rollup_inbox_repr.v" => "🐆",
      "Tx_rollup_inbox_storage.v" => "🐆",
      "Tx_rollup_l2_address.v" => "🐆",
      "Tx_rollup_message_repr.v" => "🐆",
      "Tx_rollup_repr.v" => "🐆",
      "Tx_rollup_services.v" => "🐆",
      "Tx_rollup_state_repr.v" => "🐆",
      "Tx_rollup_state_storage.v" => "🐆",
      "Tx_rollup_storage.v" => "🐆",
      "Utils.v" => "🧰",
      "Vote_repr.v" => "🗳️",
      "Vote_storage.v" => "🗳️",
      "Voting_period_repr.v" => "🗳️",
      "Voting_period_storage.v" => "🗳️",
      "Voting_services.v" => "🗳️"
    }[name]
  (emoji ? emoji + nbsp : "") + name
end

def mdx_of_v(path)
  doc_path = File.join(
    $coqdoc_path,
    File.basename("TezosOfOCaml.Proto_alpha." + get_relative_path(path).gsub("/", "."), ".v") + ".html"
  )
  content = <<-END
---
id: #{File.basename(path, ".v").downcase.sub(/\A_+/, "")}
title: #{title_for_v(path)}
hide_table_of_contents: true
---

#{code_link_of_path(path)}#{proofs_link_of_path(path)}[Gitlab #{external_link_svg}](https://gitlab.com/nomadic-labs/coq-tezos-of-ocaml/-/blob/master/#{path})#{ocaml_link_of_path(path)}

#{to_jsx(fix_links(File.read(doc_path, :encoding => 'utf-8')))}
  END
  relative_path = get_relative_path(path).downcase
  target_path_v = File.join($output_directory, relative_path)
  target_path_mdx = File.join(
    File.dirname(target_path_v),
    File.basename(target_path_v, ".v").sub(/\A_+/, "") + ".mdx"
  )
  write_if_different(target_path_mdx, content)
  $nb_documentation_files += 1
  get_target_path_for_sidebar(relative_path, ".v")
end

def documentation_of_path(path)
  list_directories(path).sort_by {|name| name.downcase}.map do |path|
    {
      "type" => "category",
      "label" => File.basename(path).gsub("_", " ").strip.capitalize,
      "items" => documentation_of_path(path)
    }
  end +
  list_md_files(path).sort_by {|name| name.downcase}.map do |path|
    md_of_md(path)
  end +
  list_v_files(path).sort_by {|name| name.downcase}.map do |path|
    mdx_of_v(path)
  end
end

sidebars = { "docs" => documentation_of_path($source_directory) }
File.open("doc/sidebars.js", "w") do |file|
  file << "module.exports = " + JSON.pretty_generate(sidebars) + ";"
end

def nb_lines(&predicate)
  (Dir.glob(File.join($source_directory, "**", "*.v")) - $v_files_black_list)
    .select(&predicate)
    .map {|path| File.read(path, :encoding => 'utf-8').split("\n").size}
    .reduce(:+)
end

def is_mi_cho_coq(path)
  Pathname(path).each_filename.include?("Michocoq")
end

def is_proof(path)
  Pathname(path).each_filename.include?("Proofs") ||
  Pathname(path).each_filename.include?("Simulations")
end

def is_environment(path)
  !is_proof(path) &&
  Pathname(path).each_filename.include?("Environment")
end

def is_code(path)
  !is_proof(path) && !is_environment(path) && !is_mi_cho_coq(path) &&
  File.basename(path) != "Environment.v" &&
  File.basename(path) != "Environment_modules.v"
end

File.open("doc/nbLines.js", "w") do |file|
  file << <<-END
export default {
  code: #{nb_lines {|path| is_code(path)}},
  environment: #{nb_lines {|path| is_environment(path)}},
  proofs: #{nb_lines {|path| is_proof(path)}},
}
  END
end

puts "#{$nb_documentation_files} generated documentation files in #{$output_directory}"
