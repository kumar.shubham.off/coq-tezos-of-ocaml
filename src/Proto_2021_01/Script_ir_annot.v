Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.
Unset Guard Checking.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Require TezosOfOCaml.Proto_2021_01.Alpha_context.
Require TezosOfOCaml.Proto_2021_01.Script_typed_ir.

Definition unparse_field_annot
  (function_parameter : option Script_typed_ir.field_annot) : list string :=
  match function_parameter with
  | None => nil
  | Some (Script_typed_ir.Field_annot a_value) =>
    [ Pervasives.op_caret "%" a_value ]
  end.

Definition error_unexpected_annot {A : Set}
  (loc : Alpha_context.Script.location) (annot : list A) : M? unit :=
  match annot with
  | [] => Error_monad.ok_unit
  | cons _ _ =>
    Error_monad.error_value
      (Build_extensible "Unexpected_annotation" Alpha_context.Script.location
        loc)
  end.

Definition string_iter {A : Set}
  (p_value : ascii -> Pervasives.result unit A) (s : string) (i : int)
  : Pervasives.result unit A :=
  let len := String.length s in
  let fix aux (i : int) : Pervasives.result unit A :=
    if i >=i len then
      Error_monad.ok_unit
    else
      let? '_ := p_value (String.get s i) in
      aux (i +i 1) in
  aux i.

Definition check_char
  (loc : Alpha_context.Script.location) (function_parameter : ascii)
  : M? unit :=
  match function_parameter with
  |
    ("a" % char | "b" % char | "c" % char | "d" % char | "e" % char | "f" % char
    | "g" % char | "h" % char | "i" % char | "j" % char | "k" % char |
    "l" % char | "m" % char | "n" % char | "o" % char | "p" % char | "q" % char
    | "r" % char | "s" % char | "t" % char | "u" % char | "v" % char |
    "w" % char | "x" % char | "y" % char | "z" % char | "A" % char | "B" % char
    | "C" % char | "D" % char | "E" % char | "F" % char | "G" % char |
    "H" % char | "I" % char | "J" % char | "K" % char | "L" % char | "M" % char
    | "N" % char | "O" % char | "P" % char | "Q" % char | "R" % char |
    "S" % char | "T" % char | "U" % char | "V" % char | "W" % char | "X" % char
    | "Y" % char | "Z" % char | "_" % char | "." % char | "%" % char |
    "@" % char | "0" % char | "1" % char | "2" % char | "3" % char | "4" % char
    | "5" % char | "6" % char | "7" % char | "8" % char | "9" % char) =>
    Error_monad.ok_unit
  | _ =>
    Error_monad.error_value
      (Build_extensible "Unexpected_annotation" Alpha_context.Script.location
        loc)
  end.

Definition max_annot_length : int := 255.

Inductive annot_opt : Set :=
| Field_annot_opt : option string -> annot_opt
| Type_annot_opt : option string -> annot_opt
| Var_annot_opt : option string -> annot_opt.

Definition parse_one_annot (loc : Alpha_context.Script.location) (s : string)
  : M? annot_opt :=
  let sub_or_wildcard {A : Set} (wrap : option string -> A) (s : string)
    : M? A :=
    let len := String.length s in
    if len >i max_annot_length then
      Error_monad.error_value
        (Build_extensible "Unexpected_annotation" Alpha_context.Script.location
          loc)
    else
      if len =i 1 then
        return? (wrap None)
      else
        match String.get s 1 with
        |
          ("a" % char | "b" % char | "c" % char | "d" % char | "e" % char |
          "f" % char | "g" % char | "h" % char | "i" % char | "j" % char |
          "k" % char | "l" % char | "m" % char | "n" % char | "o" % char |
          "p" % char | "q" % char | "r" % char | "s" % char | "t" % char |
          "u" % char | "v" % char | "w" % char | "x" % char | "y" % char |
          "z" % char | "A" % char | "B" % char | "C" % char | "D" % char |
          "E" % char | "F" % char | "G" % char | "H" % char | "I" % char |
          "J" % char | "K" % char | "L" % char | "M" % char | "N" % char |
          "O" % char | "P" % char | "Q" % char | "R" % char | "S" % char |
          "T" % char | "U" % char | "V" % char | "W" % char | "X" % char |
          "Y" % char | "Z" % char | "_" % char | "0" % char | "1" % char |
          "2" % char | "3" % char | "4" % char | "5" % char | "6" % char |
          "7" % char | "8" % char | "9" % char) =>
          let? '_ := string_iter (check_char loc) s 2 in
          return? (wrap (Some (String.sub s 1 (len -i 1))))
        | _ =>
          Error_monad.error_value
            (Build_extensible "Unexpected_annotation"
              Alpha_context.Script.location loc)
        end in
  if (String.length s) =i 0 then
    Error_monad.error_value
      (Build_extensible "Unexpected_annotation" Alpha_context.Script.location
        loc)
  else
    match String.get s 0 with
    | ":" % char =>
      sub_or_wildcard (fun (a_value : option string) => Type_annot_opt a_value)
        s
    | "@" % char =>
      sub_or_wildcard (fun (a_value : option string) => Var_annot_opt a_value) s
    | "%" % char =>
      sub_or_wildcard (fun (a_value : option string) => Field_annot_opt a_value)
        s
    | _ =>
      Error_monad.error_value
        (Build_extensible "Unexpected_annotation" Alpha_context.Script.location
          loc)
    end.

Definition opt_field_of_field_opt (function_parameter : option string)
  : option Script_typed_ir.field_annot :=
  match function_parameter with
  | None => None
  | Some a_value => Some (Script_typed_ir.Field_annot a_value)
  end.

Definition parse_field_annot (loc : int) (annot : string)
  : M? (option Script_typed_ir.field_annot) :=
  let? function_parameter := parse_one_annot loc annot in
  match function_parameter with
  | (Type_annot_opt _ | Var_annot_opt _) =>
    Error_monad.error_value
      (Build_extensible "Unexpected_annotation" Alpha_context.Script.location
        loc)
  | Field_annot_opt a_value => return? (opt_field_of_field_opt a_value)
  end.

Fixpoint extract_field_annot_extract_first
  (annot : list string) (acc_value : list string)
  (function_parameter : list string) {struct function_parameter}
  : option string * list string :=
  match function_parameter with
  | [] => (None, annot)
  | cons s rest =>
    if
      ((String.length s) >i 0) &&
      (Compare.Char.(Compare.S.op_eq) (String.get s 0) "%" % char)
    then
      ((Some s), (List.rev_append acc_value rest))
    else
      extract_field_annot_extract_first annot (cons s acc_value) rest
  end.

Definition extract_field_annot (function_parameter : Alpha_context.Script.node)
  : M? (Alpha_context.Script.node * option Script_typed_ir.field_annot) :=
  match function_parameter with
  | Micheline.Prim loc prim args annot =>
    let '(field_annot, annot) :=
      extract_field_annot_extract_first annot nil annot in
    let? field_annot :=
      match field_annot with
      | None => Error_monad.ok_none
      | Some field_annot => parse_field_annot loc field_annot
      end in
    return? ((Micheline.Prim loc prim args annot), field_annot)
  | expr => return? (expr, None)
  end.

Definition parse_entrypoint_annot (loc : int) (annot : list string)
  : M? (option Script_typed_ir.field_annot) :=
  List.fold_left
    (fun (found : M? (option Script_typed_ir.field_annot)) =>
      fun (a_value : string) =>
        let? found := found in
        let? function_parameter := parse_one_annot loc a_value in
        match function_parameter with
        | Var_annot_opt _ => return? found
        | Type_annot_opt _ =>
          Error_monad.error_value
            (Build_extensible "Unexpected_annotation"
              Alpha_context.Script.location loc)
        | Field_annot_opt a_value =>
          match found with
          | Some _ =>
            Error_monad.error_value
              (Build_extensible "Unexpected_annotation"
                Alpha_context.Script.location loc)
          | None => return? (opt_field_of_field_opt a_value)
          end
        end) Error_monad.ok_none annot.
