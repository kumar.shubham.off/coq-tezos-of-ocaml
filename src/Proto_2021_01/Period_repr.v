Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_2021_01.Environment.

Definition t : Set := Int64.t.

Definition period : Set := t.

(** Inclusion of the module [Compare.Int64] *)
Definition op_eq := Compare.Int64.(Compare.S.op_eq).

Definition op_ltgt := Compare.Int64.(Compare.S.op_ltgt).

Definition op_lt := Compare.Int64.(Compare.S.op_lt).

Definition op_lteq := Compare.Int64.(Compare.S.op_lteq).

Definition op_gteq := Compare.Int64.(Compare.S.op_gteq).

Definition op_gt := Compare.Int64.(Compare.S.op_gt).

Definition compare := Compare.Int64.(Compare.S.compare).

Definition equal := Compare.Int64.(Compare.S.equal).

Definition max := Compare.Int64.(Compare.S.max).

Definition min := Compare.Int64.(Compare.S.min).

Definition encoding : Data_encoding.encoding int64 := Data_encoding.int64_value.

Definition rpc_arg : RPC_arg.arg int64 := RPC_arg.int64_value.

Definition pp (ppf : Format.formatter) (v : int64) : unit :=
  Format.fprintf ppf
    (CamlinternalFormatBasics.Format
      (CamlinternalFormatBasics.Int64 CamlinternalFormatBasics.Int_d
        CamlinternalFormatBasics.No_padding
        CamlinternalFormatBasics.No_precision
        CamlinternalFormatBasics.End_of_format) "%Ld") v.

(** Init function; without side-effects in Coq *)
Definition init_module_repr : unit :=
  let '_ :=
    Error_monad.register_error_kind Error_monad.Permanent "malformed_period"
      "Malformed period" "Period is negative."
      (Some
        (fun (ppf : Format.formatter) =>
          fun (function_parameter : unit) =>
            let '_ := function_parameter in
            Format.fprintf ppf
              (CamlinternalFormatBasics.Format
                (CamlinternalFormatBasics.String_literal "Malformed period"
                  CamlinternalFormatBasics.End_of_format) "Malformed period")))
      Data_encoding.empty
      (fun (function_parameter : Error_monad._error) =>
        match function_parameter with
        | Build_extensible tag _ payload =>
          if String.eqb tag "Malformed_period" then
            Some tt
          else None
        end)
      (fun (function_parameter : unit) =>
        let '_ := function_parameter in
        Build_extensible "Malformed_period" unit tt) in
  Error_monad.register_error_kind Error_monad.Permanent "invalid_arg"
    "Invalid arg" "Negative multiple of periods are not allowed."
    (Some
      (fun (ppf : Format.formatter) =>
        fun (function_parameter : unit) =>
          let '_ := function_parameter in
          Format.fprintf ppf
            (CamlinternalFormatBasics.Format
              (CamlinternalFormatBasics.String_literal "Invalid arg"
                CamlinternalFormatBasics.End_of_format) "Invalid arg")))
    Data_encoding.empty
    (fun (function_parameter : Error_monad._error) =>
      match function_parameter with
      | Build_extensible tag _ payload =>
        if String.eqb tag "Invalid_arg" then
          Some tt
        else None
      end)
    (fun (function_parameter : unit) =>
      let '_ := function_parameter in
      Build_extensible "Invalid_arg" unit tt).

Definition of_seconds (t_value : int64) : M? int64 :=
  if t_value >=i64 0 then
    return? t_value
  else
    Error_monad.error_value (Build_extensible "Malformed_period" unit tt).

Definition to_seconds {A : Set} (t_value : A) : A := t_value.

Definition of_seconds_exn (t_value : int64) : int64 :=
  match of_seconds t_value with
  | Pervasives.Ok t_value => t_value
  | _ => Pervasives.invalid_arg "Period.of_seconds_exn"
  end.

Definition mult (i : int32) (p_value : int64) : M? int64 :=
  if i <i32 0 then
    Error_monad.error_value (Build_extensible "Invalid_arg" unit tt)
  else
    return? ((Int64.of_int32 i) *i64 p_value).

Definition zero : int64 := of_seconds_exn 0.

Definition one_second : int64 := of_seconds_exn 1.

Definition one_minute : int64 := of_seconds_exn 60.

Definition one_hour : int64 := of_seconds_exn 3600.
