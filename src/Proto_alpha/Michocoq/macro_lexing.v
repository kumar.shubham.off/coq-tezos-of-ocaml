(* Open Source License *)
(* Copyright (c) 2021 Nomadic Labs. <contact@nomadic-labs.com> *)

(* Permission is hereby granted, free of charge, to any person obtaining a *)
(* copy of this software and associated documentation files (the "Software"), *)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense, *)
(* and/or sell copies of the Software, and to permit persons to whom the *)
(* Software is furnished to do so, subject to the following conditions: *)

(* The above copyright notice and this permission notice shall be included *)
(* in all copies or substantial portions of the Software. *)

(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR *)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER *)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER *)
(* DEALINGS IN THE SOFTWARE. *)

Require Import Coq.Strings.String Ascii Coq.Lists.List Arith Lia.
Import ListNotations.
Require primitives micheline_lexer.
Import error.Notations.
Import error base.
Require Coq.Program.Wf.

Open Scope string.

(* Aux function currently duplicated from micheline_pp.v to avoid
adding a dependency, it would probably be better to move it somewhere
else. This function is used for DUUUP and DIIIP *)
Fixpoint make_string (a : ascii) (n : nat) :=
  match n with
  | 0 => ""
  | S n' => String a (make_string a n')
  end.

(* Syntax of Michelson macros. *)

(* comparison operators *)
Inductive op := EQ | NEQ | GT | LT | GE | LE.

(* Parameter for the C[AD]+R, SET_C[AD]+R, and MAP_C[AD]+R families of macros *)
Inductive cadr : Set :=
| Cadr_CAR : cadr -> cadr
| Cadr_CDR : cadr -> cadr
| Cadr_nil : cadr.

(* We need to forbid "CR", "CAR", and "CDR"; we do this by requesting
the length of the cadr parameter to be at least 2.

Similarly we forbid SET_CR and MAP_CR by requesting the length to be
at least 1. *)
Fixpoint cadr_length (c : cadr) : nat :=
  match c with
  | Cadr_nil => 0
  | Cadr_CAR c => S (cadr_length c)
  | Cadr_CDR c => S (cadr_length c)
  end.

(* Parameter for the P[AIP]+R and UNP[AIP]+R familier of macros. *)

Inductive direction := Left | Right.

Inductive papair_d : direction -> Set :=
| Papair_d_P d : papair_d Left -> papair_d Right -> papair_d d
| Papair_d_A : papair_d Left
| Papair_d_I : papair_d Right.

Inductive papair : Set := Papair_P : papair_d Left -> papair_d Right -> papair.

(* PAIR is not a macro (it is a primitive) so we need to forbid it. *)
Definition nontrivial_papair (p : papair) :=
  match p with Papair_P Papair_d_A Papair_d_I => false | _ => true end.

(* Syntax of Michelson primitives and macros. *)

Inductive macro :=
| Prim : primitives.primitive -> macro
| FAIL
| ASSERT
| ASSERT_NONE
| ASSERT_SOME
| ASSERT_LEFT
| ASSERT_RIGHT
| IF_SOME
| IF_RIGHT
| IF_NIL
| IFop : op -> macro
| CMPop : op -> macro
| IFCMPop : op -> macro
| ASSERTop : op -> macro
| ASSERT_CMPop : op -> macro
(* DUP is forbidden because it is already a primitive *)
| DUUP (n : nat)  : Bool.Is_true (2 <=? n) -> macro
(* DIP is forbidden for the same reason *)
| DIIP (n : nat)  : Bool.Is_true (2 <=? n) -> macro
(* CR, CAR, and CDR are forbidden. *)
| CADR (c : cadr) : Bool.Is_true (2 <=? cadr_length c) -> macro
(* SET_CR is forbidden. *)
| SET_CADR (c : cadr) : Bool.Is_true (1 <=? cadr_length c) -> macro
(* MAP_CR is forbidden. *)
| MAP_CADR (c : cadr) : Bool.Is_true (1 <=? cadr_length c) -> macro
(* PAIR and UNPAIR are forbidden *)
| PAPAIR (p : papair) : Bool.Is_true (nontrivial_papair p) -> macro
| UNPAPAIR (p : papair) : Bool.Is_true (nontrivial_papair p) -> macro.

(* Constant macros (macros that do not depend on a parameter and are
as easy to parse as instruction). *)

Definition constant_macro_of_string (s : String.string) :=
  match s with
  | "FAIL" => Return FAIL
  | "ASSERT" => Return ASSERT
  | "ASSERT_NONE" => Return ASSERT_NONE
  | "ASSERT_SOME" => Return ASSERT_SOME
  | "ASSERT_LEFT" => Return ASSERT_LEFT
  | "ASSERT_RIGHT" => Return ASSERT_RIGHT
  | "IF_SOME" => Return IF_SOME
  | "IF_RIGHT" => Return IF_RIGHT
  | "IF_NIL" => Return IF_NIL
  | s =>
    (* Actually this error is always caught and ignored in the main
    of_string function bellow so the exact error does not matter. Same
    is true for all the *_of_string functions of this module. *)
    Failed _ (Macro_expansion s)
  end.

(* IF/CMP/ASSERT op *)

Definition op_to_string (op : op) : string :=
  match op with
  | EQ => "EQ"
  | NEQ => "NEQ"
  | GT => "GT"
  | LT => "LT"
  | GE => "GE"
  | LE => "LE"
  end.

Definition op_of_string (s : string) : M op :=
  match s with
  | "EQ" => Return EQ
  | "NEQ" => Return NEQ
  | "GT" => Return GT
  | "LT" => Return LT
  | "GE" => Return GE
  | "LE" => Return LE
  | s => Failed _ (Macro_expansion s)
  end.

Goal forall p s, op_of_string s = Return p <-> op_to_string p = s.
Proof.
  intros p s.
  split; intro H.
  - repeat primitives.continue p.
  - subst s.
    destruct p; reflexivity.
Qed.


Definition IFop_of_string (s : String.string) :=
  match s with
  | String "I" (String "F" s) =>
    let! op := op_of_string s in
    Return (IFop op)
  | s => Failed _ (Macro_expansion s)
  end.

Definition CMPop_of_string (s : String.string) :=
  match s with
  | String "C" (String "M" (String "P" s)) =>
    let! op := op_of_string s in
    Return (CMPop op)
  | s => Failed _ (Macro_expansion s)
  end.

Definition IFCMPop_of_string (s : String.string) :=
  match s with
  | String "I" (String "F" (String "C" (String "M" (String "P" s)))) =>
    let! op := op_of_string s in
    Return (IFCMPop op)
  | s => Failed _ (Macro_expansion s)
  end.

Definition ASSERTop_of_string (s : String.string) :=
  match s with
  | String "A" (String "S" (String "S" (String "E" (String "R" (String "T"
   (String "_" s)))))) =>
    let! op := op_of_string s in
    Return (ASSERTop op)
  | s => Failed _ (Macro_expansion s)
  end.

Definition ASSERT_CMPop_of_string (s : String.string) :=
  match s with
  | String "A" (String "S" (String "S" (String "E" (String "R" (String "T"
   (String "_" (String "C" (String "M" (String "P" s))))))))) =>
    let! op := op_of_string s in
    Return (ASSERT_CMPop op)
  | s => Failed _ (Macro_expansion s)
  end.




(* The DUUP and DIIP case are not easy to factorize because they use
"U" or "I" in patterns. That's why the code is duplicated. *)

(* Returns n if s is UⁿP *)
Fixpoint read_uup s err :=
  match s with
  | "P" => Return 0
  | String "U" s =>
    let! n := read_uup s err in
    Return (S n)
  | _ => Failed _ err
  end.

Lemma read_uup_make_string n err :
  read_uup (make_string "U" n ++ "P") err = Return n.
Proof.
  induction n.
  + reflexivity.
  + simpl.
    rewrite IHn.
    reflexivity.
Qed.

Fixpoint read_uup_correct s err n {struct s}:
  read_uup s err = Return n <-> s = make_string "U" n ++ "P".
Proof.
  split.
  - primitives.continue n.
    + simpl.
      case_eq (read_uup s err); simpl.
      * discriminate.
      * intros n' Hn' H.
        apply unreturn in H.
        subst n.
        simpl.
        f_equal.
        apply (read_uup_correct s err n').
        assumption.
    + simpl.
      primitives.continue n.
      intro H.
      apply unreturn in H.
      subst n.
      reflexivity.
  - intro; subst.
    apply read_uup_make_string.
Qed.

Definition DUUP_of_string (str : String.string) :=
  match str with
  | String "D" s =>
    let! n := read_uup s (Macro_expansion str) in
    let! H := assume _ (Macro_expansion s) in
    Return (DUUP n H)
  | s => Failed _ (Macro_expansion s)
  end.

Definition DUUP_to_string n := "D" ++ make_string "U" n ++ "P".

(* Same for DIIP. *)
(* Returns n if s is IⁿP *)
Fixpoint read_iip s err :=
  match s with
  | "P" => Return 0
  | String "I" s =>
    let! n := read_iip s err in
    Return (S n)
  | _ => Failed _ err
  end.

Lemma read_iip_make_string n err :
  read_iip (make_string "I" n ++ "P") err = Return n.
Proof.
  induction n.
  + reflexivity.
  + simpl.
    rewrite IHn.
    reflexivity.
Qed.

Fixpoint read_iip_correct s err n {struct s}:
  read_iip s err = Return n <-> s = make_string "I" n ++ "P".
Proof.
  split.
  - primitives.continue n.
    + simpl.
      case_eq (read_iip s err); simpl.
      * discriminate.
      * intros n' Hn' H.
        apply unreturn in H.
        subst n.
        simpl.
        f_equal.
        apply (read_iip_correct s err n').
        assumption.
    + simpl.
      primitives.continue n.
      intro H.
      apply unreturn in H.
      subst n.
      reflexivity.
  - intro; subst.
    apply read_iip_make_string.
Qed.


Definition DIIP_of_string (str : String.string) :=
  match str with
  | String "D" s =>
    let! n := read_iip s (Macro_expansion str) in
    let! H := assume _ (Macro_expansion s) in
    Return (DIIP n H)
  | s => Failed _ (Macro_expansion s)
  end.

Definition DIIP_to_string n := "D" ++ make_string "I" n ++ "P".

(* {SET_,MAP_,}C[AD]+R *)

Fixpoint CADR_of_string_aux s :=
  match s with
  | String "A" s =>
    let! c := CADR_of_string_aux s in
    Return (Cadr_CAR c)
  | String "D" s =>
    let! c := CADR_of_string_aux s in
    Return (Cadr_CDR c)
  | "R" => Return Cadr_nil
  | s => Failed _ (Macro_expansion s)
  end.

Fixpoint CADR_to_string cadr :=
  match cadr with
  | Cadr_CAR cadr => "A" ++ CADR_to_string cadr
  | Cadr_CDR cadr => "D" ++ CADR_to_string cadr
  | Cadr_nil => ""
  end.

Lemma CADR_to_of_string c :
  CADR_of_string_aux (CADR_to_string c ++ "R") = Return c.
Proof.
  induction c; simpl.
  - rewrite IHc.
    reflexivity.
  - rewrite IHc.
    reflexivity.
  - reflexivity.
Qed.

Lemma CADR_of_to_string c s :
  CADR_of_string_aux s = Return c <-> CADR_to_string c ++ "R" = s.
Proof.
  split.
  - generalize s; clear s; induction c; intro s.
    + simpl.
      primitives.continue c.
      * simpl.
        case_eq (CADR_of_string_aux s).
        -- simpl; discriminate.
        -- intros c' Hc' H.
           simpl in H. apply unreturn in H.
           injection H.
           intro; subst c'.
           specialize (IHc s Hc').
           congruence.
      * destruct s; discriminate.
      * simpl.
        destruct (CADR_of_string_aux s); discriminate.
    + simpl.
      primitives.continue c.
      * simpl.
        destruct (CADR_of_string_aux s); discriminate.
      * destruct s; discriminate.
      * simpl.
        case_eq (CADR_of_string_aux s).
        -- simpl; discriminate.
        -- intros c' Hc' H.
           simpl in H. apply unreturn in H.
           injection H.
           intro; subst c'.
           specialize (IHc s Hc').
           congruence.
    + simpl.
      primitives.continue c.
      * simpl.
        destruct (CADR_of_string_aux s); discriminate.
      * simpl.
        destruct s; congruence.
      * simpl.
        destruct (CADR_of_string_aux s); discriminate.
  - intro; subst s. apply CADR_to_of_string.
Qed.

Definition CADR_of_string s :=
  match s with
  | String "C" s =>
    let! c := CADR_of_string_aux s in
    let! H := assume _ (Macro_expansion s) in
    Return (CADR c H)
  | s => Failed _ (Macro_expansion s)
  end.

Definition SET_CADR_of_string s :=
  match s with
  | String "S" (String "E" (String "T" (String "_" (String "C" s)))) =>
    let! c := CADR_of_string_aux s in
    let! H := assume _ (Macro_expansion s) in
    Return (SET_CADR c H)
  | s => Failed _ (Macro_expansion s)
  end.

Definition MAP_CADR_of_string s :=
  match s with
  | String "M" (String "A" (String "P" (String "_" (String "C" s)))) =>
    let! c := CADR_of_string_aux s in
    let! H := assume _ (Macro_expansion s) in
    Return (MAP_CADR c H)
  | s => Failed _ (Macro_expansion s)
  end.


(* {UN,}P[AIP]+R *)

(* From string to macro we perform:
   - lexing (from [string] to [list papair_token]), and then
   - parsing (from [list papair_token] + direction) to papair *)

Inductive papair_token := token_P | token_A | token_I.

Fixpoint lex_papair (s : string) (fail : exception) :
  M (list papair_token) :=
  match s with
  | String "P" s =>
    let! l := lex_papair s fail in
    Return (cons token_P l)
  | String "A" s =>
    let! l := lex_papair s fail in
    Return (cons token_A l)
  | String "I" s =>
    let! l := lex_papair s fail in
    Return (cons token_I l)
  | "R" => Return nil
  | _ => Failed _ fail
  end.

Definition unlex_papair_token (t : papair_token) : string :=
  match t with
  | token_P => "P"
  | token_A => "A"
  | token_I => "I"
  end.

(* Inverse of lex_papair but without training "R" *)
Fixpoint unlex_papair (l : list papair_token) : string :=
  match l with
  | nil => ""
  | cons token l => unlex_papair_token token ++ unlex_papair l
  end.

Lemma lex_unlex_papair l fail :
  lex_papair (unlex_papair l ++ "R") fail = Return l.
Proof.
  induction l as [|[| |] l]; simpl.
  - reflexivity.
  - rewrite IHl; reflexivity.
  - rewrite IHl; reflexivity.
  - rewrite IHl; reflexivity.
Qed.

Fixpoint unlex_lex_papair l s fail {struct s} :
  lex_papair s fail = Return l -> unlex_papair l ++ "R" = s.
Proof.
  primitives.continue s; simpl.
  - case_eq (lex_papair s fail); [discriminate|simpl].
    intros l' H' H.
    apply unlex_lex_papair in H'.
    apply unreturn in H.
    subst.
    reflexivity.
  - case_eq (lex_papair s fail); [discriminate|simpl].
    intros l' H' H.
    apply unlex_lex_papair in H'.
    apply unreturn in H.
    subst.
    reflexivity.
  - primitives.continue s; simpl.
    intro H; apply unreturn in H; subst.
    reflexivity.
  - case_eq (lex_papair s fail); [discriminate|simpl].
    intros l' H' H.
    apply unlex_lex_papair in H'.
    apply unreturn in H.
    subst.
    reflexivity.
Qed.

(* Parsing of PAPAIR using the Barga method for delaying the termination proof *)

(* the first two parameters are the inputs, the last two are the outputs *)
Inductive parse_papair_graph :
  forall d : direction, list papair_token -> papair_d d -> list papair_token -> Prop :=
| parse_papair_g_A l :
    (* If we are parsing a left subtree and we see an A, we produce Papair_A *)
    parse_papair_graph Left (token_A :: l) Papair_d_A l
| parse_papair_g_I l : parse_papair_graph Right (token_I :: l) Papair_d_I l
    (* If we are parsing a right subtree and we see an I, we produce Papair_I *)
| parse_papair_g_P l1 l2 l3 d lp rp :
    (* In both directions, if we see a P, we parse a left subtree, then a right
       subtree, and produce a Papair_P applied to both subtrees. *)
    parse_papair_graph Left l1 lp l2 ->
    parse_papair_graph Right l2 rp l3 ->
    parse_papair_graph d (token_P :: l1) (Papair_d_P d lp rp) l3.

Definition direction_proj2_eq d (p1 p2 : papair_d d) :
  existT papair_d d p1 = existT papair_d d p2 -> p1 = p2.
Proof.
  intro H.
  apply existT_eq_3 in H.
  destruct H as (He, H).
  assert (He = eq_refl) by (apply Eqdep_dec.UIP_dec; decide equality).
  subst He.
  exact H.
Defined.

Definition parse_papair_graph_functional_2 d l p1 l1 p2 l2 :
  parse_papair_graph d l p1 l1 ->
  parse_papair_graph d l p2 l2 ->
  l1 = l2.
Proof.
  intro G1.
  generalize dependent p2.
  generalize dependent l2.
  induction G1; intros l' p2 G2; inversion G2.
  - reflexivity.
  - reflexivity.
  - apply direction_proj2_eq in H0.
    subst.
    specialize (IHG1_1 _ _ H2).
    rewrite <- IHG1_1 in H3.
    exact (IHG1_2 _ _ H3).
Defined.

Definition parse_papair_graph_functional_1 d l p1 l1 p2 l2 :
  parse_papair_graph d l p1 l1 ->
  parse_papair_graph d l p2 l2 ->
  p1 = p2.
Proof.
  intros G1 G2.
  assert (l1 = l2) by (exact (parse_papair_graph_functional_2 d l p1 l1 p2 l2 G1 G2)).
  subst l2.
  generalize dependent p2.
  induction G1; intros p2 G2; inversion G2.
  - apply direction_proj2_eq in H0; assumption.
  - apply direction_proj2_eq in H0; assumption.
  - assert (l2 = l4) by (eapply parse_papair_graph_functional_2; eassumption).
    apply direction_proj2_eq in H0.
    subst.
    erewrite IHG1_1; [| apply H2].
    erewrite IHG1_2; [| apply H3].
    reflexivity.
Defined.

(* parse_papair_domain d l is meant to be equivalent to
   exists p l', parse_papair_graph d l p l' *)
Inductive parse_papair_domain : direction -> list papair_token -> Prop :=
| parse_papair_d_A l :
    parse_papair_domain Left (token_A :: l)
| parse_papair_d_I l :
    parse_papair_domain Right (token_I :: l)
| parse_papair_d_P l1 l2 d lp :
    parse_papair_domain Left l1 ->
    parse_papair_graph Left l1 lp l2 ->
    parse_papair_domain Right l2 ->
    parse_papair_domain d (token_P :: l1).

Definition parse_papair_d_P_inv_1 d l :
  parse_papair_domain d (token_P :: l) ->
  parse_papair_domain Left l.
Proof.
  intro D.
  inversion D.
  assumption.
Defined.

Definition parse_papair_d_P_inv_2 d l1 lp l2 :
  parse_papair_domain d (token_P :: l1) ->
  parse_papair_graph Left l1 lp l2 ->
  parse_papair_domain Right l2.
Proof.
  intros D G.
  inversion D.
  subst.
  rewrite (parse_papair_graph_functional_2 _ _ _ _ _ _ G H1).
  assumption.
Defined.

Definition parse_papair_d_ex d l p l' :
  parse_papair_graph d l p l' ->
  parse_papair_domain d l.
Proof.
  intro G; induction G; econstructor; eassumption.
Defined.

Fixpoint parse_papair_pwc (d : direction) (l : list papair_token)
         (D : parse_papair_domain d l) (fail : exception) {struct D} :
  ({p : papair_d d & {l2 : list papair_token | parse_papair_graph d l p l2}} ).
Proof.
  destruct l as [|p l].
  - exfalso.
    inversion D.
  - destruct p.
    + (* Case token_P *)
      generalize (parse_papair_d_P_inv_1 d l D).
      intro Dl.
      generalize (parse_papair_pwc Left l Dl fail).
      intro IHl.
      refine (let '(existT _ lp (exist _ l2 Gl)) := IHl in _).
      generalize (parse_papair_d_P_inv_2 d l lp l2 D Gl).
      intro Dr.
      generalize (parse_papair_pwc Right l2 Dr fail).
      intro IHr.
      refine (let '(existT _ rp (exist _ l3 Gr)) := IHr in _).
      refine (existT _ (Papair_d_P d lp rp) (exist _ l3 _)).
      econstructor; eassumption.
    + destruct d.
      * eexists; eexists; econstructor.
      * exfalso.
        inversion D.
    + destruct d.
      * exfalso.
        inversion D.
      * eexists; eexists; econstructor.
Defined.

(* Termination *)
Lemma parse_papair_graph_wf d l1 p l2 :
  parse_papair_graph d l1 p l2 ->
  List.length l2 < List.length l1.
Proof.
  intro G.
  induction G; simpl; lia.
Defined.

Fixpoint parse_papair_domain_fueled d l (fail : exception) (fuel : nat)
         {struct fuel}: M (parse_papair_domain d l).
Proof.
  destruct fuel as [|fuel].
  - exact (Failed _ fail).
  - destruct l as [|p l].
    + exact (Failed _ fail).
    + destruct p.
      * refine (let! Dl := parse_papair_domain_fueled Left l fail fuel in _).
        refine (let '(existT _ lp (exist _ l2 Gl)) := parse_papair_pwc Left l Dl fail in _).
        refine (let! Dr := parse_papair_domain_fueled Right l2 fail fuel in _).
        apply Return.
        econstructor; eassumption.
      * destruct d.
        -- apply Return.
           constructor.
        -- exact (Failed _ fail).
      * destruct d.
        -- exact (Failed _ fail).
        -- apply Return.
           constructor.
Defined.

Definition parse_papair_domain_full d l fail :=
  parse_papair_domain_fueled d l fail (List.length l).

Lemma parse_papair_domain_fuel_monotonic d l fail D fuel1 fuel2 :
  fuel1 <= fuel2 ->
  parse_papair_domain_fueled d l fail fuel1 = Return D ->
  parse_papair_domain_fueled d l fail fuel2 = Return D.
Proof.
  intro Hfuel.
  generalize dependent d.
  generalize dependent l.
  generalize dependent fuel2.
  induction fuel1; [discriminate|]; intros fuel2 Hfuel l d D.
  destruct fuel2; [inversion Hfuel|].
  assert (fuel1 <= fuel2) as Hfuelbis by lia.
  specialize (IHfuel1 fuel2 Hfuelbis).
  clear Hfuelbis Hfuel.
  simpl; intro HD; destruct l.
  - assumption.
  - destruct p.
    + rewrite bind_eq_return in HD.
      destruct HD as (Dl, (HDl, HD)).
      rewrite (IHfuel1 _ _ _ HDl).
      simpl.
      destruct parse_papair_pwc as (pl, (l2, Gl)).
      rewrite bind_eq_return in HD.
      destruct HD as (Dr, (HDr, HD)).
      rewrite (IHfuel1 _ _ _ HDr).
      simpl.
      assumption.
    + destruct d; assumption.
    + destruct d; assumption.
Defined.

Lemma parse_papair_domain_fuel_length_is_enough d l fail D fuel :
  List.length l <= fuel ->
  parse_papair_domain_fueled d l fail fuel = Return D ->
  parse_papair_domain_full d l fail = Return D.
Proof.
  unfold parse_papair_domain_full.
  intro Hfuel.
  generalize dependent d.
  generalize dependent l.
  induction fuel; [discriminate|]; intros l Hfuel d D.
  destruct l; [inversion Hfuel; intro; assumption|].
  simpl in Hfuel.
  assert (List.length l <= fuel) as Hfuelbis by lia.
  clear Hfuel.
  simpl; intro HD; destruct p.
  - rewrite bind_eq_return in HD.
    destruct HD as (Dl, (HDl, HD)).
    rewrite (IHfuel l Hfuelbis _ _ HDl).
    simpl.
    destruct parse_papair_pwc as (pl, (l2, Gl)).
    simpl.
    rewrite bind_eq_return in HD.
    destruct HD as (Dr, (HDr, HD)).
    generalize (parse_papair_graph_wf _ _ _ _ Gl); intro Hlen.
    assert (List.length l2 <= List.length l) as Hlenle by lia.
    assert (List.length l2 <= fuel) as Hfuel2 by lia.
    specialize (IHfuel l2 Hfuel2 _ _ HDr).
    apply (parse_papair_domain_fuel_monotonic _ _ _ _ _ _ Hlenle) in IHfuel.
    rewrite IHfuel.
    assumption.
  - destruct d; assumption.
  - destruct d; assumption.
Defined.

Lemma parse_papair_domain_fueled_failed d l fail fuel e :
  parse_papair_domain_fueled d l fail fuel = Failed _ e -> e = fail.
Proof.
  generalize dependent e.
  generalize dependent d.
  generalize dependent l.
  induction fuel; destruct l; simpl; try congruence.
  destruct p.
  - case_eq (parse_papair_domain_fueled Left l fail fuel); simpl.
    + intros e He d e'.
      specialize (IHfuel l Left e He).
      congruence.
    + intros p Hp d e.
      destruct (parse_papair_pwc Left l p fail) as (lp, (l2, Gl)).
      case_eq (parse_papair_domain_fueled Right l2 fail fuel); simpl.
      * intros e' He'.
        specialize (IHfuel l2 Right e' He').
        congruence.
      * congruence.
  - destruct d; congruence.
  - destruct d; congruence.
Qed.

Lemma parse_papair_domain_case d l fail fuel :
  parse_papair_domain_fueled d l fail fuel = Failed _ fail \/
  exists D, parse_papair_domain_fueled d l fail fuel = Return D.
Proof.
  case_eq (parse_papair_domain_fueled d l fail fuel).
  - intros e He.
    left.
    apply parse_papair_domain_fueled_failed in He.
    congruence.
  - right; eexists; reflexivity.
Qed.

Lemma parse_papair_domain_fuel_is_length d l fail fuel :
  List.length l <= fuel ->
  parse_papair_domain_fueled d l fail fuel = parse_papair_domain_full d l fail.
Proof.
  intro Hfuel.
  unfold parse_papair_domain_full.
  case (parse_papair_domain_case d l fail fuel).
  - case (parse_papair_domain_case d l fail (List.length l)).
    + (* Failed, Failed *)
      congruence.
    + (* Failed, Return *)
      intros (D, HD).
      apply (parse_papair_domain_fuel_monotonic _ _ _ _ _ _ Hfuel) in HD.
      congruence.
  - intros (D, HD).
    rewrite HD.
    apply parse_papair_domain_fuel_length_is_enough in HD; [|assumption].
    symmetry.
    assumption.
Qed.

Lemma parse_papair_domain_fueled_complete d l fail fuel :
  List.length l <= fuel ->
  parse_papair_domain d l ->
  success (parse_papair_domain_fueled d l fail fuel).
Proof.
  intro Hfuel.
  generalize dependent d.
  generalize dependent l.
  induction fuel; (destruct l; [intros Hfuel d D; inversion D|]); simpl; [lia|].
  intros HfuelS d D.
  assert (List.length l <= fuel) as Hfuel by lia.
  clear HfuelS.
  destruct p.
  - assert (parse_papair_domain Left l) as Dl by (inversion D; assumption).
    destruct (success_eq_return_rev _ _ (IHfuel l Hfuel Left Dl)) as (Dl', HDl).
    rewrite HDl.
    simpl.
    destruct parse_papair_pwc as (lp, (l2, Gl)).
    assert (parse_papair_domain Right l2) as Dr by
          apply (parse_papair_d_P_inv_2 _ _ _ _ D Gl).
    generalize (parse_papair_graph_wf _ _ _ _ Gl); intro Hlen.
    assert (List.length l2 <= List.length l) as Hlenle by lia.
    assert (List.length l2 <= fuel) as Hfuel2 by lia.
    destruct (success_eq_return_rev _ _ (IHfuel l2 Hfuel2 Right Dr)) as (Dr', HDr).
    rewrite HDr.
    constructor.
  - destruct d.
    + constructor.
    + inversion D.
  - destruct d.
    + inversion D.
    + constructor.
Defined.

Lemma parse_papair_domain_complete d l fail :
  parse_papair_domain d l ->
  success (parse_papair_domain_full d l fail).
Proof.
  unfold parse_papair_domain_full.
  apply parse_papair_domain_fueled_complete.
  lia.
Defined.

Definition parse_papair (d : direction) (l : list papair_token) (fail : exception) :
  M (papair_d d * list papair_token) :=
  let! D := parse_papair_domain_full d l fail in
  let '(existT _ p (exist _ l _)) := parse_papair_pwc d l D fail in
  Return (p, l).

Lemma parse_papair_domain_full_P d l fail :
  parse_papair_domain_full d (token_P :: l) fail =
  let! Dl := parse_papair_domain_full Left l fail in
  let 'existT _ pl (exist _ l2 Gl) := parse_papair_pwc Left l Dl fail in
  let! Dr := parse_papair_domain_full Right l2 fail in
  Return (parse_papair_d_P l l2 d pl Dl Gl Dr).
Proof.
  unfold parse_papair_domain_full.
  simpl.
  destruct (parse_papair_domain_fueled Left l fail (Datatypes.length l)).
  - reflexivity.
  - simpl.
    destruct (parse_papair_pwc Left l p fail) as (lp, (l2, Gl)).
    assert (List.length l2 <= List.length l)
      by (apply parse_papair_graph_wf in Gl; lia).
    rewrite (parse_papair_domain_fuel_is_length _ _ _ _ H).
    reflexivity.
Qed.

Lemma parse_papair_P d l fail :
  parse_papair d (token_P :: l) fail =
  let! (pl, l2) := parse_papair Left l fail in
  let! (pr, l3) := parse_papair Right l2 fail in
  Return (Papair_d_P d pl pr, l3).
Proof.
  unfold parse_papair.
  rewrite (parse_papair_domain_full_P d l fail).
  unfold parse_papair_domain_full.
  case (parse_papair_domain_case Left l fail (List.length l)).
  - intro HFailed.
    rewrite HFailed; reflexivity.
  - intros (Dl, HDl); rewrite HDl; simpl.
    case_eq (parse_papair_pwc Left l Dl fail); intros pl (l2, Gl) HGl; simpl.
    case (parse_papair_domain_case Right l2 fail (List.length l2)).
    + intro HFailed.
      rewrite HFailed; reflexivity.
    + intros (Dr, HDr).
      rewrite HDr; simpl.
      change (parse_papair_pwc Left l _ fail) with (parse_papair_pwc Left l Dl fail).
      rewrite HGl.
      destruct (parse_papair_pwc Right) as (pr, (l3, Gr)).
      destruct (parse_papair_pwc Right) as (pr', (l3', Gr')).
      simpl.
      rewrite (parse_papair_graph_functional_1 Right l2 pr l3 pr' l3');
        try assumption.
      rewrite (parse_papair_graph_functional_2 Right l2 pr l3 pr' l3');
        try assumption.
      reflexivity.
Qed.

Fixpoint unparse_papair (d : direction) (p : papair_d d) : list papair_token :=
  match p with
  | Papair_d_P d pl pr =>
    token_P :: unparse_papair Left pl ++ unparse_papair Right pr
  | Papair_d_A => [token_A]
  | Papair_d_I => [token_I]
  end.

Lemma parse_unparse_papair d p l fail :
  parse_papair d (unparse_papair d p ++ l) fail = Return (p, l).
Proof.
  generalize dependent l.
  induction p; intro l; simpl.
  - rewrite parse_papair_P.
    rewrite <- app_assoc.
    rewrite IHp1; simpl.
    rewrite IHp2; simpl.
    reflexivity.
  - reflexivity.
  - reflexivity.
Qed.

Lemma unparse_parse_papair d p l fail l2 :
  parse_papair d l fail = Return (p, l2) <->
  l = (unparse_papair d p ++ l2)%list.
Proof.
  split.
  - intro H.
    unfold parse_papair in H.
    rewrite bind_eq_return in H.
    destruct H as (D, (HD, H)).
    destruct parse_papair_pwc as (p2, (l1, G)).
    injection H; intros; subst; clear H.
    apply success_eq_return in HD; clear D.
    induction G.
    + reflexivity.
    + reflexivity.
    + simpl.
      f_equal.
      rewrite <- app_assoc.
      transitivity (unparse_papair Left lp ++ l2)%list.
      * apply IHG1.
        apply parse_papair_domain_complete.
        eapply parse_papair_d_ex; eassumption.
      * f_equal.
        apply IHG2.
        apply parse_papair_domain_complete.
        eapply parse_papair_d_ex; eassumption.
  - intro; subst; apply parse_unparse_papair.
Qed.

Definition papair_d_to_string d (p : papair_d d) := unlex_papair (unparse_papair d p).

(* Usage: without the leading "P" but with the trailing "R". *)
Definition papair_of_string (s : string) :=
  let fail := Macro_expansion s in
  let! l : list papair_token := lex_papair s fail in
  let! (pl, l) :=
     parse_papair Left l fail in
  let! (pr, l) :=
     parse_papair Right l fail in
  match l with
  | nil => Return (Papair_P pl pr)
  | _ => Failed _ fail
  end.

Definition papair_to_string (p : papair) :=
  let 'Papair_P pl pr := p in
  "P" ++ papair_d_to_string Left pl ++ papair_d_to_string Right pr.

Lemma remove_final_R s1 s2 : s1 ++ "R" = s2 ++ "R" -> s1 = s2.
Proof.
  generalize dependent s2.
  induction s1 as [|c1 s1]; intros [|c2 s2].
  - reflexivity.
  - destruct s2; simpl; congruence.
  - destruct s1; simpl; congruence.
  - simpl.
    intro H; injection H.
    intro H2; specialize (IHs1 s2 H2); congruence.
Qed.

Lemma unlex_app l1 l2:
  unlex_papair l1 ++ unlex_papair l2 = unlex_papair (l1 ++ l2).
Proof.
  generalize dependent l2.
  induction l1; simpl.
  - reflexivity.
  - intro l2.
    rewrite append_assoc.
    f_equal.
    apply IHl1.
Qed.

Lemma papair_to_of_string p s :
  papair_of_string (s ++ "R") = Return p <->
  papair_to_string p = "P" ++ s.
Proof.
  unfold papair_to_string, papair_of_string.
  split.
  - case_eq (lex_papair (s ++ "R") (Macro_expansion (s ++ "R"))); [discriminate|]; simpl.
    intros l Hl.
    case_eq (parse_papair Left l (Macro_expansion (s ++ "R"))); [discriminate|]; simpl.
    intros (pl, l2) Hpl.
    case_eq (parse_papair Right l2 (Macro_expansion (s ++ "R"))); [discriminate|]; simpl.
    intros (pr, l3) Hpr.
    destruct l3; [|discriminate].
    apply unlex_lex_papair in Hl; simpl.
    apply remove_final_R in Hl; subst s.
    rewrite unparse_parse_papair in Hpl; subst l; simpl.
    rewrite unparse_parse_papair in Hpr; subst l2; simpl.
    intro H; apply unreturn in H; subst p.
    unfold papair_d_to_string.
    rewrite app_nil_r.
    rewrite unlex_app.
    reflexivity.
  - destruct p as (pl, pr).
    intro Hp.
    injection Hp; clear Hp; intro; subst s.
    unfold papair_d_to_string.
    rewrite unlex_app.
    rewrite lex_unlex_papair.
    simpl.
    rewrite parse_unparse_papair.
    simpl.
    rewrite <- (app_nil_r (unparse_papair Right pr)).
    rewrite parse_unparse_papair.
    reflexivity.
Qed.

Definition PAPAIR_of_string (s : string) :=
  let fail := Macro_expansion s in
  match s with
  | String "P" s =>
    let! p := papair_of_string s in
    let! H := assume _ fail in
    Return (PAPAIR p H)
  | _ => Failed _ fail
  end.

Definition PAPAIR_to_string papair := papair_to_string papair.

Lemma lex_papair_ends_in_R s fail :
  success (lex_papair s fail) ->
  exists s', s = s' ++ "R".
Proof.
  induction s as [|c s]; simpl.
  - contradiction.
  - destruct c as [[|] [|] [|] [|] [|] [|] [|] [|]]; try contradiction.
    + (* case I *)
      intro Hs.
      apply success_bind_arg in Hs.
      destruct (IHs Hs) as (s', Hs').
      subst s.
      exists (String "I" s').
      reflexivity.
    + (* case A *)
      intro Hs.
      apply success_bind_arg in Hs.
      destruct (IHs Hs) as (s', Hs').
      subst s.
      exists (String "A" s').
      reflexivity.
    + (* case R *)
      destruct s; [|contradiction].
      intros _.
      exists "".
      reflexivity.
    + (* case P *)
      intro Hs.
      apply success_bind_arg in Hs.
      destruct (IHs Hs) as (s', Hs').
      subst s.
      exists (String "P" s').
      reflexivity.
Qed.

Lemma parse_papair_ends_in_R s :
  success (papair_of_string s) ->
  exists s', s = s' ++ "R".
Proof.
  unfold papair_of_string.
  intro H.
  apply success_bind_arg in H.
  apply lex_papair_ends_in_R in H.
  assumption.
Qed.

Lemma papair_of_to_string s p :
  papair_of_string s = Return p
  <-> papair_to_string p ++ "R" = "P" ++ s.
Proof.
  split.
  - intro Hp.
    assert (exists s', s = s' ++ "R") as Hs by
        (apply parse_papair_ends_in_R; rewrite Hp; constructor).
    destruct Hs as (s', Hs).
    subst s.
    apply papair_to_of_string in Hp.
    rewrite Hp.
    apply append_assoc.
  - intro Hs.
    assert (exists s', s = s' ++ "R") as Hs'.
    + destruct (papair_to_string p) as [|c s2]; [discriminate|].
      destruct c as [[|][|][|][|][|][|][|][|]]; try discriminate.
      simpl in Hs.
      injection Hs; intros.
      exists s2.
      congruence.
    + destruct Hs' as (s', Hs').
      subst s.
      apply papair_to_of_string.
      apply remove_final_R.
      rewrite Hs.
      rewrite append_assoc.
      reflexivity.
Qed.

Definition UNPAPAIR_of_string (s : string) :=
  let fail := Macro_expansion s in
  match s with
  | String "U" (String "N" (String "P" s)) =>
    let! p := papair_of_string s in
    let! H := assume _ fail in
    Return (UNPAPAIR p H)
  | _ => Failed _ fail
  end.

Fixpoint tryall {A : Set} (l : list (M A)) (last : M A) : M A :=
  match l with
  | nil => last
  | cons a l => try a (tryall l last)
  end.

Definition of_string (s : String.string) :=
  tryall
    [
      let! p := primitives.of_string s in Return (Prim p);
      constant_macro_of_string s;
      IFop_of_string s;
      CMPop_of_string s;
      IFCMPop_of_string s;
      ASSERTop_of_string s;
      ASSERT_CMPop_of_string s;
      DUUP_of_string s;
      DIIP_of_string s;
      CADR_of_string s;
      SET_CADR_of_string s;
      MAP_CADR_of_string s;
      PAPAIR_of_string s;
      UNPAPAIR_of_string s
     ]
    (Failed _ (Macro_expansion s)).

Definition to_string (m : macro) : string :=
  match m with
  | Prim p => primitives.to_string p
  | FAIL => "FAIL"
  | ASSERT => "ASSERT"
  | ASSERT_NONE => "ASSERT_NONE"
  | ASSERT_SOME => "ASSERT_SOME"
  | ASSERT_LEFT => "ASSERT_LEFT"
  | ASSERT_RIGHT => "ASSERT_RIGHT"
  | IF_SOME => "IF_SOME"
  | IF_RIGHT => "IF_RIGHT"
  | IF_NIL => "IF_NIL"
  | IFop op => "IF" ++ op_to_string op
  | CMPop op => "CMP" ++ op_to_string op
  | IFCMPop op => "IFCMP" ++ op_to_string op
  | ASSERTop op => "ASSERT_" ++ op_to_string op
  | ASSERT_CMPop op => "ASSERT_CMP" ++ op_to_string op
  | DUUP n _ => DUUP_to_string n
  | DIIP n _ => DIIP_to_string n
  | CADR cadr _ => "C" ++ CADR_to_string cadr ++ "R"
  | SET_CADR cadr _ => "SET_C" ++ CADR_to_string cadr ++ "R"
  | MAP_CADR cadr _ => "MAP_C" ++ CADR_to_string cadr ++ "R"
  | PAPAIR papair _ => PAPAIR_to_string papair ++ "R"
  | UNPAPAIR papair _ => "UN" ++ PAPAIR_to_string papair ++ "R"
  end.

Lemma bind_return_simpl {A B : Set} (y : A) (f : A -> M B):
  (let! x := Return y in f x) = f y.
Proof.
  reflexivity.
Qed.

Lemma of_to_string m s :
  of_string s = Return m <-> to_string m = s.
Proof.
  split.
  - unfold of_string.
    case_eq (primitives.of_string s); simpl.
    + intros _ _.
      case_eq (constant_macro_of_string s); simpl; [|intros m1 H Hm1; apply unreturn in Hm1; subst m1; repeat primitives.continue m].
      intros _ _.
      case_eq (IFop_of_string s); simpl; [|intros m1 H Hm1; apply unreturn in Hm1; subst m1; repeat primitives.continue m].
      intros _ _.
      case_eq (CMPop_of_string s); simpl; [|intros m1 H Hm1; apply unreturn in Hm1; subst m1; repeat primitives.continue m].
      intros _ _.
      case_eq (IFCMPop_of_string s); simpl; [|intros m1 H Hm1; apply unreturn in Hm1; subst m1; repeat primitives.continue m].
      intros _ _.
      case_eq (ASSERTop_of_string s); simpl; [|intros m1 H Hm1; apply unreturn in Hm1; subst m1; repeat primitives.continue m].
      intros _ _.
      case_eq (ASSERT_CMPop_of_string s); simpl; [|intros m1 H Hm1; apply unreturn in Hm1; subst m1; repeat primitives.continue m].
      intros _ _.
      case_eq (DUUP_of_string s); simpl.
      * intros _ _.
        case_eq (DIIP_of_string s); simpl.
        -- intros _ _.
           case_eq (CADR_of_string s); simpl.
           ++ intros _ _.
              case_eq (SET_CADR_of_string s); simpl.
              ** intros _ _.
                 case_eq (MAP_CADR_of_string s); simpl.
                 --- intros _ _.
                     case_eq (PAPAIR_of_string s); simpl.
                     +++ intros _ _.
                         case_eq (UNPAPAIR_of_string s); simpl; [discriminate|].
                         intros m' Hm' Hm.
                         apply unreturn in Hm.
                         subst m.
                         do 3 primitives.continue m'.
                         simpl in Hm'.
                         case_eq (papair_of_string s);
                           [intros e He; rewrite He in Hm'; discriminate|].
                         intros p Hp.
                         rewrite Hp in Hm'.
                         simpl in Hm'.
                         case_eq (assume (nontrivial_papair p) (Macro_expansion (String "U" (String "N" (String "P" s))))); [intros e He; rewrite He in Hm'; discriminate|].
                         intros H HH.
                         rewrite HH in Hm'.
                         simpl in Hm'.
                         apply unreturn in Hm'.
                         subst m'.
                         simpl.
                         unfold PAPAIR_to_string.
                         apply papair_of_to_string in Hp.
                         rewrite Hp; reflexivity.
                     +++ intros m' Hm' Hm.
                         apply unreturn in Hm.
                         subst m.
                         primitives.continue m'.
                         simpl in Hm'.
                         case_eq (papair_of_string s);
                           [intros e He; rewrite He in Hm'; discriminate|].
                         intros p Hp.
                         rewrite Hp in Hm'.
                         simpl in Hm'.
                         case_eq (assume (nontrivial_papair p) (Macro_expansion (String "P" s))); [intros e He; rewrite He in Hm'; discriminate|].
                         intros H HH.
                         rewrite HH in Hm'.
                         simpl in Hm'.
                         apply unreturn in Hm'.
                         subst m'.
                         simpl.
                         unfold PAPAIR_to_string.
                         apply papair_of_to_string in Hp.
                         assumption.
                 --- intros m' Hm' Hm.
                     apply unreturn in Hm.
                     subst m.
                     primitives.continue m'.
                     primitives.continue m'.
                     primitives.continue m'.
                     primitives.continue m'.
                     primitives.continue m'.
                     simpl in *.
                     generalize dependent Hm'.
                     case_eq (CADR_of_string_aux s); [intros; discriminate|].
                     intros c Hc.
                     simpl.
                     apply CADR_of_to_string in Hc.
                     subst s.
                     destruct c as [[c|c|]|[c|c|]|]; try discriminate; simpl;
                       intro H; apply unreturn in H; subst;
                         reflexivity.
              ** intros m' Hm' Hm.
                 apply unreturn in Hm.
                 subst m.
                 primitives.continue m'.
                 primitives.continue m'.
                 primitives.continue m'.
                 primitives.continue m'.
                 primitives.continue m'.
                 simpl in *.
                 generalize dependent Hm'.
                 case_eq (CADR_of_string_aux s); [intros; discriminate|].
                 intros c Hc.
                 simpl.
                 apply CADR_of_to_string in Hc.
                 subst s.
                 destruct c as [[c|c|]|[c|c|]|]; try discriminate; simpl;
                   intro H; apply unreturn in H; subst;
                     reflexivity.
           ++ intros m' Hm' Hm.
              apply unreturn in Hm.
              subst m.
              primitives.continue m'.
              simpl in *.
              generalize dependent Hm'.
              case_eq (CADR_of_string_aux s); [intros; discriminate|].
              intros c Hc.
              apply CADR_of_to_string in Hc.
              subst s.
              simpl.
              destruct c as [[c|c|]|[c|c|]|]; try discriminate; simpl;
                intro H; apply unreturn in H; subst; reflexivity.
        -- intros m' H' H.
           apply unreturn in H; subst m'.
           primitives.continue m.
           simpl in *.
           generalize dependent H'.
           case_eq (read_iip s (Macro_expansion (String "D" s))); [discriminate|].
           simpl.
           intros n' Hn' H.
           apply read_iip_correct in Hn'.
           subst s.
           destruct n' as [|[|n']]; [discriminate|discriminate|].
           simpl in H.
           apply unreturn in H; subst.
           reflexivity.
      * intros m' H' H.
           apply unreturn in H; subst m'.
           primitives.continue m.
           simpl in *.
           generalize dependent H'.
           case_eq (read_uup s (Macro_expansion (String "D" s))); [discriminate|].
           simpl.
           intros n' Hn' H.
           apply read_uup_correct in Hn'.
           subst s.
           destruct n' as [|[|n']]; [discriminate|discriminate|].
           simpl in H.
           apply unreturn in H; subst.
           reflexivity.
    + intros p Hp.
      apply primitives.of_to_string in Hp.
      subst s.
      intro He.
      apply unreturn in He.
      subst m.
      reflexivity.
  - destruct m; try (intro; subst s; reflexivity).
    + intro Hs.
      apply primitives.of_to_string in Hs.
      unfold of_string.
      rewrite Hs.
      reflexivity.
    + intro; subst s;
        destruct o; reflexivity.
    + intro; subst s;
        destruct o; reflexivity.
    + intro; subst s;
        destruct o; reflexivity.
    + intro; subst s;
        destruct o; reflexivity.
    + intro; subst s;
        destruct o; reflexivity.
    + intro; subst s.
      destruct n as [|[|n]]; [contradiction|contradiction|].
      simpl in i.
      destruct i.
      unfold of_string.
      simpl.
      rewrite read_uup_make_string.
      simpl.
      reflexivity.
    + intro; subst s.
      destruct n as [|[|n]]; [contradiction|contradiction|].
      simpl in i.
      destruct i.
      unfold of_string.
      simpl.
      rewrite read_iip_make_string.
      simpl.
      reflexivity.
    + simpl.
      intro; subst s.
      destruct c as [[c|c|]|[c|c|]|]; destruct i;
        unfold of_string;
        simpl;
        rewrite CADR_to_of_string;
        reflexivity.
    + simpl.
      intro; subst s.
      destruct c as [c|c|]; destruct i;
        unfold of_string;
        simpl;
        rewrite CADR_to_of_string;
        reflexivity.
    + simpl.
      intro; subst s.
      destruct c as [c|c|]; destruct i;
        unfold of_string;
        simpl;
        rewrite CADR_to_of_string;
        reflexivity.
    + simpl.
      destruct p as (pl, pr).
      intro; subst s.
      unfold of_string.
      assert (primitives.of_string (PAPAIR_to_string (Papair_P pl pr) ++ "R") =
              Failed _ (Unknown_primitive
                 (String "P"
                    ((papair_d_to_string Left pl ++ papair_d_to_string Right pr) ++
                                                                                 "R"))))
      as Hf.
      * simpl.
        simpl in i.
        destruct pl; simpl.
        -- reflexivity.
        -- destruct pr; simpl.
           ++ reflexivity.
           ++ reflexivity.
           ++ contradiction.
        -- reflexivity.
      * rewrite Hf.
        simpl.
        clear Hf.
        fold papair_to_string.
        assert (papair_of_string
                  ((papair_d_to_string Left pl ++ papair_d_to_string Right pr) ++ "R") = Return (Papair_P pl pr))
        as Hp.
        -- apply papair_to_of_string.
           reflexivity.
        -- rewrite Hp.
           change (let! p : papair := Return ?m in ?F p) with (F m).
           rewrite bind_return_simpl.
           rewrite (assume_ok _ _ i).
           reflexivity.
    + simpl.
      destruct p as (pl, pr).
      intro; subst s.
      unfold of_string.
      assert (primitives.of_string (String "U" (String "N" (PAPAIR_to_string (Papair_P pl pr) ++ "R"))) =
              Failed _ (Unknown_primitive
                 (String "U" (String "N" (String "P"
                    ((papair_d_to_string Left pl ++ papair_d_to_string Right pr) ++
                                                                                 "R"))))))
      as Hf.
      * simpl.
        simpl in i.
        destruct pl; simpl.
        -- reflexivity.
        -- destruct pr; simpl.
           ++ reflexivity.
           ++ reflexivity.
           ++ contradiction.
        -- reflexivity.
      * rewrite Hf.
        simpl.
        clear Hf.
        fold papair_to_string.
        assert (papair_of_string
                  ((papair_d_to_string Left pl ++ papair_d_to_string Right pr) ++ "R") = Return (Papair_P pl pr))
        as Hp.
        -- apply papair_to_of_string.
           reflexivity.
        -- rewrite Hp.
           change (let! p : papair := Return ?m in ?F p) with (F m).
           rewrite bind_return_simpl.
           rewrite (assume_ok _ _ i).
           reflexivity.
Qed.
