Require Coq.Strings.String.
Require Import Ascii.
Require Import base.

Definition is_field_annotation (s : String.string) : bool :=
  match s with
  | String.String c _ => (c =? "%")%char
  | String.EmptyString => false
  end.

Inductive annotation :=
  Mk_field_annotation (s : String.string) (H : is_field_annotation s).
Definition annot_o := Datatypes.option annotation.

Module default_entrypoint.
  Import Coq.Strings.String.

  Definition default : annotation := Mk_field_annotation "%default" I.

End default_entrypoint.

Inductive simple_comparable_type : Set :=
| never
| string
| nat
| int
| bytes
| bool
| mutez
| address
| key_hash
| timestamp
| key
| unit
| signature
| chain_id.

Inductive comparable_type : Set :=
| Comparable_type_simple : simple_comparable_type -> comparable_type
| Cpair : comparable_type -> comparable_type -> comparable_type
| Coption : comparable_type -> comparable_type
| Cor : comparable_type -> comparable_type -> comparable_type.

Lemma comparable_type_dec (a b : comparable_type) : {a = b} + {a <> b}.
Proof.
  repeat decide equality.
Defined.

Definition memo_size := Datatypes.nat.

Inductive type : Set :=
| Comparable_type (_ : simple_comparable_type)
| option (a : type)
| list (a : type)
| set (a : comparable_type)
| contract (a : type)
| operation
| pair (a : type) (b : type)
| or (a : type) (b : type)
| lambda (a b : type)
| map (k : comparable_type) (v : type)
| big_map (k : comparable_type) (v : type)
| ticket (a : comparable_type)
| sapling_state (ms : memo_size)
| sapling_transaction (ms : memo_size)
| bls12_381_fr | bls12_381_g1 | bls12_381_g2.

Fixpoint comparable_type_to_type (c : comparable_type) : type :=
  match c with
  | Comparable_type_simple a => Comparable_type a
  | Cpair a b => pair (comparable_type_to_type a) (comparable_type_to_type b)
  | Coption a => option (comparable_type_to_type a)
  | Cor a b =>
    or (comparable_type_to_type a) (comparable_type_to_type b)
  end.


Coercion comparable_type_to_type : comparable_type >-> type.
Coercion Comparable_type_simple : simple_comparable_type >-> comparable_type.
(* Coercion Comparable_type : simple_comparable_type >-> type. *)

Fixpoint is_pushable (a : type) : Datatypes.bool :=
  match a with
  | operation | big_map _ _ | contract _ => false
  | Comparable_type _ | lambda _ _ | set _ | sapling_transaction _
  | bls12_381_fr | bls12_381_g1 | bls12_381_g2 => true
  | option ty
  | list ty
  | map _ ty => is_pushable ty
  | pair a b | or a b => is_pushable a && is_pushable b
  | ticket _ => false
  | sapling_state _ => false
  end.

Fixpoint is_passable (a : type) : Datatypes.bool :=
  match a with
  | operation => false
  | Comparable_type _ | lambda _ _ | set _ | contract _
  | sapling_transaction _
  | bls12_381_fr | bls12_381_g1 | bls12_381_g2 => true
  | option ty
  | list ty
  | map _ ty
  | big_map _ ty => is_passable ty
  | pair a b | or a b => is_passable a && is_passable b
  | ticket _ => true
  | sapling_state _ => true
  end.

Fixpoint is_storable (a : type) : Datatypes.bool :=
  match a with
  | operation | contract _ => false
  | Comparable_type _ | lambda _ _ | set _
  | sapling_transaction _
  | bls12_381_fr | bls12_381_g1 | bls12_381_g2 => true
  | option ty
  | list ty
  | map _ ty
  | big_map _ ty => is_storable ty
  | pair a b | or a b => is_storable a && is_storable b
  | ticket _ => true
  | sapling_state _ => true
  end.

Fixpoint is_packable (a : type) : Datatypes.bool :=
  match a with
  | operation | big_map _ _=> false
  | Comparable_type _ | lambda _ _ | set _ | contract _
  | sapling_transaction _
  | bls12_381_fr | bls12_381_g1 | bls12_381_g2 => true
  | option ty
  | list ty
  | map _ ty => is_packable ty
  | pair a b | or a b => is_packable a && is_packable b
  | ticket _ => false
  | sapling_state _ => false
  end.

Fixpoint is_big_map_value (a : type) : Datatypes.bool :=
  match a with
  | operation | big_map _ _=> false
  | Comparable_type _ | lambda _ _ | set _ | contract _
  | sapling_transaction _
  | bls12_381_fr | bls12_381_g1 | bls12_381_g2 => true
  | option ty
  | list ty
  | map _ ty => is_big_map_value ty
  | pair a b | or a b => is_big_map_value a && is_big_map_value b
  | ticket _ => true
  | sapling_state _ => false
  end.

Fixpoint is_duplicable (a : type) : Datatypes.bool :=
  match a with
  | operation | Comparable_type _ | lambda _ _ | set _ | contract _
  | sapling_transaction _
  | bls12_381_fr | bls12_381_g1 | bls12_381_g2 => true
  | option ty
  | list ty
  | map _ ty
  | big_map _ ty => is_duplicable ty
  | pair a b | or a b => is_duplicable a && is_duplicable b
  | ticket _ => false
  | sapling_state _ => true
  end.

Lemma type_dec (a b : type) : {a = b} + {a <> b}.
Proof.
  repeat decide equality.
Defined.

Lemma stype_dec (A B : Datatypes.list type) : {A = B} + {A <> B}.
Proof.
  decide equality; apply type_dec.
Defined.

Infix ":::" := (@cons type) (at level 60, right associativity).
Infix "+++" := (@app type) (at level 60, right associativity).
