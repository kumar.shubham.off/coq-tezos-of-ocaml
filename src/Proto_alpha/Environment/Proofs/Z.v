Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Compare.

Lemma compare_is_valid
  : Proofs.Compare.Valid.t id Z.compare.
  exact Compare.Valid.z.
Qed.

Axiom to_string_of_string_eq : forall s,
  to_string (of_string s) = s.
