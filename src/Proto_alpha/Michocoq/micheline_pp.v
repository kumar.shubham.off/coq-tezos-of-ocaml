Require Coq.Lists.List.
Require bytes_repr.
Require Import Ascii Coq.Strings.String ZArith.
Require Import base location error.
Require Import micheline_syntax.

Open Scope string_scope.

(* Length taken by a Micheline expression when printed on a single line *)
(* TODO: avoid computing Z.to_int twice for each number litteral *)
Fixpoint micheline_length (mich : loc_micheline String.string) (in_seq : bool) :=
  let '(Mk_loc_micheline (_, _, m)) := mich in
  match m with
  | NUMBER _ z => String.length (string_of_Z z)
  | STR _ s => 2 + String.length s
  | BYTES _ s => 2 + 2 * String.length s
  | SEQ nil => 2
  | SEQ es => List.fold_left (fun acc m => 2 + micheline_length m true + acc) es 0
  | PRIM (_, _, s) nil nil => String.length s
  | PRIM (_, _, s) annots es =>
    (if in_seq then 0 else 2) + String.length s +
    List.fold_left (fun acc m => 1 + micheline_length m false + acc) es 0 +
    List.fold_left (fun acc '(Mk_annot (_, _, annot)) => 1 + String.length annot + acc) annots 0
  end.

Fixpoint micheline_pp_single_line (mich : loc_micheline String.string) (in_seq : bool) :=
  let '(Mk_loc_micheline (_, _, m)) := mich in
  match m with
  | NUMBER _ z => string_of_Z z
  | STR _ s => """" ++ s ++ """"
  | BYTES _ bs => "0x" ++ (bytes_repr.to_string bs)
  | SEQ es => "{" ++ String.concat "; " (List.map (fun m => micheline_pp_single_line m true) es) ++ "}"
  | PRIM (_, _, s) nil nil => s
  | PRIM (_, _, s) annots es =>
    let annots_strings := List.map (fun '(Mk_annot (_, _, annot)) => annot) annots in
    let args_strings := List.map (fun m => micheline_pp_single_line m false) es in
    let res := s ++ " " ++ String.concat " " (List.app annots_strings args_strings) in
    (if in_seq then res else "(" ++ res ++")")
  end.

Fixpoint micheline_pp (mich : loc_micheline String.string) (indent : nat) (in_seq : bool)
         (seq_lf : bool) :=
  if (micheline_length mich in_seq) + indent <? 80 then
    micheline_pp_single_line mich in_seq
  else
  match mich with
  | Mk_loc_micheline (_, _, NUMBER _ z) => (string_of_Z z)
  | Mk_loc_micheline (_, _, STR _ s) => """"++s++""""
  | Mk_loc_micheline (_, _, BYTES _ bs) => "0x"++bytes_repr.to_string bs
  | Mk_loc_micheline (_, _, SEQ es) =>
    let indent_space := (make_string " " indent) in
    let separator := (";"  ++ lf ++ indent_space ++ "  ") in
    "{ " ++(String.concat separator
                          (List.map
                             (fun m => micheline_pp m (indent+2) true seq_lf)
                             es))
         ++ lf ++ indent_space ++ "}"
  | Mk_loc_micheline (_, _, PRIM (_, _, s) nil nil) => s
  | Mk_loc_micheline (_, _, PRIM (_, _, s) annots es) =>
    let newIndent := indent + 1 + String.length s in
    let separator := lf++(make_string " " newIndent) in
    let annots_strings := List.map (fun '(Mk_annot (_, _, annot)) => annot) annots in
    let args_strings := (List.map
                           (fun m =>
                              micheline_pp m newIndent false
                                           (negb (String.eqb s "PUSH")))
                           es) in
    let res := s++" "++
                (String.concat
                   separator
                   (List.app annots_strings args_strings)) in
    if in_seq then res else "("++res++")"
  end.
