Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require Import TezosOfOCaml.Proto_alpha.Destination_repr.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Signature.
Require TezosOfOCaml.Proto_alpha.Proofs.Contract_hash.
Require TezosOfOCaml.Proto_alpha.Proofs.Contract_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Tx_rollup_repr.

Lemma compare_is_valid : Compare.Valid.t id Destination_repr.compare.
Admitted.

Axiom of_b58_to_b58_eq : forall v, exists error,
  Contract_repr.of_b58check (Tx_rollup_repr.to_b58check v) =
  Pervasives.Error error.

Lemma encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True) Destination_repr.encoding.
Proof.
  Data_encoding.Valid.data_encoding_auto.
  intros [] ?;
    unfold Destination_repr.of_b58check, Destination_repr.to_b58check.
  { rewrite Contract_repr.of_b58_to_b58_eq.
    dtauto.
  }
  { match goal with
    | v : Tx_rollup_repr.t |- _ =>
      destruct (of_b58_to_b58_eq v) as [? H_eq];
      rewrite H_eq
    end.
    rewrite Tx_rollup_repr.of_b58_to_b58_eq; simpl.
    tauto.
  }
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.
