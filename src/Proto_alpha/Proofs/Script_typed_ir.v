Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Script_typed_ir.

Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Chain_id.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.
Require TezosOfOCaml.Proto_alpha.Environment.Proofs.Signature.

Module Script_signature.
  Import Proto_alpha.Script_typed_ir.Script_signature.

  Lemma make_get (s : t) : make (get s) = s.
  Proof.
    now destruct s.
  Qed.

  Lemma get_make (s : Signature.t) : get (make s) = s.
  Proof.
    reflexivity.
  Qed.

  Lemma encoding_is_valid : Data_encoding.Valid.t (fun _ => True) encoding.
  Proof.
    Data_encoding.Valid.data_encoding_auto.
  Qed.

  Definition canonize (s : t) : t :=
    make (Signature.canonize (get s)).

  Lemma compare_is_valid : Compare.Valid.t canonize compare.
  Proof.
    apply (Compare.Valid.equality (Compare.projection get Signature.compare));
      [sauto lq: on|].
    eapply Compare.Valid.f_implies.
    { apply Compare.Valid.projection.
      apply Signature.compare_is_valid.
    }
    { unfold canonize, get.
      hauto lq: on.
    }
  Qed.
End Script_signature.

Module Script_chain_id.
  Import Proto_alpha.Script_typed_ir.Script_chain_id.

  Lemma compare_is_valid : Compare.Valid.t id compare.
  Proof.
    apply (Compare.Valid.equality (
      let proj '(Chain_id_tag x) := x in
      Compare.projection proj Chain_id.compare
    )); [sauto q: on|].
    eapply Compare.Valid.f_implies.
    { apply Compare.Valid.projection.
      apply Chain_id.compare_is_valid.
    }
    { sauto q: on. }
  Qed.
End Script_chain_id.

Module Ticket.
  Module Valid.
    Import Script_typed_ir.ticket.

    Record t {a : Set} (x : Script_typed_ir.ticket a) : Prop := {
      amount : 0 <= let 'Alpha_context.Script_int.Num_tag x' := x.(amount) in x';
    }.
  End Valid.
End Ticket.
