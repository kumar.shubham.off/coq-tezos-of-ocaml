Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Tx_rollup_l2_qty.

Require Import TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.

Module Valid.
  Definition t (v : Tx_rollup_l2_qty.t) : Prop :=
    Int64.Valid.non_negative v.
End Valid.

Lemma encoding_is_valid : Data_encoding.Valid.t Valid.t
  Tx_rollup_l2_qty.encoding.
Admitted.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.