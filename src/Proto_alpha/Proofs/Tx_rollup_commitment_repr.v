Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Tx_rollup_commitment_repr.

Require Import TezosOfOCaml.Proto_alpha.Environment.Proofs.Blake2B.
Require Import TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.
Require TezosOfOCaml.Proto_alpha.Proofs.Tx_rollup_inbox_repr.
Require TezosOfOCaml.Proto_alpha.Proofs.Tx_rollup_level_repr.

Module Commitment_hash.
  Lemma encoding_is_valid :
    Data_encoding.Valid.t (fun _ => True)
      Tx_rollup_commitment_repr.Commitment_hash.encoding.
  Proof.
    apply Blake2B.Make_is_valid.
  Qed.
  #[global] Hint Resolve encoding_is_valid : Data_encoding_db.
End Commitment_hash.

Module Batch.
  Lemma encoding_is_valid :
    Data_encoding.Valid.t (fun _ => True)
      Tx_rollup_commitment_repr.Batch.encoding.
  Proof.
    Data_encoding.Valid.data_encoding_auto.
  Qed.
  #[global] Hint Resolve encoding_is_valid : Data_encoding_db.
End Batch.

Module Valid.
  Import Tx_rollup_commitment_repr.t.

  Record t (x : Tx_rollup_commitment_repr.t) : Prop := {
    level : Tx_rollup_level_repr.Valid.t x.(level);
  }.
End Valid.

Lemma encoding_is_valid :
  Data_encoding.Valid.t Valid.t Tx_rollup_commitment_repr.encoding.
Proof.
  Data_encoding.Valid.data_encoding_auto.
  sauto.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.
