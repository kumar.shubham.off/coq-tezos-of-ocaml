Require Import Coq.Program.Equality.

Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Script_interpreter.
Require TezosOfOCaml.Proto_alpha.Script_timestamp_repr.
Require TezosOfOCaml.Proto_alpha.Tez_repr.

Require Import TezosOfOCaml.Proto_alpha.Simulations.Script_typed_ir.
Require Import TezosOfOCaml.Proto_alpha.Simulations.Script_interpreter_defs.

Fixpoint eval_comb_gadt_witness {s f} (w : With_family.comb_gadt_witness s f)
  : Family.Stack_ty.to_Set s -> Family.Stack_ty.to_Set f :=
  match w with
  | With_family.Comb_one => fun xs => xs
  | With_family.Comb_succ w' => fun '(x, xs) =>
    let '(y, ys) := eval_comb_gadt_witness w' xs in
    ((x, y), ys)
  end.

Fixpoint eval_uncomb_gadt_witness {s f}
  (w : With_family.uncomb_gadt_witness s f) :
  Family.Stack_ty.to_Set s -> Family.Stack_ty.to_Set f :=
  match w with
  | With_family.Uncomb_one => fun xs => xs
  | With_family.Uncomb_succ w' => fun '((x, y), xs) =>
    (x, eval_uncomb_gadt_witness w' (y, xs))
  end.

Fixpoint eval_comb_get_gadt_witness {a b}
  (w : With_family.comb_get_gadt_witness a b)
  : Family.Ty.to_Set a -> Family.Ty.to_Set b :=
  match w with
  | With_family.Comb_get_zero => fun x => x
  | With_family.Comb_get_one =>
    fun '(x, _) => x
  | With_family.Comb_get_plus_two w' =>
    fun '(_, y) => eval_comb_get_gadt_witness w' y
  end.

Fixpoint eval_comb_set_gadt_witness {a b c}
  (w : With_family.comb_set_gadt_witness a b c)
  : Family.Ty.to_Set a -> Family.Ty.to_Set b -> Family.Ty.to_Set c :=
  match w with
  | With_family.Comb_set_zero => fun v _ => v
  | With_family.Comb_set_one => fun v '(_, y) => (v, y)
  | With_family.Comb_set_plus_two w' => fun v '(x, y) =>
      (x, eval_comb_set_gadt_witness w' v y)
  end.

Fixpoint eval_dup_n_gadt_witness {b s}
  (w : With_family.dup_n_gadt_witness s b)
  : Family.Stack_ty.to_Set s -> Family.Ty.to_Set b :=
  match w with
  | With_family.Dup_n_zero =>
      fun '(x, _) => x
  | With_family.Dup_n_succ w' =>
      fun '(_, xs) => eval_dup_n_gadt_witness w' xs
  end.

Fixpoint eval_stack_prefix_preservation_witness {s t u v}
  (w : With_family.stack_prefix_preservation_witness s t u v) :
  (Family.Stack_ty.to_Set s -> Family.Stack_ty.to_Set t) ->
  Family.Stack_ty.to_Set u -> Family.Stack_ty.to_Set v :=
  match w with
  | With_family.KRest =>
      fun f => f
  | With_family.KPrefix w' =>
      fun f '(x, stack) => (x, eval_stack_prefix_preservation_witness w' f stack)
  end.

Fixpoint eval_sppw_for_IDig {X s t u v}
  (w : With_family.stack_prefix_preservation_witness s t u v) :
  (Family.Stack_ty.to_Set s -> X * Family.Stack_ty.to_Set t) ->
  Family.Stack_ty.to_Set u -> X * Family.Stack_ty.to_Set v :=
  match w with
  | With_family.KRest =>
      fun f => f
  | With_family.KPrefix w' =>
      fun f '(y, stack) => 
      let '(x, stack') := eval_sppw_for_IDig w' f stack in
      (x, (y, stack'))
  end.

Fixpoint eval_sspw_for_IDropn {u s}
  (w : With_family.stack_prefix_preservation_witness u u s s) :
  Family.Stack_ty.to_Set s ->
  Family.Stack_ty.to_Set u.
  dependent destruction w. (*TODO: remove this?*)
  - exact (fun s => s).
  - exact (fun '(_,s) => eval_sspw_for_IDropn _ _ w s).
Defined.

Fixpoint kundip {s z u w t}
  (wit : With_family.stack_prefix_preservation_witness s z u w) :
  Family.Stack_ty.to_Set u ->
  With_family.kinstr w t ->
  Family.Stack_ty.to_Set s * With_family.kinstr z t :=
  match wit with
  | With_family.KPrefix wit' => fun '(a,s) i =>
    let k := With_family.IConst axiom a i in
    kundip wit' s k
  | With_family.KRest => fun s i => (s, i)
  end.

Fixpoint dep_step {s t f}
  (fuel : nat)
  (g : Local_gas_counter.outdated_context * Script_typed_ir.step_constants)
  (gas : Local_gas_counter.local_gas_counter)
  (i_value : With_family.kinstr s t)
  (ks : With_family.continuation t f)
  (accu_stack : Family.Stack_ty.to_Set s)
  {struct fuel}
  : M? (
    Family.Stack_ty.to_Set f *
    Local_gas_counter.outdated_context *
    Local_gas_counter.local_gas_counter
  ) :=
  match fuel with
  | 0%nat =>
    Error_monad.fail
      (Build_extensible "Out of fuel." unit tt)
  | Datatypes.S fuel' =>
    let '(ctxt, sc) := g in
    match Script_interpreter_defs.dep_consume_instr gas i_value accu_stack with
    | None =>
      Error_monad.fail
        (Build_extensible "Operation_quota_exceeded" unit tt)
    | Some gas =>
      match i_value, accu_stack, ks with
      | With_family.IHalt _, _, _ =>
          dep_next fuel' g gas ks accu_stack
      | With_family.IDrop _ k, (_, s), _ =>
          dep_step fuel' g gas k ks s
      | With_family.IDup _ k, (a, s), _ =>
          dep_step fuel' g gas k ks (a, (a, s))
      | With_family.ISwap _ k, (a, (b, s)), _ =>
          dep_step fuel' g gas k ks (b, (a, s))
      | With_family.IConst _ v k, _, _ =>
          dep_step fuel' g gas k ks (v, accu_stack)
      | With_family.ICons_some _ k, (a, s), _ =>
          dep_step fuel' g gas k ks (Some a, s)
      | With_family.ICons_none _ k, _, _ =>
          dep_step fuel' g gas k ks (None, accu_stack)
      | With_family.IIf_none _ b_none b_some k, (a, s), _ =>
          match a with
          | None =>
              dep_step fuel' g gas b_none axiom s
          | Some v =>
              dep_step fuel' g gas b_some axiom (v, s)
          end
      | With_family.IOpt_map _ body k, (a, s), _ =>
          match a with
          | None =>
              dep_step fuel' g gas k ks (None, s)
          | Some v =>
              dep_step fuel' g gas body axiom (v, s)
          end
      | With_family.ICons_pair _ k,  (a, (b, s)), _ =>
          dep_step fuel' g gas k ks ((a,b), s)
      | With_family.IUnpair _ k, ((a, b) , s), _ =>
          dep_step fuel' g gas k ks (a, (b, s))
      | With_family.ICar _ k, ((a, _), s), _ =>
          dep_step fuel' g gas k ks (a, s)
      | With_family.ICdr _ k, ((_, b), s), _ =>
          dep_step fuel' g gas k ks (b, s)
      | With_family.ICons_left _ k, (a, s), _ =>
          dep_step fuel' g gas k ks (Script_typed_ir.L a, s)
      | With_family.ICons_right _ k, (a, s), _ =>
          dep_step fuel' g gas k ks (Script_typed_ir.R a, s)
      | With_family.IIf_left _ b_left b_right k, (a, s), _ =>
          match a with
          | Script_typed_ir.L v =>
              dep_step fuel' g gas b_left axiom (v, s)
          | Script_typed_ir.R v =>
              dep_step fuel' g gas b_right axiom (v, s)
          end
      | With_family.ICons_list _ k, (a, (tl, s)), _ =>
          dep_step fuel' g gas k ks (Script_list.cons_value a tl, s)
      | With_family.INil _ k, s, _ =>
          dep_step fuel' g gas k ks (Script_list.empty, s)
      | With_family.IIf_cons _ b_cons b_nil k, (a, s), _ =>
          match a.(Script_typed_ir.boxed_list.elements) with
          | [] => dep_step fuel' g gas b_nil axiom s
          | hd :: tl =>
              let tl' :=
                Script_typed_ir.boxed_list.Build _
                  tl (a.(Script_typed_ir.boxed_list.length) - 1) in
              dep_step fuel' g gas b_cons axiom (hd, (tl', s))
          end
      | With_family.IList_map _ body k, (a, s), _ => axiom (*TODO*)
      | With_family.IList_size _ k, (a, s), _ =>
          let len :=
            Script_int_repr.of_int a.(Script_typed_ir.boxed_list.length) in
          dep_step fuel' g gas k ks (len, s)
      | With_family.IList_iter _ body k, (a, s), _ => axiom (*TODO*)
      | With_family.IEmpty_set _ ty k, s, _ =>
          let res := Script_set.empty axiom in (*TODO*)
          dep_step fuel' g gas k ks (res, s)
      | With_family.ISet_iter _ body k, (a,s), _ =>
          axiom (*TODO*)
      | With_family.ISet_mem _ k, (a, (set, s)), _ =>
          let res := Script_set.mem a set in
          dep_step fuel' g gas k ks (res, s)
      | With_family.ISet_update _ k, (a, (pres, (set, s))), _ =>
          let res := Script_set.update a pres set in
          dep_step fuel' g gas k ks (res, s)
      | With_family.ISet_size _ k, (a, s), _ =>
          let res := Script_set.size_value a in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IEmpty_map _ ty k, s, _ =>
          let res := Script_map.empty axiom in (*TODO*)
          dep_step fuel' g gas k ks (res, s)
      | With_family.IMap_map _ body k, (a, s), _ =>
          axiom (*TODO*)
      | With_family.IMap_iter _ _ k, s, _ =>
          axiom (*TODO*)
      | With_family.IMap_mem _ k, (a, (map, s)), _ =>
          let res := Script_map.mem a map in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IMap_get _ k, (a, (map, s)), _ =>
          let res := Script_map.get a map in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IMap_update _ k, (a, (v, (map, s))), _ =>
          let res := Script_map.update a v map in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IMap_get_and_update _ k, (a, (v, (map, s))), _ =>
          let map' := Script_map.update a v map in
          let v' := Script_map.get a map in
          dep_step fuel' g gas k ks (v', (map', s))
      | With_family.IMap_size _ k, (a, s), _ =>
          let res := Script_map.size_value a in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IEmpty_big_map _ tk tv k, s, _ =>
          let ebm := Script_ir_translator.empty_big_map axiom axiom in
          dep_step fuel' g gas k ks (ebm, s)
      | With_family.IBig_map_mem _ k, (a, (map, s)), _ =>
          axiom (*TODO*)
      | With_family.IBig_map_get _ k, (a, (map, s)), _ =>
          axiom (*TODO*)
      | With_family.IBig_map_update _ k, (a, (mv, (map, s))), _ =>
          axiom (*TODO*)
      | With_family.IBig_map_get_and_update _ k, (a, (v, (map, s))), _ =>
          axiom (*TODO*)
      | With_family.IAdd_seconds_to_timestamp _ k, (a, (t, s)), _ =>
          let res := Script_timestamp_repr.add_delta t a in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IAdd_timestamp_to_seconds _ k, (a, (n, s)), _ =>
          let res := Script_timestamp_repr.add_delta a n in
          dep_step fuel' g gas k ks (res, s)
      | With_family.ISub_timestamp_seconds _ k, (a, (x, s)), _ =>
          let res := Script_timestamp_repr.sub_delta a x in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IDiff_timestamps _ k, (a, (t, s)), _ =>
          let res := Script_timestamp_repr.diff_value a t in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IConcat_string_pair _ k, (a, (y, s)), _ =>
          let res := Script_string_repr.concat_pair a y in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IConcat_string _ k, (a, s), _ =>
          axiom (*TODO*)
      | With_family.ISlice_string _ k, (a, (len, (s, stack))), _ =>
          let offset := Script_int_repr.to_zint a in
          let s_length := Z.of_int (Script_string_repr.length s) in
          let length := Script_int_repr.to_zint len in
          if (offset <? s_length) && (Z.add offset length <=? s_length)
          then
            let s' :=
              Script_string_repr.sub s (Z.to_int offset) (Z.to_int length) in
            dep_step fuel' g gas k ks (Some s, stack)
          else
            dep_step fuel' g gas k ks (None, stack)
      | With_family.IString_size _ k, (a, s), _ =>
          let res :=
            Script_int_repr.abs (
              Script_int_repr.of_int (Script_string_repr.length a)
            ) in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IConcat_bytes_pair _ k, (x, (y, s)), _ =>
          let res := Bytes.cat x y in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IConcat_bytes _ k, (a, s), _ =>
          axiom (*TODO*)
      | With_family.ISlice_bytes _ k, (offset, (length, (s, stack))), _ =>
          let s_length := Z.of_int (Bytes.length s) in
          let offset' := Script_int_repr.to_zint offset in
          let length' := Script_int_repr.to_zint length in
          if (offset' <? s_length) && (Z.add offset' length' <=? s_length)
          then
            let s' := Bytes.sub s (Z.to_int offset') (Z.to_int length') in
            dep_step fuel' g gas k ks (Some s', stack)
          else
            dep_step fuel' g gas k ks (None, stack)
      | With_family.IBytes_size _ k, (a, s), _ =>
          let res :=
            Script_int_repr.abs (Script_int_repr.of_int (Bytes.length a)) in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IAdd_tez _ k, (x, (y, s)), _ =>
          let? z := Tez_repr.op_plusquestion x y in
          dep_step fuel' g gas k ks (z, s)
      | With_family.ISub_tez _ k, (x, (y, s)), _ =>
          let z := Tez_repr.sub_opt x y in
          dep_step fuel' g gas k ks (z, s)
      | With_family.ISub_tez_legacy _ k, (x, (y, s)), _ =>
          let? z := Tez_repr.op_minusquestion x y in
          dep_step fuel' g gas k ks (z, s)
      | With_family.IMul_teznat _ k, (a, s), _ =>
          axiom(*TODO*)
      | With_family.IMul_nattez _ k, (a, s), _ =>
          axiom (*TODO*)
      | With_family.IOr _ k, (x, (y, s)), _ =>
          dep_step fuel' g gas k ks ((x || y), s)
      | With_family.IAnd _ k, (x, (y, s)), _ =>
          dep_step fuel' g gas k ks ((x && y), s)
      | With_family.IXor _ k, (x, (y, s)), _ =>
          dep_step fuel' g gas k ks ((xorb x y), s)
      | With_family.INot _ k, (a, s), _ =>
          dep_step fuel' g gas k ks (negb a, s)
      | With_family.IIs_nat _ k, (a, s), _ =>
          let res := Script_int_repr.is_nat a in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IAbs_int _ k, (a, s), _ =>
          let res := Script_int_repr.abs a in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IInt_nat _ k, (a, s), _ =>
          dep_step fuel' g gas k ks (a, s)
      | With_family.INeg _ k, (a, s), _ =>
          let res := Script_int_repr.neg a in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IAdd_int _ k, (x, (y, s)), _ =>
          let res := Script_int_repr.add x y in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IAdd_nat _ k, (x, (y, s)), _ =>
          let res := Script_int_repr.add_n x y in
          dep_step fuel' g gas k ks (res, s)
      | With_family.ISub_int _ k, (x, (y, s)), _ =>
          let res := Script_int_repr.sub x y in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IMul_int _ k, (x, (y, s)), _ =>
          let res := Script_int_repr.mul x y in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IMul_nat _ k, (x, (y, s)), _ =>
          let res := Script_int_repr.mul_n x y in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IEdiv_teznat _ k, (x, (y, s)), _ =>
          let x' := Script_int_repr.of_int64 (Tez_repr.to_mutez x) in
          let res :=
            match Script_int_repr.ediv x' y with
            | None => None
            | Some (q,r) =>
              match (Script_int_repr.to_int64 q, Script_int_repr.to_int64 r) with
              | (Some q, Some r) =>
                match (Tez_repr.of_mutez q, Tez_repr.of_mutez r) with
                | (Some q', Some r') => Some (q', r')
                | _ => None (*dummy value*)
                end
              | _ => None (* dummy value *)
              end
            end in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IEdiv_tez _ k, (x, (y, s)), _ =>
          let x' :=
            Script_int_repr.abs (Script_int_repr.of_int64 (Tez_repr.to_mutez x)) in
          let y' :=
            Script_int_repr.abs (Script_int_repr.of_int64 (Tez_repr.to_mutez y)) in
          let res :=
            match Script_int_repr.ediv_n x' y' with
            | None => None
            | Some (q,r) =>
                match Script_int_repr.to_int64 r with
                | None => None (*dummy value*)
                | Some r' =>
                    match Tez_repr.of_mutez r' with
                    | None => None (*dummy value*)
                    | Some r'' => Some (q, r'')
                    end
                end
            end in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IEdiv_int _ k, (x, (y, s)), _ =>
          let res := Script_int_repr.ediv x y in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IEdiv_nat _ k, (x, (y, s)), _ =>
          let res := Script_int_repr.ediv_n x y in
          dep_step fuel' g gas k ks (res, s)
      | With_family.ILsl_nat _ k, (a, s), _ =>
          axiom (*TODO*)
      | With_family.ILsr_nat _ k, (a, s), _ =>
          axiom (*TODO*)
      | With_family.IOr_nat _ k, (x, (y, s)), _ =>
          let res := Script_int_repr.logor x y in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IAnd_nat _ k, (x, (y, s)), _ =>
          let res := Script_int_repr.logand x y in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IAnd_int_nat _ k, (x, (y, s)), _ =>
          let res := Script_int_repr.logand x y in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IXor_nat _ k, (x, (y, s)), _ =>
          let res := Script_int_repr.logxor x y in
          dep_step fuel' g gas k ks (res, s)
      | With_family.INot_int _ k, (x, s), _ =>
          let res := Script_int_repr.lognot x in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IIf _ b_true b_false k, (a, s), _ =>
          if a
          then
            dep_step fuel' g gas b_true axiom s
          else
            dep_step fuel' g gas b_false axiom s
      | With_family.ILoop _ body k, s, _ =>
          axiom (*TODO*)
      | With_family.ILoop_left _ bl br, s, _ =>
          axiom (*TODO*)
      | With_family.IDip _ b k, (a, s), _ =>
          let ks := axiom in 
          dep_step fuel' g gas b ks s
      | With_family.IExec _ k, s, _ =>
          axiom (*TODO*)
      | With_family.IApply _ ty k, (cap, (lam, s)), _ =>
          let? '(lam', ctxt', gas') :=
            Script_interpreter_defs.apply ctxt gas axiom cap lam in (*TODO*)
          dep_step fuel' (ctxt', sc) gas' k ks (lam', s)
      | With_family.ILambda _ lam k, s, _ =>
          let lam' := axiom in (*TODO*)
          dep_step fuel' g gas k ks (lam', s)
      | With_family.IFailwith _ _ _, s, _ =>
          axiom (*TODO*)
      | With_family.ICompare _ ty k, (a, (b, s)), _ =>
          let res :=
            Alpha_context.Script_int.of_int
              (Script_comparable.compare_comparable axiom a b ) in (*TODO*)
          dep_step fuel' g gas k ks (res, s)
      | With_family.IEq _ k, (a, s), _ =>
          let a' := Script_int_repr.compare a Script_int_repr.zero in
          let a'' := a' =i 0 in
          dep_step fuel' g gas k ks (a'', s)
      | With_family.INeq _ k, (a, s), _ =>
          let a' := Script_int_repr.compare a Script_int_repr.zero in
          let a'' := a' <>i 0 in
          dep_step fuel' g gas k ks (a'', s)
      | With_family.ILt _ k, (a, s), _ =>
          let a' := Script_int_repr.compare a Script_int_repr.zero in
          let a'' := a' <i 0 in
          dep_step fuel' g gas k ks (a'', s)
      | With_family.ILe _ k, (a, s), _ =>
          let a' := Script_int_repr.compare a Script_int_repr.zero in
          let a'' := a' <=i 0 in
          dep_step fuel' g gas k ks (a'', s)
      | With_family.IGt _ k, (a, s), _ =>
          let a' := Script_int_repr.compare a Script_int_repr.zero in
          let a'' := a' >i 0 in
          dep_step fuel' g gas k ks (a'', s)
      | With_family.IGe _ k, (a, s), _ =>
          let a' := Script_int_repr.compare a Script_int_repr.zero in
          let a'' := a' >=i 0 in
          dep_step fuel' g gas k ks (a'', s)
      | With_family.IPack _ ty k, (a, s), _ =>
          let? '(bytes_value, ctxt', gas') :=
            Local_gas_counter.use_gas_counter_in_context ctxt gas
            (fun ct => Script_ir_translator.pack_data ct axiom a) in (*TODO*)
          dep_step fuel' (ctxt', sc) gas' k ks (bytes_value, s)
      | With_family.IUnpack _ ty k, (a, s), _ =>
          let? '(bytes_value, ctxt', gas') :=
            Local_gas_counter.use_gas_counter_in_context ctxt gas
            (fun ct => Script_interpreter_defs.unpack ct axiom a) in (*TODO*)
          dep_step fuel' (ctxt', sc) gas' k axiom (bytes_value, s)
      | With_family.IAddress _ k, (a, s), _ =>
          dep_step fuel' g gas k ks (a.(Script_typed_ir.typed_contract.address), s)
      | With_family.IContract kinfo_value t_value entrypoint k_value,
          (addr, s),
          _ =>
          let entrypoint_opt :=
            if Alpha_context.Entrypoint.is_default
              addr.(Script_typed_ir.address.entrypoint)
            then
              Some entrypoint
            else
              if Alpha_context.Entrypoint.is_default entrypoint
              then
                Some addr.(Script_typed_ir.address.entrypoint)
              else
                None in
            match entrypoint_opt with
            | Some entrypoint =>
              let ctxt := Local_gas_counter.update_context gas ctxt in
              let? '(ctxt, maybe_contract) :=
                Script_ir_translator.parse_contract_for_script ctxt
                  (*kinfo_value.(Script_typed_ir.kinfo.iloc)*) axiom axiom
                  addr.(Script_typed_ir.address.destination) entrypoint in
                    let '(gas, ctxt) :=
                      Local_gas_counter.local_gas_counter_and_outdated_context
                        ctxt in
                    let accu_value := maybe_contract in
                    dep_step fuel' (ctxt, sc) gas k_value axiom (accu_value, s)
                  | None =>
                    dep_step fuel' (ctxt, sc) gas k_value axiom
                      (None, s)
                  end
      | With_family.ITransfer_tokens _ k, (p, (amount, (tcontract, s))), _ =>
          let tp := tcontract.(Script_typed_ir.typed_contract.arg_ty) in
          let destination :=
            tcontract.(Script_typed_ir.typed_contract.address)
              .(Script_typed_ir.address.destination) in
          let entrypoint :=
            tcontract.(Script_typed_ir.typed_contract.address)
              .(Script_typed_ir.address.entrypoint) in
          let? '(acc, ctxt', gas') :=
            Script_interpreter_defs.transfer (ctxt, sc) gas amount tp p
              destination entrypoint in
          dep_step fuel' (ctxt', sc) gas' k ks (acc, s)
      | With_family.IImplicit_account _ k, (key_value, s), _ =>
          let arg_ty := Script_typed_ir.unit_t in
          let contract := Alpha_context.Contract.implicit_contract key_value in
          let address :=
            {| Script_typed_ir.address.destination :=
              Alpha_context.Destination.Contract
                (Alpha_context.Contract.implicit_contract key_value);
              Script_typed_ir.address.entrypoint :=
                Alpha_context.Entrypoint.default |} in
          let res :=
            {| Script_typed_ir.typed_contract.arg_ty := arg_ty;
              Script_typed_ir.typed_contract.address := address |} in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IView _ (With_family.View_signature name input_ty output_ty) k_value,
        (accu_value, stack_value),
        _ =>
        let input := accu_value in
        let '(addr, stack_value) := stack_value in
        let ctxt := Local_gas_counter.update_context gas ctxt in
        let c_value := addr.(Script_typed_ir.address.destination) in
        let return_none (ctxt : Alpha_context.context) :=
          let '(gas, ctxt) :=
            Local_gas_counter.local_gas_counter_and_outdated_context ctxt in
          dep_step fuel' (ctxt, sc)
            gas k_value axiom
            (None, stack_value) in
        match c_value with
        | Alpha_context.Destination.Contract c_value =>
          let? '(ctxt, script_opt) :=
            Alpha_context.Contract.get_script ctxt c_value in
          match script_opt with
          | None => return_none ctxt
          | Some script =>
            let?
              '(Script_ir_translator.Ex_script {|
                Script_typed_ir.script.storage := storage_value;
                  Script_typed_ir.script.storage_type := storage_type;
                  Script_typed_ir.script.views := views
                  |}, ctxt) :=
              Script_ir_translator.parse_script None ctxt true true script
              in
            let 'existT _ __Ex_script_'b
              [ctxt, views, storage_type, storage_value] :=
              cast_exists (Es := Set)
                (fun __Ex_script_'b =>
                  [Alpha_context.context **
                    Script_typed_ir.view_map **
                    Script_typed_ir.ty ** __Ex_script_'b])
                [ctxt, views, storage_type, storage_value] in
            let? ctxt :=
              Alpha_context.Gas.consume ctxt
                (Script_interpreter_defs.Interp_costs.view_get name views)
              in
            match Script_map.get name views with
            | None => return_none ctxt
            | Some view =>
              let view_result :=
                (Script_ir_translator.parse_view_returning :
                  option Script_ir_translator.type_logger ->
                  Alpha_context.context -> bool -> Script_typed_ir.ty ->
                  Script_typed_ir.view ->
                  M?
                    (Script_ir_translator.ex_view * Alpha_context.context))
                  None ctxt true storage_type view in
              let? '(Script_ir_translator.Ex_view f_value, ctxt) :=
                Error_monad.trace_eval
                  (fun (function_parameter : unit) =>
                    let '_ := function_parameter in
                    Build_extensible "Ill_typed_contract"
                      (Micheline.canonical Alpha_context.Script.prim *
                        Script_tc_errors.type_map)
                      ((Micheline.strip_locations
                        view.(Script_typed_ir.view.view_code)), nil))
                  view_result in
              match f_value with
              |
                Script_typed_ir.Lam {|
                  Script_typed_ir.kdescr.kloc := kloc;
                    Script_typed_ir.kdescr.kbef :=
                      Script_typed_ir.Item_t bef_ty
                        Script_typed_ir.Bot_t;
                    Script_typed_ir.kdescr.kaft :=
                      Script_typed_ir.Item_t aft_ty
                        Script_typed_ir.Bot_t;
                    Script_typed_ir.kdescr.kinstr := kinstr
                    |} _script_view =>
                let? pair_ty :=
                  Script_typed_ir.pair_t kloc ((*input_ty*) axiom) (*TODO*)
                    storage_type in
                let io_ty :=
                  Gas_monad.Syntax.op_letstar
                    (Script_ir_translator.ty_eq
                      Script_tc_errors.Fast kloc aft_ty (*output_ty*) axiom)
                    (fun (out_eq : Script_ir_translator.eq) =>
                      Gas_monad.Syntax.op_letplus
                        (Script_ir_translator.ty_eq
                          Script_tc_errors.Fast kloc bef_ty pair_ty)
                        (fun (in_eq : Script_ir_translator.eq) =>
                          (out_eq, in_eq))) in
                let? '(eq_value, ctxt) := Gas_monad.run ctxt io_ty in
                match eq_value with
                |
                  Pervasives.Error
                    Script_tc_errors.Inconsistent_types_fast =>
                  return_none ctxt
                |
                  Pervasives.Ok
                    (Script_ir_translator.Eq, Script_ir_translator.Eq) =>
                  let kkinfo :=
                    Script_typed_ir.kinfo_of_kinstr (* k_value *) axiom in
                  match kkinfo.(Script_typed_ir.kinfo.kstack_ty) with
                  | Script_typed_ir.Item_t _ s_value =>
                    let kstack_ty :=
                      Script_typed_ir.Item_t (* output_ty *) axiom s_value in
                    let kkinfo :=
                      Script_typed_ir.kinfo.with_kstack_ty kstack_ty
                        kkinfo in
                    let ks :=
                      Script_typed_ir.KCons
                        (Script_typed_ir.ICons_some kkinfo (* k_value *) axiom) axiom in
                      axiom (*TODO*)
                      (*dep_step
                      ((Local_gas_counter.outdated ctxt),
                        (Script_typed_ir.step_constants.with_amount
                          Alpha_context.Tez.zero
                          (Script_typed_ir.step_constants.with_self
                            c_value
                            (Script_typed_ir.step_constants.with_source
                              sc.(Script_typed_ir.step_constants.self) sc))))
                      (Local_gas_counter.update_local_gas_counter ctxt)
                      axiom (*kinstr *)
                      (*(Script_typed_ir.KView_exit sc
                        (Script_typed_ir.KReturn stack_value ks))*)
                      axiom
                      axiom (*
                      ((input, storage_value) ,
                      (Script_typed_ir.EmptyCell,
                        Script_typed_ir.EmptyCell)) *) *)
                  | _ => unreachable_gadt_branch
                  end
                end
              | _ => unreachable_gadt_branch
              end
            end
          end
        | Alpha_context.Destination.Tx_rollup _ => return_none ctxt
        end
    | With_family.ICreate_contract
        _ storage_type arg_type (With_family.Lam _ code) views root_name k,
      (delegate, (credit, (init, stack_value))),
      _ =>
        let? '(res, contract, ctxt, gas) :=
          Script_interpreter_defs.create_contract
            g
            gas
            axiom (* storage_type *)
            axiom (* arg_type *)
            code
            views
            root_name
            delegate
            credit
            init
        in
        let stack_value :=
          ({|
            Script_typed_ir.address.destination :=
              Alpha_context.Destination.Contract contract;
            Script_typed_ir.address.entrypoint :=
              Alpha_context.Entrypoint.default |}, stack_value) in
        dep_step fuel' (ctxt, sc) gas k ks (res, stack_value)
    | With_family.ISet_delegate _ k, (delegate, s), _ =>
        let operation := Alpha_context.Delegation delegate in
        let ctxt := Local_gas_counter.update_context gas ctxt in
        let? '(ctxt, nonce_value) := Alpha_context.fresh_internal_nonce ctxt in
        let piop :=
          Alpha_context.Internal_operation
            {|
              Alpha_context.internal_operation.source :=
                sc.(Script_typed_ir.step_constants.self);
              Alpha_context.internal_operation.operation := operation;
              Alpha_context.internal_operation.nonce := nonce_value |}
          in
        let res :=
          {| Script_typed_ir.operation.piop := piop;
            Script_typed_ir.operation.lazy_storage_diff :=
              (None : option Alpha_context.Lazy_storage.diffs) |} in
        let '(gas, ctxt) :=
          Local_gas_counter.local_gas_counter_and_outdated_context ctxt in
        dep_step fuel' (ctxt, sc) gas k axiom (res, s)
     | With_family.IBalance _ k_value, s, _ =>
          let ctxt := Local_gas_counter.update_context gas ctxt in
          let? '(ctxt, balance) :=
          Alpha_context.Contract.get_balance_carbonated ctxt
            sc.(Script_typed_ir.step_constants.self) in
          let '(gas, ctxt) :=
            Local_gas_counter.local_gas_counter_and_outdated_context ctxt in
          let g_value := (ctxt, sc) in
          dep_step fuel' g_value gas k_value axiom (balance, s)
      | With_family.ILevel _ k, s, _ =>
          let res := sc.(Script_typed_ir.step_constants.level) in
          dep_step fuel' g gas k ks (res, s)
      | With_family.INow _ k, s, _ =>
          let res := sc.(Script_typed_ir.step_constants.now) in
          dep_step fuel' g gas k ks (res, s)
      | With_family.ICheck_signature _ k, (key, (signature, (message, s))), _ =>
          let res :=
            Script_typed_ir.Script_signature.check None key
              signature message in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IHash_key _ k, (key, s), _ =>
          let res :=
            Signature.Public_key.(S.SIGNATURE_PUBLIC_KEY.hash_value) key in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IBlake2b _ k, (bytes, s), _ =>
          let hash := Raw_hashes.blake2b bytes in
          dep_step fuel' g gas k ks (hash, s)
      | With_family.ISha256 _ k, (bytes, s), _ =>
          let hash := Raw_hashes.sha256 bytes in
          dep_step fuel' g gas k ks (hash, s)
      | With_family.ISha512 _ k, (bytes, s), _ =>
          let hash := Raw_hashes.sha512 bytes in
          dep_step fuel' g gas k ks (hash, s)
      | With_family.ISource _ k, s, _ =>
          let destination :=
            Alpha_context.Destination.Contract
              sc.(Script_typed_ir.step_constants.payer) in
          let res :=
            {|
              Script_typed_ir.address.destination := destination;
              Script_typed_ir.address.entrypoint :=
                Alpha_context.Entrypoint.default |} in
          dep_step fuel' g gas k ks (res, s)
      | With_family.ISender _ k, s, _ =>
          let destination :=
            Alpha_context.Destination.Contract
              sc.(Script_typed_ir.step_constants.source) in
          let res :=
            {|
              Script_typed_ir.address.destination := destination;
              Script_typed_ir.address.entrypoint :=
                Alpha_context.Entrypoint.default |} in
          dep_step fuel' g gas k ks (res, s)
      | With_family.ISelf _ ty entrypoint k, s, _ =>
          let destination :=
            Alpha_context.Destination.Contract
              sc.(Script_typed_ir.step_constants.self) in
          let res :=
            {| Script_typed_ir.typed_contract.arg_ty := With_family.to_ty ty;
              Script_typed_ir.typed_contract.address :=
                {|
                  Script_typed_ir.address.destination := destination;
                  Script_typed_ir.address.entrypoint := entrypoint |} |} in
          dep_step fuel' g gas k ks (res, s)
      | With_family.ISelf_address _ k, s, _ =>
          let destination :=
            Alpha_context.Destination.Contract
              sc.(Script_typed_ir.step_constants.self) in
          let res :=
            {|
              Script_typed_ir.address.destination := destination;
              Script_typed_ir.address.entrypoint :=
                Alpha_context.Entrypoint.default |} in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IAmount _ k, s, _ =>
          let a := sc.(Script_typed_ir.step_constants.amount) in
          dep_step fuel' g gas k ks (a, s)
      | With_family.IDig _ n w k, stack, _ =>
          let stack' :=
            eval_sppw_for_IDig w (fun x => x) stack in
          dep_step fuel' g gas k ks stack'
      | With_family.IDug _ n w k, (a, stack), _ =>
          let stack' :=
            eval_stack_prefix_preservation_witness w (pair a) stack in
          dep_step fuel' g gas k ks stack'
      | With_family.IDipn _ n w b k, s, _ =>
          let '(s', restore) := kundip w s k in
          let ks' := With_family.KCons restore ks in
          dep_step fuel' g gas b ks' s'
      | With_family.IDropn _ n w k, s, _ =>
          let s' := eval_sspw_for_IDropn w s in
          dep_step fuel' g gas k ks s'
      | With_family.ISapling_empty_state _ memo_size k, s, _ =>
          let state := Alpha_context.Sapling.empty_state None memo_size tt in
          dep_step fuel' g gas k ks (state, s)
      | With_family.ISapling_verify_update _ k_value, (accu_value, stack_value), _ =>
          let transaction := accu_value in
          let '(state_value, stack_value) := stack_value in
          let address :=
            Alpha_context.Contract.to_b58check
              sc.(Script_typed_ir.step_constants.self) in
          let chain_id :=
            Chain_id.to_b58check
              sc.(Script_typed_ir.step_constants.chain_id) in
          let anti_replay := Pervasives.op_caret address chain_id in
          let ctxt := Local_gas_counter.update_context gas ctxt in
          let? '(ctxt, balance_state_opt) :=
            Alpha_context.Sapling.verify_update ctxt state_value
              transaction anti_replay in
          let '(gas, ctxt) :=
            Local_gas_counter.local_gas_counter_and_outdated_context ctxt in
          match balance_state_opt with
          | Some (balance, state_value) =>
            let state_value :=
              Some
                ((Alpha_context.Script_int.of_int64 balance), state_value)
              in
            dep_step fuel' (ctxt, sc) gas k_value axiom
              (state_value, stack_value)
          | None =>
            dep_step fuel' (ctxt, sc) gas k_value axiom
              (None, stack_value)
          end
      | With_family.IChainId _ k, s, _ =>
          let res :=
            Script_typed_ir.Script_chain_id.make
              sc.(Script_typed_ir.step_constants.chain_id) in
          dep_step fuel' g gas k ks (res, s)
      | With_family.INever _, (n, s), _ =>
          match n with
          end
      | With_family.IVoting_power _ k_value, (key_hash, stack_value), _ =>
          let ctxt := Local_gas_counter.update_context gas ctxt in
          let? '(ctxt, rolls) :=
            Alpha_context.Vote.get_voting_power ctxt key_hash in
          let power :=
            Alpha_context.Script_int.abs
              (Alpha_context.Script_int.of_int32 rolls) in
          let '(gas, ctxt) :=
            Local_gas_counter.local_gas_counter_and_outdated_context ctxt in
          dep_step fuel' (ctxt, sc) gas k_value axiom (power, stack_value)
      | With_family.ITotal_voting_power _ k_value, s, _ =>
          let ctxt := Local_gas_counter.update_context gas ctxt in
          let? '(ctxt, rolls) :=
            Alpha_context.Vote.get_total_voting_power ctxt in
          let power :=
            Alpha_context.Script_int.abs
              (Alpha_context.Script_int.of_int32 rolls) in
          let '(gas, ctxt) :=
            Local_gas_counter.local_gas_counter_and_outdated_context ctxt in
          let g_value := (ctxt, sc) in
          dep_step fuel' g gas k_value axiom (power, s)
      | With_family.IKeccak _ k, (bytes, s), _ =>
          let hash := Raw_hashes.keccak256 bytes in
          dep_step fuel' g gas k ks (hash, s)
      | With_family.ISha3 _ k, (bytes, s), _ =>
          let hash := Raw_hashes.sha3_256 bytes in
          dep_step fuel' g gas k ks (hash, s)
      | With_family.IAdd_bls12_381_g1 _ k, (x, (y, s)), _ =>
          let res := Script_typed_ir.Script_bls.G1.add x y in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IAdd_bls12_381_g2 _ k, (x, (y, s)), _ =>
          let res := Script_typed_ir.Script_bls.G2.add x y in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IAdd_bls12_381_fr _ k, (x, (y, s)), _ =>
          let res := Script_typed_ir.Script_bls.Fr.add x y in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IMul_bls12_381_g1 _ k, (x, (y, s)), _ =>
          let res := Script_typed_ir.Script_bls.G1.mul x y in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IMul_bls12_381_g2 _ k, (x, (y, s)), _ =>
          let res := Script_typed_ir.Script_bls.G2.mul x y in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IMul_bls12_381_fr _ k, (x, (y, s)), _ =>
          let res := Script_typed_ir.Script_bls.Fr.mul x y in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IMul_bls12_381_fr_z _ k, (x, (y, s)), _ =>
          let x := Script_typed_ir.Script_bls.Fr.of_z (Script_int_repr.to_zint x) in
          let res := Script_typed_ir.Script_bls.Fr.mul x y in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IMul_bls12_381_z_fr _ k, (y, (x, s)), _ =>
          let x := Script_typed_ir.Script_bls.Fr.of_z (Script_int_repr.to_zint x) in
          let res := Script_typed_ir.Script_bls.Fr.mul x y in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IInt_bls12_381_fr _ k, (x, s), _ =>
          let res := Script_int_repr.of_zint (Script_typed_ir.Script_bls.Fr.to_z x) in
          dep_step fuel' g gas k ks (res, s)
      | With_family.INeg_bls12_381_g1 _ k, (x, s), _ =>
          let res := Script_typed_ir.Script_bls.G1.negate x in
          dep_step fuel' g gas k ks (res, s)
      | With_family.INeg_bls12_381_g2 _ k, (x, s), _ =>
          let res := Script_typed_ir.Script_bls.G2.negate x in
          dep_step fuel' g gas k ks (res, s)
      | With_family.INeg_bls12_381_fr _ k, (x, s), _ =>
          let res := Script_typed_ir.Script_bls.Fr.negate x in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IPairing_check_bls12_381 _ k, (pairs, s), _ =>
          let check :=
          Script_typed_ir.Script_bls.pairing_check
            pairs.(Script_typed_ir.boxed_list.elements) in
          dep_step fuel' g gas k ks (check, s)
      | With_family.IComb _ _ w k, s, _ =>
          let s' := eval_comb_gadt_witness w s in
          dep_step fuel' g gas k ks s'
      | With_family.IUncomb _ _ w k, s, _ =>
          let s' := eval_uncomb_gadt_witness w s in
          dep_step fuel' g gas k ks s'
      | With_family.IComb_get _ _ w k, (a, s), _ =>
          let res := eval_comb_get_gadt_witness w a in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IComb_set _ _ w k, (x, (y, s)), _ =>
          let res := eval_comb_set_gadt_witness w x y in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IDup_n _ _ w k, s, _ =>
          let res := eval_dup_n_gadt_witness w s in
          dep_step fuel' g gas k ks (res, s)
      | With_family.ITicket _ k, (contents, (amount, s)), _ =>
          let ticketer := sc.(Script_typed_ir.step_constants.self) in
          let res := Script_typed_ir.ticket.Build _ ticketer contents amount in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IRead_ticket _ k, (accu_value, stack_value), _ =>
          let '{|
            Script_typed_ir.ticket.ticketer := ticketer;
              Script_typed_ir.ticket.contents := contents;
              Script_typed_ir.ticket.amount := amount
              |} := accu_value in
          let stack_value := (accu_value, stack_value) in
          let destination := Alpha_context.Destination.Contract ticketer in
          let addr :=
            {| Script_typed_ir.address.destination := destination;
              Script_typed_ir.address.entrypoint :=
                Alpha_context.Entrypoint.default |} in
          let accu_value := (addr, (contents, amount)) in
          dep_step fuel' g gas k ks axiom (* TODO *)
      | With_family.ISplit_ticket _ k, (ticket, ((amount_a, amount_b), s)), _ =>
          let res :=
          if (Alpha_context.Script_int.compare
             (Alpha_context.Script_int.add_n amount_a amount_b)
             ticket.(Script_typed_ir.ticket.amount)) =i 0
          then Some ((Script_typed_ir.ticket.with_amount amount_a ticket),
                     (Script_typed_ir.ticket.with_amount amount_b ticket))
          else None in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IJoin_tickets _ ty k, ((ticket_a, ticket_b) , s), _ =>
          let res :=
          if ((Alpha_context.Contract.compare
               ticket_a.(Script_typed_ir.ticket.ticketer)
               ticket_b.(Script_typed_ir.ticket.ticketer)) =i 0) &&
             ((Script_comparable.compare_comparable axiom (*TODO*)
               ticket_a.(Script_typed_ir.ticket.contents)
               ticket_b.(Script_typed_ir.ticket.contents)) =i 0)
          then Some {|
                 Script_typed_ir.ticket.ticketer :=
                   ticket_a.(Script_typed_ir.ticket.ticketer);
                 Script_typed_ir.ticket.contents :=
                   ticket_a.(Script_typed_ir.ticket.contents);
                 Script_typed_ir.ticket.amount :=
                   Alpha_context.Script_int.add_n
                     ticket_a.(Script_typed_ir.ticket.amount)
                     ticket_b.(Script_typed_ir.ticket.amount) |}
          else None in
          dep_step fuel' g gas k ks (res, s)
      | With_family.IOpen_chest _ k, (chest_key, (chest, (time_z, s))), _ =>
          let res :=
            match Alpha_context.Script_int.to_int time_z with
            | None => Script_typed_ir.R false
            | Some time =>
              match
                Script_typed_ir.Script_timelock.open_chest chest chest_key time
              with
              | Timelock.Correct bytes_value =>
                Script_typed_ir.L bytes_value
              | Timelock.Bogus_cipher => Script_typed_ir.R false
              | Timelock.Bogus_opening => Script_typed_ir.R true
              end
            end in
          dep_step fuel' g gas k ks (res, s)
      end
    end
  end

  with dep_next {s f}
    (fuel : nat)
    (g : Local_gas_counter.outdated_context * Script_typed_ir.step_constants)
    (gas : Local_gas_counter.local_gas_counter)
    (ks : With_family.continuation s f)
    (stack : Family.Stack_ty.to_Set s)
    {struct fuel}
    : M? (
      Family.Stack_ty.to_Set f *
      Local_gas_counter.outdated_context *
      Local_gas_counter.local_gas_counter
    ) :=
    let '(ctxt, _) := g in
    let ks' := With_family.to_continuation ks in
    match fuel with
    | 0%nat =>
      Error_monad.fail
        (Build_extensible "Out of fuel." unit tt)
    | Datatypes.S fuel' =>
      match Script_interpreter_defs.consume_control gas ks' with
      | None => Error_monad.fail
                  (Build_extensible "Operation_quota_exceeded" unit tt)
      | Some gas' =>
          match ks, stack with
          | With_family.KNil, stack =>
              Pervasives.Ok (stack, ctxt, gas)
          | With_family.KCons k ks, stack =>
              dep_step fuel' g gas k ks stack
          | With_family.KLoop_in k ks0 as c, (a, s) =>
              dep_kloop_in fuel' g gas c k ks0 a s
          | With_family.KReturn stack' ks, (a,_) =>
              dep_next fuel' g gas ks (a, stack')
          | With_family.KMap_head fn ks, (a, s) =>
              dep_next fuel' g gas ks (fn a, s)
          | With_family.KLoop_in_left k ks0 as c, (a, s) =>
              dep_kloop_in_left fuel' g gas c k ks0 a s
          | With_family.KUndip b ks, s =>
              dep_next fuel' g gas ks (b, s)
          | With_family.KIter k xs ks0, (a, s) =>
              dep_kiter fuel' id g gas k xs ks0 a s
          | With_family.KList_enter_body k xs ys len ks, s =>
              dep_klist_enter fuel' id g gas k xs ys len ks s
          | With_family.KList_exit_body k xs ys len ks, (a, s) =>
              dep_klist_exit fuel' id g gas k xs ys len ks a s
          | With_family.KMap_enter_body k ps m ks, s =>
              dep_kmap_enter fuel' id g gas k ps m ks s
          | With_family.KMap_exit_body k xs ys yk ks, (a, s) =>
              dep_kmap_exit fuel' id g gas k xs ys yk ks a s
          | With_family.KView_exit constants' ks, stack =>
              let g' := (ctxt, constants') in
              dep_next fuel' g' gas ks stack
          end
      end
    end

  with dep_kloop_in {s c f}
    (fuel : nat)
    (g : Local_gas_counter.outdated_context * Script_typed_ir.step_constants)
    (gas : Local_gas_counter.local_gas_counter)
    (ks0 : With_family.continuation c f)
    (ki : With_family.kinstr s c)
    (ks : With_family.continuation s f)
    (acc : bool)
    (stack : Family.Stack_ty.to_Set s)
    {struct fuel}
    : M? (
      Family.Stack_ty.to_Set f *
      Local_gas_counter.outdated_context *
      Local_gas_counter.local_gas_counter
    ) :=
    match fuel with
    | 0%nat =>
      Error_monad.fail
        (Build_extensible "Out of fuel." unit tt)
    | Datatypes.S fuel' =>
      if acc
      then dep_step fuel' g gas ki ks0 stack
      else dep_next fuel' g gas ks stack
    end

  with dep_kloop_in_left {a b s c f}
    (fuel : nat)
    (g : Local_gas_counter.outdated_context * Script_typed_ir.step_constants)
    (gas : Local_gas_counter.local_gas_counter)
    (ks0 : With_family.continuation c f)
    (ki : With_family.kinstr (a :: s) c)
    (ks : With_family.continuation (b :: s) f)
    (acc : Family.Ty.to_Set (Family.Ty.Union a b))
    (stack : Family.Stack_ty.to_Set s)
    {struct fuel}
    : M? (
      Family.Stack_ty.to_Set f *
      Local_gas_counter.outdated_context *
      Local_gas_counter.local_gas_counter
    ) :=
    match fuel with
    | 0%nat =>
      Error_monad.fail
        (Build_extensible "Out of fuel." unit tt)
    | Datatypes.S fuel' =>
      match acc with
      | Script_typed_ir.L v =>
          dep_step fuel' g gas ki ks0 (v, stack)
      | Script_typed_ir.R v =>
          dep_next fuel' g gas ks (v, stack)
      end
    end

  with dep_kiter {a b s f}
    (fuel : nat)
    (mk : With_family.continuation (a :: s) f ->
          With_family.continuation (a :: s) f)
    (g : Local_gas_counter.outdated_context * Script_typed_ir.step_constants)
    (gas : Local_gas_counter.local_gas_counter)
    (ki : With_family.kinstr (b :: a :: s) (a :: s))
    (xs : list (Family.Ty.to_Set b))
    (ks : With_family.continuation (a :: s) f)
    (acc : Family.Ty.to_Set a)
    (stack : Family.Stack_ty.to_Set s)
    {struct fuel}
    : M? (
      Family.Stack_ty.to_Set f *
      Local_gas_counter.outdated_context *
      Local_gas_counter.local_gas_counter
    ) :=
    match fuel with
    | 0%nat =>
      Error_monad.fail
        (Build_extensible "Out of fuel." unit tt)
    | Datatypes.S fuel' =>
      match xs with
      | [] => dep_next fuel' g gas ks (acc, stack)
      | x :: xs' =>
        let ks' := mk (With_family.KIter ki xs' ks) in
        dep_step fuel' g gas ki ks' (x, (acc, stack))
      end
    end

  with dep_klist_enter {b j s f}
    (fuel : nat)
    (mk : With_family.continuation (b :: s) f ->
          With_family.continuation (b :: s) f)
    (g : Local_gas_counter.outdated_context * Script_typed_ir.step_constants)
    (gas : Local_gas_counter.local_gas_counter)
    (ki : With_family.kinstr (j :: s) (b :: s))
    (xs : list (Family.Ty.to_Set j))
    (ys : list (Family.Ty.to_Set b))
    (len : int)
    (ks : With_family.continuation (Family.Ty.List b :: s) f)
    (stack : Family.Stack_ty.to_Set s)
    {struct fuel}
    : M? (
      Family.Stack_ty.to_Set f *
      Local_gas_counter.outdated_context *
      Local_gas_counter.local_gas_counter
    ) :=
    match fuel with
    | 0%nat =>
      Error_monad.fail
        (Build_extensible "Out of fuel." unit tt)
    | Datatypes.S fuel' =>
      match xs with
      | [] => let ys' := Script_typed_ir.boxed_list.Build _ (List.rev ys) len in
        dep_next fuel' g gas ks (ys', stack)
      | x :: xs' => let ks' :=
        mk (With_family.KList_exit_body ki xs ys len ks) in
        dep_step fuel' g gas ki ks' (x, stack)
      end
    end

  with dep_klist_exit {i j s f}
    (fuel : nat)
    (mk : With_family.continuation s f ->
          With_family.continuation s f)
    (g : Local_gas_counter.outdated_context * Script_typed_ir.step_constants)
    (gas : Local_gas_counter.local_gas_counter)
    (ki : With_family.kinstr (i :: s) (j :: s))
    (xs : list (Family.Ty.to_Set i))
    (ys : list (Family.Ty.to_Set j))
    (len : int)
    (ks : With_family.continuation (Family.Ty.List j :: s) f)
    (acc : Family.Ty.to_Set j)
    (stack : Family.Stack_ty.to_Set s)
    {struct fuel}
    : M? (
      Family.Stack_ty.to_Set f *
      Local_gas_counter.outdated_context *
      Local_gas_counter.local_gas_counter
    ) :=
      match fuel with
      | 0%nat =>
        Error_monad.fail
          (Build_extensible "Out of fuel." unit tt)
      | Datatypes.S fuel' =>
        let ks' := mk (With_family.KList_enter_body ki xs (acc :: ys) len ks) in
        dep_next fuel' g gas ks' stack
      end

  with dep_kmap_enter {a j k s f}
    (fuel : nat)
    (mk : With_family.continuation (a :: s) f ->
          With_family.continuation (a :: s) f)
    (g : Local_gas_counter.outdated_context * Script_typed_ir.step_constants)
    (gas : Local_gas_counter.local_gas_counter)
    (ki : With_family.kinstr (Family.Ty.Pair j k :: s) (a :: s))
    (xs : list (Family.Ty.to_Set (Family.Ty.Pair j k)))
    (ys : Family.Ty.to_Set (Family.Ty.Map j a))
    (ks : With_family.continuation (Family.Ty.Map j a :: s) f)
    (stack : Family.Stack_ty.to_Set s)
    {struct fuel}
    : M? (
      Family.Stack_ty.to_Set f *
      Local_gas_counter.outdated_context *
      Local_gas_counter.local_gas_counter
    ) :=
    match fuel with
    | 0%nat =>
      Error_monad.fail
        (Build_extensible "Out of fuel." unit tt)
    | Datatypes.S fuel' =>
      match xs with
      | [] => dep_next fuel' g gas ks (ys, stack)
      | (xk, xv) :: xs' =>
          let ks' := mk (With_family.KMap_exit_body ki xs ys xk ks) in
          dep_step fuel' g gas ki ks' ((xk, xv), stack)
      end
    end

  with dep_kmap_exit {d f s h m n o}
    (fuel : nat)
    (mk : With_family.continuation d f -> With_family.continuation s h)
    (g : Local_gas_counter.outdated_context * Script_typed_ir.step_constants)
    (gas : Local_gas_counter.local_gas_counter)
    (ki : With_family.kinstr (Family.Ty.Pair m n :: d) (o :: d))
    (xs : list (Family.Ty.to_Set (Family.Ty.Pair m n)))
    (ys : Family.Ty.to_Set (Family.Ty.Map m o))
    (yk : Family.Ty.to_Set m)
    (ks : With_family.continuation (Family.Ty.Map m o :: d) f)
    (acc : Family.Ty.to_Set o)
    (stack : Family.Stack_ty.to_Set s)
    {struct fuel}
    : M? (
      Family.Stack_ty.to_Set h *
      Local_gas_counter.outdated_context *
      Local_gas_counter.local_gas_counter
    ) :=
    match fuel with
    | 0%nat =>
      Error_monad.fail
        (Build_extensible "Out of fuel." unit tt)
    | Datatypes.S fuel' =>
      let ys := Script_map.update yk (Some acc) ys in
      let ks' := mk (With_family.KMap_enter_body ki xs ys ks) in
      dep_next fuel' g gas ks' stack
    end.
