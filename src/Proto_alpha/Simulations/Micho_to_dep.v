Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Simulations.Script_typed_ir.
Require TezosOfOCaml.Proto_alpha.Michocoq.syntax.
Require Import TezosOfOCaml.Proto_alpha.Michocoq.semantics.
Require TezosOfOCaml.Proto_alpha.Simulations.Script_interpreter.

Fixpoint micho_ty_of_Ty (ty : Family.Ty.t) : syntax_type.type :=
  match ty with
  | Family.Ty.Unit =>
    syntax_type.Comparable_type syntax_type.unit
  | Family.Ty.Num Family.Ty.Num.Int =>
    syntax_type.Comparable_type syntax_type.int
  | Family.Ty.Num Family.Ty.Num.Nat =>
    syntax_type.Comparable_type syntax_type.nat
  | Family.Ty.Bool =>
    syntax_type.Comparable_type syntax_type.bool
  | _ => axiom
  end.

Definition Ty_of_micho_simple_comparable_ty
  (t : syntax_type.simple_comparable_type) : Family.Ty.t :=
  match t with
  | syntax_type.never =>
      Family.Ty.Never
  | syntax_type.string =>
      Family.Ty.String
  | syntax_type.nat =>
      Family.Ty.Num Family.Ty.Num.Nat
  | syntax_type.int =>
      Family.Ty.Num Family.Ty.Num.Int
  | syntax_type.bytes =>
      Family.Ty.Bytes
  | syntax_type.bool =>
      Family.Ty.Bool
  | syntax_type.mutez =>
      Family.Ty.Mutez
  | syntax_type.address =>
      Family.Ty.Address
  | syntax_type.key_hash =>
      Family.Ty.Key_hash
  | syntax_type.timestamp =>
      Family.Ty.Timestamp
  | syntax_type.key =>
      Family.Ty.Key
  | syntax_type.unit =>
      Family.Ty.Unit
  | syntax_type.signature =>
      Family.Ty.Signature
  | syntax_type.chain_id =>
      Family.Ty.Chain_id
  end.

Fixpoint Ty_of_micho_comparable_ty (t : syntax_type.comparable_type) :
  Family.Ty.t :=
  match t with
  | syntax_type.Comparable_type_simple a =>
      Ty_of_micho_simple_comparable_ty a
  | syntax_type.Cpair a b =>
      Family.Ty.Pair
        (Ty_of_micho_comparable_ty a)
        (Ty_of_micho_comparable_ty b)
  | syntax_type.Coption a =>
      Family.Ty.Option
        (Ty_of_micho_comparable_ty a)
  | syntax_type.Cor a b =>
      Family.Ty.Union
        (Ty_of_micho_comparable_ty a)
        (Ty_of_micho_comparable_ty b)
  end.

Fixpoint Ty_of_micho_ty(t : syntax_type.type) : Family.Ty.t :=
  match t with
  | syntax_type.Comparable_type c =>
      Ty_of_micho_simple_comparable_ty c
  | syntax_type.option a =>
      Family.Ty.Option
        (Ty_of_micho_ty a)
  | syntax_type.list a =>
      Family.Ty.List
        (Ty_of_micho_ty a)
  | syntax_type.set a =>
      Family.Ty.Set_
        (Ty_of_micho_comparable_ty a)
  | syntax_type.contract a =>
      Family.Ty.Contract
        (Ty_of_micho_ty a)
  | syntax_type.operation =>
      Family.Ty.Operation
  | syntax_type.pair a b =>
      Family.Ty.Pair
        (Ty_of_micho_ty a)
        (Ty_of_micho_ty b)
  | syntax_type.or a b =>
      Family.Ty.Union
        (Ty_of_micho_ty a)
        (Ty_of_micho_ty b)
  | syntax_type.lambda a b =>
      Family.Ty.Lambda
        (Ty_of_micho_ty a)
        (Ty_of_micho_ty b)
  | syntax_type.map a b =>
      Family.Ty.Map
        (Ty_of_micho_comparable_ty a)
        (Ty_of_micho_ty b)
  | syntax_type.big_map a b =>
      Family.Ty.Big_map
        (Ty_of_micho_comparable_ty a)
        (Ty_of_micho_ty b)
  | syntax_type.ticket a =>
      Family.Ty.Ticket
        (Ty_of_micho_comparable_ty a)
  | syntax_type.sapling_state _ =>
      Family.Ty.Sapling_state
  | syntax_type.sapling_transaction _ =>
      Family.Ty.Sapling_transaction
  | syntax_type.bls12_381_fr =>
      Family.Ty.Bls12_381_fr
  | syntax_type.bls12_381_g1 =>
      Family.Ty.Bls12_381_g1
  | syntax_type.bls12_381_g2 =>
      Family.Ty.Bls12_381_g2
  end.

(* TODO: this should work with recursion on
   first argument, like list concat
*)
Definition seq_instr {s f g} :
  With_family.kinstr s f ->
  With_family.kinstr f g ->
  With_family.kinstr s g.
Admitted.

Axiom kinfos : forall {s},
  With_family.kinfo s.

Axiom tys : forall {t},
  With_family.ty t.

Fixpoint translate_comb_get {n s t} (w : syntax.comb_get n s t) :
  With_family.comb_get_gadt_witness (Ty_of_micho_ty s) (Ty_of_micho_ty t) :=
  match w with
  | syntax.comb_get_0 _ =>
      With_family.Comb_get_zero
  | syntax.comb_get_1 _ _ =>
      With_family.Comb_get_one
  | syntax.comb_get_SSn n0 _ _ _ w0 =>
      With_family.Comb_get_plus_two
        (translate_comb_get w0)
  end.

Fixpoint translate_comb_set {n s t u} (w : syntax.comb_update n s t u) :
  With_family.comb_set_gadt_witness 
    (Ty_of_micho_ty t) (Ty_of_micho_ty s) (Ty_of_micho_ty u) :=
  match w with
  | syntax.comb_update_0 _ _ => 
    With_family.Comb_set_zero
  | syntax.comb_update_1 _ _ _ => 
    With_family.Comb_set_one
  | syntax.comb_update_SSn _ _ _ _ _ w' =>
    With_family.Comb_set_plus_two 
      (translate_comb_set w')
  end.

Fixpoint gen_uncomb_gadt_witness (t : syntax_type.type)
  (s s' : list (syntax_type.type)) {struct s}
  : With_family.uncomb_gadt_witness
      (map Ty_of_micho_ty (syntax.pairn t s :: s'))
      (map Ty_of_micho_ty (t :: s ++ s')) :=
  match s with
  | [] => With_family.Uncomb_one
  | t :: s'' =>
    With_family.Uncomb_succ (gen_uncomb_gadt_witness t s'' s')
  end.

Fixpoint gen_comb_gadt_witness (t : syntax_type.type)
  (s s' : list (syntax_type.type)) {struct s}
  : With_family.comb_gadt_witness
      (map Ty_of_micho_ty (t :: s ++ s'))
      (map Ty_of_micho_ty (syntax.pairn t s :: s')) :=
  match s with
  | [] => With_family.Comb_one
  | t :: s'' =>
    With_family.Comb_succ (gen_comb_gadt_witness t s'' s')
  end.

Axiom metadata :
  Script_typed_ir.ty_metadata.

Definition gen_comparable_ty_witness_simple
  (s : syntax_type.simple_comparable_type) :
  With_family.comparable_ty
  (Ty_of_micho_simple_comparable_ty s) :=
  match s with
  | syntax_type.never =>
      With_family.Never_key metadata
  | syntax_type.string =>
      With_family.String_key metadata
  | syntax_type.nat => With_family.Nat_key metadata
  | syntax_type.int => With_family.Int_key metadata
  | syntax_type.bytes =>
      With_family.Bytes_key metadata
  | syntax_type.bool =>
      With_family.Bool_key metadata
  | syntax_type.mutez =>
      With_family.Mutez_key metadata
  | syntax_type.address =>
      With_family.Address_key metadata
  | syntax_type.key_hash =>
      With_family.Key_hash_key metadata
  | syntax_type.timestamp =>
      With_family.Timestamp_key metadata
  | syntax_type.key => With_family.Key_key metadata
  | syntax_type.unit =>
      With_family.Unit_key metadata
  | syntax_type.signature =>
      With_family.Signature_key metadata
  | syntax_type.chain_id =>
      With_family.Chain_id_key metadata
  end.

Fixpoint gen_comparable_ty_witness (c : syntax_type.comparable_type) :
  With_family.comparable_ty
    (Ty_of_micho_comparable_ty c) :=
  match c with
  | syntax_type.Comparable_type_simple s =>
      gen_comparable_ty_witness_simple s
  | syntax_type.Cpair c1 c2 => With_family.Pair_key
      (gen_comparable_ty_witness c1)
      (gen_comparable_ty_witness c2) metadata
  | syntax_type.Cor c1 c2 => With_family.Union_key
      (gen_comparable_ty_witness c1)
      (gen_comparable_ty_witness c2) metadata
  | syntax_type.Coption c' => With_family.Option_key
      (gen_comparable_ty_witness c') metadata
  end.

Fixpoint gen_comparable_ty_witness2 (c : syntax_type.comparable_type) :
  With_family.comparable_ty 
    (Ty_of_micho_ty (syntax_type.comparable_type_to_type c)) :=
  match c with
  | syntax_type.Comparable_type_simple s =>
      gen_comparable_ty_witness_simple s
  | syntax_type.Cpair c1 c2 => With_family.Pair_key
      (gen_comparable_ty_witness2 c1)
      (gen_comparable_ty_witness2 c2) metadata
  | syntax_type.Cor c1 c2 => With_family.Union_key
      (gen_comparable_ty_witness2 c1)
      (gen_comparable_ty_witness2 c2) metadata
  | syntax_type.Coption c' => With_family.Option_key
      (gen_comparable_ty_witness2 c') metadata
  end.

Fixpoint gen_dup_n_gadt_witness l t l0 : With_family.dup_n_gadt_witness
  (@map syntax_type.type Family.Ty.t Ty_of_micho_ty  (l ++ t :: l0)%list) 
  (Ty_of_micho_ty t) := 
  match l with
  | [] => With_family.Dup_n_zero
  | x :: xs => With_family.Dup_n_succ (gen_dup_n_gadt_witness xs t l0)
  end.

Fixpoint gen_sppw_for_IDig t s l {struct l} :
  With_family.stack_prefix_preservation_witness
  (Ty_of_micho_ty t :: map Ty_of_micho_ty s)
  (map Ty_of_micho_ty s)
  (map Ty_of_micho_ty (l ++ t :: s))
  (map Ty_of_micho_ty (l ++ s)).
  destruct l.
  simpl.
  apply With_family.KRest.
  apply With_family.KPrefix.
  apply gen_sppw_for_IDig.
Defined.

Fixpoint gen_sppw_for_IDug t s l {struct l} :
  With_family.stack_prefix_preservation_witness
  (map Ty_of_micho_ty s)
  (map Ty_of_micho_ty (t :: s))
  (map Ty_of_micho_ty (l ++ s))
  (map Ty_of_micho_ty (l ++ t :: s)).
  destruct l.
  simpl.
  apply With_family.KRest.
  apply With_family.KPrefix.
  apply gen_sppw_for_IDug.
Defined.

Fixpoint gen_sppw_for_IDropn s s' {struct s} :
  With_family.stack_prefix_preservation_witness
  (map Ty_of_micho_ty s')
  (map Ty_of_micho_ty s')
  (map Ty_of_micho_ty (s ++ s'))
  (map Ty_of_micho_ty (s ++ s')).
  destruct s.
  simpl.
  apply With_family.KRest.
  apply With_family.KPrefix.
  apply gen_sppw_for_IDropn.
Defined.

Lemma Ty_of_micho_ty_comparable_type_to_type :
  forall c, Ty_of_micho_ty (syntax_type.comparable_type_to_type c)
   = Ty_of_micho_comparable_ty c.
Proof.
  induction c; simpl; congruence.
Defined.

Definition dep_of_micho_opcode {self_type s s'}
  (i : @syntax.opcode self_type s s')(f : list syntax_type.type) :
    With_family.kinstr (map Ty_of_micho_ty s') (map Ty_of_micho_ty f) ->
    With_family.kinstr (map Ty_of_micho_ty s) (map Ty_of_micho_ty f).
  refine ( (*:=*)
  match i with
  | syntax.APPLY => With_family.IApply kinfos tys
  | syntax.DUP _ => With_family.IDup kinfos
  | syntax.SWAP => With_family.ISwap kinfos
  | syntax.UNIT => With_family.IConst kinfos _
  | syntax.EQ => With_family.IEq kinfos
  | syntax.NEQ => With_family.INeq kinfos
  | syntax.LT => With_family.ILt kinfos
  | syntax.GT => With_family.IGt kinfos
  | syntax.LE => With_family.ILe kinfos
  | syntax.GE => With_family.IGe kinfos
  | @syntax.OR _ _ v _ =>
    match v with
    | syntax.Bitwise_variant _ bv =>
      match bv with
      | syntax.Bitwise_variant_bool => With_family.IOr kinfos
      | syntax.Bitwise_variant_nat => With_family.IOr_nat kinfos
      end
    end
  | @syntax.AND _ _ _ v _ =>
    match v with
    | syntax.Mk_and _ _ _ vr => 
      match vr with
      | syntax.And_variant_bool => With_family.IAnd kinfos
      | syntax.And_variant_nat => With_family.IAnd_nat kinfos
      | syntax.And_variant_int => With_family.IAnd_int_nat kinfos
      end
    end
  | @syntax.XOR _ _ v _ =>
    match v with
    | syntax.Bitwise_variant _ bv =>
      match bv with
      | syntax.Bitwise_variant_bool =>
          With_family.IXor kinfos
      | syntax.Bitwise_variant_nat =>
          With_family.IXor_nat kinfos
      end
    end
  | @syntax.NOT _ _ v _ =>
    match v with
    | syntax.Mk_not _ _ vr => 
      match vr with
      | syntax.Not_variant_bool => With_family.INot kinfos
      | syntax.Not_variant_nat => With_family.INot_int kinfos
      | syntax.Not_variant_int => With_family.INot_int kinfos
      end
    end
  | @syntax.NEG _ _ v _ =>
    match v with
    | syntax.Mk_neg _ _ vr => 
      match vr with
      | syntax.Neg_variant_nat => With_family.INeg kinfos
      | syntax.Neg_variant_int => With_family.INeg kinfos
      | syntax.Neg_variant_fr => With_family.INeg_bls12_381_fr kinfos
      | syntax.Neg_variant_g1 => With_family.INeg_bls12_381_g1 kinfos
      | syntax.Neg_variant_g2 => With_family.INeg_bls12_381_g2 kinfos
      end
    end
  | syntax.ABS => With_family.IAbs_int kinfos
  | syntax.ISNAT => With_family.IIs_nat kinfos
  | @syntax.INT _ _ v _ =>
    match v with
    | syntax.Mk_int _ vr =>
      match vr with
      | syntax.Int_variant_nat => With_family.IInt_nat kinfos
      | syntax.Int_variant_fr => With_family.IInt_bls12_381_fr kinfos
      end
    end
  | @syntax.ADD _ _ _ v _ =>
    match v with
    | syntax.Mk_add _ _ _ vr => 
      match vr with
      | syntax.Add_variant_nat_nat => With_family.IAdd_nat kinfos
      | syntax.Add_variant_nat_int => With_family.IAdd_int kinfos
      | syntax.Add_variant_int_nat => With_family.IAdd_int kinfos
      | syntax.Add_variant_int_int => With_family.IAdd_int kinfos
      | syntax.Add_variant_timestamp_int =>
          With_family.IAdd_timestamp_to_seconds kinfos
      | syntax.Add_variant_int_timestamp =>
          With_family.IAdd_seconds_to_timestamp kinfos
      | syntax.Add_variant_tez_tez => With_family.IAdd_tez kinfos
      | syntax.Add_variant_fr => With_family.IAdd_bls12_381_fr kinfos
      | syntax.Add_variant_g1 => With_family.IAdd_bls12_381_g1 kinfos
      | syntax.Add_variant_g2 => With_family.IAdd_bls12_381_g2 kinfos
      end
    end
  | @syntax.SUB _ _ _ v _ =>
    match v with
    | syntax.Mk_sub _ _ _ vr => 
      match vr with
      | syntax.Sub_variant_nat_nat => With_family.ISub_int kinfos
      | syntax.Sub_variant_nat_int => With_family.ISub_int kinfos
      | syntax.Sub_variant_int_nat => With_family.ISub_int kinfos
      | syntax.Sub_variant_int_int => With_family.ISub_int kinfos
      | syntax.Sub_variant_timestamp_int =>
          With_family.ISub_timestamp_seconds kinfos
      | syntax.Sub_variant_timestamp_timestamp =>
          With_family.IDiff_timestamps kinfos
      | syntax.Sub_variant_tez_tez => With_family.ISub_tez_legacy kinfos
      end
    end
  | @syntax.MUL _ _ _ v _ =>
    match v with
    | syntax.Mk_mul _ _ _ vr =>
      match vr with
      | syntax.Mul_variant_nat_nat => With_family.IMul_nat kinfos
      | syntax.Mul_variant_nat_int => With_family.IMul_int kinfos
      | syntax.Mul_variant_int_nat => With_family.IMul_int kinfos
      | syntax.Mul_variant_int_int => With_family.IMul_int kinfos
      | syntax.Mul_variant_tez_nat => With_family.IMul_teznat kinfos
      | syntax.Mul_variant_nat_tez => With_family.IMul_nattez kinfos
      | syntax.Mul_variant_fr_fr => With_family.IMul_bls12_381_fr kinfos
      | syntax.Mul_variant_g1_fr => With_family.IMul_bls12_381_g1 kinfos
      | syntax.Mul_variant_g2_fr => With_family.IMul_bls12_381_g2 kinfos
      | syntax.Mul_variant_fr_nat =>
          With_family.IMul_bls12_381_z_fr kinfos
      | syntax.Mul_variant_fr_int =>
          With_family.IMul_bls12_381_z_fr kinfos
      | syntax.Mul_variant_nat_fr =>
          With_family.IMul_bls12_381_fr_z kinfos
      | syntax.Mul_variant_int_fr =>
          With_family.IMul_bls12_381_fr_z kinfos
      end
    end
  | @syntax.EDIV _ _ _ v _ =>
    match v with
    | syntax.Mk_ediv _ _ _ _ vr => 
      match vr with
      | syntax.Ediv_variant_nat_nat => With_family.IEdiv_nat kinfos
      | syntax.Ediv_variant_nat_int => With_family.IEdiv_int kinfos
      | syntax.Ediv_variant_int_nat => With_family.IEdiv_int kinfos
      | syntax.Ediv_variant_int_int => With_family.IEdiv_int kinfos
      | syntax.Ediv_variant_tez_nat => With_family.IEdiv_teznat kinfos
      | syntax.Ediv_variant_tez_tez => With_family.IEdiv_tez kinfos
      end
    end
  | syntax.LSL => With_family.ILsl_nat kinfos
  | syntax.LSR => With_family.ILsr_nat kinfos
  | @syntax.COMPARE _ v _ => 
    With_family.ICompare kinfos (gen_comparable_ty_witness2 v)
  | @syntax.CONCAT _ _ v _ =>
    match v with
    | syntax.Mk_stringlike _ vr =>
      match vr with
      | syntax.Stringlike_variant_string =>
        With_family.IConcat_string_pair kinfos
      | syntax.Stringlike_variant_bytes =>
        With_family.IConcat_bytes_pair kinfos
      end
    end
  | @syntax.CONCAT_list _ _ v _ =>
    match v with
    | syntax.Mk_stringlike _ vr =>
      match vr with
      | syntax.Stringlike_variant_string =>
        With_family.IConcat_string kinfos
      | syntax.Stringlike_variant_bytes =>
          With_family.IConcat_bytes kinfos
      end
    end
  | @syntax.SIZE _ _ v _=>
    match v with
    | syntax.Mk_size _ vr =>
      match vr with
      | syntax.Size_variant_set _ => With_family.ISet_size kinfos
      | syntax.Size_variant_map key val => With_family.IMap_size kinfos
      | syntax.Size_variant_list _ => With_family.IList_size kinfos
      | syntax.Size_variant_string => With_family.IString_size kinfos
      | syntax.Size_variant_bytes => With_family.IBytes_size kinfos
      end
    end
  | @syntax.SLICE _ _ v _ =>
    match v with
    | syntax.Mk_stringlike _ vr =>
      match vr with
      | syntax.Stringlike_variant_string =>
        With_family.ISlice_string kinfos
      | syntax.Stringlike_variant_bytes =>
        With_family.ISlice_bytes kinfos
      end
    end
  | syntax.PAIR => With_family.ICons_pair kinfos
  | syntax.CAR => With_family.ICar kinfos
  | syntax.CDR => With_family.ICdr kinfos
  | syntax.UNPAIR => With_family.IUnpair kinfos
  | @syntax.EMPTY_SET _ v _  =>
    With_family.IEmpty_set kinfos (gen_comparable_ty_witness v)
  | @syntax.MEM _ _ _ v _  => _
  | @syntax.UPDATE _ _ _ _ v _ => _
  | @syntax.EMPTY_MAP _ k _ _ =>
    With_family.IEmpty_map kinfos (gen_comparable_ty_witness k)
  | @syntax.EMPTY_BIG_MAP _ k _ _ _ =>
    With_family.IEmpty_big_map kinfos (gen_comparable_ty_witness k) tys
  | @syntax.GET => _
  | @syntax.GET_AND_UPDATE => _
  | syntax.SOME => With_family.ICons_some kinfos
  | @syntax.NONE _ v _ => With_family.ICons_none kinfos
  | @syntax.LEFT _ _ _ => With_family.ICons_left kinfos
  | @syntax.RIGHT _ _ _ => With_family.ICons_right kinfos
  | @syntax.CONS _ _ _ => With_family.ICons_list kinfos
  | @syntax.NIL _ v _ => With_family.INil kinfos
  | @syntax.TRANSFER_TOKENS _ _ _ _ => With_family.ITransfer_tokens kinfos
  | syntax.SET_DELEGATE => With_family.ISet_delegate kinfos
  | @syntax.BALANCE _ v => With_family.IBalance kinfos
  | syntax.ADDRESS => With_family.IAddress kinfos
  | @syntax.CONTRACT _ _ v _ _ => _
  | @syntax.SOURCE _ _ => With_family.ISource kinfos
  | @syntax.SENDER _ _ => With_family.ISender kinfos
  | @syntax.AMOUNT => With_family.IAmount kinfos
  | syntax.IMPLICIT_ACCOUNT => With_family.IImplicit_account kinfos
  | @syntax.NOW => With_family.INow kinfos
  | @syntax.LEVEL => With_family.ILevel kinfos
  | syntax.PACK _ => With_family.IPack kinfos tys
  | syntax.UNPACK _ _ => With_family.IUnpack kinfos tys
  | syntax.HASH_KEY => With_family.IHash_key kinfos
  | syntax.BLAKE2B => With_family.IBlake2b kinfos
  | syntax.SHA256 => With_family.ISha256 kinfos
  | syntax.SHA512 => With_family.ISha512 kinfos
  | syntax.KECCAK => With_family.IKeccak kinfos
  | syntax.SHA3 => With_family.ISha3 kinfos
  | syntax.CHECK_SIGNATURE => With_family.ICheck_signature kinfos
  | @syntax.DIG _ _ _ _ _ _ => _
  | @syntax.DUG _ _ _ _ _ _ => _
  | @syntax.DROP _ _ _ _ _ => _
  | @syntax.DUPN _ _ _ _ _ _ _ => _
  | @syntax.CHAIN_ID => With_family.IChainId kinfos
  | @syntax.SELF_ADDRESS => With_family.ISelf_address kinfos
  | syntax.VOTING_POWER => With_family.IVoting_power kinfos
  | @syntax.TOTAL_VOTING_POWER => With_family.ITotal_voting_power kinfos
  | @syntax.PAIRN _ n t u s s' _ => 
    With_family.IComb kinfos 
      (Z.of_nat n) (gen_comb_gadt_witness t (u :: s) s')
  | @syntax.UNPAIRN _ n t u s s' _ =>
    With_family.IUncomb kinfos 
      (Z.of_nat n) (gen_uncomb_gadt_witness t (u :: s) s')
  | syntax.GETN n w =>
      With_family.IComb_get kinfos (Z.of_nat n) (translate_comb_get w)
  | syntax.UPDATEN n w => 
      With_family.IComb_set kinfos (Z.of_nat n) _
  | @syntax.TICKET => _
  | @syntax.READ_TICKET => _
  | syntax.SPLIT_TICKET => With_family.ISplit_ticket kinfos
  | @syntax.JOIN_TICKETS => _
  | @syntax.SAPLING_EMPTY_STATE _ _ _  => _
  | syntax.SAPLING_VERIFY_UPDATE => With_family.ISapling_verify_update kinfos
  | syntax.PAIRING_CHECK => With_family.IPairing_check_bls12_381 kinfos
  end
  ).
  { exact tt. }
  {
    destruct v.
    destruct mem_variant_field.
    {
      simpl; rewrite Ty_of_micho_ty_comparable_type_to_type;
      apply With_family.ISet_mem; apply kinfos.
    }
    {
      simpl; rewrite Ty_of_micho_ty_comparable_type_to_type;
      apply With_family.IMap_mem; apply kinfos.
    }
    {
      simpl; rewrite Ty_of_micho_ty_comparable_type_to_type;
      apply With_family.IBig_map_mem; apply kinfos.
    }
  }
  {
    destruct v.
    destruct update_variant_field.
    { 
      simpl; rewrite Ty_of_micho_ty_comparable_type_to_type;
      apply With_family.ISet_update; apply kinfos.
    }
    { 
      simpl; rewrite Ty_of_micho_ty_comparable_type_to_type;
      apply With_family.IMap_update; apply kinfos.
    }
    { 
      simpl; rewrite Ty_of_micho_ty_comparable_type_to_type;
      apply With_family.IBig_map_update; apply kinfos.
    }
  }
  {
    destruct g.
    destruct get_variant_field.
    {
      simpl; rewrite Ty_of_micho_ty_comparable_type_to_type;
      apply With_family.IMap_get; apply kinfos.
    }
    {
      simpl; rewrite Ty_of_micho_ty_comparable_type_to_type;
      apply With_family.IBig_map_get; apply kinfos.
    }
  }
  {
    destruct g.
    destruct get_variant_field.
    {
      simpl; rewrite Ty_of_micho_ty_comparable_type_to_type;
      apply With_family.IMap_get_and_update; apply kinfos.
    }
    {
      simpl; rewrite Ty_of_micho_ty_comparable_type_to_type;
      apply With_family.IBig_map_get_and_update; apply kinfos.
    }
  }
  {
    apply With_family.IContract. apply kinfos.
    { apply tys. }
    {
      destruct v.
      - apply a.
      - exact "". (* TODO: Need to confirm if empty string this is valid here *)
    }
  }
  {
    (* @syntax.DIG *)
    eapply With_family.IDig.
    { apply kinfos. }
    { exact (Z.of_nat n). }
    { apply gen_sppw_for_IDig. }
  }
  {
    (* @syntax.DUG *)
    eapply With_family.IDug.
    { apply kinfos. }
    { exact (Z.of_nat n). }
    { apply gen_sppw_for_IDug. }
  }
  {
    (* @syntax.DROP *)
    apply With_family.IDropn.
    { apply kinfos. }
    { exact (Z.of_nat n). }
    { apply gen_sppw_for_IDropn. }
  }
  {
    apply With_family.IDup_n. apply kinfos.
    { apply (Z.of_nat n). }
    { apply gen_dup_n_gadt_witness. }
  }
  { apply axiom.
    (* refine (translate_comb_set w). *)
    (* @syntax.UPDATEN *)
  }
  {
    simpl; rewrite Ty_of_micho_ty_comparable_type_to_type;
    apply With_family.ITicket; apply kinfos.
  }
  { pose (@With_family.IRead_ticket
      (Ty_of_micho_comparable_ty c)
      (map Ty_of_micho_ty l)
      (map Ty_of_micho_ty f)
      ).
    simpl.
    intro.
    apply k.
    { apply kinfos. }
    { apply With_family.ICons_pair.
      apply kinfos.
      rewrite Ty_of_micho_ty_comparable_type_to_type
        in H.
      exact H.
    }
    (* @syntax.READ_TICKET *)
  }
  {
    apply With_family.IJoin_tickets. apply kinfos.
    rewrite <- Ty_of_micho_ty_comparable_type_to_type.
    apply gen_comparable_ty_witness2.
  }
  {
    apply With_family.ISapling_empty_state. apply kinfos.
    (* G: Alpha_context.Sapling.Memo_size.t *)
    (* @syntax.SAPLING_EMPTY_STATE *)
    apply axiom.
  }
Defined.

Fixpoint dep_of_micho_iseq {self_type tff s f}
  (i : @syntax.instruction_seq self_type tff s f) : 
  With_family.kinstr (map Ty_of_micho_ty s) (map Ty_of_micho_ty f) :=
  match i with
  | _ => axiom
  end

  with dep_of_micho_inst {self_type tff s s'}
    (i : @syntax.instruction self_type tff s s') : forall f,
    With_family.kinstr (map Ty_of_micho_ty s') (map Ty_of_micho_ty f) ->
    With_family.kinstr (map Ty_of_micho_ty s) (map Ty_of_micho_ty f) :=
    match i with
    | syntax.Instruction_seq seq =>
        axiom
    | syntax.Instruction_opcode o =>
        dep_of_micho_opcode o
    | _ => axiom
    end.

Module Correctness(C : semantics.ContractContext).

  Module S := semantics.Semantics C.

(*comparable.simple_comparable_data s ->
Family.Ty.to_Set
  (Ty_of_micho_simple_comparable_ty s)*)

  Axiom cheat : nat -> forall {X},X.
  (* nat arg to help determine where
     axiom is coming from *)

  Definition convert_simple_data_comparable (c : syntax_type.simple_comparable_type)
    : comparable.simple_comparable_data c ->
      Family.Ty.to_Set (Ty_of_micho_simple_comparable_ty c).
    destruct c eqn:G; simpl; try (exact (fun x => x)).
    - exact (fun e => match e with end).
    - exact Script_string_repr.String_tag.
    - exact (fun n => Script_int_repr.Num_tag (Z.of_N n)).
    - exact Script_int_repr.Num_tag.
    - exact (fun x => Tez_repr.Tez_tag (Int63.to_Z x)).
    - exact (cheat 0). (*TODO*)
    - exact (cheat 1). (*TODO*)
    - exact Script_timestamp_repr.Timestamp_tag.
    - exact (cheat 2). (*TODO*)
    - exact (cheat 3). (*TODO*)
    - exact (cheat 4). (*TODO*)
  Defined.

  
  Fixpoint convert_data_comparable {c}
    : comparable.comparable_data c ->
      Family.Ty.to_Set (Ty_of_micho_comparable_ty c) :=
    match c with
    | syntax_type.Comparable_type_simple a => convert_simple_data_comparable a
    | syntax_type.Cpair a b => fun s => 
      match s with
      | (c0, c1) => (convert_data_comparable c0, convert_data_comparable c1)
      end
    | syntax_type.Coption a => fun s => 
      match s with
      | Some v => Some (convert_data_comparable v)
      | None => None
      end
    | syntax_type.Cor a b => fun s => 
      match s with
      | inl c0 => Script_typed_ir.L (convert_data_comparable c0)
      | inr c1 => Script_typed_ir.R (convert_data_comparable c1)
      end
    end.
  
  Definition address_constant_to_contract_t : 
    comparable.address_constant -> Alpha_context.Contract.t.
    intros.
    destruct H eqn:G.
    - apply Alpha_context.Contract.Implicit.
      apply axiom. (* TODO: public_key_hash have 3 constructor and 
        key_hash_constant just one with a string*)
    - apply Alpha_context.Contract.Originated.
      pose Contract_hash.H.(S.HASH.t).
      unfold S.HASH.t in S.
      apply axiom. (* TODO: Make_t is opaque. Ask Guillaume. *)
  Defined.

  Fixpoint convert_data (t : syntax_type.type) :
    S.data t -> Family.Ty.to_Set (Ty_of_micho_ty t).
    unfold S.data.
    destruct t; simpl.
    - apply convert_simple_data_comparable.
    - simpl.
      apply Option.map.
      apply convert_data.
    - exact (fun xs =>
        let ys := map (convert_data t) xs in
        Script_typed_ir.boxed_list.Build
          _ ys (length ys)).
    - exact (cheat 5). (*TODO*)
    - apply (cheat 6). (*TODO*)
    - apply (cheat 7). (*TODO*)
    - exact (fun '(x,y) =>
        (convert_data t1 x,
         convert_data t2 y)).
    - exact (fun s =>
        match s with
        | inl x => Script_typed_ir.L (convert_data t1 x)
        | inr y => Script_typed_ir.R (convert_data t2 y)
        end).
    - apply (cheat 8). (*TODO*)
    - apply (cheat 9). (*TODO*)
    - apply (cheat 10). (*TODO*)
    -  intro ticket.
      destruct ticket.
      constructor.
      + simpl in ticketer.
        apply address_constant_to_contract_t.
        exact ticketer.
      + apply (convert_data_comparable content).
      + constructor.
        unfold Alpha_context.Script_int.repr.
        unfold Z.t.
        apply Z.of_N.
        exact tamount.
    - apply (cheat 12). (*TODO*)
    - apply (cheat 13). (*TODO*)
    - apply (cheat 14). (*TODO*)
    - apply (cheat 15). (*TODO*)
    - apply (cheat 16). (*TODO*)
  Defined.

  Fixpoint Stack_ty_of_stack {f} {struct f} : S.stack f ->
    Family.Stack_ty.to_Set (map Ty_of_micho_ty f) :=
    match f with
    | [] => fun _ => tt
    | t :: g => fun '(x, ys) =>
      (convert_data t x, Stack_ty_of_stack ys)
    end.

  Definition to_error {A E} (e : Pervasives.result A E) : error.M A :=
    match e with
    | Pervasives.Ok a => error.Return a
    | Pervasives.Error _ =>
        error.Failed _ error.Parsing_Out_of_Fuel (*TODO: fix*)
    end.

  Definition error_map {A B} (f : A -> B)
    (e : error.M A) : error.M B :=
    match e with
    | error.Return a => error.Return (f a)
    | error.Failed _ e => error.Failed _ e
    end.

  Definition local_gas_counter_of_nat (n : nat) :
    Local_gas_counter.local_gas_counter :=
    Local_gas_counter.Local_gas_counter (Z.of_nat n).

  (* varying gas/fuel does not alter a successful outcome *)
  Lemma dep_step_returns_one_result : forall s t stack g gas1 gas2 s1 s2 fuel1 fuel2
    (i : With_family.kinstr s t),
    Script_interpreter.dep_step fuel1 g gas1 i With_family.KNil stack = Pervasives.Ok s1 ->
    Script_interpreter.dep_step fuel2 g gas2 i With_family.KNil stack = Pervasives.Ok s2 ->
    s1 = s2.
  Admitted.

  (*TODO: find a better location for this*)
  Fixpoint stack_app {s t} :
    Family.Stack_ty.to_Set s ->
    Family.Stack_ty.to_Set t ->
    Family.Stack_ty.to_Set (s ++ t)%list :=
    match s with
    | [] => fun _ st => st
    | x :: s' => fun '(x, st) st' => (x, stack_app st st')
    end.

  Lemma eval_gen_sppw_IDig :
    forall s t s'
      (stack : S.stack (s ++ t :: s')%list)
      (st1 : S.stack s)
      (st2 : S.stack (t :: s')),
      stack = S.stack_app st1 st2 ->
       Script_interpreter.eval_sppw_for_IDig
          (gen_sppw_for_IDig t s' s)
          (fun x => x)
          (Stack_ty_of_stack stack) = 
       (convert_data _ (fst st2),
          (Stack_ty_of_stack (S.stack_app st1 (snd st2)))).
    induction s; intros.
    { simpl.
      destruct stack.
      simpl in *.
      rewrite <- H; reflexivity.
    }
    { simpl.
      destruct stack.
      simpl in *.
      destruct st1.
      rewrite (IHs _ _ _ s1 st2).
      { destruct st2; simpl.
        inversion H; reflexivity.
      }
      { inversion H; auto. }
    }
  Qed.

  Lemma eval_gen_sppw_IDug :
    forall s t s'
      (stack : S.stack (s ++ s')%list)
      (st1 : S.stack s)
      (st2 : S.stack s')
      (x : S.data t),
      stack = S.stack_app st1 st2 ->
        Script_interpreter.eval_stack_prefix_preservation_witness
          (gen_sppw_for_IDug t s' s)
          (pair (convert_data t x))
          (Stack_ty_of_stack stack) =
        Stack_ty_of_stack
          (@S.stack_app _ (t :: s') st1 (x, st2)).
    induction s; intros.
    { simpl in *; congruence. }
    { simpl in *.
      destruct stack.
      destruct st1.
      inversion H.
      f_equal.
      apply IHs; auto.
    }
  Qed.

  Lemma eval_gen_sppw_IDropn :
    forall s s' (st1 : S.stack s) (st2 : S.stack s'),
      Script_interpreter.eval_sspw_for_IDropn
        (gen_sppw_for_IDropn s s')
        (Stack_ty_of_stack (S.stack_app st1 st2))
      = Stack_ty_of_stack st2.
    induction s; intros.
    { reflexivity. }
    { destruct st1; simpl. apply IHs. }
  Qed.

  Lemma stack_app_split : forall s s' (stack : S.stack (s ++ s')%list) st st',
    S.stack_split stack = (st, st') -> S.stack_app st st' = stack.
    induction s; intros.
    { inversion H; reflexivity. }
    { inversion H.
      destruct stack.
      destruct (S.stack_split s0) eqn:G.
      destruct st.
      inversion H1.
      pose (IHs _ _ _ _ G).
      simpl; f_equal; congruence.
    }
  Qed.

  Lemma dep_of_micho_opcode_correct : forall
    {self_type} {A B C}
    (fuel2 fuel1 : nat)
    (env : @S.proto_env self_type)
    (o : @syntax.opcode self_type A B)
    (g : Local_gas_counter.outdated_context * Script_typed_ir.step_constants)
    (gas1 gas2 : Local_gas_counter.local_gas_counter)
    (i : With_family.kinstr (map Ty_of_micho_ty B) (map Ty_of_micho_ty C))
    (stack : S.stack A)
    s s1 s2,
    S.eval_opcode _ env o stack = error.Return s ->
      let s' := Stack_ty_of_stack s in
    Script_interpreter.dep_step
      fuel1 g gas1 i With_family.KNil s' = Pervasives.Ok s1 ->
    Script_interpreter.dep_step
      fuel2 g gas2 (dep_of_micho_opcode o C i) With_family.KNil
      (Stack_ty_of_stack stack) = Pervasives.Ok s2 -> s1 = s2.
  Proof.
    destruct fuel2; intros.
    { discriminate. }
    { destruct o.
      { (* APPLY *) 
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        destruct s0.
        destruct d0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        (* TODO: remove axiom *)
        admit.
      }
      { (* DUP *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        inversion H.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        eapply dep_step_returns_one_result; eauto.
      }
      { (* SWAP *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        destruct s0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        simpl Stack_ty_of_stack in H0.
        inversion H.
        rewrite <- H3 in H0.
        eapply dep_step_returns_one_result; eauto.
      }
      { (* UNIT *)
        simpl S.eval_opcode in H.
        simpl in H1;
        destruct g.
        simpl Stack_ty_of_stack in s'.
        destruct s.
        inversion H.
        assert ((tt, Stack_ty_of_stack stack) = s').
        {
          rewrite H3.
          rewrite H4.
          reflexivity.
        }
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate;
        rewrite <- H2 in H0.
        eapply dep_step_returns_one_result; eauto.
      }
      { (* EQ *)
        simpl S.eval_opcode in H.
        destruct stack.
        inversion H.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        simpl in H0.
        assert (d =? 0 = Compare.Int.op_eq (
          Z.compare d Z.zero) 0) by admit.
        rewrite H2 in H0.
        eapply dep_step_returns_one_result; eauto.
      }
      { (* NEQ *)
        simpl S.eval_opcode in H.
        destruct stack.
        inversion H.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        simpl in H0.
        assert (negb (d =? 0) = Compare.Int.op_ltgt (
          Z.compare d Z.zero) 0) by admit.
        rewrite H2 in H0.
        eapply dep_step_returns_one_result; eauto.
      }
      { (* LT *)
        simpl S.eval_opcode in H.
        destruct stack.
        inversion H.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        simpl in H0.
        assert (d <? 0 = Compare.Int.op_lt (
          Z.compare d Z.zero) 0) by admit.
        rewrite H2 in H0.
        eapply dep_step_returns_one_result; eauto.
      }
      {
        (* GT *)
        simpl S.eval_opcode in H.
        destruct stack.
        inversion H.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        simpl in H0.
        assert (d >? 0 = Compare.Int.op_gt (
          Z.compare d Z.zero) 0) by admit.
        rewrite H2 in H0.
        eapply dep_step_returns_one_result; eauto.
      }
      { (* LE *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        inversion H.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        simpl in H0.
        assert (d <=? 0 = Compare.Int.op_lteq (
          Z.compare d Z.zero) 0) by admit.
        rewrite H2 in H0.
        eapply dep_step_returns_one_result; eauto.
      }
      { (* GE *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        inversion H.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        simpl in H0.
        assert (d >=? 0 = Compare.Int.op_gteq (
          Z.compare d Z.zero) 0) by admit.
        rewrite H2 in H0.
        eapply dep_step_returns_one_result; eauto.
      }
      { (* OR *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        destruct s3.
        inversion H.
        rewrite <- H3 in H0.
        destruct s0.
        destruct bvariant eqn:G;
        destruct g;
        simpl in H1;
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        { eapply dep_step_returns_one_result; eauto. }
        { 
          simpl Stack_ty_of_stack in H0.
          assert (forall a b,
            (Z.of_N (N.lor a b)) = (Z.logor (Z.of_N a) (Z.of_N b))).
            {
              intros.
              unfold Z.of_N.
              unfold N.lor.
              destruct a, b;
              admit.
              (* TODO: Z.logor is opaque. *)
            }
          rewrite H2 in H0.
          eapply dep_step_returns_one_result; eauto.
        }
      }
      { (* AND *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        destruct s3.
        inversion H.
        rewrite <- H3 in H0.
        destruct s0.
        destruct and_variant_field  eqn:G;
        destruct g;
        simpl in H1;
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        { eapply dep_step_returns_one_result; eauto. }
        { 
          simpl Stack_ty_of_stack in H0.
          assert (forall a b,
            Z.of_N (N.land a b) = Z.logand (Z.of_N a) (Z.of_N b)).
            {
              intros.
              unfold Z.of_N.
              unfold N.land.
              destruct a, b;
              admit.
              (* TODO: Z.logand is opaque. *)
            }
          rewrite H2 in H0.
          eapply dep_step_returns_one_result; eauto.
        }
        { 
          simpl Stack_ty_of_stack in H0.
          assert (forall a b,
            Z.of_N (Z.to_N (Z.land a (Z.of_N b))) = Z.logand a (Z.of_N b)).
          {
            intros.
            unfold Z.of_N.
            unfold Z.to_N.
            unfold Z.land.
            destruct a, b;
            admit.
            (* TODO: Z.logand is opaque. *)
          }
          rewrite H2 in H0.
          eapply dep_step_returns_one_result; eauto.
        }
      }
      { (* XOR *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        destruct s3.
        inversion H.
        rewrite <- H3 in H0.
        destruct s0.
        destruct bvariant eqn:G;
        destruct g;
        simpl in H1;
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        { eapply dep_step_returns_one_result; eauto. }
        { 
          simpl Stack_ty_of_stack in H0.
          assert (forall a b,
            (Z.of_N (N.lxor a b)) = (Z.logxor (Z.of_N a) (Z.of_N b))).
            {
              intros.
              unfold Z.of_N.
              unfold N.lxor.
              destruct a, b;
              admit.
              (* TODO: Z.logxor is opaque. *)
            }
          rewrite H2 in H0.
          eapply dep_step_returns_one_result; eauto.
        }
      }
      { (* NOT *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0;
        destruct s0;
        destruct not_variant_field eqn:G;
        simpl in H1;
        destruct g;
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate;
        simpl Stack_ty_of_stack in H0;
        destruct s;
        inversion H.
        {
          rewrite <- H3 in H0.
          rewrite <- H4 in H0.
          eapply dep_step_returns_one_result; eauto.
        }
        {
          rewrite <- H4 in H0.
          assert ((Z.lognot (Z.of_N d)) = d0).
          {
            rewrite <- H3.
            (* TODO: Z.lognot is opaque *)
            admit.
          }
          rewrite <- H2 in H0.
          eapply dep_step_returns_one_result; eauto.
        }
        {
          rewrite <- H4 in H0.
          assert ((Z.lognot d) = d0).
          {
            rewrite <- H3.
            (* TODO: Z.lognot is opaque *)
            admit.
          }
          rewrite <- H2 in H0.
          eapply dep_step_returns_one_result; eauto.
        }
      }
      { (* NEG *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        inversion H.
        rewrite <- H3 in H0.
        destruct s0;
        destruct neg_variant_field eqn:G;
        simpl in H1;
        destruct g;
        simpl in H0;
        simpl Stack_ty_of_stack in H0;
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate;
        try eapply dep_step_returns_one_result; eauto.
        {
          (* TODO: remove axiom from hypothesis*)
          admit. 
        }
        {
          (* TODO: remove axiom from hypothesis*)
          admit.
        }
        {
          (* TODO: remove axiom from hypothesis*)
          admit.
        }
      }
      { (* ABS *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        destruct s.
        inversion H.
        rewrite <- H3 in H0.
        rewrite <- H4 in H0.
        destruct g.
        simpl in H1.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        simpl Stack_ty_of_stack in H0.
        assert ((Z.abs d) = (Z.of_N (Z.abs_N d))) by admit.
        rewrite <- H2 in H0.
        eapply dep_step_returns_one_result; eauto.
      }
      { (* ISNAT *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        inversion H.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g;
        destruct d;
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate;
        simpl Stack_ty_of_stack in H0;
        eapply dep_step_returns_one_result; eauto.
      }
      { (* INT *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        inversion H.
        rewrite <- H3 in H0.
        destruct s0.
        destruct int_variant_field eqn:G;
        destruct g;
        simpl in H1;
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        { eapply dep_step_returns_one_result; eauto. }
        {  admit. (* TODO: The last goal uses an axiom*) }
      }
      { (* ADD *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        destruct s0;
        destruct add_variant_field eqn:G;
        destruct s3;
        destruct s;
        simpl in H1;
        destruct g;
        inversion H;
        simpl Stack_ty_of_stack in H0;
        try rewrite <- H3 in H0;
        try rewrite <- H4 in H0;
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate;
        try eapply dep_step_returns_one_result; eauto.
        {
          assert (Z.of_N (d + d0) = Z.add (Z.of_N d) (Z.of_N d0)) by admit.
          rewrite H2.
          eauto.
        }
        {
          assert ((Z.add d0 d) = (d + d0)%Z) by admit.
          rewrite <- H2.
          eauto.
        }
        {
          (* TODO: Error_monad.op_gtgtquestion *)
          admit.
        }
        {
          (* TODO: Remove axiom *)
          admit.
        }
        {
          (* TODO: Remove axiom *)
          admit.
        }
        {
          (* TODO: Remove axiom *)
          admit.
        }
      }
      { (* SUB *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        destruct s0;
        destruct sub_variant_field eqn:G;
        destruct s3;
        destruct s;
        destruct g;
        inversion H;
        simpl in H1;
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate;
        simpl Stack_ty_of_stack in H0;
        try rewrite <- H3 in H0;
        try rewrite <- H4 in H0;
        try eapply dep_step_returns_one_result; eauto.
        {
          (* TODO:  Error_monad.op_gtgtquestion *)
          admit.
        }
      }
      { (* MUL *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        destruct s0;
        destruct s3;
        destruct mul_variant_field eqn:G;
        destruct s;
        destruct g;
        inversion H;
        simpl in H1;
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate;
        simpl Stack_ty_of_stack in H0.
        {
          assert ((Z.of_N d1) = Z.mul (Z.of_N d) (Z.of_N d0)) by admit.
          rewrite H2 in H0.
          rewrite <- H4 in H0.
          try eapply dep_step_returns_one_result; eauto.
        }
        {
          rewrite <- H3 in H0.
          rewrite <- H4 in H0.
          try eapply dep_step_returns_one_result; eauto.
        }
        {
          rewrite <- H3 in H0.
          rewrite <- H4 in H0.
          try eapply dep_step_returns_one_result; eauto.
        }
        {
          rewrite <- H3 in H0.
          rewrite <- H4 in H0.
          try eapply dep_step_returns_one_result; eauto.
        }
        {
          (* TODO: remove axiom *)
          admit.
        }
        {
          (* TODO: remove axiom *)
          admit.
        }
        {
          (* TODO: remove axiom *)
          admit.
        }
        {
          (* TODO: remove axiom *)
          admit.
        }
        {
          (* TODO: remove axiom *)
          admit.
        }
        {
          (* TODO: remove axiom *)
          admit.
        }
        {
          (* TODO: remove axiom *)
          admit.
        }
        {
          (* TODO: remove axiom *)
          admit.
        }
        {
          (* TODO: remove axiom *)
          admit.
        }
      }
      { (* EDIV *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0;
        destruct s0;
        destruct s3;
        destruct ediv_variant_field eqn:G;
        destruct s;
        destruct g;
        inversion H;
        simpl in H1;
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate;
        simpl Stack_ty_of_stack in H0.
        (* TODO: Option.catch None *)
        - admit.
        - admit.
        - admit.
        - admit.
        - admit.
        - admit.
      }
      { 
        (* LSL *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        destruct s0;
        simpl in H1;
        destruct g;
        destruct s;
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate;
        simpl Stack_ty_of_stack in H0.
        (* TODO: remove axiom *)
        admit.
      }
      { (* LSR *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        destruct s0;
        simpl in H1;
        destruct g;
        destruct s;
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate;
        simpl Stack_ty_of_stack in H0.
        (* TODO: remove axiom *)
        admit.
      }
      { (* COMPARE *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        simpl in H1;
        destruct s0;
        destruct g;
        destruct s;
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        (* TODO: remove axiom *)
        admit.
      }
      { (* CONCAT *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        destruct i0;
        destruct s0;
        destruct s;
        destruct g;
        destruct stringlike_variant_field;
        simpl in H1;
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate;
        inversion H;
        rewrite <- H3 in H0;
        rewrite <- H4 in H0.
        { eapply dep_step_returns_one_result; eauto. }
        {
          assert (d ++ d0 = Bytes.cat d d0).
          {
            (* TODO: Bytes.cat is opaque *)
            admit.
          }
          rewrite H2 in H0.
          eapply dep_step_returns_one_result; eauto.
        }
      }
      { (* CONCAT_list *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        inversion H;
        rewrite <- H3 in H0;
        destruct i0;
        destruct stringlike_variant_field eqn:G;
        simpl in H1;
        destruct g;
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        { 
          (* TODO: remove axiom *)
          admit.
        }
        {
          (* TODO: remove axiom *)
          admit.
        }
      }
      { (* SIZE *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        destruct i0;
        destruct size_variant_field eqn:G;
        simpl in H1;
        destruct g;
        destruct s;
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate;
        simpl Stack_ty_of_stack in H0;
        simpl in H;
        inversion H.
        { 
          (* TODO: remove axiom *)
          admit.
        }
        { 
          (* TODO: remove axiom *)
          admit.
        }
        {
          assert (
            length (map (convert_data a) d) = (Z.of_N d0)) by admit.
          rewrite <- H2 in H0.
          rewrite <- H4 in H0.
          eapply dep_step_returns_one_result; eauto.
        }
        {
          assert (
            Z.abs (Z.of_int (String.length d)) = (Z.of_N d0)) by admit.
          rewrite <- H2 in H0.
          rewrite <- H4 in H0.
          eapply dep_step_returns_one_result; eauto.
        }
        {
          assert (
            Z.abs (Z.of_int (Bytes.length d)) = (Z.of_N d0)) by admit.
          rewrite <- H2 in H0.
          rewrite <- H4 in H0.
          eapply dep_step_returns_one_result; eauto.
        }
      }
      { (* SLICE *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        destruct s0.
        destruct i0.
        destruct stringlike_variant_field eqn:G.
        destruct g.
        simpl in H1.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        (* TODO: Couldn't eliminate the nested `let` in H1. *)
        admit.
        admit.
      }
      { (* PAIR *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        destruct s0.
        destruct g.
        simpl in H1.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        simpl Stack_ty_of_stack in H0.
        destruct s.
        destruct d1.
        inversion H.
        rewrite <- H3 in H0.
        rewrite <- H4 in H0.
        rewrite <- H5 in H0.
        eapply dep_step_returns_one_result; eauto.
      }
      { (* CAR *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        destruct g.
        destruct s.
        simpl in H1.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        destruct d.
        inversion H.
        rewrite <- H3 in H0.
        rewrite <- H4 in H0.
        eapply dep_step_returns_one_result; eauto.
      }
      { (* CDR *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        destruct g.
        simpl in H1.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        destruct d.
        inversion H.
        rewrite <- H3 in H0.
        eapply dep_step_returns_one_result; eauto.
      }
      { (* UNPAIR *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        destruct g.
        simpl in H1.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
          destruct d.
        inversion H.
        rewrite <- H3 in H0.
        eapply dep_step_returns_one_result; eauto.
      }
      { (* EMPTY_SET *)
        simpl S.eval_opcode in H.
        destruct g.
        simpl in H1.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        inversion H.
        (* TODO: remove axiom *)
        admit.
      }
      { (* MEM *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        destruct i0.
        destruct g.
        destruct mem_variant_field eqn:G.
        destruct s0.
        unfold eq_rec_r in H1.
        unfold eq_rec in H1.
        unfold eq_rect in H1.
        unfold eq_sym in H1.
        simpl in H1.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        unfold Ty_of_micho_ty_comparable_type_to_type in H1.
        unfold syntax_type.comparable_type_ind in H1.
        destruct a; simpl in H1.
        - (* TODO: Remove axiom *)
          admit.
        - unfold eq_trans in H1.
          unfold f_equal in H1.
          destruct a2; destruct a1;
          (* TODO: remove axiom *)
          admit.
        - unfold f_equal in H1.
          unfold eq_trans in H1.
          (* TODO: remove axiom *)
          admit.
        - admit.
        - admit.
        - admit.
      }
      { (* UPDATE *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        destruct i0.
        destruct update_variant_field  eqn:G.
        unfold eq_rec_r in H1.
        unfold eq_rec in H1.
        unfold eq_rect in H1.
        unfold eq_sym in H1.
        simpl in H1.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        destruct g.
        unfold Ty_of_micho_ty_comparable_type_to_type in H1.
        unfold syntax_type.comparable_type_ind in H1.
        (* TODO: Eliminate match (fix ...) *)
        admit.
        admit.
        admit.
        admit.
      }
      { (* EMPTY_MAP *)
        simpl S.eval_opcode in H.
        inversion H.
        destruct g.
        simpl in H1.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        (* TODO: remove axiom *)
        admit.
      }
      { (* EMPTY_BIG_MAP *)
        simpl S.eval_opcode in H.
        inversion H.
        destruct g.
        simpl in H1.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        (* TODO: remove axiom *)
        admit.
      }
      { (* GET *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        destruct g.
        destruct i0.
        destruct s0.
        destruct get_variant_field eqn:G.
        unfold eq_rec_r in H1.
        unfold eq_rec in H1.
        unfold eq_rect in H1.
        unfold eq_sym in H1.
        simpl in H1.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        unfold Ty_of_micho_ty_comparable_type_to_type in H1.
        unfold syntax_type.comparable_type_ind in H1.
        (* TODO: Eliminate match (fix ...) *)
        admit.
        admit.
      }
      { (* GET_AND_UPDATE *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        destruct g.
        destruct i0.
        destruct s0.
        destruct get_variant_field eqn:G.
        unfold eq_rec_r in H1.
        unfold eq_rec in H1.
        unfold eq_rect in H1.
        unfold eq_sym in H1.
        simpl in H1.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        unfold Ty_of_micho_ty_comparable_type_to_type in H1.
        unfold syntax_type.comparable_type_ind in H1.
        (* TODO: Eliminate match (fix ...) *)
        admit.
        admit.
      }
      { (* SOME *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        inversion H.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        eapply dep_step_returns_one_result; eauto.
      }
      { (* NONE *)
        simpl S.eval_opcode in H.
        inversion H.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        eapply dep_step_returns_one_result; eauto.
      }
      { (* LEFT *)
        simpl S.eval_opcode in H.
        destruct stack.
        inversion H.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        eapply dep_step_returns_one_result; eauto.
      }
      { (* RIGHT *)
        simpl S.eval_opcode in H.
        destruct stack.
        inversion H.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        eapply dep_step_returns_one_result; eauto.
      }
      { (* CONS *)
        simpl S.eval_opcode in H.
        destruct stack, s0.
        inversion H.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        simpl in H0.
        unfold Script_list.cons_value in H1.
        simpl in H1.
        rewrite length_cons in H0.
        unfold Pervasives.op_plus in H1.
        rewrite Z.add_comm in H0.
        (* TODO: stack cannot exceed 2^63 *)
        eapply dep_step_returns_one_result; eauto; admit.
      }
      { (* NIL *)
        simpl S.eval_opcode in H.
        inversion H.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        eapply dep_step_returns_one_result; eauto.
      }
      { (* TRANSFER_TOKENS *)
        admit. (*TODO*) 
      }
      { (* SET_DELEGATE *)
        admit. (*TODO*) 
      }
      { (* BALANCE *)
        admit. (*TODO*) 
      }
      { (* ADDRESS *)
        simpl S.eval_opcode in H.
        destruct stack.
        inversion H.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl Stack_ty_of_stack in *.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        admit. (*TODO*) 
      }
      { (* CONTRACT *)
        simpl S.eval_opcode in H.
        destruct stack.
        inversion H.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl Stack_ty_of_stack in *.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        admit. (*TODO*) 
      }
      { (* SOURCE *)
        simpl S.eval_opcode in H.
        inversion H.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        (* env vs s0? *)
        admit. (*TODO*) 
      }
      { (* SENDER *)
        simpl S.eval_opcode in H.
        inversion H.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        admit. (*TODO*) 
      }
      { (* AMOUNT *)
        simpl S.eval_opcode in H.
        inversion H.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        admit. (*TODO*) 
      }
      { (* IMPLICIT_AMOUNT *)
        simpl S.eval_opcode in H.
        destruct stack.
        inversion H.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        admit. (*TODO*) 
      }
      { (* NOW *)
        simpl S.eval_opcode in H.
        inversion H.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        admit. (*TODO*)
        (* we need to relate env on micho side
          with step constants on dep side *) 
      }
      { (* LEVEL *)
        simpl S.eval_opcode in H.
        inversion H.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        simpl in H0.
        admit. (*TODO*) 
      }
      { (* IMPLICIT_AMOUNT *)
        simpl S.eval_opcode in H.
        destruct stack.
        inversion H.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        admit. (*TODO*) 
      }
      { (* UNPACK *)
        simpl S.eval_opcode in H.
        destruct stack.
        inversion H.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        admit. (*TODO*) 
      }
      { (* HASH_KEY *)
        simpl S.eval_opcode in H.
        destruct stack.
        inversion H.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        admit. (*TODO*) 
      }
      { (* BLAKE2B *)
        simpl S.eval_opcode in H.
        destruct stack.
        inversion H.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        simpl in H0.
        destruct env; simpl in H0.
        admit. (*TODO*) 
      }
      { (* SHA256 *)
        simpl S.eval_opcode in H.
        destruct stack.
        inversion H.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        admit. (*TODO*) 
      }
      { (* SHA512 *)
        simpl S.eval_opcode in H.
        destruct stack.
        inversion H.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        admit. (*TODO*) 
      }
      { (* KECCAK *)
        simpl S.eval_opcode in H.
        destruct stack.
        inversion H.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        admit. (*TODO*) 
      }
      { (* SHA3 *)
        simpl S.eval_opcode in H.
        destruct stack.
        inversion H.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        admit. (*TODO*)
      }
      { (* CHECK_SIGNATURE *)
        simpl S.eval_opcode in H.
        destruct stack.
        destruct s0.
        destruct s0.
        inversion H.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        admit. (*TODO*)
      }
      { (* DIG *)
        simpl S.eval_opcode in H.
        inversion H.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        unfold S.stack_dig in H0.
        destruct (S.stack_split stack) eqn:G.
        destruct s4.
        pose (stack_app_split _ _ _ _ _ G) as Happ.
        symmetry in Happ.
        erewrite eval_gen_sppw_IDig in H1.
        2:{ exact Happ. }
        eapply dep_step_returns_one_result; eauto.
      }
      { (* DUG *)
        simpl S.eval_opcode in H.
        inversion H.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl Script_interpreter.dep_step in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        unfold S.stack_dug in H0.
        destruct stack.
        destruct (S.stack_split s3) eqn:G.
        pose (stack_app_split _ _ _ _ _ G) as Happ.
        symmetry in Happ.
        pose (eval_gen_sppw_IDug _ _ _ s3 s4 s5 d Happ).
        fold @map in H1.
        simpl in e0.
        rewrite e0 in H1.
        eapply dep_step_returns_one_result; eauto.
      }
      { (* DROP *)
        simpl S.eval_opcode in H.
        destruct (S.stack_split stack) eqn:G.
        inversion H.
        unfold s' in H0.
        rewrite <- H3 in H0.
        simpl Script_interpreter.dep_step in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        pose (stack_app_split _ _ _ _ _ G) as Happ.
        rewrite <- Happ in H1.
        rewrite eval_gen_sppw_IDropn in H1.
        eapply dep_step_returns_one_result; eauto.
      }
      { (* DUPN *)
        simpl S.eval_opcode in H.
        admit.
      }
      { (* CHAIN_ID *)
        simpl S.eval_opcode in H.
        admit.
      }
      { (* SELF_ADDRESS *)
        simpl S.eval_opcode in H.
        admit.
      }
      { (* VOTING_POWER *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        inversion H.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        eapply dep_step_returns_one_result; eauto.
        admit.
      }
      { (* TOTAL_VOTING_POWER *)
        simpl S.eval_opcode in H.
        admit.
      }
      { (* PAIRN *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        admit.
      }
      { (* UNPAIRN *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        inversion H.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        eapply dep_step_returns_one_result; eauto.
        admit.
      }
      { (* GETN *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        inversion H.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        eapply dep_step_returns_one_result; eauto.
        admit.
      }
      { (* UPDATEN *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        admit.
      }
      { (* TICKET *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        destruct s0.
        unfold eq_rec_r in H1;
        unfold eq_rec in H1;
        unfold eq_rect in H1;
        unfold eq_sym in H1;
        unfold Ty_of_micho_ty_comparable_type_to_type in H1;
        unfold syntax_type.comparable_type_ind in H1;
        simpl in H1;
        destruct g;
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate;
        destruct a.
        - destruct s4;
          destruct s;
          simpl Stack_ty_of_stack in H0;
          inversion H;
          rewrite <- H3 in H0;
          rewrite <- H4 in H0;
          try eapply dep_step_returns_one_result; eauto;
          (* TODO: all cases uses cheat 11 *)
          admit.
        - unfold f_equal in H1.
          unfold eq_trans in H1.
          destruct a2; destruct a1;
          simpl in H1;
          inversion H;
          rewrite <- H3 in H0;
          simpl in H0;
          (* TODO: eliminate match fix *)
          admit.
        - unfold eq_trans in H1.
          unfold f_equal in H1.
          (* TODO: eliminate match fix *)
          admit.
        - unfold eq_trans in H1.
          unfold f_equal in H1.
          (* TODO: eliminate match fix *)
          admit.
      }
      { (* READ_TICKET *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        unfold eq_rect in H1.
        unfold Ty_of_micho_ty_comparable_type_to_type in H1.
        unfold syntax_type.comparable_type_ind in H1.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        destruct a.
        - admit. (* TODO: remove axiom *)
        - unfold eq_trans in H1.
          unfold f_equal in H1.
          (* TODO: remove match fix *)
          admit.
        - unfold eq_trans in H1.
          unfold f_equal in H1.
          (* TODO: remove match fix *)
          admit.
        - unfold eq_trans in H1.
          unfold f_equal in H1.
          (* TODO: remove match fix *)
          admit.
      }
      { (* SPLIT_TICKET *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        destruct s0.
        destruct d0.
        simpl Stack_ty_of_stack in H0.
        destruct s.
        (* TODO: remove axiom from H0 *)
        admit.
      }
      { (* JOIN_TICKETS *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        destruct d.
        unfold eq_rect in H1.
        unfold Ty_of_micho_ty_comparable_type_to_type in H1.
        unfold syntax_type.comparable_type_ind in H1.
        destruct a.
        simpl Stack_ty_of_stack in H0.
        destruct s;
        simpl in H1;
        destruct g;
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        - (* TODO: remove axiom from H0 *)
          admit.
        - unfold eq_trans in H1.
          unfold f_equal in H1.
          (* TODO: remove match fix *)
          admit.
        - unfold eq_trans in H1.
          unfold f_equal in H1.
          (* TODO: remove match fix *)
          admit.
        - unfold eq_trans in H1.
          unfold f_equal in H1.
          (* TODO: remove match fix *)
          admit.
      }
      { (* SAPLING_EMPTY_STATE *)
        simpl S.eval_opcode in H.
        destruct g.
        simpl in H1.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        unfold s' in H0.
        unfold Stack_ty_of_stack in H0.
        destruct s.
        (* TODO: remove match fix in H0 and axiom in H1 *)
        admit.
      }
      { (* SAPLING_VERIFY_UPDATE *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        simpl Stack_ty_of_stack in H0.
        destruct g.
        destruct s.
        simpl in H1.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        destruct s0.
        (* TODO: remove axiom in H0 and H1 *)
        admit.
      }
      { (* PAIRING_CHECK *)
        simpl S.eval_opcode in H.
        destruct stack.
        simpl dep_of_micho_opcode in H1.
        unfold s' in H0.
        inversion H.
        rewrite <- H3 in H0.
        simpl in H1.
        destruct g.
        destruct Script_interpreter_defs.dep_consume_instr;
          try discriminate.
        (* TODO: remove axiom in H1 *)
        admit.
      }
    }
Admitted.

(*
  Lemma dep_of_micho_opcode_correct : forall
    (fuel : nat)
    {self_type} {A B C}
    (env : @S.proto_env self_type)
    (o : @syntax.opcode self_type A B)
    (g : Local_gas_counter.outdated_context * Script_typed_ir.step_constants)
    (gas : Local_gas_counter.local_gas_counter)
    (i : With_family.kinstr (map Ty_of_micho_ty B) (map Ty_of_micho_ty C))
    (stack : S.stack A),
    match S.eval_opcode _ env o stack with
    | error.Return s =>
        let s' := Stack_ty_of_stack s in
          Script_interpreter.dep_step fuel g gas i With_family.KNil s'
        = Script_interpreter.dep_step fuel g gas
            (dep_of_micho_opcode o C i) With_family.KNil (Stack_ty_of_stack stack)
    | _ => True
    end.
    induction fuel; intros.
    { simpl.
      destruct S.eval_opcode; auto.
    }
    { destruct o.
      2:{ unfold S.eval_opcode.
          simpl in IHfuel.
          destruct stack.
          simpl dep_of_micho_opcode.
          destruct g.
          simpl Stack_ty_of_stack.
          simpl Script_interpreter.dep_step at 2.
          destruct (Script_interpreter_defs.dep_consume_instr).
          { admit. }
          { admit. }
  Admitted.
*)

(*
  Lemma dep_of_micho_correct : forall {self_type tff s f}
    (i : @syntax.instruction_seq self_type tff s f)
    (env : S.proto_env)
    (fuel : nat)
    (stack : S.stack s),
    match
      S.eval_seq env i fuel stack,
      Script_interpreter.dep_step axiom
        (local_gas_counter_of_nat fuel)
        (dep_of_micho_iseq i) With_family.KNil (Stack_ty_of_stack stack)
    with
    | error.Return s, Pervasives.Ok ((s',_),_) =>
        Stack_ty_of_stack s = s'
    | _, _ => True
    end.
    induction fuel; intros.
    unfold S.eval_seq.
    unfold S.eval_seq_body.
    { destruct i.
      unfold S.eval_seq.
      unfold S.eval_seq_body.
      unfold local_gas_counter_of_nat.
      unfold Z.of_nat.
      { admit. }
      { exact I. }
      { exact I. }
    }
    { admit. }
  Admitted.
*)
End Correctness.