Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require Import TezosOfOCaml.Proto_alpha.Script_ir_translator.

Require Import TezosOfOCaml.Proto_alpha.Simulations.Script_typed_ir.

Fixpoint dep_ty_of_comparable_ty {t} (ty : With_family.comparable_ty t) :
  With_family.ty t :=
  match ty with
  | With_family.Unit_key tname => With_family.Unit_t tname
  | With_family.Never_key tname => With_family.Never_t tname
  | With_family.Int_key tname => With_family.Int_t tname
  | With_family.Nat_key tname => With_family.Nat_t tname
  | With_family.Signature_key tname => With_family.Signature_t tname
  | With_family.String_key tname => With_family.String_t tname
  | With_family.Bytes_key tname => With_family.Bytes_t tname
  | With_family.Mutez_key tname => With_family.Mutez_t tname
  | With_family.Bool_key tname => With_family.Bool_t tname
  | With_family.Key_hash_key tname => With_family.Key_hash_t tname
  | With_family.Key_key tname => With_family.Key_t tname
  | With_family.Timestamp_key tname => With_family.Timestamp_t tname
  | With_family.Address_key tname => With_family.Address_t tname
  | With_family.Tx_rollup_l2_address_key =>
    With_family.Tx_rollup_l2_address_t
  | With_family.Chain_id_key tname => With_family.Chain_id_t tname
  | With_family.Pair_key l_value r_value tname =>
    With_family.Pair_t (dep_ty_of_comparable_ty l_value)
      (dep_ty_of_comparable_ty r_value) tname
  | With_family.Union_key l_value r_value tname =>
    With_family.Union_t (dep_ty_of_comparable_ty l_value)
      (dep_ty_of_comparable_ty r_value) tname
  | With_family.Option_key t_value tname =>
    With_family.Option_t (dep_ty_of_comparable_ty t_value) tname
  end.

Fixpoint dep_unparse_comparable_ty_uncarbonated {loc : Set} {t}
  (loc_value : loc) (ty : With_family.comparable_ty t) :
  Script_repr.michelson_node loc :=
  match ty with
  | With_family.Unit_key meta =>
    Micheline.Prim loc_value Michelson_v1_primitives.T_unit nil nil
  | With_family.Never_key meta =>
    Micheline.Prim loc_value Michelson_v1_primitives.T_never nil nil
  | With_family.Int_key meta =>
    Micheline.Prim loc_value Michelson_v1_primitives.T_int nil nil
  | With_family.Nat_key meta =>
    Micheline.Prim loc_value Michelson_v1_primitives.T_nat nil nil
  | With_family.Signature_key meta =>
    Micheline.Prim loc_value Michelson_v1_primitives.T_signature nil nil
  | With_family.String_key meta =>
    Micheline.Prim loc_value Michelson_v1_primitives.T_string nil nil
  | With_family.Bytes_key meta =>
    Micheline.Prim loc_value Michelson_v1_primitives.T_bytes nil nil
  | With_family.Mutez_key meta =>
    Micheline.Prim loc_value Michelson_v1_primitives.T_mutez nil nil
  | With_family.Bool_key meta =>
    Micheline.Prim loc_value Michelson_v1_primitives.T_bool nil nil
  | With_family.Key_hash_key meta =>
    Micheline.Prim loc_value Michelson_v1_primitives.T_key_hash nil nil
  | With_family.Key_key meta =>
    Micheline.Prim loc_value Michelson_v1_primitives.T_key nil nil
  | With_family.Timestamp_key meta =>
    Micheline.Prim loc_value Michelson_v1_primitives.T_timestamp nil nil
  | With_family.Address_key meta =>
    Micheline.Prim loc_value Michelson_v1_primitives.T_address nil nil
  | With_family.Tx_rollup_l2_address_key =>
    Micheline.Prim
      loc_value Michelson_v1_primitives.T_tx_rollup_l2_address nil nil
  | With_family.Chain_id_key meta =>
    Micheline.Prim loc_value Michelson_v1_primitives.T_chain_id nil nil
  | With_family.Pair_key l_value r_value meta =>
    let tl := dep_unparse_comparable_ty_uncarbonated loc_value l_value in
    let tr := dep_unparse_comparable_ty_uncarbonated loc_value r_value in
    match tr with
    | Micheline.Prim _ Michelson_v1_primitives.T_pair ts [] =>
      Micheline.Prim loc_value Michelson_v1_primitives.T_pair (cons tl ts) nil
    | _ =>
      Micheline.Prim loc_value Michelson_v1_primitives.T_pair [ tl; tr ] nil
    end
  | With_family.Union_key l_value r_value meta =>
    let tl := dep_unparse_comparable_ty_uncarbonated loc_value l_value in
    let tr := dep_unparse_comparable_ty_uncarbonated loc_value r_value in
    Micheline.Prim loc_value Michelson_v1_primitives.T_or [ tl; tr ] nil
  | With_family.Option_key t_value meta =>
    Micheline.Prim loc_value Michelson_v1_primitives.T_option
      [ dep_unparse_comparable_ty_uncarbonated loc_value t_value ] nil
  end.

Fixpoint dep_unparse_ty_uncarbonated {loc : Set} {t} (loc_value : loc)
  (ty : With_family.ty t) : Script_repr.michelson_node loc :=
  let prim {B : Set} (function_parameter : B * list (Micheline.node loc B))
    : Micheline.node loc B :=
    let '(name, args) := function_parameter in
    Micheline.Prim loc_value name args [] in
  match ty with
  | With_family.Unit_t meta =>
    prim (Michelson_v1_primitives.T_unit, nil)
  | With_family.Int_t meta =>
    prim (Michelson_v1_primitives.T_int, nil)
  | With_family.Nat_t meta =>
    prim (Michelson_v1_primitives.T_nat, nil)
  | With_family.Signature_t meta =>
    prim (Michelson_v1_primitives.T_signature, nil)
  | With_family.String_t meta =>
    prim (Michelson_v1_primitives.T_string, nil)
  | With_family.Bytes_t meta =>
    prim (Michelson_v1_primitives.T_bytes, nil)
  | With_family.Mutez_t meta =>
    prim (Michelson_v1_primitives.T_mutez, nil)
  | With_family.Bool_t meta =>
    prim (Michelson_v1_primitives.T_bool, nil)
  | With_family.Key_hash_t meta =>
    prim (Michelson_v1_primitives.T_key_hash, nil)
  | With_family.Key_t meta =>
    prim (Michelson_v1_primitives.T_key, nil)
  | With_family.Timestamp_t meta =>
    prim (Michelson_v1_primitives.T_timestamp, nil)
  | With_family.Address_t meta =>
    prim (Michelson_v1_primitives.T_address, nil)
  | With_family.Tx_rollup_l2_address_t =>
    prim (Michelson_v1_primitives.T_tx_rollup_l2_address, nil)
  | With_family.Operation_t meta =>
    prim (Michelson_v1_primitives.T_operation, nil)
  | With_family.Chain_id_t meta =>
    prim (Michelson_v1_primitives.T_chain_id, nil)
  | With_family.Never_t meta =>
    prim (Michelson_v1_primitives.T_never, nil)
  | With_family.Bls12_381_g1_t meta =>
    prim (Michelson_v1_primitives.T_bls12_381_g1, nil)
  | With_family.Bls12_381_g2_t meta =>
    prim (Michelson_v1_primitives.T_bls12_381_g2, nil)
  | With_family.Bls12_381_fr_t meta =>
    prim (Michelson_v1_primitives.T_bls12_381_fr, nil)
  | With_family.Contract_t ut meta =>
    let t_value := dep_unparse_ty_uncarbonated loc_value ut in
    prim (Michelson_v1_primitives.T_contract, [ t_value ])
  | With_family.Pair_t utl utr meta =>
    let tl := dep_unparse_ty_uncarbonated loc_value utl in
    let tr := dep_unparse_ty_uncarbonated loc_value utr in
    prim
      match tr with
      | Micheline.Prim _ Michelson_v1_primitives.T_pair ts [] =>
        (Michelson_v1_primitives.T_pair, (cons tl ts))
      | _ => (Michelson_v1_primitives.T_pair, [ tl; tr ])
      end
  | With_family.Union_t utl utr meta =>
    let utl := dep_unparse_ty_uncarbonated loc_value utl in
    let utr := dep_unparse_ty_uncarbonated loc_value utr in
    prim (Michelson_v1_primitives.T_or, [ utl; utr ])
  | With_family.Lambda_t uta utr meta =>
    let ta := dep_unparse_ty_uncarbonated loc_value uta in
    let tr := dep_unparse_ty_uncarbonated loc_value utr in
    prim (Michelson_v1_primitives.T_lambda, [ ta; tr ])
  | With_family.Option_t ut meta =>
    let ut := dep_unparse_ty_uncarbonated loc_value ut in
    prim (Michelson_v1_primitives.T_option, [ ut ])
  | With_family.List_t ut meta =>
    let t_value := dep_unparse_ty_uncarbonated loc_value ut in
    prim (Michelson_v1_primitives.T_list, [ t_value ])
  | With_family.Ticket_t ut meta =>
    let t_value := dep_unparse_comparable_ty_uncarbonated loc_value ut in
    prim (Michelson_v1_primitives.T_ticket, [ t_value ])
  | With_family.Set_t ut meta =>
    let t_value := dep_unparse_comparable_ty_uncarbonated loc_value ut in
    prim (Michelson_v1_primitives.T_set, [ t_value ])
  | With_family.Map_t uta utr meta =>
    let ta := dep_unparse_comparable_ty_uncarbonated loc_value uta in
    let tr := dep_unparse_ty_uncarbonated loc_value utr in
    prim (Michelson_v1_primitives.T_map, [ ta; tr ])
  | With_family.Big_map_t uta utr meta =>
    let ta := dep_unparse_comparable_ty_uncarbonated loc_value uta in
    let tr := dep_unparse_ty_uncarbonated loc_value utr in
    prim (Michelson_v1_primitives.T_big_map, [ ta; tr ])
  | With_family.Sapling_transaction_t memo_size meta =>
    prim (Michelson_v1_primitives.T_sapling_transaction,
      [ unparse_memo_size loc_value memo_size ])
  | With_family.Sapling_state_t memo_size meta =>
    prim (Michelson_v1_primitives.T_sapling_state,
      [ unparse_memo_size loc_value memo_size ])
  | With_family.Chest_key_t meta =>
    prim (Michelson_v1_primitives.T_chest_key, nil)
  | With_family.Chest_t meta =>
    prim (Michelson_v1_primitives.T_chest, nil)
  end.

Definition dep_serialize_ty_for_error {t} (ty : With_family.ty t) :
  Micheline.canonical Alpha_context.Script.prim :=
  Micheline.strip_locations (dep_unparse_ty_uncarbonated tt ty).

Definition dep_comparable_comb_witness2 (ty : Family.Ty.t) : comb_witness :=
  match ty with
  | Family.Ty.Pair _ (Family.Ty.Pair _ _) =>
    Comb_Pair (Comb_Pair Comb_Any)
  | Family.Ty.Pair _ _ => Comb_Pair Comb_Any
  | _ => Comb_Any
  end.

Fixpoint dep_unparse_comparable_data {loc : Set}
  (l : loc) (ctxt : Raw_context.t) (mode : Script_ir_translator.unparsing_mode)
  (ty : Family.Ty.t) (x : Family.Ty.to_Set ty) :
  M? (Script_repr.michelson_node loc * Raw_context.t) :=
  let? ctxt :=
    Alpha_context.Gas.consume ctxt Unparse_costs.unparse_data_cycle in
  match ty, x with
  | Family.Ty.Unit, x =>
    unparse_unit l ctxt x
  | Family.Ty.Num Family.Ty.Num.Int, x =>
    unparse_int l ctxt x
  | Family.Ty.Num Family.Ty.Num.Nat, x =>
    unparse_nat l ctxt x
  | Family.Ty.String, s_value =>
    unparse_string l ctxt s_value
  | Family.Ty.Bytes, s_value =>
    unparse_bytes l ctxt s_value
  | Family.Ty.Bool, b_value =>
    unparse_bool l ctxt b_value
  | Family.Ty.Timestamp, t_value =>
    unparse_timestamp l ctxt mode t_value
  | Family.Ty.Address, address =>
    unparse_address l ctxt mode address
  | Family.Ty.Tx_rollup_l2_address, address =>
    unparse_tx_rollup_l2_address l ctxt mode address
  | Family.Ty.Signature, s_value =>
    unparse_signature l ctxt mode s_value
  | Family.Ty.Mutez, x =>
    unparse_mutez l ctxt x
  | Family.Ty.Key, k_value =>
    unparse_key l ctxt mode k_value
  | Family.Ty.Key_hash, k_value =>
    unparse_key_hash l ctxt mode k_value
  | Family.Ty.Chain_id, chain_id =>
    unparse_chain_id l ctxt mode chain_id
  | Family.Ty.Pair tl tr, pair_value =>
    let r_witness := dep_comparable_comb_witness2 tr in
    let unparse_l ctxt x := dep_unparse_comparable_data l ctxt mode tl x in
    let unparse_r ctxt x := dep_unparse_comparable_data l ctxt mode tr x in
    unparse_pair l unparse_l unparse_r ctxt mode r_witness pair_value
  | Family.Ty.Union tl tr, x =>
    let unparse_l ctxt x := dep_unparse_comparable_data l ctxt mode tl x in
    let unparse_r ctxt x := dep_unparse_comparable_data l ctxt mode tr x in
    unparse_union l unparse_l unparse_r ctxt x
  | Family.Ty.Option t, x =>
    let unparse_v ctxt x := dep_unparse_comparable_data l ctxt mode t x in
    unparse_option l unparse_v ctxt x
  | _, _ => Error_monad.error_value (Build_extensible "Unexpected" unit tt)
  end.

Definition dep_comparable_comb_witness1 (ty : Family.Ty.t) : comb_witness :=
  match ty with
  | Family.Ty.Pair _ _ => Comb_Pair Comb_Any
  | _ => Comb_Any
  end.

Fixpoint dep_parse_comparable_data {t} ctxt
  (ty : With_family.comparable_ty t) (expr : Script_repr.node) :
  M? (Family.Ty.to_Set t * Raw_context.t) :=
  let parse_data_error (_ : unit) : Error_monad._error :=
    let ty := dep_serialize_ty_for_error (dep_ty_of_comparable_ty ty) in
    Build_extensible "Invalid_constant"
      (Alpha_context.Script.location *
        Micheline.canonical Alpha_context.Script.prim *
        Micheline.canonical Alpha_context.Script.prim)
      ((location expr), (Micheline.strip_locations expr), ty) in
  let traced_no_lwt {B : Set} (body : M? B) : M? B :=
    Error_monad.record_trace_eval parse_data_error body in
  let traced {B : Set} (body : M? B) : M? B :=
    Error_monad.trace_eval parse_data_error body in
  let? ctxt :=
    Alpha_context.Gas.consume ctxt Typecheck_costs.parse_data_cycle in
  let legacy := false in
  match t, ty in With_family.comparable_ty t return M? (Family.Ty.to_Set t * Raw_context.t) with
  | Family.Ty.Unit, _ =>
    traced_no_lwt (parse_unit ctxt legacy expr)
  | Family.Ty.Bool, _ =>
    traced_no_lwt (parse_bool ctxt legacy expr)
  | Family.Ty.String, _ =>
    traced_no_lwt (parse_string ctxt expr)
  | Family.Ty.Bytes, _ =>
    traced_no_lwt (parse_bytes ctxt expr)
  | Family.Ty.Num Family.Ty.Num.Int, _ =>
    traced_no_lwt (parse_int ctxt expr)
  | Family.Ty.Num Family.Ty.Num.Nat, _ =>
    traced_no_lwt (parse_nat ctxt expr)
  | Family.Ty.Mutez, _ =>
    traced_no_lwt (parse_mutez ctxt expr)
  | Family.Ty.Timestamp, _ =>
    traced_no_lwt (parse_timestamp ctxt expr)
  | Family.Ty.Key, _ =>
    traced_no_lwt (parse_key ctxt expr)
  | Family.Ty.Key_hash, _ =>
    traced_no_lwt (parse_key_hash ctxt expr)
  | Family.Ty.Signature, _ =>
    traced_no_lwt (parse_signature ctxt expr)
  | Family.Ty.Chain_id, _ =>
    traced_no_lwt (parse_chain_id ctxt expr)
  | Family.Ty.Address, _ =>
    traced_no_lwt (parse_address ctxt expr)
  | Family.Ty.Pair tl tr, With_family.Pair_key tyl tyr _ =>
    let r_witness := dep_comparable_comb_witness1 tr in
    let parse_l
      (ctxt : Alpha_context.context) (v_value : Alpha_context.Script.node) :=
      dep_parse_comparable_data ctxt tyl v_value in
    let parse_r
      (ctxt : Alpha_context.context) (v_value : Alpha_context.Script.node) :=
      dep_parse_comparable_data ctxt tyr v_value in
    traced (parse_pair parse_l parse_r ctxt legacy r_witness expr)
  | Family.Ty.Union tl tr, With_family.Union_key tyl tyr _ =>
    let parse_l
      (ctxt : Alpha_context.context) (v_value : Alpha_context.Script.node) :=
      dep_parse_comparable_data ctxt tyl v_value in
    let parse_r
      (ctxt : Alpha_context.context) (v_value : Alpha_context.Script.node) :=
      dep_parse_comparable_data ctxt tyr v_value in
    traced (parse_union parse_l parse_r ctxt legacy expr)
  | Family.Ty.Option _, With_family.Option_key t_value _ =>
    let parse_v
      (ctxt : Alpha_context.context) (v_value : Alpha_context.Script.node) :=
      dep_parse_comparable_data ctxt t_value v_value in
    traced (parse_option parse_v ctxt legacy expr)
  | Family.Ty.Never, _ =>
    traced_no_lwt (parse_never expr)
  | _, _ => Error_monad.error_value (Build_extensible "Unexpected" unit tt)
  end.

Inductive ex_family_comparable_ty : Set :=
| Ex_family_comparable_ty {t : Family.Ty.t} :
  With_family.comparable_ty t -> ex_family_comparable_ty.

(** We define [dep_parse_ty_aux] as dependent-typed version of
    [parse_ty_aux]. We do this in order to remove casts and
    be able to prove statemens about [dep_parse_ty_aux] 
    (for example its compatibility with [dep_unparse_ty_aux])
   
    Next step is to prove that dep_parse_ty_aux equals to parse_ty_aux.
    We do in [Proofs/Script_ir_translator.v] file 
 
    We also redefine types [ex_ty] and [ex_comparable_ty] and replace these
    types by [ret_fam] and [ex_family_comparable_ty] respectively. For types  
    we also prove equality with original types. 
 *)

Fixpoint dep_parse_comparable_ty_aux
  (fuel : nat) (ctxt : Alpha_context.context)
  (node_value : Alpha_context.Script.node) {struct fuel}
  : M? (ex_family_comparable_ty * Alpha_context.context) :=
  let? ctxt := Alpha_context.Gas.consume ctxt Typecheck_costs.parse_type_cycle
    in
  match fuel with
  | Datatypes.O =>
    Error_monad.error_value
      (Build_extensible "Typechecking_too_many_recursive_calls" unit tt)
  | Datatypes.S fuel =>
    match node_value with
    | Micheline.Prim loc_value Michelson_v1_primitives.T_unit [] annot =>
      let? '_ := Script_ir_annot.check_type_annot loc_value annot in
      return? ((Ex_family_comparable_ty (
                  Simulations.Script_typed_ir.With_family.Unit_key
                    Simulations.Script_typed_ir.Ty_metadata.default)), ctxt)
    | Micheline.Prim loc_value Michelson_v1_primitives.T_never [] annot =>
      let? '_ := Script_ir_annot.check_type_annot loc_value annot in
      return? ((Ex_family_comparable_ty (
                  Simulations.Script_typed_ir.With_family.Never_key
                    Simulations.Script_typed_ir.Ty_metadata.default)), ctxt)
    | Micheline.Prim loc_value Michelson_v1_primitives.T_int [] annot =>
      let? '_ := Script_ir_annot.check_type_annot loc_value annot in
      return? ((Ex_family_comparable_ty (
                  Simulations.Script_typed_ir.With_family.Int_key
                    Simulations.Script_typed_ir.Ty_metadata.default)), ctxt)
    | Micheline.Prim loc_value Michelson_v1_primitives.T_nat [] annot =>
      let? '_ := Script_ir_annot.check_type_annot loc_value annot in
      return? ((Ex_family_comparable_ty (
                  Simulations.Script_typed_ir.With_family.Nat_key
                    Simulations.Script_typed_ir.Ty_metadata.default)), ctxt)
    | Micheline.Prim loc_value Michelson_v1_primitives.T_signature [] annot =>
      let? '_ := Script_ir_annot.check_type_annot loc_value annot in
      return? ((Ex_family_comparable_ty (
                  Simulations.Script_typed_ir.With_family.Signature_key
                    Simulations.Script_typed_ir.Ty_metadata.default)), ctxt)
    | Micheline.Prim loc_value Michelson_v1_primitives.T_string [] annot =>
      let? '_ := Script_ir_annot.check_type_annot loc_value annot in
      return? ((Ex_family_comparable_ty (
                  Simulations.Script_typed_ir.With_family.String_key
                    Simulations.Script_typed_ir.Ty_metadata.default)), ctxt)
    | Micheline.Prim loc_value Michelson_v1_primitives.T_bytes [] annot =>
      let? '_ := Script_ir_annot.check_type_annot loc_value annot in
      return? ((Ex_family_comparable_ty (
                  Simulations.Script_typed_ir.With_family.Bytes_key
                    Simulations.Script_typed_ir.Ty_metadata.default)), ctxt)
    | Micheline.Prim loc_value Michelson_v1_primitives.T_mutez [] annot =>
      let? '_ := Script_ir_annot.check_type_annot loc_value annot in
      return? ((Ex_family_comparable_ty (
                  Simulations.Script_typed_ir.With_family.Mutez_key
                    Simulations.Script_typed_ir.Ty_metadata.default)), ctxt)
    | Micheline.Prim loc_value Michelson_v1_primitives.T_bool [] annot =>
      let? '_ := Script_ir_annot.check_type_annot loc_value annot in
      return? ((Ex_family_comparable_ty (
                  Simulations.Script_typed_ir.With_family.Bool_key
                    Simulations.Script_typed_ir.Ty_metadata.default)), ctxt)
    | Micheline.Prim loc_value Michelson_v1_primitives.T_key_hash [] annot =>
      let? '_ := Script_ir_annot.check_type_annot loc_value annot in
      return? ((Ex_family_comparable_ty (
                  Simulations.Script_typed_ir.With_family.Key_hash_key
                    Simulations.Script_typed_ir.Ty_metadata.default)), ctxt)
    | Micheline.Prim loc_value Michelson_v1_primitives.T_key [] annot =>
      let? '_ := Script_ir_annot.check_type_annot loc_value annot in
      return? ((Ex_family_comparable_ty (
                  Simulations.Script_typed_ir.With_family.Key_key
                    Simulations.Script_typed_ir.Ty_metadata.default)), ctxt)
    | Micheline.Prim loc_value Michelson_v1_primitives.T_timestamp [] annot =>
      let? '_ := Script_ir_annot.check_type_annot loc_value annot in
      return? ((Ex_family_comparable_ty (
                  Simulations.Script_typed_ir.With_family.Timestamp_key
                    Simulations.Script_typed_ir.Ty_metadata.default)), ctxt)
    | Micheline.Prim loc_value Michelson_v1_primitives.T_chain_id [] annot =>
      let? '_ := Script_ir_annot.check_type_annot loc_value annot in
      return? ((Ex_family_comparable_ty (
                  Simulations.Script_typed_ir.With_family.Chain_id_key
                    Simulations.Script_typed_ir.Ty_metadata.default)), ctxt)
    | Micheline.Prim loc_value Michelson_v1_primitives.T_address [] annot =>
      let? '_ := Script_ir_annot.check_type_annot loc_value annot in
      return? ((Ex_family_comparable_ty (
                  Simulations.Script_typed_ir.With_family.Address_key
                    Simulations.Script_typed_ir.Ty_metadata.default)), ctxt)
    |
      Micheline.Prim loc_value Michelson_v1_primitives.T_tx_rollup_l2_address []
        annot =>
      let? '_ := Script_ir_annot.check_type_annot loc_value annot in
      return? (
        Ex_family_comparable_ty With_family.Tx_rollup_l2_address_key,
        ctxt
      )
    |
      Micheline.Prim loc_value
        ((Michelson_v1_primitives.T_unit | Michelson_v1_primitives.T_never |
        Michelson_v1_primitives.T_int | Michelson_v1_primitives.T_nat |
        Michelson_v1_primitives.T_string | Michelson_v1_primitives.T_bytes |
        Michelson_v1_primitives.T_mutez | Michelson_v1_primitives.T_bool |
        Michelson_v1_primitives.T_key_hash | Michelson_v1_primitives.T_timestamp
        | Michelson_v1_primitives.T_address | Michelson_v1_primitives.T_chain_id
        | Michelson_v1_primitives.T_signature | Michelson_v1_primitives.T_key)
          as prim) l_value _ =>
      Error_monad.error_value
        (Build_extensible "Invalid_arity"
          (Alpha_context.Script.location * Alpha_context.Script.prim * int * int)
          (loc_value, prim, 0, (List.length l_value)))
    |
      Micheline.Prim loc_value Michelson_v1_primitives.T_pair
        (cons _left _right) annot =>
      let? '_ := Script_ir_annot.check_type_annot loc_value annot in
      let? _left := Script_ir_annot.remove_field_annot _left in
      let? _right :=
        match _right with
        | cons _right [] => Script_ir_annot.remove_field_annot _right
        | _right =>
          return?
            (Micheline.Prim loc_value Michelson_v1_primitives.T_pair _right nil)
        end in
      let? '(Ex_family_comparable_ty _right, ctxt) :=
        dep_parse_comparable_ty_aux fuel ctxt _right in
      let? '(Ex_family_comparable_ty _left, ctxt) :=
        dep_parse_comparable_ty_aux fuel ctxt _left in
      let? pair_check :=
        Script_typed_ir.pair_key
          loc_value
          (Simulations.Script_typed_ir.With_family.to_comparable_ty _left)
          (Simulations.Script_typed_ir.With_family.to_comparable_ty _right) in
          let md := Script_typed_ir.comparable_ty_metadata (pair_check) in
      let ty := Simulations.Script_typed_ir.With_family.Pair_key _left _right md in
      return? ((Ex_family_comparable_ty ty), ctxt)
    |
      Micheline.Prim loc_value Michelson_v1_primitives.T_or
        (cons _left (cons _right [])) annot =>
      let? '_ := Script_ir_annot.check_type_annot loc_value annot in
      let? _left := Script_ir_annot.remove_field_annot _left in
      let? _right := Script_ir_annot.remove_field_annot _right in
      let? '(Ex_family_comparable_ty _right, ctxt) :=
        dep_parse_comparable_ty_aux fuel ctxt _right in
      let? '(Ex_family_comparable_ty _left, ctxt) :=
        dep_parse_comparable_ty_aux fuel ctxt _left in

 let? union_check := Script_typed_ir.union_key 
                       loc_value (Simulations.Script_typed_ir.With_family.to_comparable_ty _left)
  (Simulations.Script_typed_ir.With_family.to_comparable_ty _right) in
          let md := Script_typed_ir.comparable_ty_metadata (union_check) in
      
      let ty := Simulations.Script_typed_ir.With_family.Union_key _left _right
                    md in
      return? ((Ex_family_comparable_ty ty), ctxt)
    |
      Micheline.Prim loc_value
        ((Michelson_v1_primitives.T_pair | Michelson_v1_primitives.T_or) as prim)
        l_value _ =>
      Error_monad.error_value
        (Build_extensible "Invalid_arity"
          (Alpha_context.Script.location * Alpha_context.Script.prim * int * int)
          (loc_value, prim, 2, (List.length l_value)))
    |
      Micheline.Prim loc_value Michelson_v1_primitives.T_option
        (cons t_value []) annot =>
      let? '_ := Script_ir_annot.check_type_annot loc_value annot in
      let? '(Ex_family_comparable_ty t_value, ctxt) :=
        dep_parse_comparable_ty_aux fuel ctxt t_value in

      let? option_check := Script_typed_ir.option_key 
              loc_value (Simulations.Script_typed_ir.With_family.to_comparable_ty t_value) in
          let md := Script_typed_ir.comparable_ty_metadata (option_check) in
      
      let ty := Simulations.Script_typed_ir.With_family.Option_key t_value md in
      return? ((Ex_family_comparable_ty ty), ctxt)
    | Micheline.Prim loc_value Michelson_v1_primitives.T_option l_value _ =>
      Error_monad.error_value
        (Build_extensible "Invalid_arity"
          (Alpha_context.Script.location * Alpha_context.Script.prim * int * int)
          (loc_value, Michelson_v1_primitives.T_option, 1, (List.length l_value)))
    |
      Micheline.Prim loc_value
        (Michelson_v1_primitives.T_set | Michelson_v1_primitives.T_map |
        Michelson_v1_primitives.T_list | Michelson_v1_primitives.T_lambda |
        Michelson_v1_primitives.T_contract | Michelson_v1_primitives.T_operation)
        _ _ =>
      Error_monad.error_value
        (Build_extensible "Comparable_type_expected"
          (Alpha_context.Script.location *
            Micheline.canonical Alpha_context.Script.prim)
          (loc_value, (Micheline.strip_locations node_value)))
    | expr =>
      Error_monad.error_value
        (unexpected expr nil Michelson_v1_primitives.Type_namespace
          [
            Michelson_v1_primitives.T_unit;
            Michelson_v1_primitives.T_never;
            Michelson_v1_primitives.T_int;
            Michelson_v1_primitives.T_nat;
            Michelson_v1_primitives.T_string;
            Michelson_v1_primitives.T_bytes;
            Michelson_v1_primitives.T_mutez;
            Michelson_v1_primitives.T_bool;
            Michelson_v1_primitives.T_key_hash;
            Michelson_v1_primitives.T_timestamp;
            Michelson_v1_primitives.T_address;
            Michelson_v1_primitives.T_pair;
            Michelson_v1_primitives.T_or;
            Michelson_v1_primitives.T_option;
            Michelson_v1_primitives.T_chain_id;
            Michelson_v1_primitives.T_signature;
            Michelson_v1_primitives.T_key
          ])
    end
  end.

(** redefined types *)

Inductive ret_fam : parse_ty_ret -> Set :=
| Ex_f_ty {t : Family.Ty.t} : With_family.ty t -> ret_fam Don't_parse_entrypoints
| Ex_f_entrypoints {t} : With_family.ty t -> Script_typed_ir.entrypoints ->
                         ret_fam Parse_entrypoints.

Fixpoint dep_parse_ty_aux 
  (ctxt : Alpha_context.context) (fuel : nat) (legacy : bool)
  (allow_lazy_storage : bool) (allow_operation : bool) (allow_contract : bool)
  (allow_ticket : bool) {ret_value : parse_ty_ret}
  (node_value : Alpha_context.Script.node) {struct fuel}
  : M? (ret_fam ret_value * Alpha_context.context) :=
  let parse_passable_ty_aux_with_ret := 'parse_passable_ty_aux_with_ret in
  let parse_any_ty_aux := 'parse_any_ty_aux in
  let parse_big_map_value_ty_aux := 'parse_big_map_value_ty_aux in
  let? ctxt := Alpha_context.Gas.consume ctxt Typecheck_costs.parse_type_cycle
  in
  match fuel with
  | Datatypes.O =>
    Error_monad.error_value
      (Build_extensible "Typechecking_too_many_recursive_calls" unit tt)
  | Datatypes.S fuel =>
    let parse_big_map_ty
      (ctxt : Alpha_context.context) (legacy : bool)
      (big_map_loc : Alpha_context.Script.location)
      (args :
        list
      (Micheline.node Alpha_context.Script.location Alpha_context.Script.prim))
      (map_annot : Micheline.annot)
        : M? (ret_fam Don't_parse_entrypoints * Alpha_context.context) :=
      let parse_big_map_value_ty_aux := 'parse_big_map_value_ty_aux in
      let? ctxt := Alpha_context.Gas.consume ctxt Typecheck_costs.parse_type_cycle
      in
      match args with
        | cons key_ty (cons value_ty []) =>
      let? '(Ex_family_comparable_ty key_ty, ctxt) :=
        dep_parse_comparable_ty_aux fuel ctxt key_ty in
      let? '(res, ctxt) :=
        parse_big_map_value_ty_aux ctxt fuel legacy value_ty in
      let ' (Ex_f_ty value_ty) := res in 
      let? '_ := Script_ir_annot.check_type_annot big_map_loc map_annot in
      return? ((Ex_f_ty (Simulations.Script_typed_ir.With_family.Big_map_t
                 key_ty value_ty
                   Simulations.Script_typed_ir.Ty_metadata.default)), ctxt)
      | args =>
          Error_monad.error_value
            (Build_extensible "Invalid_arity"
                              (Alpha_context.Script.location * Alpha_context.Script.prim * int * int)
                              (big_map_loc, Michelson_v1_primitives.T_big_map, 2, (List.length args)))
      end in
    let? '(node_value, name) :=
      match ret_value with
      | Don't_parse_entrypoints => return? (node_value, None)
      | Parse_entrypoints => Script_ir_annot.extract_entrypoint_annot node_value
      end in
    let _return {t} (ctxt : Alpha_context.context) (ty : With_family.ty t)
      : ret_fam ret_value * Alpha_context.context :=
      match ret_value with
      | Don't_parse_entrypoints => (Ex_f_ty ty, ctxt)
      | Parse_entrypoints => (Ex_f_entrypoints ty
                {|
                  Script_typed_ir.entrypoints.name := name;
                  Script_typed_ir.entrypoints.nested :=
                    Script_typed_ir.Entrypoints_None
                |}, ctxt)
      end in
    match
      (node_value,
        match node_value with
        | Micheline.Prim loc_value Michelson_v1_primitives.T_big_map args annot
          => allow_lazy_storage
        | _ => false
        end,
        match node_value with
        |
          Micheline.Prim loc_value Michelson_v1_primitives.T_sapling_state
                         (cons memo_size []) annot => allow_lazy_storage
        | _ => false
        end) with
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_unit [] annot, _, _)
      =>
        let? '_ := Script_ir_annot.check_type_annot loc_value annot in
        return? (_return ctxt (Simulations.Script_typed_ir.With_family.Unit_t
                               Simulations.Script_typed_ir.Ty_metadata.default))
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_int [] annot, _, _) =>
        let? '_ := Script_ir_annot.check_type_annot loc_value annot in
        return? (_return ctxt (Simulations.Script_typed_ir.With_family.Int_t
                               Simulations.Script_typed_ir.Ty_metadata.default))
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_nat [] annot, _, _) =>
        let? '_ := Script_ir_annot.check_type_annot loc_value annot in
        return? (_return ctxt (Simulations.Script_typed_ir.With_family.Nat_t
                               Simulations.Script_typed_ir.Ty_metadata.default))
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_string [] annot, _, _)
      =>
        let? '_ := Script_ir_annot.check_type_annot loc_value annot in
        return? (_return ctxt (Simulations.Script_typed_ir.With_family.String_t
                               Simulations.Script_typed_ir.Ty_metadata.default))
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_bytes [] annot, _, _)
      =>
        let? '_ := Script_ir_annot.check_type_annot loc_value annot in
        return? (_return ctxt (Simulations.Script_typed_ir.With_family.Bytes_t
                               Simulations.Script_typed_ir.Ty_metadata.default))
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_mutez [] annot, _, _)
      =>
        let? '_ := Script_ir_annot.check_type_annot loc_value annot in
        return? (_return ctxt (Simulations.Script_typed_ir.With_family.Mutez_t
                               Simulations.Script_typed_ir.Ty_metadata.default))
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_bool [] annot, _, _)
      =>
        let? '_ := Script_ir_annot.check_type_annot loc_value annot in
        return? (_return ctxt (Simulations.Script_typed_ir.With_family.Bool_t
                               Simulations.Script_typed_ir.Ty_metadata.default))
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_key [] annot, _, _) =>
        let? '_ := Script_ir_annot.check_type_annot loc_value annot in
        return? (_return ctxt (Simulations.Script_typed_ir.With_family.Key_t
                               Simulations.Script_typed_ir.Ty_metadata.default))
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_key_hash [] annot, _,
        _) =>
       let? '_ := Script_ir_annot.check_type_annot loc_value annot in
       return? (_return ctxt (Simulations.Script_typed_ir.With_family.Key_hash_t
                               Simulations.Script_typed_ir.Ty_metadata.default))
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_chest_key [] annot, _,
        _) =>
      let? '_ := Script_ir_annot.check_type_annot loc_value annot in
      return? (_return ctxt (Simulations.Script_typed_ir.With_family.Chest_key_t
                               Simulations.Script_typed_ir.Ty_metadata.default))
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_chest [] annot, _, _)
      =>
        let? '_ := Script_ir_annot.check_type_annot loc_value annot in
        return? (_return ctxt (Simulations.Script_typed_ir.With_family.Chest_t
                               Simulations.Script_typed_ir.Ty_metadata.default))
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_timestamp [] annot, _,
        _) =>
      let? '_ := Script_ir_annot.check_type_annot loc_value annot in
      return? (_return ctxt (Simulations.Script_typed_ir.With_family.Timestamp_t
                               Simulations.Script_typed_ir.Ty_metadata.default))
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_address [] annot, _, _)
      =>
        let? '_ := Script_ir_annot.check_type_annot loc_value annot in
        return? (_return ctxt (Simulations.Script_typed_ir.With_family.Address_t
                               Simulations.Script_typed_ir.Ty_metadata.default))
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_signature [] annot, _,
        _) =>
      let? '_ := Script_ir_annot.check_type_annot loc_value annot in
      return? (_return ctxt (Simulations.Script_typed_ir.With_family.Signature_t
                               Simulations.Script_typed_ir.Ty_metadata.default))
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_operation [] annot, _,
        _) =>
        if allow_operation then
          let? '_ := Script_ir_annot.check_type_annot loc_value annot in
          return?
            (_return ctxt (Simulations.Script_typed_ir.With_family.Operation_t
              Simulations.Script_typed_ir.Ty_metadata.default))
        else Error_monad.error_value
          (Build_extensible "Unexpected_operation" Alpha_context.Script.location
            loc_value)
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_chain_id [] annot, _,
        _) =>
        let? '_ := Script_ir_annot.check_type_annot loc_value annot in
       return? (_return ctxt (Simulations.Script_typed_ir.With_family.Chain_id_t
                               Simulations.Script_typed_ir.Ty_metadata.default))
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_never [] annot, _, _)
      =>
        let? '_ := Script_ir_annot.check_type_annot loc_value annot in
        return? (_return ctxt (Simulations.Script_typed_ir.With_family.Never_t
                               Simulations.Script_typed_ir.Ty_metadata.default))
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_bls12_381_g1 [] annot,
        _, _) =>
      let? '_ := Script_ir_annot.check_type_annot loc_value annot in
      return?
         (_return ctxt (Simulations.Script_typed_ir.With_family.Bls12_381_g1_t
                            Simulations.Script_typed_ir.Ty_metadata.default))
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_bls12_381_g2 [] annot,
        _, _) =>
        let? '_ := Script_ir_annot.check_type_annot loc_value annot in
        return?
         (_return ctxt (Simulations.Script_typed_ir.With_family.Bls12_381_g2_t
                           Simulations.Script_typed_ir.Ty_metadata.default))
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_bls12_381_fr [] annot,
        _, _) =>
        let? '_ := Script_ir_annot.check_type_annot loc_value annot in
        return?
          (_return ctxt (Simulations.Script_typed_ir.With_family.Bls12_381_fr_t
                         Simulations.Script_typed_ir.Ty_metadata.default))
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_contract (cons utl [])
        annot, _, _) =>
        if allow_contract then
          let? '(res,ctxt) := parse_passable_ty_aux_with_ret ctxt
                               fuel legacy
                               Don't_parse_entrypoints utl in
          let '(Ex_f_ty tl) := res in
          let? '_ := Script_ir_annot.check_type_annot loc_value annot in
          let? cont_check := Script_typed_ir.contract_t
              loc_value (Simulations.Script_typed_ir.With_family.to_ty tl) in
          let md := Script_typed_ir.ty_metadata_value (cont_check) in
          return? (_return ctxt
              (Simulations.Script_typed_ir.With_family.Contract_t 
                tl md))
        else Error_monad.error_value
           (Build_extensible "Unexpected_contract" Alpha_context.Script.location
                                 loc_value)    
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_pair (cons utl utr)
                      annot, _, _) =>
        let? utl := Script_ir_annot.remove_field_annot utl in

        let? '(res1,ctxt) :=
          @dep_parse_ty_aux ctxt fuel legacy allow_lazy_storage
            allow_operation allow_contract allow_ticket Don't_parse_entrypoints
              utl in
        let? utr :=
          match utr with
          | cons utr [] => Script_ir_annot.remove_field_annot utr
          | utr => return?
            (Micheline.Prim loc_value Michelson_v1_primitives.T_pair utr nil)
          end in
        let? '(res2,ctxt) := @dep_parse_ty_aux ctxt fuel
                               legacy allow_lazy_storage
                               allow_operation allow_contract
                               allow_ticket Don't_parse_entrypoints utr in
        let? '_ := Script_ir_annot.check_type_annot loc_value annot in
        match res1, res2 with
        | Ex_f_ty tl, Ex_f_ty tr => let? '_ :=
           Script_ir_annot.check_type_annot loc_value annot in
           return? (_return ctxt (Simulations.Script_typed_ir.With_family.Pair_t
           tl tr Simulations.Script_typed_ir.Ty_metadata.default))
        end      
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_or
                      (cons utl (cons utr [])) annot, _, _) =>
        let? '(utl, utr) :=
          match ret_value with
          | Don't_parse_entrypoints =>
              let? utl := Script_ir_annot.remove_field_annot utl in
              let? utr := Script_ir_annot.remove_field_annot utr in
              return? (utl, utr)
          | Parse_entrypoints => return? (utl, utr)
          end in
        let? '(parsed_l, ctxt) :=
          dep_parse_ty_aux ctxt fuel
            legacy allow_lazy_storage
            allow_operation allow_contract allow_ticket utl in
        let? '(parsed_r, ctxt) :=
          dep_parse_ty_aux ctxt fuel legacy allow_lazy_storage
                           allow_operation allow_contract allow_ticket
                           utr in
        let? '_ := Script_ir_annot.check_type_annot loc_value annot in
        (match ret_value as rv return
               (ret_fam rv -> ret_fam rv -> M? (ret_fam rv * _)) with
         | Don't_parse_entrypoints =>
             (fun parsed_l parsed_r =>
                let 'Ex_f_ty tl := parsed_l in
                let 'Ex_f_ty tr := parsed_r in
                let? cont_check :=
                  Script_typed_ir.union_t
                    loc_value (Simulations.Script_typed_ir.With_family.to_ty tl)
                              (Simulations.Script_typed_ir.With_family.to_ty tr)
                in
                let md := Script_typed_ir.ty_metadata_value (cont_check) in
                return?
                 ((Ex_f_ty
                 (Simulations.Script_typed_ir.With_family.Union_t tl tr
                 md)), ctxt))
         (** @TODO Parse_enrtypoints case defined with error! metadata is not `default`  *)
         | Parse_entrypoints =>  
             (fun parsed_l parsed_r =>
                let 'Ex_f_entrypoints tl ep_l := parsed_l in
                let 'Ex_f_entrypoints tr ep_r := parsed_r in   
                 (let arg_type :=
                    (Simulations.Script_typed_ir.With_family.Union_t tl tr
                     Simulations.Script_typed_ir.Ty_metadata.default) in
                  let entrypoints :=
              {| Script_typed_ir.entrypoints.name := name;
                       Script_typed_ir.entrypoints.nested :=
                       Script_typed_ir.Entrypoints_Union
              {|
               Script_typed_ir.nested_entrypoints.Entrypoints_Union._left :=
               ep_l;
               Script_typed_ir.nested_entrypoints.Entrypoints_Union._right :=
               ep_r |} |} in
               return?
               (Ex_f_entrypoints arg_type entrypoints, ctxt)))
         end parsed_l parsed_r)  

    | (Micheline.Prim loc_value Michelson_v1_primitives.T_lambda
                      (cons uta (cons utr [])) annot, _, _) =>
        let? '(res1, ctxt) :=
          parse_any_ty_aux ctxt fuel legacy uta in
        let '(Ex_f_ty ta) := res1 in
        let? '(res2, ctxt) :=
          parse_any_ty_aux ctxt fuel legacy utr in
        let '(Ex_f_ty tr) := res2 in
        let? '_ :=
          Script_ir_annot.check_type_annot loc_value annot in
        let? lambda_check := Script_typed_ir.lambda_t
                               loc_value (Simulations.Script_typed_ir.With_family.to_ty ta)
                                (Simulations.Script_typed_ir.With_family.to_ty tr)
        in
        let md := Script_typed_ir.ty_metadata_value (lambda_check) in
        return? (_return ctxt
          (Simulations.Script_typed_ir.With_family.Lambda_t ta tr md))
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_option (cons ut [])
                      annot, _, _) =>
        let? ut :=
          if legacy then
            let? ut := Script_ir_annot.remove_field_annot ut in
            let? '_ := Script_ir_annot.check_composed_type_annot loc_value annot
            in return? ut
          else
            let? '_ := Script_ir_annot.check_type_annot loc_value annot in
            return? ut in
        let? '(res,ctxt) := @dep_parse_ty_aux ctxt
                              fuel legacy allow_lazy_storage
                              allow_operation allow_contract allow_ticket
                              Don't_parse_entrypoints ut in
        let '(Ex_f_ty tl) := res in
        let? option_check := Script_typed_ir.option_t
            loc_value (Simulations.Script_typed_ir.With_family.to_ty tl) in
        let md := Script_typed_ir.ty_metadata_value (option_check) in
        return? (_return ctxt (Simulations.Script_typed_ir.With_family.Option_t
                   tl md))
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_list (cons ut [])
                      annot, _, _) =>
        let? '(res,ctxt) :=  @dep_parse_ty_aux ctxt fuel legacy
                             allow_lazy_storage
                             allow_operation allow_contract allow_ticket
                             Don't_parse_entrypoints ut in
        let '(Ex_f_ty tl) := res in
        let? '_ := Script_ir_annot.check_type_annot loc_value annot in
          let? list_check := Script_typed_ir.list_t
              loc_value (Simulations.Script_typed_ir.With_family.to_ty tl) in
          let md := Script_typed_ir.ty_metadata_value (list_check) in
        return? (_return ctxt
                  (Simulations.Script_typed_ir.With_family.List_t tl md))
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_ticket (cons ut [])
                      annot, _, _) =>
        if allow_ticket then
          let? '(Ex_family_comparable_ty t_value, ctxt) :=
            dep_parse_comparable_ty_aux fuel ctxt ut in
          let? '_ := Script_ir_annot.check_type_annot loc_value annot in
          return? (_return ctxt
                     (Simulations.Script_typed_ir.With_family.Ticket_t
                     t_value Simulations.Script_typed_ir.Ty_metadata.default))
        else
          Error_monad.error_value
            (Build_extensible "Unexpected_ticket" Alpha_context.Script.location
                              loc_value)
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_set
                      (cons ut []) annot, _, _) =>
        let? '(Ex_family_comparable_ty t_value, ctxt) :=
          dep_parse_comparable_ty_aux fuel ctxt ut in
        let? '_ := Script_ir_annot.check_type_annot loc_value annot in
        return? (_return ctxt (Simulations.Script_typed_ir.With_family.Set_t
                   t_value Simulations.Script_typed_ir.Ty_metadata.default))
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_map
                      (cons uta (cons utr [])) annot, _, _) =>
        let? '(Ex_family_comparable_ty ta, ctxt) :=
          dep_parse_comparable_ty_aux fuel ctxt uta in
        let? '(ret, ctxt) :=
          @dep_parse_ty_aux ctxt fuel legacy allow_lazy_storage
                           allow_operation allow_contract allow_ticket
                           Don't_parse_entrypoints
                           utr in
        let '(Ex_f_ty tr) := ret in
        let? '_ := Script_ir_annot.check_type_annot loc_value annot in
        return?
          (_return ctxt
             (Simulations.Script_typed_ir.With_family.Map_t ta tr
               Simulations.Script_typed_ir.Ty_metadata.default))
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_sapling_transaction
                      (cons memo_size []) annot, _, _) =>
        let? '_ := Script_ir_annot.check_type_annot loc_value annot in
        let? memo_size := parse_memo_size memo_size in
        return? (_return ctxt
          (Simulations.Script_typed_ir.With_family.Sapling_transaction_t
          memo_size Simulations.Script_typed_ir.Ty_metadata.default))
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_big_map args annot,
        true, _) =>
        let? '(res, ctxt) := parse_big_map_ty ctxt legacy loc_value args annot in
        let '(Ex_f_ty ty) := res in return? (_return ctxt ty)
    | (Micheline.Prim loc_value Michelson_v1_primitives.T_sapling_state
                      (cons memo_size []) annot, _, true) =>
        let? '_ := Script_ir_annot.check_type_annot loc_value annot in
        let? memo_size := parse_memo_size memo_size in
        return? (_return ctxt
          (Simulations.Script_typed_ir.With_family.Sapling_state_t
             memo_size Simulations.Script_typed_ir.Ty_metadata.default))
    | (Micheline.Prim loc_value
                      (Michelson_v1_primitives.T_big_map |
                      Michelson_v1_primitives.T_sapling_state) _ _, _, _) =>
        Error_monad.error_value
          (Build_extensible "Unexpected_lazy_storage"
                            Alpha_context.Script.location loc_value)
    | (Micheline.Prim loc_value
        ((Michelson_v1_primitives.T_unit | Michelson_v1_primitives.T_signature |
          Michelson_v1_primitives.T_int | Michelson_v1_primitives.T_nat |
          Michelson_v1_primitives.T_string | Michelson_v1_primitives.T_bytes |
          Michelson_v1_primitives.T_mutez | Michelson_v1_primitives.T_bool |
          Michelson_v1_primitives.T_key | Michelson_v1_primitives.T_key_hash |
          Michelson_v1_primitives.T_timestamp |
          Michelson_v1_primitives.T_address |
          Michelson_v1_primitives.T_chain_id |
          Michelson_v1_primitives.T_operation | Michelson_v1_primitives.T_never)
            as prim) l_value _, _, _) =>
        Error_monad.error_value
          (Build_extensible "Invalid_arity"
          (Alpha_context.Script.location * Alpha_context.Script.prim * int * int)
          (loc_value, prim, 0, (List.length l_value)))
    | (Micheline.Prim loc_value
        ((Michelson_v1_primitives.T_set | Michelson_v1_primitives.T_list |
          Michelson_v1_primitives.T_option | Michelson_v1_primitives.T_contract |
          Michelson_v1_primitives.T_ticket) as prim) l_value _, _, _) =>
        Error_monad.error_value
        (Build_extensible "Invalid_arity"
         (Alpha_context.Script.location * Alpha_context.Script.prim * int * int)
            (loc_value, prim, 1, (List.length l_value)))
    | (Micheline.Prim loc_value
      ((Michelson_v1_primitives.T_pair | Michelson_v1_primitives.T_or |
      Michelson_v1_primitives.T_map | Michelson_v1_primitives.T_lambda) as
           prim) l_value _, _, _) =>
        Error_monad.error_value
      (Build_extensible "Invalid_arity"
      (Alpha_context.Script.location * Alpha_context.Script.prim * int * int)
      (loc_value, prim, 2, (List.length l_value)))
    | (expr, _, _) =>
        Error_monad.error_value
          (unexpected expr nil Michelson_v1_primitives.Type_namespace
                      [
                        Michelson_v1_primitives.T_pair;
                        Michelson_v1_primitives.T_or;
                        Michelson_v1_primitives.T_set;
                        Michelson_v1_primitives.T_map;
                        Michelson_v1_primitives.T_list;
                        Michelson_v1_primitives.T_option;
                        Michelson_v1_primitives.T_lambda;
                        Michelson_v1_primitives.T_unit;
                        Michelson_v1_primitives.T_signature;
                        Michelson_v1_primitives.T_contract;
                        Michelson_v1_primitives.T_int;
                        Michelson_v1_primitives.T_nat;
                        Michelson_v1_primitives.T_operation;
                        Michelson_v1_primitives.T_string;
                        Michelson_v1_primitives.T_bytes;
                        Michelson_v1_primitives.T_mutez;
                        Michelson_v1_primitives.T_bool;
                        Michelson_v1_primitives.T_key;
                        Michelson_v1_primitives.T_key_hash;
                        Michelson_v1_primitives.T_timestamp;
                        Michelson_v1_primitives.T_chain_id;
                        Michelson_v1_primitives.T_never;
                        Michelson_v1_primitives.T_bls12_381_g1;
                        Michelson_v1_primitives.T_bls12_381_g2;
                        Michelson_v1_primitives.T_bls12_381_fr;
                        Michelson_v1_primitives.T_ticket
          ])
    end
end
where "'parse_passable_ty_aux_with_ret" :=
  (fun (ctxt : Alpha_context.context) (fuel : nat) (legacy : bool) =>
     ((@dep_parse_ty_aux ctxt fuel legacy true false true true) :
        forall (r : parse_ty_ret), Alpha_context.Script.node ->
          M? (ret_fam r * Alpha_context.context)))
and "'parse_any_ty_aux" :=
  (fun (ctxt : Alpha_context.context) (fuel : nat) (legacy : bool) =>
     ((dep_parse_ty_aux ctxt fuel legacy true true true true) :
       Alpha_context.Script.node ->
       M? (ret_fam Don't_parse_entrypoints * Alpha_context.context)))
and "'parse_big_map_value_ty_aux" :=
      (fun (ctxt : Alpha_context.context) (fuel : nat) (legacy : bool)
         (value_ty :
           Micheline.node
             Alpha_context.Script.location Alpha_context.Script.prim) =>
          ((dep_parse_ty_aux ctxt fuel legacy false false legacy true
             value_ty) :
              M? (ret_fam Don't_parse_entrypoints * Alpha_context.context))).
