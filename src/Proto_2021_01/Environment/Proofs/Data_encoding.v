Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Require TezosOfOCaml.Proto_2021_01.Environment.Proofs.Option.

Module Binary.
  Definition is_complete {a : Set} (encoding : Data_encoding.encoding a)
    : Prop :=
    forall value,
      Option.terminates (Data_encoding.Binary.to_bytes encoding value).

  Axiom of_bytes_to_bytes : forall {a : Set},
    forall (encoding : Data_encoding.encoding a) (value : a),
    is_complete encoding ->
    (let* bytes := Data_encoding.Binary.to_bytes encoding value in
    Data_encoding.Binary.of_bytes encoding bytes) =
    return* value.

  Axiom of_bytes_to_bytes_exn : forall {a : Set},
    forall (encoding : Data_encoding.encoding a) (value : a),
    (let bytes := Data_encoding.Binary.to_bytes_exn encoding value in
    Data_encoding.Binary.of_bytes encoding bytes) =
    Some value.
End Binary.
