Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_alpha.Environment.
Require TezosOfOCaml.Proto_alpha.Tx_rollup_inbox_repr.

Require Import TezosOfOCaml.Proto_alpha.Environment.Proofs.Data_encoding.
Require Import TezosOfOCaml.Proto_alpha.Proofs.Tx_rollup_message_repr.

Lemma hash_encoding_is_valid :
  Data_encoding.Valid.t (fun _ => True) Tx_rollup_inbox_repr.hash_encoding.
Proof.
  apply Blake2B.Make_is_valid.
Qed.
#[global] Hint Resolve hash_encoding_is_valid : Data_encoding_db.

Module Valid.
  Import Tx_rollup_inbox_repr.t.

  Record t (p : Tx_rollup_inbox_repr.t) : Prop := {
    cumulated_size : Pervasives.Int31.Valid.t 
      p.(Tx_rollup_inbox_repr.t.cumulated_size)
  }.
End Valid.

Lemma encoding_is_valid : Data_encoding.Valid.t Valid.t
  Tx_rollup_inbox_repr.encoding.
Proof.
  Data_encoding.Valid.data_encoding_auto.
  sauto l: on.
Qed.
#[global] Hint Resolve encoding_is_valid : Data_encoding_db.
