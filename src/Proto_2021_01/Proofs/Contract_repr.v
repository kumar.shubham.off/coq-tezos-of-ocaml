Require Import CoqOfOCaml.CoqOfOCaml.
Require Import CoqOfOCaml.Settings.

Require Import TezosOfOCaml.Proto_2021_01.Environment.
Require TezosOfOCaml.Proto_2021_01.Contract_repr.

Require TezosOfOCaml.Proto_2021_01.Environment.Proofs.Signature.
Require TezosOfOCaml.Proto_2021_01.Proofs.Contract_hash.

Lemma compare_refl contract : Contract_repr.compare contract contract = 0.
  destruct contract; unfold Contract_repr.compare; simpl.
  - apply Signature.Public_key_hash_compare_refl.
  - apply Contract_hash.compare_refl.
Qed.
